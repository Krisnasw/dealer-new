function postSales() {
	$("#form-sales").submit(function (e) {
        e.preventDefault();
        var formdata = new FormData( $("#form-sales")[0] );

        $.ajax({
            url:        $("#form-sales").attr('action'),
            method:     "POST",
            data:       new FormData(this),
            processData: false,
            contentType: false
        })
        .done(function(response) {
            sweetAlert({
                title:  ((response.status === false) ? "Opps!" : 'Success'),
                text:   response.msg,
                type:   ((response.status === false) ? "error" : "success"),
            },
            function(){
                if(response.status != false) {
                    window.location.href = "{{ url('/home/master/sales') }}";
                    return;
                }
            });
        })
        .fail(function() {
            sweetAlert({
                title:  "Opss!",
                text:   "Ada Yang Salah! , Silahkan Coba Lagi Nanti",
                type:   "error",
            },
            function() {

            });
        })
    });
}