<nav class="pcoded-navbar">
    <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
    <div class="pcoded-inner-navbar main-menu">
        <div class="">
            <div class="main-menu-header">
                <img class="img-40 img-radius" src="{{ asset('assets/images/avatar-4.jpg') }}" alt="User-Profile-Image">
                <div class="user-details">
                    <span>{!! Auth::user()->user_first_name !!}</span>
                    <span id="more-details">{!! Auth::user()->email !!}<i class="ti-angle-down"></i></span>
                </div>
            </div>

            <div class="main-menu-content">
                <ul>
                    <li class="more-details">
                        <a href="{{ url('home/profile') }}"><i class="ti-user"></i>View Profile</a>
                        <a href="{{ url('home/logout') }}"><i class="ti-layout-sidebar-left"></i>Logout</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="pcoded-navigatio-lavel" data-i18n="nav.category.navigation">Navigation</div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="active pcoded-trigger">
                <a href="{{ url('home/main') }}">
                    <span class="pcoded-micon"><i class="ti-home"></i><b>D</b></span>
                    <span class="pcoded-mtext" data-i18n="nav.dash.main">Dashboard</span>
                </a>
            </li>
        </ul>

        <div class="pcoded-navigatio-lavel" data-i18n="nav.category.navigation">Menu</div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="pcoded-hasmenu">
                <a href="javascript::void(0)">
                    <span class="pcoded-micon"><i class="ti-layout-grid2-alt"></i><b>MD</b></span>
                    <span class="pcoded-mtext"  data-i18n="nav.Management-menu.main">Master Data</span>
                    <span class="pcoded-mcaret"></span>
                </a>

                <ul class="pcoded-submenu">
                    <li class=" ">
                        <a href="{{ url('home/master/sales') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Sales</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/master/barang') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Barang</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/master/supplier') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Supplier</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/master/promo') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Promo</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/master/supervisior') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Supervisior</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" pcoded-hasmenu">
                        <a href="javascript:void(0)">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext">Customer</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                        <ul class="pcoded-submenu">
                            <li class=" ">
                                <a href="{{ url('home/master/customer-member') }}">
                                    <span class="pcoded-micon"><i class="icon-chart"></i></span>
                                    <span class="pcoded-mtext" data-i18n="nav.page_layout.vertical.static-layout">Member</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class=" ">
                                <a href="{{ url('home/master/customer-pending') }}">
                                    <span class="pcoded-micon"><i class="icon-chart"></i></span>
                                    <span class="pcoded-mtext" data-i18n="nav.page_layout.vertical.header-fixed">Pending</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/master/paket-barang') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Paket Barang</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/master/rak-barang') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Rak Barang</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/master/pegawai') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Pegawai</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>

        <ul class="pcoded-item pcoded-left-item">
            <li class="pcoded-hasmenu">
                <a href="javascript::void(0)">
                    <span class="pcoded-micon"><i class="ti-harddrive"></i><b>SD</b></span>
                    <span class="pcoded-mtext"  data-i18n="nav.Management-menu.main">Setup Data</span>
                    <span class="pcoded-mcaret"></span>
                </a>

                <ul class="pcoded-submenu">
                    <li class=" ">
                        <a href="{{ url('home/setup/grade') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Grade</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/setup/branch') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Cabang</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/setup/motor') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Motor</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/setup/merk') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Merk</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/setup/wilayah') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Wilayah</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/setup/truck') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Kendaraan Operasional</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/setup/biaya') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Biaya</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/setup/unit') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Satuan</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/setup/position') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Jabatan</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/setup/coa') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">COA</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>

        <ul class="pcoded-item pcoded-left-item">
            <li class="pcoded-hasmenu">
                <a href="javascript::void(0)">
                    <span class="pcoded-micon"><i class="ti-settings"></i><b>ST</b></span>
                    <span class="pcoded-mtext"  data-i18n="nav.Management-menu.main">Setting</span>
                    <span class="pcoded-mcaret"></span>
                </a>

                <ul class="pcoded-submenu">
                    <li class=" ">
                        <a href="{{ url('home/setting/user') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">User</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/setting/roles') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Type User</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/setting/password') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Password Edit</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>

        <ul class="pcoded-item pcoded-left-item">
            <li class="pcoded-hasmenu">
                <a href="javascript::void(0)">
                    <span class="pcoded-micon"><i class="ti-money"></i><b>PJ</b></span>
                    <span class="pcoded-mtext"  data-i18n="nav.Management-menu.main">Penjualan</span>
                    <span class="pcoded-mcaret"></span>
                </a>

                <ul class="pcoded-submenu">
                    <li class=" ">
                        <a href="{{ url('home/penjualan/proses') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Proses</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/penjualan/faktur') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Faktur</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/penjualan/retur-customer') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Retur Customer</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>

        <ul class="pcoded-item pcoded-left-item">
            <li class="pcoded-hasmenu">
                <a href="javascript::void(0)">
                    <span class="pcoded-micon"><i class="ti-shopping-cart"></i><b>PB</b></span>
                    <span class="pcoded-mtext"  data-i18n="nav.Management-menu.main">Pembelian</span>
                    <span class="pcoded-mcaret"></span>
                </a>

                <ul class="pcoded-submenu">
                    <li class=" ">
                        <a href="{{ url('home/pembelian/purchase') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Pembelian</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/pembelian/receipt') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Penerimaan</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/pembelian/retur-supplier') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Retur Supplier</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>

        <ul class="pcoded-item pcoded-left-item">
            <li class="pcoded-hasmenu">
                <a href="javascript::void(0)">
                    <span class="pcoded-micon"><i class="ti-view-list-alt"></i><b>GD</b></span>
                    <span class="pcoded-mtext"  data-i18n="nav.Management-menu.main">Gudang</span>
                    <span class="pcoded-mcaret"></span>
                </a>

                <ul class="pcoded-submenu">
                    <li class=" ">
                        <a href="{{ url('home/gudang/packaging') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Packaging</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/gudang/delivery') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Delivery</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/gudang/penerimaan') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Penerimaan</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>

        <ul class="pcoded-item pcoded-left-item">
            <li class="pcoded-hasmenu">
                <a href="javascript::void(0)">
                    <span class="pcoded-micon"><i class="ti-layout-media-right"></i><b>PR</b></span>
                    <span class="pcoded-mtext"  data-i18n="nav.Management-menu.main">Pembayaran</span>
                    <span class="pcoded-mcaret"></span>
                </a>

                <ul class="pcoded-submenu">
                    <li class=" ">
                        <a href="{{ url('home/pembayaran/payment') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Customer</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/pembayaran/entrusted') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Titip Uang</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/pembayaran/sales-cost') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Biaya Keliling Sales</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/pembayaran/payment-sup') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Supplier</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/pembayaran/oprational') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Operational Lain Lain</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/pembayaran/debit') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Debet / Kredit</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <ul class="pcoded-item pcoded-left-item">
            <li class="pcoded-hasmenu">
                <a href="javascript::void(0)">
                    <span class="pcoded-micon"><i class="ti-calendar"></i><b>LP</b></span>
                    <span class="pcoded-mtext"  data-i18n="nav.Management-menu.main">Laporan</span>
                    <span class="pcoded-mcaret"></span>
                </a>

                <ul class="pcoded-submenu">
                    <li class=" ">
                        <a href="{{ url('home/report') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Pembelian Customer</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/report/supplier') }}">
                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Penjualan Supplier</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/report/buku-besar') }}">
                            <span class="pcoded-micon"><i class="icon-book"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Buku Besar</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/report/hutang') }}">
                            <span class="pcoded-micon"><i class="icon-book"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Pembantu Hutang</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/report/piutang') }}">
                            <span class="pcoded-micon"><i class="icon-book"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Pembantu Piutang</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/report/komisi-sales') }}">
                            <span class="pcoded-micon"><i class="icon-book"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Komisi Sales</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ url('home/report/biaya-sales') }}">
                            <span class="pcoded-micon"><i class="icon-book"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.page_layout.bottom-menu">Biaya Keliling Sales</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>