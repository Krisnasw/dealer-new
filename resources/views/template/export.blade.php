<style type="text/css">
    table.tableku tr td,table.tableku tr th {
        border:1px solid #000;
        vertical-align: middle;
    }
    table.tableku tr td.success {
        background: #f1c40f;
        color: #fff;
    }
    table.tableku tr td.nulled {
        background: #f44336;
        color: #fff;
    }
    table.tableku tr td.yellow {
        background: #f44336;
        color: #fff;
    }
    table.tableku tr td.default {
        background: #eee;
        color: #000;
    }
    table.tableku tr th {
        background: #eee;
    }
</style>

@yield('content')