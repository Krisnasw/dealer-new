@extends('template.export')

@section('content')
<table>
  <tr>
    <td colspan="5" align="left">
        <p style="background-color: #3498db;">Buku Besar Pembantu Hutang</p>
    </td>
  </tr>
  <tr>
    <td colspan="5" align="left">
        <p style="background-color: #3498db;">Periode : {!! date('M', strtotime($month)) !!} {!! $year !!}</p>
    </td>
  </tr>
  <tr>
    <td colspan="5" align="left">
        <p style="background-color: #3498db;">Nama Supplier : {!! $sup['supplier_name'] !!}</p>
    </td>
  </tr>
</table>
<table class="tableku">
    <tr>
        <th bgcolor="#f1c40f" align="center">Tanggal</th>
        <th bgcolor="#f1c40f" align="center">Uraian</th>
        <th bgcolor="#f1c40f" align="center">No. Faktur Pajak</th>
        <th bgcolor="#f1c40f" align="center">No. Invoice</th>
        <th bgcolor="#f1c40f" align="center">Debet</th>
        <th bgcolor="#f1c40f" align="center">Kredit</th>
        <th bgcolor="#f1c40f" align="center">Saldo</th>
        <th bgcolor="#f1c40f" align="center">Keterangan</th>
    </tr>
    <tbody>
    @foreach($nota as $index => $row)
        <tr align="center">
            <td align="center">{!! date('Y-m-d', strtotime($row['debt_date'])) !!}</td>
            <td align="center">{!! $row['receipt_code'] !!}</td>
            <td align="center"></td>
            <td align="center"></td>
            <td align="center"></td>
            <td align="center">{!! number_format($row['debt_nominal']) !!}</td>
            <td align="center">-</td>
            <td align="center"></td>
        </tr>
    @endforeach
    </tbody>
</table>
@stop