@extends('template.export')

@section('content')
<table>
  <tr>
    <td colspan="5" align="left">
        <p style="background-color: #3498db;">Data Barang</p>
    </td>
  </tr>
</table>
<table class="tableku">
    <tr>
        <th bgcolor="#f1c40f" align="center">Tanggal</th>
        <th bgcolor="#f1c40f" align="center">Uraian</th>
        <th bgcolor="#f1c40f" align="center">No. Faktur Pajak</th>
        <th bgcolor="#f1c40f" align="center">No. Invoice</th>
        <th bgcolor="#f1c40f" align="center">Debet</th>
        <th bgcolor="#f1c40f" align="center">Kredit</th>
        <th bgcolor="#f1c40f" align="center">Saldo</th>
        <th bgcolor="#f1c40f" align="center">Keterangan</th>
    </tr>
    <tbody>
    @foreach($item->chunk(500) as $index => $row)
        @foreach($row as $rows)
        <tr align="center">
            <td align="center"></td>
            <td align="center">{!! $rows['item_name'] !!}</td>
            <td align="center"></td>
            <td align="center"></td>
            <td align="center"></td>
            <td align="center"></td>
            <td align="center">-</td>
            <td align="center"></td>
        </tr>
        @endforeach
    @endforeach
    </tbody>
</table>
@stop