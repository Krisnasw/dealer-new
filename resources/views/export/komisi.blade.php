@extends('template.export')

@section('content')
<table>
  <tr>
    <td colspan="5" align="left">
        <p style="background-color: #3498db;">Laporan Komisi Sales</p>
    </td>
  </tr>
  <tr>
    <td colspan="5" align="left">
        <p style="background-color: #3498db;">Bulan : {!! getBulan($month) !!}</p>
    </td>
  </tr>
  <tr>
    <td colspan="5" align="left">
        <p style="background-color: #3498db;">Nama Sales : {!! $user['sales_name'] !!}</p>
    </td>
  </tr>
  <tr>
    <td colspan="5" align="left">
        <p style="background-color: #3498db;">Merk : {!! $year !!}</p>
    </td>
  </tr>
</table>
<table class="tableku">
    <tr>
        <th bgcolor="#f1c40f" align="center">Tanggal Nota</th>
        <th bgcolor="#f1c40f" align="center">No. Nota</th>
        <th bgcolor="#f1c40f" align="center">Nama Customer</th>
        <th bgcolor="#f1c40f" align="center">Tanggal Bayar</th>
        <th bgcolor="#f1c40f" align="center">Total</th>
        <th bgcolor="#f1c40f" align="center">Nilai Komisi</th>
        <th bgcolor="#f1c40f" align="center">Rupiah Komisi</th>
    </tr>
    <tbody>
    @php $total = 0; @endphp
    @foreach($nota as $datas => $row)
    @php $tempo = date('Y-m-d', strtotime('+'.$row['nota_tempo'].' days', strtotime($row['nota_date']))); @endphp
        <tr>
            <td width="1%" align="center" style="color: {!! ($row['payment_date'] > $tempo) ? 'red' : '' !!};">{!! ++$datas !!}</td>
            <td align="center" style="color: {!! ($row['payment_date'] > $tempo) ? 'red' : '' !!};">{!! $row['nota_date'] !!}</td>
            <td align="center" style="color: {!! ($row['payment_date'] > $tempo) ? 'red' : '' !!};">{!! $row['nota_code'] !!}</td>
            <td align="center" style="color: {!! ($row['payment_date'] > $tempo) ? 'red' : '' !!};">{!! $row['customer_name'] !!}</td>
            <td align="center" style="color: {!! ($row['payment_date'] > $tempo) ? 'red' : '' !!};">{!! $row['payment_date'] !!}</td>
            <td align="center" style="color: {!! ($row['payment_date'] > $tempo) ? 'red' : '' !!};">{!! number_format($row['nota_total']) !!}</td>
            @if ($row['payment_date'] > $tempo)
            <td align="center" style="color: {!! ($row['payment_date'] > $tempo) ? 'red' : '' !!};">0.1%</td>
            @elseif ($row['payment_detail_status'] == 0)
            <td align="center" style="color: {!! ($row['payment_date'] > $tempo) ? 'red' : '' !!};">0%</td>
            @else
            <td align="center" style="color: {!! ($row['payment_date'] > $tempo) ? 'red' : '' !!};">1%</td>
            @endif
            @if ($row['payment_date'] > $tempo)
            <td align="center" style="color: {!! ($row['payment_date'] > $tempo) ? 'red' : '' !!};">{!! number_format($row['nota_total'] * 0.1 / 100) !!}</td>
            @elseif ($row['payment_detail_status'] == 0)
            <td align="center" style="color: {!! ($row['payment_date'] > $tempo) ? 'red' : '' !!};">0</td>
            @else
            <td align="center" style="color: {!! ($row['payment_date'] > $tempo) ? 'red' : '' !!};">{!! number_format($row['nota_total'] * 1 / 100) !!}</td>
            @endif
        </tr>
    @if ($row['payment_date'] > $tempo)
    @php $total += $row['nota_total'] * 0.1 / 100; @endphp
    @elseif ($row['payment_detail_status'] == 0)
    @php $total += 0; @endphp
    @else
    @php $total += $row['nota_total'] * 1 / 100; @endphp
    @endif
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th>Tanggal Nota</th>
            <th>No. Nota</th>
            <th>Nama Customer</th>
            <th>Tanggal Bayar</th>
            <th>Total Netto</th>
            <th>Nilai Komisi</th>
            <th>Rupiah Komisi</th>
        </tr>
        <tr>
            <th colspan="8" style="text-align: right;">Total Komisi : {!! number_format($total) !!}</th>
        </tr>
    </tfoot>
</table>
@stop