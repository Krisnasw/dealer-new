@extends('template.export')

@section('content')
<table>
  <tr>
    <td colspan="5" align="left">
        <p style="background-color: #3498db;">Buku Besar Pembantu Piutang</p>
    </td>
  </tr>
  <tr>
    <td colspan="5" align="left">
        <p style="background-color: #3498db;">Periode : {!! getBulan($month) !!} {!! $year !!}</p>
    </td>
  </tr>
</table>
<table class="tableku">
    <tr>
        <th bgcolor="#f1c40f" align="center">Tanggal Nota</th>
        <th bgcolor="#f1c40f" align="center">Jatuh Tempo</th>
        <th bgcolor="#f1c40f" align="center">No. Nota</th>
        <th bgcolor="#f1c40f" align="center">Nama Customer</th>
        <th bgcolor="#f1c40f" align="center">Total</th>
        <th bgcolor="#f1c40f" align="center">Bayar</th>
        <th bgcolor="#f1c40f" align="center">Sisa</th>
    </tr>
    <tbody>
    @php $total_netto = 0; @endphp
    @php $total_bayar = 0; @endphp
    @foreach($nota as $index => $row)
    @php $nitip = Access::getListTitipUang($row['customer_id'], $month, $year); @endphp
        <tr align="center">
            <td align="center">{!! $row['nota_date'] !!}</td>
            <td align="center">{!! date('Y-m-d', strtotime('+'.$row['nota_tempo'].' days', strtotime($row['nota_date']))); !!}</td>
            <td align="center">{!! $row['nota_code'] !!}</td>
            <td align="center">{!! $row['customer_name'] !!}</td>
            <td align="center">{!! number_format($row['nota_netto']) !!}</td>
            <td align="center">0</td>
            <td align="center">0</td>
        </tr>
        @php $total_netto += $row['nota_netto']; @endphp
        @foreach($nitip as $index => $row)
        @php $total_bayar += $row['entrusted_total']; @endphp
            <tr align="center">
                <td align="center">{!! $row['entrusted_date'] !!}</td>
                <td align="center">0</td>
                <td align="center">{!! $row['entrusted_code'] !!}</td>
                <td align="center">{!! $row['customer']['customer_name'] !!}</td>
                <td align="center">0</td>
                <td align="center">{!! number_format($row['entrusted_total']) !!}</td>
                <td align="center">{!! number_format(($total_netto - $total_bayar)) !!}</td>
            </tr>
        @endforeach
    @endforeach
    </tbody>
</table>
@stop