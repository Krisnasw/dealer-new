@extends('template.export')

@section('content')
<table>
  <tr>
      <td colspan="5" align="left">
          <p style="background-color: #3498db;">Buku Besar Kas</p>
      </td>
  </tr>
  <tr>
      <td colspan="5" align="left">
          <p style="background-color: #3498db;">Periode : {!! date('M', strtotime($month)) !!} {!! $year !!}</p>
      </td>
  </tr>
</table>
<table class="tableku">
    <tr>
        <th bgcolor="#f1c40f" align="center">Tanggal</th>
        <th bgcolor="#f1c40f" align="center">No Account Sistem</th>
        <th bgcolor="#f1c40f" align="center">Kode Account</th>
        <th bgcolor="#f1c40f" align="center">Nama Account</th>
        <th bgcolor="#f1c40f" align="center">Keterangan</th>
        <th bgcolor="#f1c40f" align="center">DPP + PPN</th>
        <th bgcolor="#f1c40f" align="center">Debet</th>
        <th bgcolor="#f1c40f" align="center">Kredit</th>
    </tr>
    <tbody>
    @php $total_dpp_ppn = 0; @endphp
    @php $total_debet = 0; @endphp
    @php $total_kredit = 0; @endphp
    @foreach($nota as $index => $row)
        <tr align="center">
            <td align="center">{!! $row['nota_date'] !!}</td>
            <td align="center">{!! $row['nota_code'] !!}</td>
            <td align="center">{!! $coaNota['coa_nomor'] !!}</td>
            <td align="center">{!! $coaNota['coa_name'] !!}</td>
            <td align="center"></td>
            <td align="center">{!! round(number_format($row['nota_total'] * 110 / 100)) !!}</td>
            <td align="center">{!! number_format($row['nota_total']) !!}</td>
            <td align="center"></td>
        </tr>
    @php $total_dpp_ppn += $row['nota_total'] * 110 / 100; @endphp
    @php $total_debet += $row['nota_total'] + $row['nota_total'] * 10 / 100; @endphp
    @php $total_kredit += 0; @endphp
    @endforeach
    @foreach($payment as $index => $row)
        <tr align="center">
            <td align="center">{!! $row['payment_date'] !!}</td>
            <td align="center">{!! $row['payment_code'] !!}</td>
            <td align="center">{!! $coaPayment['coa_nomor'] !!}</td>
            <td align="center">{!! $coaPayment['coa_name'] !!}</td>
            <td align="center"></td>
            <td align="center">{!! number_format(round($row['payment_total'] * 110 / 100)) !!}</td>
            <td align="center">{!! number_format($row['payment_total']) !!}</td>
            <td align="center"></td>
        </tr>
        <tr align="center">
            <td align="center"></td>
            <td align="center"></td>
            <td align="center">212.7</td>
            <td align="center">PPN Keluaran</td>
            <td align="center"></td>
            <td align="center"></td>
            <td align="center">{!! number_format(round($row['payment_total'] * 10 / 100)) !!}</td>
            <td align="center"></td>
        </tr>
    @php $total_dpp_ppn += $row['payment_total'] * 110 / 100; @endphp
    @php $total_debet += $row['payment_total'] + $row['payment_total'] * 10 / 100; @endphp
    @php $total_kredit += 0; @endphp
    @endforeach
    @foreach($entrusted as $row)
        <tr align="center">
            <td align="center">{!! $row['entrusted_date'] !!}</td>
            <td align="center">{!! $row['entrusted_code'] !!}</td>
            <td align="center">{!! $coaTitip['coa_nomor'] !!}</td>
            <td align="center">{!! $coaTitip['coa_name'] !!}</td>
            <td align="center"></td>
            <td align="center">{!! number_format(round($row['entrusted_total'] * 110 / 100)) !!}</td>
            <td align="center">{!! number_format($row['entrusted_total']) !!}</td>
            <td align="center"></td>
        </tr>
    @php $total_dpp_ppn += $row['entrusted_total'] * 110 / 100; @endphp
    @php $total_debet += $row['entrusted_total'] + $row['entrusted_total'] * 10 / 100; @endphp
    @php $total_kredit += 0; @endphp
    @endforeach
    @foreach($cost as $row)
        <tr align="center">
            <td align="center">{!! $row['sales_cost_start'] !!}</td>
            <td align="center"></td>
            <td align="center">{!! $coaCost['coa_nomor'] !!}</td>
            <td align="center">{!! $coaCost['coa_name'] !!}</td>
            <td align="center"></td>
            <td align="center">{!! number_format(round($row['sales_cost_total'] * 110 / 100)) !!}</td>
            <td align="center"></td>
            <td align="center">{!! number_format($row['sales_cost_total']) !!}</td>
        </tr>
    @php $total_dpp_ppn += $row['sales_cost_total'] * 110 / 100; @endphp
    @php $total_debet += 0; @endphp
    @php $total_kredit += $row['sales_cost_total'] + $row['sales_cost_total'] * 10 / 100; @endphp
    @endforeach
    @foreach($payment_sup as $row)
        <tr align="center">
            <td align="center">{!! $row['payment_sup_date'] !!}</td>
            <td align="center">{!! $row['payment_sup_code'] !!}</td>
            <td align="center">{!! $coaSup['coa_nomor'] !!}</td>
            <td align="center">{!! $coaSup['coa_name'] !!}</td>
            <td align="center"></td>
            <td align="center">{!! number_format(round($row['payment_sup_total'] * 110 / 100)) !!}</td>
            <td align="center"></td>
            <td align="center">{!! number_format($row['payment_sup_total']) !!}</td>
        </tr>
    @php $total_dpp_ppn += $row['payment_sup_total'] * 110 / 100; @endphp
    @php $total_debet += 0; @endphp
    @php $total_kredit += $row['payment_sup_total'] + $row['payment_sup_total'] * 10 / 100; @endphp
    @endforeach
    @foreach($operational as $row)
        <tr align="center">
            <td align="center">{!! $row['oprational_date'] !!}</td>
            <td align="center"></td>
            <td align="center">{!! $coaOperasional['coa_nomor'] !!}</td>
            <td align="center">{!! $coaOperasional['coa_name'] !!}</td>
            <td align="center">{!! $row['oprational_desc'] !!}</td>
            <td align="center">{!! number_format(round($row['oprational_nominal'] * 110 / 100)) !!}</td>
            <td align="center"></td>
            <td align="center">{!! number_format($row['oprational_nominal']) !!}</td>
        </tr>
    @php $total_dpp_ppn += $row['oprational_nominal'] * 110 / 100; @endphp
    @php $total_debet += 0; @endphp
    @php $total_kredit += $row['oprational_nominal'] + $row['oprational_nominal'] * 10 / 100; @endphp
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th align="center"></th>
            <th align="center"></th>
            <th align="center"></th>
            <th align="center"></th>
            <th align="center"></th>
            <th align="center">Total : {!! number_format(round($total_dpp_ppn)) !!}</th>
            <th align="center">Total : {!! number_format(round($total_debet)) !!}</th>
            <th align="center">Total : {!! number_format(round($total_kredit)) !!}</th>
        </tr>
    </tfoot>
</table>
@stop