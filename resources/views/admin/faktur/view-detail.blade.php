<div class="card">
    <div class="card-block">
        <div class="dt-responsive table-responsive">
            <table id="basic-btn" class="table table-striped table-bordered nowrap">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Barang</th>
                        <th>Permintaan</th>
                        <th>Diskon</th>
                        <th>HET</th>
                        <th>Dikirim</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody id="tbody-isi">
                @php $total_row = 0; @endphp
                @php $total_diskon = 0; @endphp
                @foreach($list as $val => $keys)

                    @php $total = $keys['nota_detail_price']; @endphp
                    @php $diskon = $keys['nota_detail_discount']; @endphp
                    @php $diskon_val = $keys['nota_detail_discount'] / $keys['nota_detail_price'] * 100; @endphp

                    <tr>
                        <td>{!! ++$val !!}</td>
                        <td>{!! $keys->item_name !!}</td>
                        <td>{!! $keys->memo_detail_qty !!}</td>
                        <td>{!! $diskon_val !!}</td>
                        <td>{!! number_format($keys->nota_detail_price) !!}</td>
                        <td>{!! $keys->nota_fill_qty !!}</td>
                        <td align="right">{!! number_format($total) !!}</td>
                    </tr>
                    @php $total_row += $total; @endphp
                    @php $total_diskon += $diskon; @endphp
                @endforeach
                </tbody>
                <tfoot>
                @php $grand_total = $total_row - $total_diskon; @endphp
                <tr>
                    <th colspan="6" style="text-align:right;">Total</th>
                    <th style="text-align:right;">
                        <input style="text-align: right; border: none; background: transparent;" type="text" readonly="readonly" class="form-control" value="{!! number_format($total_row, 2) !!}" name="i_total_row">
                    </th>
                </tr>
                <tr>
                    <th colspan="6" style="text-align:right;">Diskon</th>
                    <th style="text-align:right;">
                        <input style="text-align: right; border: none; background: transparent;" type="text" readonly="readonly" class="form-control" value="{!! number_format($total_diskon, 2) !!}" name="i_total_diskon">
                    </th>
                </tr>
                <tr>
                    <th colspan="6" style="text-align:right">Total Nota Akhir</th>
                    <th style="text-align:right"><input style="text-align: right; border: none; background: transparent;" type="text" readonly="readonly" class="form-control" value="{!! number_format($grand_total, 2) !!}" name="i_grand_total">
                    </th>
                </tr>
                </tfoot>
            </table>

        </div>
    </div>
</div>