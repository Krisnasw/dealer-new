@extends('template.index')

@section('title')
    Dealer Information System - Edit Faktur Baru
@stop

@section('style')
    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}"/>
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}"/>

@stop

@section('content')
    <div class="main-body">
        <div class="page-wrapper">

            <!-- Page-header start -->
            <div class="page-header card">
                <div class="row align-items-end">
                    <div class="col-lg-8">
                        <div class="page-header-title">
                            <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                            <div class="d-inline">
                                <h4>Edit Faktur</h4>
                                <span>Edit Faktur - {!! $nota->nota_code !!}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="page-header-breadcrumb">
                            <ul class="breadcrumb-title">
                                <li class="breadcrumb-item">
                                    <a href="{{ url('home/main') }}">
                                        <i class="icofont icofont-home"></i>
                                    </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">Penjualan - Faktur</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Page-header end -->

            <div class="page-body">
                <div class="row">
                    <div class="col-sm-12">

                        <div id="selected_body">
                        <!-- Tooltip Validation card start -->
                        <div class="card">
                            <div class="card-header">
                                <h5>Edit Faktur - {!! $nota->nota_code !!}</h5>
                                <div class="card-header-right">
                                    <i class="icofont icofont-spinner-alt-5"></i>
                                </div>
                            </div>
                            <div class="card-block">
                                {!! Form::model($nota, ['id' => 'second', 'method' => 'PATCH', 'novalidate' => 'novalidate', 'action' => ['FakturController@update', base64_encode($nota->nota_id)]]) !!}
                                <div class="form-group row">

                                    <label class="col-sm-2 col-form-label">Pilih Customer</label>
                                    <div class="col-sm-4">
                                        <select class="col-sm-12 js-example-basic-single select2-hidden-accessible" name="customer" onchange="get_sales(this.value)" id="customer">
                                            <option selected>- Pilih Customer -</option>
                                            @foreach($cust as $key)
                                                <option value="{!! $key->customer_id !!}" {!! ($nota->customer_id == $key->customer_id) ? 'selected' : '' !!}>{!! $key->customer_code !!} - {!! $key->customer_name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    
                                    <label class="col-sm-2 col-form-label">Pilih Sales</label>
                                    <div class="col-sm-4">
                                        <select class="col-sm-12 js-example-basic-single select2-hidden-accessible" name="sales" id="sales">
                                            <option selected>- Pilih Sales -</option>
                                            @foreach($sales as $key)
                                                <option value="{!! $key->sales_id !!}" {!! ($nota->sales_id == $key->sales_id) ? 'selected' : '' !!}>{!! $key->sales_name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>

                                <div class="form-group row">                                    
                                    <label class="col-sm-2 col-form-label">Pilih Merk</label>
                                    <div class="col-sm-4">
                                        <select class="col-sm-12 js-example-basic-single select2-hidden-accessible" id="merks" name="merk">
                                            <option selected>- Pilih Merk -</option>
                                            @foreach($brand as $key)
                                                <option value="{!! $key->brand_id !!}" {!! ($nota->brand_id == $key->brand_id) ? 'selected' : '' !!}>{!! $key->brand_name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label class="col-sm-2 col-form-label">Tipe Pembayaran</label>
                                    <div class="col-sm-4">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="type" value="0" onclick="tempo(1)" {!! ($nota->nota_type_pay == 0) ? 'checked' : '' !!}> Debet
                                            </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="type" value="1" onclick="tempo(2)" {!! ($nota->nota_type_pay == 1) ? 'checked' : '' !!}> Kredit
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Nominal</label>
                                    <div class="col-sm-4">
                                        <input type="number" class="form-control" min="0" name="i_tempo" value="{!! $nota->nota_tempo !!}">
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="button" class="btn btn-info m-b-0" onclick="submitData();">Submit</button>
                                        <button type="button" class="btn btn-danger m-b-0" onclick="history.go(-1);">Kembali</button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>

                        <div id="view-detail">
                            <div class="card">
                                <div class="card-block">
                                    <div class="dt-responsive table-responsive">
                                        <table id="basic-btn" class="table table-striped table-bordered nowrap">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Barang</th>
                                                    <th>Permintaan</th>
                                                    <th>Diskon</th>
                                                    <th>HET</th>
                                                    <th>Dikirim</th>
                                                    <th>Total</th>
                                                    <th>Config</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbody-isi">
                                            @php $total_row = 0; @endphp
                                            @php $total_diskon = 0; @endphp
                                            @foreach($list as $val => $keys)

                                                @php $total = $keys['nota_detail_price']; @endphp
                                                @php $diskon = $keys['nota_detail_discount']; @endphp
                                                @php $diskon_val = $keys['nota_detail_discount'] / $keys['nota_detail_price'] * 100; @endphp

                                                <tr>
                                                    <td>{!! ++$val !!}</td>
                                                    <td>{!! $keys->item_name !!}</td>
                                                    <td>{!! $keys->memo_detail_qty !!}</td>
                                                    <td>{!! $diskon_val !!}</td>
                                                    <td>{!! number_format($keys->nota_detail_price) !!}</td>
                                                    <td>{!! $keys->nota_fill_qty !!}</td>
                                                    <td align="right">{!! number_format($total) !!}</td>
                                                    <td align="center">
                                                        <button type="button" class="btn btn-info btn-outline-info" data-toggle="modal" data-target="#edit-target{{{ $keys->nota_detail_id }}}"><i class="icofont icofont-edit-alt"></i></button>
                                                    </td>
                                                </tr>

                                                <div class="modal fade" id="edit-target{{{ $keys->nota_detail_id }}}" tabindex="-1" role="dialog">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header bg-info">
                                                                <h4 class="modal-title">Edit Barang</h4>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            {!! Form::open(['method' => 'POST', 'action' => 'ApiController@updateNotaDetailItem']) !!}
                                                            <div class="modal-body">
                                                                <div class="card">
                                                                    <div class="card-block">
                                                                        <h4 class="sub-title">Form Edit Barang - {!! $keys->item_name !!}</h4>
                                                                            <div class="form-group row">
                                                                                <label class="col-sm-2 col-form-label">Nama Barang</label>
                                                                                <div class="col-sm-10">
                                                                                    <input type="hidden" name="item_id" value="{!! $keys->item_id !!}">
                                                                                    <input type="hidden" name="detail_id" value="{!! $keys->nota_detail_id !!}">
                                                                                    <input type="hidden" name="permint" value="{!! $keys->memo_detail_qty !!}">
                                                                                    <input type="text" class="form-control" name="nama_barang" value="{!! $keys->item_name !!}" readonly="readonly">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label class="col-sm-2 col-form-label">Harga H.E.T</label>
                                                                                <div class="col-sm-10">
                                                                                    <input type="number" class="form-control" name="het" value="{!! $keys->item_het !!}" readonly="readonly">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label class="col-sm-2 col-form-label">Diskon</label>
                                                                                <div class="col-sm-4">
                                                                                    <input type="number" min="0" class="form-control" name="diskon" value="{!! $diskon_val !!}" readonly="readonly">
                                                                                </div>
                                                                                <label class="col-sm-2 col-form-label">Dikirim</label>
                                                                                <div class="col-sm-4">
                                                                                    <input type="number" min="0" class="form-control" name="dikirim" value="{!! $keys->nota_fill_qty !!}">
                                                                                </div>
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-primary waves-effect">Submit</button>
                                                                <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                            </div>
                                                            {!! Form::close() !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                @php $total_row += $total; @endphp
                                                @php $total_diskon += $diskon; @endphp
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                            @php $grand_total = $total_row - $total_diskon; @endphp
                                            <tr>
                                                <th colspan="7" style="text-align:right;">Total</th>
                                                <th style="text-align:right;">
                                                    <input style="text-align: right; border: none; background: transparent;" type="text" readonly="readonly" class="form-control" value="{!! number_format($total_row, 2) !!}" name="i_total_row">
                                                    <input type="hidden" class="form-control" value="{!! $total_row !!}" name="i_total_row_real">
                                                </th>
                                            </tr>
                                            <tr>
                                                <th colspan="7" style="text-align:right;">Diskon</th>
                                                <th style="text-align:right;">
                                                    <input style="text-align: right; border: none; background: transparent;" type="text" readonly="readonly" class="form-control" value="{!! number_format($total_diskon, 2) !!}" name="i_total_diskon">
                                                    <input type="hidden" class="form-control" value="{!! $total_diskon !!} " name="i_total_diskon_real"></th>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th colspan="6" style="text-align:right;">Diskon Global</th>
                                                <th><input type="number" min="0" style="width: 90px;" class="form-control" value="{!! (!empty($diskon)) ? $diskon : '0' !!}" name="i_discount" onchange="get_discount(this.value)"></th>
                                                <th style="text-align:right">
                                                    <input style="text-align: right; border: none; background: transparent;" type="text" readonly="readonly" class="form-control" value="{!! (!empty($diskon)) ? number_format($diskon / 100 * $total_row, 2) : '0' !!}" name="i_total_diskon_global">
                                                </th>
                                            </tr>
                                            <tr>
                                                <th colspan="7" style="text-align:right">Total Nota Akhir</th>
                                                <th style="text-align:right"><input style="text-align: right; border: none; background: transparent;" type="text" readonly="readonly" class="form-control" value="{!! number_format($grand_total, 2) !!}" name="i_grand_total">
                                                    <input type="hidden" class="form-control" value="{!! $grand_total !!}" name="i_grand_total_real">
                                                </th>
                                            </tr>
                                            </tfoot>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <!-- Select 2 js -->
    <script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
    <!-- Multiselect js -->
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function get_sales(id) {

            $.ajax({
              type: 'POST',
              url: '{{ url('home/ajax/sales/get') }}',
              data: { id:id },
              dataType: 'JSON',
              success: function(data) {
                $('#sales').html(data.content);
              }
            });

            $.ajax({
              type: 'POST',
              url: '{{ url('home/ajax/brand-dot') }}',
              data: { id:id },
              dataType: 'JSON',
              success: function(data) {
                $('#merks').html(data.content);
              }
            });

        }

        function tempo(id) {

            var brand = $("#merks").val();

            $.ajax({
                type: 'POST',
                url: '{{ url('home/ajax/brand-limit') }}',
                data: { brand : brand },
                dataType: 'JSON',
                success:function (data) {
                    if (id == 1) {
                        $('input[name="i_tempo"]').val(data.cash);
                    } else {
                        $('input[name="i_tempo"]').val(data.kredit);
                    }
                }
            });

        }

        function showData() {
            var sales = $("#sales").val();
            var customer = $("#customer").val();
            var brand = $("#merks").val();
            var i_type         = $('input[name="type"]:checked').val();
            var i_tempo         = $('input[name="i_tempo"]').val();

            $("#view-detail").load('{{ url('home/ajax/nota-detail') }}/'+{!! $nota->nota_id !!}, function () {
                $("#select-barang").select2({
                    ajax: {
                        url: "{{ url('home/ajax/items') }}",
                        dataType: 'JSON',
                        delay: 100,
                        data: function (params) {
                            return {
                                q: params.term, // search term
                                page: params.page
                            };
                        },
                        processResults: function (data, params) {
                            // parse the results into the format expected by Select2
                            // since we are using custom formatting functions we do not need to
                            // alter the remote JSON data, except to indicate that infinite
                            // scrolling can be used
                            params.page = params.page || 1;

                            return {
                                results: data.items,
                                pagination: {
                                    more: (params.page * 30) < data.total_count
                                }
                            };
                        },
                        cache: true
                    },
                    escapeMarkup: function (markup) {
                        return markup;
                    }, // let our custom formatter work
                    placeholder: 'Pilih Barang',
                    minimumInputLength: 2,
                    templateResult: formatRepo, // omitted for brevity, see the source of this page
                    templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
                });
            });
            // $("#selected_body").load('{{ url('home/ajax/faktur-detail') }}/'+brand+'/'+customer+'/'+sales+'/'+i_type+'/'+i_tempo+'/form');
        }

        function get_item_add(id) {
            $.ajax({
                type: 'POST',
                url: '{{ url('home/ajax/get-het') }}',
                data: { id : id},
                dataType: 'JSON',
                success:function (data) {
                    $("#het").val(data.content);
                }
            });
        }

        function add_detail_new() {
            var sales           = $("#sales").val();
            var customer        = $("#customer").val();
            var brand           = $("#merks").val();
            var i_type          = $('input[name="type"]:checked').val();
            var i_tempo         = $('input[name="i_tempo"]').val();
            var i_item          = $('select[name="barang"]').val();
            var i_het           = $('input[name="het"]').val();
            var i_diskon        = $('input[name="diskon-det"]').val();
            var i_fill          = $('input[name="quantity"]').val();

            $.post("{{ url('home/ajax/add-new-detail') }}",
            {
                sales: sales,
                customer: customer,
                brand: brand,
                type: i_type,
                tempo: i_tempo,
                item: i_item,
                het: i_het,
                diskon: i_diskon,
                fill: i_fill
            },
            function(data) {
                $("#view-detail").load('{{ url('home/ajax/faktur-detail') }}/'+{!! $nota->nota_id !!});
                // $("#selected_body").load('{{ url('home/ajax/faktur-detail') }}/'+brand+'/'+customer+'/'+sales+'/'+i_type+'/'+i_tempo+'/form');
            });
        }

        function submitData() {
            var i_type          = $('input[name="type"]:checked').val();
            var i_tempo         = $('input[name="i_tempo"]').val();
            var grandTotal      = $('input[name="i_grand_total_real"]').val();
            var netto           = $('input[name="i_total_row_real"]').val();
            var diskon          = $('input[name="i_total_diskon_real"]').val();

            $.post('{{ url('home/penjualan/faktur-edit/update') }}', {
                sales : {!! $nota->sales_id !!},
                customer : {!! $nota->customer_id !!},
                brand : {!! $nota->brand_id !!},
                type : i_type,
                tempo : i_tempo,
                grandTotal : grandTotal,
                netto : netto,
                discount : diskon,
                nota_id : {!! $nota->nota_id !!}
            }, function (data) {
                if (data.status == 'error') {
                    swal('Oops!', data.message, 'error');
                } else {
                    swal('Successfully', data.message, 'success');
                    window.location.href = '{{ url('home/penjualan/faktur') }}';
                }
            });
        }

        function get_discount(value) {

            var total           = $('input[name="i_total_row_real"]').val();
            var diskon_detail   = $('input[name="i_total_diskon_real"]').val();

            var diskon = value / 100 * total;

            var total_diskon_format = diskon.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            $('input[name="i_total_diskon_global').val(total_diskon_format); 

            var grand_total = total - diskon_detail - diskon;
            var grand_total_format = grand_total.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            $('input[name="i_grand_total"]').val(grand_total_format);
            $('input[name="i_grand_total_real"]').val(grand_total);
        }

        // function updateBarang() {
        //     if ($('input[name="dikirim"]').val() > $('input[name="memo_qty"]').val()) {
        //         swal('Oops!', 'Barang Yang Dikirim Tidak Boleh Lebih Dari Permintaan', 'error');
        //     } else {
        //         $.post('{{ url('home/ajax/update-nota-detail-item') }}', {
        //             item_id : $('input[name="item_id"]').val(),
        //             nota_detail_id : $('input[name="detail_id"]').val(),
        //             newFill : $('input[name="dikirim"]').val()
        //         }, function (data) {
        //             if (data.status == 'success') {
        //                 swal('Successfully', data.message, 'success');
        //                 window.location.href = "{{ url('home/penjualan/faktur') }}";
        //             } else {
        //                 swal('Oops!', data.message, 'error');
        //             }
        //         });
        //     }
        // }
    </script>
@stop