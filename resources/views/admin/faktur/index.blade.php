@extends('template.index')

@section('title')
    Penjualan - Faktur
@stop

@section('style')

    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/css/buttons.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/switchery/css/switchery.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/component.css') }}">

@stop

@section('content')
    <!-- Main-body start -->
    <div class="main-body">
        <div class="page-wrapper">
            <!-- Page-header start -->
            <div class="page-header card">
                <div class="row align-items-end">
                    <div class="col-lg-8">
                        <div class="page-header-title">
                            <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                            <div class="d-inline">
                                <h4>Data Faktur</h4>
                                <span>List All Faktur</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="page-header-breadcrumb">
                            <ul class="breadcrumb-title">
                                <li class="breadcrumb-item">
                                    <a href="{{ url('home/main') }}">
                                        <i class="icofont icofont-home"></i>
                                    </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">Penjualan - Faktur</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Page-header end -->

            <!-- Page-body start -->
            <div class="page-body">
                <div class="row">
                    <div class="col-sm-12">

                        <!-- HTML5 Export Buttons table start -->
                        <div class="card">
                            <div class="card-header table-card-header">
                                <h5>List Faktur</h5>
                            </div>
                            <div class="card-block">
                                <a href="{{ url('home/penjualan/faktur/create') }}" id="addRow" class="btn btn-primary m-b-20">+ Add New</a>
                                <div class="dt-responsive table-responsive">
                                    <table id="item-tbl" class="table table-striped table-bordered nowrap">
                                        <thead>
                                        <tr>
                                            <th>Kode</th>
                                            <th>Nama Sales</th>
                                            <th>Nama Customer</th>
                                            <th>Tanggal</th>
                                            <th>Config</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {{-- @foreach($nota as $data => $key)
                                            <tr>
                                                <td>{!! $key->nota_code !!}</td>
                                                <td>{!! (empty($key->sales->sales_name) ? 'Tidak Ditemukan' : $key->sales->sales_name) !!}</td>
                                                <td>{!! (empty($key->customer->customer_name) ? 'Tidak Ditemukan' : $key->customer->customer_name ) !!}</td>
                                                <td>{!! $key->nota_date !!}</td>
                                                <td align="center">
                                                    <a href="{{ url('home/penjualan/faktur') }}/{!! base64_encode($key->nota_id) !!}/edit" class="btn btn-success btn-outline-success"><i class="icofont icofont-edit-alt"></i></a>
                                                    <button type="button" class="btn btn-danger btn-outline-info" data-toggle="modal" data-target="#delete-Modal{{{ $key->nota_id }}}"><i class="icofont icofont-delete-alt"></i></button>
                                                </td>
                                            </tr>

                                            <div class="modal fade" id="delete-Modal{{{ $key->nota_id }}}" tabindex="-1" role="dialog">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header bg-danger">
                                                            <h4 class="modal-title">Hapus Data</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>Apakah Anda Yakin Akan Menghapus <b>{!! $key->nota_code !!}</b> ?</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            {!! Form::open(['method' => 'DELETE', 'route' => array('faktur.destroy', base64_encode($key->nota_id))]) !!}
                                                            {!! Form::submit("Ya", array('class' => 'btn btn-primary waves-effect waves-light')) !!}
                                                            {!! Form::close() !!}
                                                            <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach --}}
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>Kode</th>
                                            <th>Nama Sales</th>
                                            <th>Nama Customer</th>
                                            <th>Tanggal</th>
                                            <th>Config</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Page-body end -->
        </div>
    </div>
    </div>
@stop

@section('script')
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>

    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>

    <script type="text/javascript" src="{{ asset('bower_components/switchery/js/switchery.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/swithces.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/modalEffects.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/classie.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
        $('#item-tbl').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax":{
                     "url": "{{ url('home/penjualan/nota') }}",
                     "dataType": "JSON",
                     "type": "POST",
                     "data":{ _token: "{{csrf_token()}}"}
                   },
            "columns": [
                { "data": "nota_code", sorttable: true },
                { "data": "nota_sales", sorttable: true },
                { "data": "nota_customer", sorttable: true },
                { "data": "nota_date", sorttable: true },
                { "data": "config", sorttable: true }
            ]
        });
    });
    </script>
@endsection