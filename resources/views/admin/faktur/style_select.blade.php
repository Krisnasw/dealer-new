<div class="card">
                            <div class="card-header">
                                <h5>Buat Faktur Baru</h5>
                                <div class="card-header-right">
                                    <i class="icofont icofont-spinner-alt-5"></i>
                                </div>
                            </div>
                            <div class="card-block">
                                {!! Form::open(['id' => 'second', 'method' => 'POST', 'novalidate' => 'novalidate', 'action' => 'FakturController@store']) !!}
                                <div class="form-group row">

                                    <label class="col-sm-2 col-form-label">Pilih Customer</label>
                                    <div class="col-sm-4">
                                        <select class="col-sm-12 form-control" name="customer" onchange="get_sales(this.value)" id="customer">
                                            <option selected>- Pilih Customer -</option>
                                            @foreach($cust as $key)
                                                <option value="{!! $key->customer_id !!}" {!! ($data['customer_id'] == $key->customer_id) ? 'selected' : '' !!}>{!! $key->customer_code !!} - {!! $key->customer_name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    
                                    <label class="col-sm-2 col-form-label">Pilih Sales</label>
                                    <div class="col-sm-4">
                                        <select class="col-sm-12 form-control" name="sales" id="sales">
                                            <option selected>- Pilih Sales -</option>
                                            @foreach($sales as $key)
                                                <option value="{!! $key->sales_id !!}" {!! ($data['sales_id'] == $key->sales_id) ? 'selected' : '' !!}>{!! $key->sales_name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>

                                <div class="form-group row">                                    
                                    <label class="col-sm-2 col-form-label">Pilih Merk</label>
                                    <div class="col-sm-4">
                                        <select class="col-sm-12 form-control" id="merks" name="merk">
                                            <option selected>- Pilih Merk -</option>
                                            @foreach($brand as $key)
                                                <option value="{!! $key->brand_id !!}" {!! ($data['brand_id'] == $key->brand_id) ? 'selected' : '' !!}>{!! $key->brand_name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label class="col-sm-2 col-form-label">Tipe Pembayaran</label>
                                    <div class="col-sm-4">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="type" value="0" onclick="tempo(1)" {!! ($data['nota_type_pay'] == 0) ? 'checked' : '' !!}> Debet
                                            </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="type" value="1" onclick="tempo(2)" {!! ($data['nota_type_pay'] == 1) ? 'checked' : '' !!}> Kredit
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Nominal</label>
                                    <div class="col-sm-4">
                                        <input type="number" class="form-control" min="0" name="i_tempo" value="{!! $data['nota_tempo'] !!}">
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="button" class="btn btn-primary m-b-0" onclick="submitData();">Submit</button>
                                        <button type="button" class="btn btn-info m-b-0" onclick="showData();">Preview</button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>