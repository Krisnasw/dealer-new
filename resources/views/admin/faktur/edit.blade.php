@extends('template.index')

@section('title')
    Dealer Information System - Edit Faktur Baru
@stop

@section('style')
    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}"/>
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}"/>

@stop

@section('content')
    <div class="main-body">
        <div class="page-wrapper">

            <!-- Page-header start -->
            <div class="page-header card">
                <div class="row align-items-end">
                    <div class="col-lg-8">
                        <div class="page-header-title">
                            <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                            <div class="d-inline">
                                <h4>Edit Faktur</h4>
                                <span>Edit Faktur - {!! $nota->nota_code !!}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="page-header-breadcrumb">
                            <ul class="breadcrumb-title">
                                <li class="breadcrumb-item">
                                    <a href="{{ url('home/main') }}">
                                        <i class="icofont icofont-home"></i>
                                    </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">Penjualan - Faktur</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Page-header end -->

            <div class="page-body">
                <div class="row">
                    <div class="col-sm-12">

                        <div id="selected_body">
                            <!-- Tooltip Validation card start -->
                            <div class="card">
                                <div class="card-header">
                                    <h5>Edit Faktur - {!! $nota->nota_code !!}</h5>
                                    <div class="card-header-right">
                                        <i class="icofont icofont-spinner-alt-5"></i>
                                    </div>
                                </div>
                                <div class="card-block">
                                    {!! Form::model($nota, ['id' => 'second', 'method' => 'PATCH', 'novalidate' => 'novalidate', 'action' => ['FakturController@update', base64_encode($nota->nota_id)]]) !!}
                                    <div class="form-group row">

                                        <label class="col-sm-2 col-form-label">Nama Customer</label>
                                        <div class="col-sm-4">
                                            {!! $nota->customer->customer_name !!}
                                        </div>
                                        
                                        <label class="col-sm-2 col-form-label">Nama Sales</label>
                                        <div class="col-sm-4">
                                            {!! $nota->sales->sales_name !!}
                                        </div>

                                    </div>

                                    <div class="form-group row">                                    
                                        <label class="col-sm-2 col-form-label">Pilih Merk</label>
                                        <div class="col-sm-4">
                                            {!! $nota->brand->brand_name !!}
                                        </div>

                                        <label class="col-sm-2 col-form-label">Tipe Pembayaran</label>
                                        <div class="col-sm-4">
                                            {!! ($nota->nota_type_pay == 0) ? 'Debet' : 'Kredit' !!}
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Nominal</label>
                                        <div class="col-sm-4">
                                            {!! $nota->nota_tempo !!}
                                        </div>
                                    </div>

                                    <div class="row">
                                        <label class="col-sm-2"></label>
                                        <div class="col-sm-10">
                                            <button type="button" class="btn btn-info m-b-0" data-toggle="modal" data-target="#password-modal">Edit</button>
                                            <button type="button" class="btn btn-warning m-b-0" onclick="window.print();">Print</button>
                                            <button type="button" class="btn btn-success m-b-0" onclick="swal('Oops!', 'Coming Soon', 'error');">Print Surat Jalan</button>
                                            <button type="button" class="btn btn-danger m-b-0" onclick="history.go(-1);">Kembali</button>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="password-modal" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header bg-info">
                                        <h4 class="modal-title">Masukkan Password</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="card-block">
                                                <div class="form-group row">
                                                    <input type="password" name="password" placeholder="Masukkan Password" class="form-control">
                                                </div>
                                            </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary waves-effect waves-light" onclick="checkPassword();">Submit</button>
                                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="view-detail">
                            <div class="card">
                                <div class="card-block">
                                    <div class="dt-responsive table-responsive">
                                        <table id="basic-btn" class="table table-striped table-bordered nowrap">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Barang</th>
                                                    <th>Permintaan</th>
                                                    <th>Diskon</th>
                                                    <th>HET</th>
                                                    <th>Dikirim</th>
                                                    <th>Total</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbody-isi">
                                            @php $total_row = 0; @endphp
                                            @php $total_diskon = 0; @endphp
                                            @foreach($list as $val => $keys)

                                                @php $total = $keys['nota_detail_price']; @endphp
                                                @php $diskon = $keys['nota_detail_discount']; @endphp
                                                @php $diskon_val = $keys['nota_detail_discount'] / $keys['nota_detail_price'] * 100; @endphp

                                                <tr>
                                                    <td>{!! ++$val !!}</td>
                                                    <td>{!! $keys->item_name !!}</td>
                                                    <td>{!! $keys->memo_detail_qty !!}</td>
                                                    <td>{!! $diskon_val !!}</td>
                                                    <td>{!! number_format($keys->nota_detail_price) !!}</td>
                                                    <td>{!! $keys->nota_fill_qty !!}</td>
                                                    <td align="right">{!! number_format($total) !!}</td>
                                                </tr>
                                                @php $total_row += $total; @endphp
                                                @php $total_diskon += $diskon; @endphp
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                            @php $grand_total = $total_row - $total_diskon; @endphp
                                            <tr>
                                                <th colspan="6" style="text-align:right;">Total</th>
                                                <th style="text-align:right;">
                                                    <input style="text-align: right; border: none; background: transparent;" type="text" readonly="readonly" class="form-control" value="{!! number_format($total_row, 2) !!}" name="i_total_row">
                                                </th>
                                            </tr>
                                            <tr>
                                                <th colspan="6" style="text-align:right;">Diskon</th>
                                                <th style="text-align:right;">
                                                    <input style="text-align: right; border: none; background: transparent;" type="text" readonly="readonly" class="form-control" value="{!! number_format($total_diskon, 2) !!}" name="i_total_diskon">
                                                </th>
                                            </tr>
                                            <tr>
                                                <th colspan="6" style="text-align:right">Total Nota Akhir</th>
                                                <th style="text-align:right"><input style="text-align: right; border: none; background: transparent;" type="text" readonly="readonly" class="form-control" value="{!! number_format($grand_total, 2) !!}" name="i_grand_total">
                                                </th>
                                            </tr>
                                            </tfoot>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <!-- Select 2 js -->
    <script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
    <!-- Multiselect js -->
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function checkPassword() {
            var menu = 52;
            var passw = $('input[name="password"]').val();
            $.post('{{ url('home/ajax/cek-password') }}', {
                menu : menu,
                password : passw
            }, function (data) {
                if (data.status == 'success') {
                    swal('Confirmed', data.message, 'success');
                    window.location.href = "{{ url('home/penjualan/faktur-edit') }}"+'/'+{!! $nota->nota_id !!};
                } else {
                    swal('Oops!', data.message, 'error');
                }
            });
        }
    </script>
@stop