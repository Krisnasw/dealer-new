@extends('template.index')

@section('title')
Dealer Information System - Buat Sales Baru
@stop

@section('style')

    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}"/>
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}"/>

@stop

@section('subheader')
Buat Sales Baru
@stop

@section('content')
<div class="main-body">
	<div class="page-wrapper">

        <!-- Page-header start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                        <div class="d-inline">
                            <h4>Buat Sales</h4>
                            <span>Buat Sales Baru</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ url('home/main') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Master Data - Sales</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-header end -->

		<div class="page-body">
			<div class="row">
				<div class="col-sm-12">

					<!-- Tooltip Validation card start -->
                    <div class="card">
                        <div class="card-header">
                            <h5>Buat Sales Baru</h5>
                            <div class="card-header-right">
                            	<i class="icofont icofont-spinner-alt-5"></i>
                            </div>
                        </div>
                        <div class="card-block">
                        	{!! Form::open(['id' => 'second', 'method' => 'POST', 'novalidate' => 'novalidate', 'action' => 'SalesController@store']) !!}
                                <div class="form-group row @if ($errors->has('f_kodeSales')) has-error @endif">
                                    <label class="col-sm-2 col-form-label">Kode Sales</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="usernameP" name="f_kodeSales" placeholder="Masukkan Kode Sales">
                                        @if ($errors->has('f_kodeSales')) <p class="help-block">{{ $errors->first('f_kodeSales') }}</p> @endif
                                        <span class="messages popover-valid"></span>
                                    </div>
                                </div>

                                <div class="form-group row @if ($errors->has('f_nama')) has-error @endif">
                                    <label class="col-sm-2 col-form-label">Nama</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="usernameP" name="f_nama" placeholder="Masukkan Nama Lengkap">
                                        @if ($errors->has('f_nama')) <p class="help-block">{{ $errors->first('f_nama') }}</p> @endif
                                        <span class="messages popover-valid"></span>
                                    </div>
                                </div>

                                <div class="form-group row @if ($errors->has('f_alamat')) has-error @endif">
                                    <label class="col-sm-2 col-form-label">Alamat</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="usernameP" name="f_alamat" placeholder="Masukkan Alamat Lengkap">
                                        @if ($errors->has('f_alamat')) <p class="help-block">{{ $errors->first('f_alamat') }}</p> @endif
                                        <span class="messages popover-valid"></span>
                                    </div>
                                </div>

                                <div class="form-group row @if ($errors->has('f_telepon')) has-error @endif">
                                    <label class="col-sm-2 col-form-label">Telepon</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="usernameP" name="f_telepon" placeholder="Masukkan No. Telepon">
                                        @if ($errors->has('f_telepon')) <p class="help-block">{{ $errors->first('f_telepon') }}</p> @endif
                                        <span class="messages popover-valid"></span>
                                    </div>
                                </div>

                                <div class="form-group row @if ($errors->has('f_ktp')) has-error @endif">
                                    <label class="col-sm-2 col-form-label">No. KTP</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="usernameP" name="f_ktp" placeholder="Masukkan No. KTP">
                                        @if ($errors->has('f_ktp')) <p class="help-block">{{ $errors->first('f_ktp') }}</p> @endif
                                        <span class="messages popover-valid"></span>
                                    </div>
                                </div>

                                <div class="form-group row @if ($errors->has('f_tempatLahir')) has-error @endif">
                                    <label class="col-sm-2 col-form-label">Tempat Tanggal Lahir</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="usernameP" name="f_tempatLahir" placeholder="Masukkan Tempat Lahir">
                                        @if ($errors->has('f_tempatLahir')) <p class="help-block">{{ $errors->first('f_tempatlahir') }}</p> @endif
                                        <span class="messages popover-valid"></span>
                                    </div>
                                    <div class="col-sm-4">
                                    	<input type="date" class="form-control" name="f_tgLahir" placeholder="00-00-0000">
                                    	@if ($errors->has('f_tgLahir')) <p class="help-block">{{ $errors->first('f_tgLahir') }}</p> @endif
                                    	<span class="messages popover-valid"></span>
                                    </div>
                                </div>

                                <div class="form-group row @if ($errors->has('f_sim')) has-error @endif">
                                    <label class="col-sm-2 col-form-label">No. SIM</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="usernameP" name="f_sim" placeholder="Masukkan No. SIM">
                                        @if ($errors->has('f_sim')) <p class="help-block">{{ $errors->first('f_sim') }}</p> @endif
                                        <span class="messages popover-valid"></span>
                                    </div>
                                </div>

                                <div class="form-group row @if ($errors->has('f_gaji')) has-error @endif">
                                    <label class="col-sm-2 col-form-label">Gaji Awal</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="usernameP" name="f_gaji" placeholder="Masukkan Gaji Awal">
                                        @if ($errors->has('f_gaji')) <p class="help-block">{{ $errors->first('f_gaji') }}</p> @endif
                                        <span class="messages popover-valid"></span>
                                    </div>
                                </div>

                                <div class="form-group row @if ($errors->has('f_email')) has-error @endif">
                                    <label class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" id="usernameP" name="f_email" placeholder="Masukkan Email">
                                        @if ($errors->has('f_email')) <p class="help-block">{{ $errors->first('f_email') }}</p> @endif
                                        <span class="messages popover-valid"></span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Wilayah</label>
                                    <div class="col-sm-10">
                                    	<select class="js-example-basic-multiple col-sm-12" multiple="multiple" name="f_wilayah[]">
                                            @foreach($city as $key)
                                            <option value="{!! $key->city_id !!}">{!! $key->city_name !!}</option>
                                            @endforeach
                                        </select>
                                        <span class="messages popover-valid"></span>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('script')
<!-- Select 2 js -->
<script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
<!-- Multiselect js -->
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>
@stop