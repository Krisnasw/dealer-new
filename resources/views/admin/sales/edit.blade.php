@extends('template.index')

@section('title')
Dealer Information System - Edit Sales
@stop

@section('style')

    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}"/>
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/css/buttons.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css') }}">

@stop

@section('subheader')
Edit Sales
@stop

@section('content')
<div class="main-body">
	<div class="page-wrapper">

        <!-- Page-header start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                        <div class="d-inline">
                            <h4>Edit Sales</h4>
                            <span>Edit Sales</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ url('home/main') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Master Data - Sales</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-header end -->

		<div class="page-body">
			<div class="row">
				<div class="col-sm-12">

					<!-- Tooltip Validation card start -->
                    <div class="card">
                        <div class="card-header">
                            <h5>Edit Sales</h5>
                            <div class="card-header-right">
                            	<i class="icofont icofont-spinner-alt-5"></i>
                            </div>
                        </div>
                        <div class="card-block">
                        	{!! Form::model($sales, ['id' => 'second', 'method' => 'PATCH', 'novalidate' => 'novalidate', 'route' => ['sales.update', base64_encode($sales['sales_id'])]]) !!}
                                <div class="form-group row @if ($errors->has('f_kodeSales')) has-error @endif">
                                    <label class="col-sm-2 col-form-label">Kode Sales</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="usernameP" name="f_kodeSales" placeholder="Masukkan Kode Sales" value="{!! $sales->sales_code !!}">
                                        @if ($errors->has('f_kodeSales')) <p class="help-block">{{ $errors->first('f_kodeSales') }}</p> @endif
                                        <span class="messages popover-valid"></span>
                                    </div>
                                </div>

                                <div class="form-group row @if ($errors->has('f_nama')) has-error @endif">
                                    <label class="col-sm-2 col-form-label">Nama</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="usernameP" name="f_nama" placeholder="Masukkan Nama Lengkap" value="{!! $sales->sales_name !!}">
                                        @if ($errors->has('f_nama')) <p class="help-block">{{ $errors->first('f_nama') }}</p> @endif
                                        <span class="messages popover-valid"></span>
                                    </div>
                                </div>

                                <div class="form-group row @if ($errors->has('f_alamat')) has-error @endif">
                                    <label class="col-sm-2 col-form-label">Alamat</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="usernameP" name="f_alamat" placeholder="Masukkan Alamat Lengkap" value="{!! $sales->sales_addres !!}">
                                        @if ($errors->has('f_alamat')) <p class="help-block">{{ $errors->first('f_alamat') }}</p> @endif
                                        <span class="messages popover-valid"></span>
                                    </div>
                                </div>

                                <div class="form-group row @if ($errors->has('f_telepon')) has-error @endif">
                                    <label class="col-sm-2 col-form-label">Telepon</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="usernameP" name="f_telepon" placeholder="Masukkan No. Telepon" value="{!! $sales->sales_phone !!}">
                                        @if ($errors->has('f_telepon')) <p class="help-block">{{ $errors->first('f_telepon') }}</p> @endif
                                        <span class="messages popover-valid"></span>
                                    </div>
                                </div>

                                <div class="form-group row @if ($errors->has('f_ktp')) has-error @endif">
                                    <label class="col-sm-2 col-form-label">No. KTP</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="usernameP" name="f_ktp" placeholder="Masukkan No. KTP" value="{!! $sales->sales_ktp !!}">
                                        @if ($errors->has('f_ktp')) <p class="help-block">{{ $errors->first('f_ktp') }}</p> @endif
                                        <span class="messages popover-valid"></span>
                                    </div>
                                </div>

                                <div class="form-group row @if ($errors->has('f_tempatLahir')) has-error @endif">
                                    <label class="col-sm-2 col-form-label">Tempat Tanggal Lahir</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="usernameP" name="f_tempatLahir" placeholder="Masukkan Tempat Lahir" value="{!! $sales->sales_birth_place !!}">
                                        @if ($errors->has('f_tempatLahir')) <p class="help-block">{{ $errors->first('f_tempatlahir') }}</p> @endif
                                        <span class="messages popover-valid"></span>
                                    </div>
                                    <div class="col-sm-4">
                                    	<input type="date" class="form-control" name="f_tgLahir" placeholder="00-00-0000" value="{!! $sales->sales_birth_date !!}">
                                    	@if ($errors->has('f_tgLahir')) <p class="help-block">{{ $errors->first('f_tgLahir') }}</p> @endif
                                    	<span class="messages popover-valid"></span>
                                    </div>
                                </div>

                                <div class="form-group row @if ($errors->has('f_sim')) has-error @endif">
                                    <label class="col-sm-2 col-form-label">No. SIM</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="usernameP" name="f_sim" placeholder="Masukkan No. SIM" value="{!! $sales->sales_sim !!}">
                                        @if ($errors->has('f_sim')) <p class="help-block">{{ $errors->first('f_sim') }}</p> @endif
                                        <span class="messages popover-valid"></span>
                                    </div>
                                </div>

                                <div class="form-group row @if ($errors->has('f_gaji')) has-error @endif">
                                    <label class="col-sm-2 col-form-label">Gaji Awal</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="usernameP" name="f_gaji" placeholder="Masukkan Gaji Awal" value="{!! $sales->sales_salary !!}">
                                        @if ($errors->has('f_gaji')) <p class="help-block">{{ $errors->first('f_gaji') }}</p> @endif
                                        <span class="messages popover-valid"></span>
                                    </div>
                                </div>

                                <div class="form-group row @if ($errors->has('f_email')) has-error @endif">
                                    <label class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" id="usernameP" name="f_email" placeholder="Masukkan Email" value="{!! $sales->sales_mail !!}">
                                        @if ($errors->has('f_email')) <p class="help-block">{{ $errors->first('f_email') }}</p> @endif
                                        <span class="messages popover-valid"></span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Wilayah</label>
                                    <div class="col-sm-10">
                                    	<select class="js-example-basic-multiple col-sm-12" multiple="multiple" name="f_wilayah[]">
                                            @foreach($city as $key)
                                            <option value="{!! $key->city_id !!}" {!! (in_array($key->city_id, array($sales->city_id))) ? 'selected' : '' !!}>{!! $key->city_name !!}</option>
                                            @endforeach
                                        </select>
                                        <span class="messages popover-valid"></span>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <!-- Tooltip Validation card end -->

                    <div class="card">
                        <div class="card-block">
                            <a href="javascript::void(0)" id="addRow" class="btn btn-primary m-b-20" data-toggle="modal" data-target="#large-Modal">+ Add New</a>
                            <div class="dt-responsive table-responsive">
                                <table id="basic-btn" class="table table-striped table-bordered nowrap">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Merk</th>
                                            <th>Target</th>
                                            <th>Config</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($target as $val => $keys)
                                        <tr>
                                            <td>{!! ++$val !!}</td>
                                            <td>{!! $keys->brand_name !!}</td>
                                            <td>{!! $keys->target_value !!}</td>
                                            <td>
                                                <button type="button" class="btn btn-info btn-outline-info" data-toggle="modal" data-target="#update-target{{{ $keys->target_id }}}"><i class="icofont icofont-edit"></i></button>
                                                <button type="button" class="btn btn-danger btn-outline-info" data-toggle="modal" data-target="#delete-target{{{ $keys->target_id }}}"><i class="icofont icofont-delete-alt"></i></button>
                                            </td>
                                        </tr>

                                        <div class="modal fade" id="delete-target{{{ $keys->target_id }}}" tabindex="-1" role="dialog">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header bg-danger">
                                                        <h4 class="modal-title">Hapus Data</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Apakah Anda Yakin Akan Menghapus <b>Target</b> ?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        {!! Form::open(['method' => 'POST', 'action' => 'ApiController@deleteTarget']) !!}
                                                            <input type="hidden" name="target_id" value="{!! base64_encode($keys->target_id) !!}">
                                                            {!! Form::submit("Ya", array('class' => 'btn btn-primary waves-effect waves-light')) !!}
                                                        {!! Form::close() !!}
                                                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="modal fade md-effect-1" id="update-target{!! $keys->target_id !!}" tabindex="-1" role="dialog">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header bg-info">
                                                        <h5 class="modal-title">Add Target</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                {!! Form::open(['method' => 'POST', 'action' => 'ApiController@updateTarget']) !!}
                                                    <div class="modal-body">
                                                        <!-- Basic Form Inputs card start -->
                                                        <div class="card">
                                                            <div class="card-block">
                                                                <h4 class="sub-title">Form Target</h4>
                                                                    <input type="hidden" value="{!! $keys->target_id !!}" name="target_id">
                                                                    <div class="form-group row">
                                                                        <label class="col-sm-2 col-form-label">Merk</label>
                                                                        <div class="col-sm-10">
                                                                            <select class="form-control select2" id="select2" name="merk">
                                                                                @foreach($brand as $val => $key)
                                                                                <option value="{!! $key->brand_id !!}" {!! ($key->brand_id == $keys->brand_id) ? 'selected' : '' !!}>{!! $key->brand_name !!}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-sm-2 col-form-label">Target</label>
                                                                        <div class="col-sm-10">
                                                                            <input type="number" min="0" class="form-control" placeholder="Min 0" name="target" value="{!! $keys->target_value !!}">
                                                                        </div>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                        <!-- Basic Form Inputs card end -->
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary waves-effect waves-light ">Save changes</button>
                                                    </div>
                                                {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Merk</th>
                                            <th>Target</th>
                                            <th>Config</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>

                    <div class="modal fade md-effect-1" id="large-Modal" tabindex="-1" role="dialog">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header bg-info">
                                        <h5 class="modal-title">Add Target</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                {!! Form::open(['method' => 'POST', 'action' => 'ApiController@addTarget']) !!}
                                    <div class="modal-body">
                                        <!-- Basic Form Inputs card start -->
                                        <div class="card">
                                            <div class="card-block">
                                                <h4 class="sub-title">Form Target</h4>
                                                    <input type="hidden" value="{!! $sales->sales_id !!}" name="sales_id">
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">Merk</label>
                                                        <div class="col-sm-10">
                                                            <select class="form-control select2" id="select2" name="merk">
                                                                @foreach($brand as $val => $key)
                                                                <option value="{!! $key->brand_id !!}">{!! $key->brand_name !!}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">Target</label>
                                                        <div class="col-sm-10">
                                                            <input type="number" min="0" class="form-control" placeholder="Min 0" name="target">
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                        <!-- Basic Form Inputs card end -->
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light ">Save changes</button>
                                    </div>
                                {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('script')
<!-- Select 2 js -->
<script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
<!-- Multiselect js -->
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>

<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/js/jszip.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/js/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/js/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/jszip.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>

<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js') }}"></script>
@stop