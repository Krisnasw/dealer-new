@extends('template.index')

@section('title')
Dealer Information System - Buat Penerimaan Kantor
@stop

@section('style')

    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}"/>
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/css/buttons.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css') }}">

    <!-- Date-time picker css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/advance-elements/css/bootstrap-datetimepicker.css') }}">
    <!-- Date-range picker css  -->
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/bootstrap-daterangepicker/css/daterangepicker.css') }}" />

@stop

@section('content')
<div class="main-body">
	<div class="page-wrapper">

        <!-- Page-header start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                        <div class="d-inline">
                            <h4>Buat Penerimaan Kantor</h4>
                            <span>Buat Penerimaan Kantor Baru</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ url('home/main') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Pembelian - Penerimaan Kantor</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-header end -->

		<div class="page-body">
			<div class="row">
				<div class="col-sm-12">

					<!-- Tooltip Validation card start -->
                    <div class="card">
                        <div class="card-header">
                            <h5>Buat Penerimaan Kantor Baru</h5>
                            <div class="card-header-right">
                            	<i class="icofont icofont-spinner-alt-5"></i>
                            </div>
                        </div>
                        <div class="card-block">
                        	{!! Form::open(['id' => 'second', 'method' => 'POST', 'novalidate' => 'novalidate', 'action' => 'ReceiptController@postAgency']) !!}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Nomor Surat Jalan</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="usernameP" name="suratJalan" placeholder="Masukkan No. Surat Jalan" value="{!! $receipt['receipt_sj'] !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Tanggal Penerimaan</label>
                                    <div class="col-sm-10">
                                        <input type="date" class="form-control" id="usernameP" name="date" value="{!! date('Y-m-d') !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Nomor Pembelian</label>
                                    <div class="col-sm-10">
                                        <select class="form-control js-example-basic-multiple" multiple="multiple" name="no_beli">
                                            @foreach($purchase as $row)
                                                @if (is_array($receipt['purchase_id']))
                                                    <option value="{!! $row['purchase_id'] !!}" {!! (in_array($receipt['purchase_id'], $row['purchase_id']))  ? 'selected' : '' !!}>{!! $row['purchase_code'] !!}</option>
                                                @else
                                                    <option value="{!! $row['purchase_id'] !!}" {!! ($receipt['purchase_id'] == $row['purchase_id']) ? 'selected' : '' !!}>{!! $row['purchase_code'] !!}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="basic" class="table table-striped table-bordered nowrap">
                                                <thead>
                                                    <tr>
                                                        <th>Kode PS</th>
                                                        <th>
                                                            <input type="text" class="form-control" name="kode_ps" placeholder="Masukkan Kode PS" onkeydown="if (event.keyCode == 13) { addAgencyCardboard(); }" autofocus="autofocus">
                                                        </th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Kode PS</th>
                                                        <th>Jenis Barang</th>
                                                        <th>Qty Barang</th>
                                                        <th>Config</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($cardboard as $data => $key)
                                                    <tr>
                                                        <td width="1%" align="center">{!! ++$data !!}</td>
                                                        <td>{!! $key->receipt_cardboard_barcode !!}</td>
                                                        <td>{!! Access::hitung_item($key['receipt_cardboard_id'], 1) !!}</td>
                                                        <td>{!! Access::hitung_qty_item($key['receipt_cardboard_id'], 1) !!}</td>
                                                        <td align="center">
                                                            <a href="{{ url('home/pembelian/receipt/agency') }}/{!! base64_encode($key->receipt_id) !!}/{!! base64_encode($key->receipt_cardboard_id) !!}/entry" class="btn btn-success btn-outline-success" target="_blank"><i class="icofont icofont-edit-alt"></i></a>
                                                            <button type="button" class="btn btn-danger btn-outline-info" data-toggle="modal" data-target="#delete-Modal{{{ $key->receipt_cardboard_id }}}"><i class="icofont icofont-delete-alt"></i></button>
                                                        </td>
                                                    </tr>

                                                    <div class="modal fade" id="delete-Modal{{{ $key->receipt_cardboard_id }}}" tabindex="-1" role="dialog">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header bg-danger">
                                                                    <h4 class="modal-title">Hapus Data</h4>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <p>Apakah Anda Yakin Akan Menghapus <b>{!! $key->receipt_cardboard_barcode !!}</b> ?</p>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-danger waves-effect" onclick="deleteKardus({!! $key['receipt_cardboard_id'] !!},{!! $receipt['receipt_id'] !!});">Ya</button>
                                                                    <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="button" class="btn btn-primary m-b-0" onclick="history.go(-1);">Kembali</button>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('script')
<!-- Select 2 js -->
<script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
<!-- Multiselect js -->
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>

<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/js/jszip.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/js/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/js/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/jszip.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>

<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/pages/advance-elements/moment-with-locales.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/pages/advance-elements/bootstrap-datetimepicker.min.js') }}"></script>
<!-- Date-range picker js -->
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-daterangepicker/js/daterangepicker.js') }}"></script>

<script type="text/javascript" src="{{ asset('bower_components/datedropper/js/datedropper.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/pages/advance-elements/custom-picker.js') }}"></script>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function addAgencyCardboard() {
        $.post('{{ url('home/pembelian/receipt/addAgencyCardboard') }}', {
            id : {!! $receipt['receipt_id'] !!},
            barcode : $("input[name='kode_ps']").val()
        }, function (data) {
            if (data.status == 'success') {
                swal('Success', data.message, 'success');
                window.location.reload();
            } else {
                swal('Oops!', data.message, 'error');
            }
        });
    }

    function deleteKardus(kardus,id) {
        $.post('{{ url('home/pembelian/receipt/deleteCardboardAgency') }}', {
            kardus : kardus,
            id : id
        }, function (data) {
            if (data.status == 'success') {
                swal('Success', data.message, 'success');
                window.location.reload();
            } else {
                swal('Oops!', data.message, 'error');
            }
        });
    }
</script>
@stop