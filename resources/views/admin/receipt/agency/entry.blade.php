@extends('template.index')

@section('title')
Pembelian - Entry Kardus Kantor
@stop

@section('style')

    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/switchery/css/switchery.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/component.css') }}">

@stop

@section('content')
    <!-- Main-body start -->
<div class="main-body">
    <div class="page-wrapper">
        <!-- Page-header start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                        <div class="d-inline">
                            <h4>Data Entry Kardus Kantor</h4>
                            <span>List All Entry Kardus Kantor</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ url('home/main') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Pembelian - Entry Kardus Kantor</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-header end -->

        <!-- Page-body start -->
        <div class="page-body">
            <div class="row">
                <div class="col-sm-12">

                    <!-- HTML5 Export Buttons table start -->
                    <div class="card">
                        <div class="card-header table-card-header">
                            <h5>List Barang</h5>
                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table id="basic-btn" class="table table-striped table-bordered nowrap">
                                    <thead>
                                        <tr>
                                            <th>Input</th>
                                            <th>
                                                <select class="form-control js-example-basic-single" name="item" id="selectBarang" onchange="get_purchase_detail(this.value);">
                                                    <option value="0">-- Pilih Number Part --</option>
                                                    @foreach($item as $row)
                                                    <option value="{!! $row['purchase_detail_id'] !!}">{!! $row['item_code'] !!}</option>
                                                    @endforeach
                                                </select>
                                            </th>
                                            <th>
                                              <input type="text" name="i_item_name" value="" style="border: none;background: transparent;" readonly="" id="itmNama">
                                            </th>
                                            <th>
                                                <input type="number" name="het" class="form-control" min="0" placeholder="Masukkan HET" id="hehet">
                                                <input type="hidden" name="i_item_id">
                                            </th>
                                            <th>
                                                <input type="text" name="i_outstanding" id="outstand" class="form-control" value="" style="border: none;background: transparent;width: 40px" readonly="readonly">
                                            </th>
                                            <th>
                                                <input type="number" name="dikirim" class="form-control" min="0" id="ngirim" placeholder="Dikirim" onkeypress="if (event.keyCode == 13) { add_agency_detail(this.value); }">
                                            </th>
                                        </tr>
                                        <tr>
                                            <th>No</th>
                                            <th>Number Part</th>
                                            <th>Nama Barang</th>
                                            <th>HET</th>
                                            <th>Jumlah Belum Dikirim</th>
                                            <th>Jumlah Dikirim</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($list_detail as $data => $row)
                                    @if (isset($row['receipt_agency_qty']))
                                        @php $qty =  $row['receipt_agency_qty']; @endphp
                                    @else
                                        @php $qty = 0; @endphp
                                    @endif

                                    @if ($row['item_id'])
                                        @php $code = $row['item_code2']; @endphp
                                        @php $name = $row['item_name2']; @endphp
                                        @php $het  = $row['item_het2']; @endphp
                                        @php $item_id = $row['item_id']; @endphp
                                    @else
                                        @php $code = $row['item_code']; @endphp
                                        @php $name = $row['item_name']; @endphp
                                        @php $het  = $row['item_het']; @endphp
                                        @php $item_id = $row['item_id']; @endphp
                                    @endif
                                        <tr>
                                            <td width="1%" align="center">{!! ++$data !!}</td>
                                            <td>{!! $code !!}</td>
                                            <td>{!! $name !!}</td>
                                            <td>{!! number_format($het, 2) !!}</td>
                                            <td>{!! Access::hitung_outstanding($row['purchase_detail_id']) !!}</td>
                                            <td>{!! $qty !!}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-body end -->
        </div>
    </div>
</div>
@stop

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>

    <script type="text/javascript" src="{{ asset('bower_components/switchery/js/switchery.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/swithces.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/modalEffects.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/classie.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
          // $('input[name="dikirim"]').focus();
          $("#selectBarang").focus();
          $("#selectBarang").focusout(function () {
            $("#itmNama").focus();
          });
          $("#itmNama").focusout(function () {
            $("#hehet").focus();
          });
          $("#hehet").focusout(function () {
            $("#outstand").focus();
          });
          $("#outstand").focusout(function () {
            $("#ngirim").focus();
          });
        });
        
        function get_purchase_detail(id) {
            $.post('{{ url('home/pembelian/receipt/agency/purchase-detail') }}', {
                id : id
            }, function (data) {
                if (data.status == 'success') {
                    $('input[name="i_item_name"]').val(data.content['item_name']);
                    $('input[name="i_outstanding"]').val(data.content['outstanding']);
                    $('input[name="het"]').val(data.content['item_het']);
                    $('input[name="i_item_id"]').val(data.content['item_id']);
                } else {
                    swal('Oops!', data.message, 'error');
                }
            });
        }

        function add_agency_detail(qty) {
          var dos           = '{!! $cardboard_id !!}';
          var detail_id     = $('select[name="item"]').val();
          var het           = $('input[name="het"]').val();
          var item_id           = $('input[name="i_item_id"]').val();
          // var agency_detail_id           = 0;

          $.ajax({
              type: 'POST',
              url: '{{ url('home/pembelian/receipt/agency/action-agency-detail') }}',
              data: {dos:dos,detail_id:detail_id,qty:qty,het:het,item_id:item_id},
              dataType: 'JSON',
              success: function(data) {
                if (data.status == 'success') {
                    swal('Success', data.message, 'success');
                    window.location.reload();
                } else {
                    swal('Oops!', data.message, 'error');
                }
              } 
            });
        }
    </script>
@endsection