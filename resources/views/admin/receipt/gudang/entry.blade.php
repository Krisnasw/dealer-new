@extends('template.index')

@section('title')
Pembelian - Entry Kardus Gudang
@stop

@section('style')

    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/switchery/css/switchery.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/component.css') }}">

@stop

@section('content')
    <!-- Main-body start -->
<div class="main-body">
    <div class="page-wrapper">
        <!-- Page-header start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                        <div class="d-inline">
                            <h4>Data Entry Kardus Gudang</h4>
                            <span>List All Entry Kardus Gudang</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ url('home/main') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Pembelian - Entry Kardus Gudang</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-header end -->

        <!-- Page-body start -->
        <div class="page-body">
            <div class="row">
                <div class="col-sm-12">

                    <!-- HTML5 Export Buttons table start -->
                    <div class="card">
                        <div class="card-header table-card-header">
                            <h5>List Kardus</h5>
                        </div>
                        <div class="card-block">
                            {!! Form::open(['method' => 'POST', 'class' => 'form-control', 'action' => 'ReceiptController@addDetailGudang']) !!}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Number Part</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="i_number_part" autofocus="autofocus">
                                        <input type="hidden" value="{!! base64_decode($kardus_id) !!}" name="cardboard_id">
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-outline-primary m-b-0"><i class="icofont icofont-submit"></i>Submit</button>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                            <div class="dt-responsive table-responsive">
                                <table id="basic-btn" class="table table-striped table-bordered nowrap">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Number Part</th>
                                            <th>Nama Barang</th>
                                            <th>Barcode Barang</th>
                                            <th>Qty</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($list as $data => $row)
                                        <tr>
                                            <td width="1%" align="center">{!! ++$data !!}</td>
                                            <td>{!! $row['item_code'] !!}</td>
                                            <td>{!! $row['item_name'] !!}</td>
                                            <td>{!! $row['item_barcode'] !!}</td>
                                            <td>
                                                <input type="number" class="form-control" value="{!! $row['receipt_storehouse_detail_qty'] !!}" onkeydown="if (event.keyCode == 13) { edit_qty({!! $row['receipt_storehouse_detail_id'] !!},this.value); }">
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-body end -->
        </div>
    </div>
</div>
@stop

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>

    <script type="text/javascript" src="{{ asset('bower_components/switchery/js/switchery.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/swithces.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/modalEffects.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/classie.js') }}"></script>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
        function edit_qty(id, qty) {
            $.post('{{ url('home/pembelian/gudang/update-qty-detail') }}', {
                id : id,
                qty : qty
            }, function (data) {
                if (data.status == 'success') {
                    swal('Success', data.message, 'success');
                    window.location.reload();
                } else {
                    swal('Oops!', data.message, 'error');
                }
            });
        }
    </script>
@endsection