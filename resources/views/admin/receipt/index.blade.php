@extends('template.index')

@section('title')
Pembelian - Penerimaan
@stop

@section('style')

    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/css/buttons.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/switchery/css/switchery.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/component.css') }}">

@stop

@section('content')
    <!-- Main-body start -->
<div class="main-body">
    <div class="page-wrapper">
        <!-- Page-header start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                        <div class="d-inline">
                            <h4>Data Penerimaan</h4>
                            <span>List All Penerimaan</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ url('home/main') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Pembelian - Penerimaan</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-header end -->

        <!-- Page-body start -->
        <div class="page-body">
            <div class="row">
                <div class="col-sm-12">
                    <!-- Material tab card start -->
                    <div class="card">
                        <div class="card-header">
                            <h5>Penerimaan</h5>
                            <div class="card-header-right">    <ul class="list-unstyled card-option">        <li><i class="icofont icofont-simple-left "></i></li>        <li><i class="icofont icofont-maximize full-card"></i></li>        <li><i class="icofont icofont-minus minimize-card"></i></li>        <li><i class="icofont icofont-refresh reload-card"></i></li>        <li><i class="icofont icofont-error close-card"></i></li>    </ul></div>
                        </div>
                        <div class="card-block">
                            <!-- Row start -->
                            <div class="row m-b-30">
                                <div class="col-lg-12 col-xl-12">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs md-tabs" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#home3" role="tab">Admin Kantor</a>
                                            <div class="slide"></div>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#profile3" role="tab">Admin Gudang</a>
                                            <div class="slide"></div>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#messages3" role="tab">Sinkronisasi</a>
                                            <div class="slide"></div>
                                        </li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content card-block">
                                        <div class="tab-pane active" id="home3" role="tabpanel">
                                            <div class="card">
                                                <div class="card-block">
                                                    <a href="{{ url('home/pembelian/receipt/form-agency') }}" id="addRow" class="btn btn-primary m-b-20">+ Add New</a>
                                                    <div class="dt-responsive table-responsive">
                                                        <table id="basic" class="table table-striped table-bordered nowrap">
                                                            <thead>
                                                                <tr>
                                                                    <th>No</th>
                                                                    <th>Nomor Penerimaan</th>
                                                                    <th>Nomor Surat Jalan</th>
                                                                    <th>Tanggal Terima</th>
                                                                    <th>Config</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($agency as $data => $key)
                                                                <tr>
                                                                    <td width="1%" align="center">{!! ++$data !!}</td>
                                                                    <td>{!! $key->receipt_code !!}</td>
                                                                    <td>{!! $key->receipt_sj !!}</td>
                                                                    <td>{!! $key->receipt_date !!}</td>
                                                                    <td align="center">
                                                                        <a href="{{ url('home/pembelian/receipt/form-agency/') }}/{!! base64_encode($key->receipt_id) !!}" class="btn btn-success btn-outline-success"><i class="icofont icofont-edit-alt"></i></a>
                                                                        <button type="button" class="btn btn-danger btn-outline-info" data-toggle="modal" data-target="#delete-Modal{{{ $key->receipt_id }}}"><i class="icofont icofont-delete-alt"></i></button>
                                                                    </td>
                                                                </tr>

                                                                <div class="modal fade" id="delete-Modal{{{ $key->receipt_id }}}" tabindex="-1" role="dialog">
                                                                    <div class="modal-dialog" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header bg-danger">
                                                                                <h4 class="modal-title">Hapus Data</h4>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <p>Apakah Anda Yakin Akan Menghapus <b>{!! $key->receipt_code !!}</b> ?</p>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-danger waves-effect" onclick="deleteAgency({!! $key->receipt_id !!});">Ya</button>
                                                                                <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="profile3" role="tabpanel">
                                            <div class="card">
                                                <div class="card-block">
                                                    <a href="{{ url('home/pembelian/receipt/form-gudang') }}" id="addRow" class="btn btn-primary m-b-20">+ Add New</a>
                                                    <div class="dt-responsive table-responsive">
                                                        <table id="basic1" class="table table-striped table-bordered nowrap">
                                                            <thead>
                                                                <tr>
                                                                    <th>No</th>
                                                                    <th>Tanggal Terima</th>
                                                                    <th>Config</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($gudang as $data => $row)
                                                                <tr>
                                                                    <td width="1%" align="center">{!! ++$data !!}</td>
                                                                    <td>{!! $row['receipt_storehouse_date'] !!}</td>
                                                                    <td align="center">
                                                                        <a href="{{ url('home/pembelian/receipt/form-gudang') }}/{!! base64_encode($row->receipt_storehouse_id) !!}" class="btn btn-success btn-outline-success"><i class="icofont icofont-edit-alt"></i></a>
                                                                        <button type="button" class="btn btn-danger btn-outline-info" data-toggle="modal" data-target="#delete-Modal{{{ $row->receipt_storehouse_id }}}"><i class="icofont icofont-delete-alt"></i></button>
                                                                    </td>
                                                                </tr>

                                                                <div class="modal fade" id="delete-Modal{{{ $row->receipt_storehouse_id }}}" tabindex="-1" role="dialog">
                                                                    <div class="modal-dialog" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header bg-danger">
                                                                                <h4 class="modal-title">Hapus Data</h4>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <p>Apakah Anda Yakin Akan Menghapus <b>{!! $row->receipt_storehouse_date !!}</b> ?</p>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-danger waves-effect" onclick="deleteGudang({!! $row->receipt_storehouse_id !!});">Ya</button>
                                                                                <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="messages3" role="tabpanel">
                                            <div class="card">
                                                <div class="card-block">
                                                    <a href="{{ url('home/pembelian/receipt/form-sync') }}" id="addRow" class="btn btn-primary m-b-20">+ Add New</a>
                                                    <div class="dt-responsive table-responsive">
                                                        <table id="basic2" class="table table-striped table-bordered nowrap">
                                                            <thead>
                                                                <tr>
                                                                    <th>No</th>
                                                                    <th>Tanggal Sinkronisasi</th>
                                                                    <th>Config</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($sync as $data => $row)
                                                                <tr>
                                                                    <td width="1%" align="center">{!! ++$data !!}</td>
                                                                    <td>{!! $row['receipt_sychron_date'] !!}</td>
                                                                    <td align="center">
                                                                        <a href="{{ url('home/pembelian/receipt/form-sync') }}/{!! base64_encode($row->receipt_sychron_id) !!}" class="btn btn-success btn-outline-success"><i class="icofont icofont-edit-alt"></i></a>
                                                                        <button type="button" class="btn btn-danger btn-outline-info" data-toggle="modal" data-target="#delete{{{ $row->receipt_sychron_id }}}"><i class="icofont icofont-delete-alt"></i></button>
                                                                    </td>
                                                                </tr>

                                                                <div class="modal fade" id="delete{{{ $row->receipt_sychron_id }}}" tabindex="-1" role="dialog">
                                                                    <div class="modal-dialog" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header bg-danger">
                                                                                <h4 class="modal-title">Hapus Data</h4>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <p>Apakah Anda Yakin Akan Menghapus <b>{!! $row->receipt_sychron_date !!}</b> ?</p>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-danger waves-effect" onclick="deleteSync({!! $row->receipt_sychron_id !!});">Ya</button>
                                                                                <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Row end -->
                        </div>
                    </div>
                    <!-- Material tab card end -->
                </div>
            </div>
        </div>
        <!-- Page-body end -->
	    </div>
	</div>
</div>
@stop

@section('script')
	<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>

    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>

    <script type="text/javascript" src="{{ asset('bower_components/switchery/js/switchery.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/swithces.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/modalEffects.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/classie.js') }}"></script>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).ready(function () {
            $("#basic").DataTable();
            $("#basic1").DataTable();
            $("#basic2").DataTable();
        });

        function deleteAgency(id) {
            $.post('{{ url('home/pembelian/receipt/agency/delete') }}', {
                id : id
            }, function (data) {
                if (data.status == 'success') {
                    swal('Successfully Deleted', data.message, 'success');
                    window.location.reload();
                } else {
                    swal('Oops!', data.message, 'error');
                }
            });
        }

        function deleteSync(id) {
            $.post('{{ url('home/pembelian/receipt/sync/delete') }}', {
                id : id
            }, function (data) {
                if (data.status == 'success') {
                    swal('Successfully Deleted', data.message, 'success');
                    window.location.reload();
                } else {
                    swal('Oops!', data.message, 'error');
                }
            });
        }

        function deleteGudang(id) {
            $.post('{{ url('home/pembelian/receipt/gudang/delete') }}', {
                id : id
            }, function (data) {
                if (data.status == 'success') {
                    swal('Successfully Deleted', data.message, 'success');
                    window.location.reload();
                } else {
                    swal('Oops!', data.message, 'error');
                }
            });
        }
    </script>
@endsection