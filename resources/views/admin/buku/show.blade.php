@extends('template.index')

@section('title')
Laporan - Buku Besar
@stop

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/switchery/css/switchery.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/component.css') }}">
@stop

@section('content')
    <!-- Main-body start -->
<div class="main-body">
    <div class="page-wrapper">
        <!-- Page-header start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                        <div class="d-inline">
                            <h4>Data Buku Besar</h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ url('home/main') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Laporan - Buku Besar</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-header end -->

        <!-- Page-body start -->
        <div class="page-body">
            <div class="row">
                <div class="col-sm-12">

                    <!-- HTML5 Export Buttons table start -->
                    <div class="card">
                        <div class="card-header table-card-header">
                            <h5>Data Buku Besar Periode : {!! date('M', strtotime($month)) !!} {!! $year !!}</h5>
                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table class="table table-striped table-bordered nowrap">
                                    <thead>
                                        <tr>
                                            <th align="center">Tanggal</th>
                                            <th align="center">No Account Sistem</th>
                                            <th align="center">Kode Account</th>
                                            <th align="center">Nama Account</th>
                                            <th align="center">Keterangan</th>
                                            <th align="center">DPP + PPN</th>
                                            <th align="center">Debet</th>
                                            <th align="center">Kredit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @php $total_dpp_ppn = 0; @endphp
                                    @php $total_debet = 0; @endphp
                                    @php $total_kredit = 0; @endphp
                                    @foreach($nota as $index => $row)
                                        <tr align="center">
                                            <td align="center">{!! $row['nota_date'] !!}</td>
                                            <td align="center">{!! $row['nota_code'] !!}</td>
                                            <td align="center">{!! $coaNota['coa_nomor'] !!}</td>
                                            <td align="center">{!! $coaNota['coa_name'] !!}</td>
                                            <td align="center"></td>
                                            <td align="center">{!! round(number_format($row['nota_total'] * 110 / 100)) !!}</td>
                                            <td align="center">{!! number_format($row['nota_total']) !!}</td>
                                            <td align="center"></td>
                                        </tr>
                                    @php $total_dpp_ppn += $row['nota_total'] * 110 / 100; @endphp
                                    @php $total_debet += $row['nota_total'] + $row['nota_total'] * 10 / 100; @endphp
                                    @php $total_kredit += 0; @endphp
                                    @endforeach
                                    @foreach($payment as $index => $row)
                                        <tr align="center">
                                            <td align="center">{!! $row['payment_date'] !!}</td>
                                            <td align="center">{!! $row['payment_code'] !!}</td>
                                            <td align="center">{!! $coaPayment['coa_nomor'] !!}</td>
                                            <td align="center">{!! $coaPayment['coa_name'] !!}</td>
                                            <td align="center"></td>
                                            <td align="center">{!! number_format(round($row['payment_total'] * 110 / 100)) !!}</td>
                                            <td align="center">{!! number_format($row['payment_total']) !!}</td>
                                            <td align="center"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="center"></td>
                                            <td align="center"></td>
                                            <td align="center">212.7</td>
                                            <td align="center">PPN Keluaran</td>
                                            <td align="center"></td>
                                            <td align="center"></td>
                                            <td align="center">{!! number_format(round($row['payment_total'] * 10 / 100)) !!}</td>
                                            <td align="center"></td>
                                        </tr>
                                    @php $total_dpp_ppn += $row['payment_total'] * 110 / 100; @endphp
                                    @php $total_debet += $row['payment_total'] + $row['payment_total'] * 10 / 100; @endphp
                                    @php $total_kredit += 0; @endphp
                                    @endforeach
                                    @foreach($entrusted as $row)
                                        <tr align="center">
                                            <td align="center">{!! $row['entrusted_date'] !!}</td>
                                            <td align="center">{!! $row['entrusted_code'] !!}</td>
                                            <td align="center">{!! $coaTitip['coa_nomor'] !!}</td>
                                            <td align="center">{!! $coaTitip['coa_name'] !!}</td>
                                            <td align="center"></td>
                                            <td align="center">{!! number_format(round($row['entrusted_total'] * 110 / 100)) !!}</td>
                                            <td align="center">{!! number_format($row['entrusted_total']) !!}</td>
                                            <td align="center"></td>
                                        </tr>
                                    @php $total_dpp_ppn += $row['entrusted_total'] * 110 / 100; @endphp
                                    @php $total_debet += $row['entrusted_total'] + $row['entrusted_total'] * 10 / 100; @endphp
                                    @php $total_kredit += 0; @endphp
                                    @endforeach
                                    @foreach($cost as $row)
                                        <tr align="center">
                                            <td align="center">{!! $row['sales_cost_start'] !!}</td>
                                            <td align="center"></td>
                                            <td align="center">{!! $coaCost['coa_nomor'] !!}</td>
                                            <td align="center">{!! $coaCost['coa_name'] !!}</td>
                                            <td align="center"></td>
                                            <td align="center">{!! number_format(round($row['sales_cost_total'] * 110 / 100)) !!}</td>
                                            <td align="center"></td>
                                            <td align="center">{!! number_format($row['sales_cost_total']) !!}</td>
                                        </tr>
                                    @php $total_dpp_ppn += $row['sales_cost_total'] * 110 / 100; @endphp
                                    @php $total_debet += 0; @endphp
                                    @php $total_kredit += $row['sales_cost_total'] + $row['sales_cost_total'] * 10 / 100; @endphp
                                    @endforeach
                                    @foreach($payment_sup as $row)
                                        <tr align="center">
                                            <td align="center">{!! $row['payment_sup_date'] !!}</td>
                                            <td align="center">{!! $row['payment_sup_code'] !!}</td>
                                            <td align="center">{!! $coaSup['coa_nomor'] !!}</td>
                                            <td align="center">{!! $coaSup['coa_name'] !!}</td>
                                            <td align="center"></td>
                                            <td align="center">{!! number_format(round($row['payment_sup_total'] * 110 / 100)) !!}</td>
                                            <td align="center"></td>
                                            <td align="center">{!! number_format($row['payment_sup_total']) !!}</td>
                                        </tr>
                                    @php $total_dpp_ppn += $row['payment_sup_total'] * 110 / 100; @endphp
                                    @php $total_debet += 0; @endphp
                                    @php $total_kredit += $row['payment_sup_total'] + $row['payment_sup_total'] * 10 / 100; @endphp
                                    @endforeach
                                    @foreach($operational as $row)
                                        <tr align="center">
                                            <td align="center">{!! $row['oprational_date'] !!}</td>
                                            <td align="center"></td>
                                            <td align="center">{!! $coaOperasional['coa_nomor'] !!}</td>
                                            <td align="center">{!! $coaOperasional['coa_name'] !!}</td>
                                            <td align="center">{!! $row['oprational_desc'] !!}</td>
                                            <td align="center">{!! number_format(round($row['oprational_nominal'] * 110 / 100)) !!}</td>
                                            <td align="center"></td>
                                            <td align="center">{!! number_format($row['oprational_nominal']) !!}</td>
                                        </tr>
                                    @php $total_dpp_ppn += $row['oprational_nominal'] * 110 / 100; @endphp
                                    @php $total_debet += 0; @endphp
                                    @php $total_kredit += $row['oprational_nominal'] + $row['oprational_nominal'] * 10 / 100; @endphp
                                    @endforeach
		                            </tbody>
                                    <tfoot>
                                        <tr>
                                            <th align="center"></th>
                                            <th align="center"></th>
                                            <th align="center"></th>
                                            <th align="center"></th>
                                            <th align="center"></th>
                                            <th align="center">Total : {!! number_format(round($total_dpp_ppn)) !!}</th>
                                            <th align="center">Total : {!! number_format(round($total_debet)) !!}</th>
                                            <th align="center">Total : {!! number_format(round($total_kredit)) !!}</th>
                                        </tr>
                                    </tfoot>
	                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-body end -->
	    </div>
	</div>
</div>
@stop

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>

    <script type="text/javascript" src="{{ asset('bower_components/switchery/js/switchery.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/swithces.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/modalEffects.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/classie.js') }}"></script>
@endsection