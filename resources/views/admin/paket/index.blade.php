@extends('template.index')

@section('title')
Master Data - Paket Barang
@stop

@section('style')

    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/css/buttons.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/switchery/css/switchery.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/component.css') }}">

@stop

@section('content')
    <!-- Main-body start -->
<div class="main-body">
    <div class="page-wrapper">
        <!-- Page-header start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                        <div class="d-inline">
                            <h4>Data Paket Barang</h4>
                            <span>List All Paket Barang</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ url('home/main') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Master Data - Paket Barang</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-header end -->

        <!-- Page-body start -->
        <div class="page-body">
            <div class="row">
                <div class="col-sm-12">

                    <!-- HTML5 Export Buttons table start -->
                    <div class="card">
                        <div class="card-header table-card-header">
                            <h5>List Paket Barang</h5>
                        </div>
                        <div class="card-block">
                        	<a href="{{ url('home/master/paket-barang/create') }}" id="addRow" class="btn btn-primary m-b-20">+ Add New</a>
                            <div class="dt-responsive table-responsive">
                                <table id="item-tbl" class="table table-striped table-bordered nowrap">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Number Part</th>
                                            <th>Nama</th>
                                            <th>Kode Part</th>
                                            <th>Motor</th>
                                            <th>Satuan</th>
                                            <th>H.E.T</th>
                                            <th>Harga Pokok</th>
                                            <th>Harga Beli</th>
                                            <th>Stok Barang</th>
                                            <th>Stok Minimum</th>
                                            <th>Stok Maksimum</th>
                                            <th>Barcode</th>
                                            <th>Berat</th>
                                            <th>Config</th>
                                        </tr>
                                    </thead>
                                    <tbody>
		                            </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Number Part</th>
                                            <th>Nama</th>
                                            <th>Kode Part</th>
                                            <th>Motor</th>
                                            <th>Satuan</th>
                                            <th>H.E.T</th>
                                            <th>Harga Pokok</th>
                                            <th>Harga Beli</th>
                                            <th>Stok Barang</th>
                                            <th>Stok Minimum</th>
                                            <th>Stok Maksimum</th>
                                            <th>Barcode</th>
                                            <th>Berat</th>
                                            <th>Config</th>
                                        </tr>
                                    </tfoot>
	                            </table>
                            </div>

                            <div class="modal fade" id="delete-modal" tabindex="-1" role="dialog">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header bg-danger">
                                            <h4 class="modal-title">Hapus Data</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <p id="modal-text">Apakah Anda Yakin Akan Menghapus <b>Item</b> ?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-danger waves-effect" id="btnDelete">Ya</button>
                                            <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-body end -->
	    </div>
	</div>
</div>
@stop

@section('script')
	<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>

    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>

    <script type="text/javascript" src="{{ asset('bower_components/switchery/js/switchery.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/swithces.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/modalEffects.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/classie.js') }}"></script>
    <script type="text/javascript">
    $(document).ready(function () {
        $('#item-tbl').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax":{
                     "url": "{{ url('home/ajax/item-packages') }}",
                     "dataType": "json",
                     "type": "POST",
                     "data":{ _token: "{{csrf_token()}}"}
                   },
            "columns": [
                { "data": "item_package_id", sorttable: true },
                { "data": "item_package_code", sorttable: true },
                { "data": "item_package_name", sorttable: true },
                { "data": "brand_detail_id", sorttable: true },
                { "data": "character_id", sorttable: true },
                { "data": "unit_id", sorttable: true },
                { "data": "item_package_het", sorttable: true},
                { "data": "item_package_price_primary", sorttable: true},
                { "data": "item_package_price_buy", sorttable: true},
                { "data": "item_package_stock", sorttable: true},
                { "data": "item_package_stock_min", sorttable: true},
                { "data": "item_package_stock_max", sorttable: true},
                { "data": "item_package_barcode", sorttable: true},
                { "data": "item_package_weight", sorttable: true},
                { "data": "config", sorttable: true }
            ]
        });
    });

    function showDeleteModal(item_package_id) {
        swal({
            title: "Are you sure ??",
            text: "Paket Barang Tidak Dapat Kembali Jika Sudah Terhapus", 
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $.post("{{ url('home/ajax/item-packages/delete') }}", {
                item_package_id : item_package_id,
                _token: "{{ csrf_token() }}"
            }, function (data) {
                if (data.status == "success") {
                    swal(data.msg);
                    window.location.reload();
                } else {
                    swal(data.msg);
                    window.location.reload();
                }
            });
          } else {
            swal("Paket Barang Anda Tidak Jadi Dihapus");
          }
        });
    }
    </script>
@endsection