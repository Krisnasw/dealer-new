@extends('template.index')

@section('title')
Laporan - Pembantu Piutang
@stop

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/switchery/css/switchery.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/component.css') }}">
@stop

@section('content')
    <!-- Main-body start -->
<div class="main-body">
    <div class="page-wrapper">
        <!-- Page-header start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                        <div class="d-inline">
                            <h4>Data Pembantu Piutang</h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ url('home/main') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Laporan - Piutang</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-header end -->

        <!-- Page-body start -->
        <div class="page-body">
            <div class="row">
                <div class="col-sm-12">

                    <!-- HTML5 Export Buttons table start -->
                    <div class="card">
                        <div class="card-header table-card-header">
                            <h5>Data Pembantu Piutang Periode : {!! getBulan($month) !!} {!! $year !!}</h5>
                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table class="table table-striped table-bordered nowrap">
                                    <thead>
                                        <tr>
                                            <th align="center">Tanggal</th>
                                            <th align="center">Jatuh Tempo</th>
                                            <th align="center">No. Nota</th>
                                            <th align="center">Nama Customer</th>
                                            <th align="center">Total</th>
                                            <th align="center">Bayar</th>
                                            <th align="center">Sisa</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @php $total_netto = 0; @endphp
                                    @php $total_bayar = 0; @endphp
                                    @php $total_sisa = 0; @endphp
                                    @foreach($nota as $index => $row)
                                    @php $nitip = Access::getListTitipUang($row['customer_id'], $month, $year); @endphp
                                        <tr align="center">
                                            <td align="center">{!! $row['nota_date'] !!}</td>
                                            <td align="center">{!! date('Y-m-d', strtotime('+'.$row['nota_tempo'].' days', strtotime($row['nota_date']))); !!}</td>
                                            <td align="center">{!! $row['nota_code'] !!}</td>
                                            <td align="center">{!! $row['customer_name'] !!}</td>
                                            <td align="center">{!! number_format($row['nota_netto']) !!}</td>
                                            <td align="center">0</td>
                                            <td align="center">0</td>
                                        </tr>
                                        @php $total_netto += $row['nota_netto']; @endphp
                                        @foreach($nitip as $index => $row)
                                        @php $total_bayar += $row['entrusted_total']; @endphp
                                            <tr align="center">
                                                <td align="center">{!! $row['entrusted_date'] !!}</td>
                                                <td align="center">0</td>
                                                <td align="center">{!! $row['entrusted_code'] !!}</td>
                                                <td align="center">{!! $row['customer']['customer_name'] !!}</td>
                                                <td align="center">0</td>
                                                <td align="center">{!! number_format($row['entrusted_total']) !!}</td>
                                                <td align="center">{!! number_format(($total_netto - $total_bayar)) !!}</td>
                                            </tr>
                                        @endforeach
                                    @endforeach
		                            </tbody>
                                    <tfoot>
                                        <tr>
                                            <th align="center">Tanggal</th>
                                            <th align="center">Jatuh Tempo</th>
                                            <th align="center">No. Nota</th>
                                            <th align="center">Nama Customer</th>
                                            <th align="center">Total</th>
                                            <th align="center">Bayar</th>
                                            <th align="center">Sisa</th>
                                        </tr>
                                    </tfoot>
	                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-body end -->
	    </div>
	</div>
</div>
@stop

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>

    <script type="text/javascript" src="{{ asset('bower_components/switchery/js/switchery.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/swithces.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/modalEffects.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/classie.js') }}"></script>
@endsection