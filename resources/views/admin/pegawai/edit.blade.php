@extends('template.index')

@section('title')
Dealer Information System - Edit Pegawai
@stop

@section('style')

    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}"/>
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}"/>

    <link href="{{ asset('assets/pages/jquery.filer/css/jquery.filer.css') }}" type="text/css" rel="stylesheet" />
    <link href="{{ asset('assets/pages/jquery.filer/css/themes/jquery.filer-dragdropbox-theme.css') }}" type="text/css" rel="stylesheet" />

    <!-- lightbox Fremwork -->
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/lightbox2/css/lightbox.min.css') }}">

@stop

@section('content')
<div class="main-body">
	<div class="page-wrapper">

        <!-- Page-header start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                        <div class="d-inline">
                            <h4>Edit Pegawai</h4>
                            <span>Edit Pegawai - {!! $emp->employee_name !!}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ url('home/main') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Master Data - Pegawai</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-header end -->

		<div class="page-body">
			<div class="row">
				<div class="col-sm-12">

					<!-- Tooltip Validation card start -->
                    <div class="card">
                        <div class="card-header">
                            <h5>Edit Pegawai - {!! $emp->employee_name !!}</h5>
                            <div class="card-header-right">
                            	<i class="icofont icofont-spinner-alt-5"></i>
                            </div>
                        </div>
                        <div class="card-block">
                        	{!! Form::model($emp, ['id' => 'second', 'method' => 'PATCH', 'novalidate' => 'novalidate', 'action' => ['PegawaiController@update', base64_encode($emp->employee_id)], 'files' => true]) !!}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Nama Lengkap</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="usernameP" name="nama" placeholder="Masukkan Nama Lengkap" value="{!! $emp->employee_name !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Telepon</label>
                                    <div class="col-sm-10">
                                        <input type="number" min="0" class="form-control" id="usernameP" name="telp" placeholder="Masukkan Telepon" value="{!! $emp->employee_no_telp !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Nomor KTP</label>
                                    <div class="col-sm-10">
                                        <input type="number" min="0" class="form-control" id="usernameP" name="no_ktp" placeholder="Masukkan No. KTP" value="{!! $emp->employee_ktp !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Jabatan</label>
                                    <div class="col-sm-10">
                                        <select class="col-sm-12 js-example-basic-single select2-hidden-accessible" name="posisi">
                                            @foreach($pos as $key)
                                            <option value="{!! $key->position_id !!}" {!! ($key->position_id == $emp->position_id) ? 'selected' : '' !!}>{!! $key->position_name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Jenis Kelamin</label>
                                    <div class="col-sm-10 ">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="gender" value="0" {!! ($emp->employee_gender == 0) ? 'checked' : '' !!}> Laki - Laki
                                            </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="gender" value="1" {!! ($emp->employee_gender == 1) ? 'checked' : '' !!}> Perempuan
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Tempat Tanggal Lahir</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="usernameP" name="f_tempatLahir" placeholder="Masukkan Tempat Lahir" value="{!! $emp->employee_birth_place !!}">
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="date" class="form-control" name="f_tgLahir" placeholder="00-00-0000" value="{!! $emp->employee_birth_date !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Nomor SIM</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="usernameP" name="sim" placeholder="Masukkan No. SIM" value="{!! $emp->employee_sim !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Gaji</label>
                                    <div class="col-sm-10">
                                        <input type="number" class="form-control" id="usernameP" name="gaji" min="0" placeholder="Masukkan Gaji" value="{!! $emp->employee_first_salary !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" id="usernameP" name="email" placeholder="Masukkan Email" value="{!! $emp->employee_email !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Alamat</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" name="alamat" placeholder="Masukkan Alamat Lengkap">{!! $emp->employee_address_ktp !!}</textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Foto</label>
                                    <div class="col-sm-10">
                                        <input type="file" name="image" accept="image/x-png,image/gif,image/jpeg">
                                        <img src="{{ asset($emp->employee_photo) }}" class="img-responsive">
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                    <!-- Image grid card start -->
                    <div class="card">
                        <div class="card-header">
                            <h5>List Galeri Pegawai - {!! $emp->employee_name !!}</h5>
                            <button class="btn btn-info m-b-0" type="button" data-toggle="modal" data-target="#upload">Upload Galeri</button>
                        </div>
                        <div class="card-block">
                            <div class="row">
                                @if(count($gal) <= 0)
                                <div class="col-sm-12">
                                    <center>Tidak Ada Data</center>
                                </div>
                                @else
                                    @foreach($gal as $key => $val)
                                    <div class="col-lg-4 col-sm-6">
                                        <div class="thumbnail">
                                            <div class="thumb">
                                                <a href="{{ asset($val->employee_galery_file) }}" data-lightbox="1">
                                                    <img src="{{ asset($val->employee_galery_file) }}" alt="" class="img-fluid img-thumbnail">
                                                </a>
                                            </div>
                                        </div>

                                        <button type="button" data-toggle="modal" data-target="#delete-gal" class="btn btn-danger m-b-0">Delete</button>
                                    </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- Image grid card end -->

                    <div class="modal fade md-effect-1" id="upload" tabindex="-1" role="dialog">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header bg-info">
                                    <h5 class="modal-title">Upload Gallery</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            {!! Form::open(['method' => 'POST', 'action' => 'ApiController@uploadGaleriPegawai', 'files' => true]) !!}
                                <div class="modal-body">
                                    <!-- Basic Form Inputs card start -->
                                    <div class="card">
                                        <div class="card-block">
                                            <h4 class="sub-title">Form Gallery</h4>
                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label">Foto</label>
                                                    <div class="col-sm-10">
                                                        <input type="hidden" name="id" value="{!! $emp->employee_id !!}">
                                                        <input type="file" class="form-control" placeholder="Pilih Foto" name="images">
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                    <!-- Basic Form Inputs card end -->
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light" id="submits">Save changes</button>
                                </div>
                            {!! Form::close() !!}
                            </div>
                        </div>
                    </div>

                    <div class="modal fade md-effect-1" id="delete-gal" tabindex="-1" role="dialog">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header bg-info">
                                    <h5 class="modal-title">Hapus Gallery</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            {!! Form::open(['method' => 'POST', 'action' => 'ApiController@deleteGallery']) !!}
                                <div class="modal-body">
                                    <!-- Basic Form Inputs card start -->
                                    <input type="hidden" name="id" value="{!! $emp->employee_id !!}">
                                    Apakah Anda Yakin Menghapus Gambar Ini ?
                                    <!-- Basic Form Inputs card end -->
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light" id="submits">Ya</button>
                                </div>
                            {!! Form::close() !!}
                            </div>
                        </div>
                    </div>

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('script')
<!-- Select 2 js -->
<script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
<!-- Multiselect js -->
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>

<script src="{{ asset('assets/pages/jquery.filer/js/jquery.filer.min.js') }}"></script>
<script src="{{ asset('assets/pages/filer/custom-filer.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/filer/jquery.fileuploads.init.js') }}" type="text/javascript"></script>

<script type="text/javascript" src="{{ asset('bower_components/lightbox2/js/lightbox.min.js') }}"></script>
<script>
    lightbox.option({
        'resizeDuration': 200,
        'wrapAround': true
    })

</script>
@stop