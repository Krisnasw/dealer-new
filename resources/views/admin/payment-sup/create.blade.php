@extends('template.index')

@section('title')
    Dealer Information System - Buat Payment Supplier
@stop

@section('style')

    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}"/>
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}"/>

@stop

@section('content')
    <div class="main-body">
        <div class="page-wrapper">

            <!-- Page-header start -->
            <div class="page-header card">
                <div class="row align-items-end">
                    <div class="col-lg-8">
                        <div class="page-header-title">
                            <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                            <div class="d-inline">
                                <h4>Buat Payment Supplier</h4>
                                <span>Buat Payment Supplier Baru</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="page-header-breadcrumb">
                            <ul class="breadcrumb-title">
                                <li class="breadcrumb-item">
                                    <a href="{{ url('home/main') }}">
                                        <i class="icofont icofont-home"></i>
                                    </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">Pembelian - Payment Supplier</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Page-header end -->

            <div class="page-body">
                <div class="row">
                    <div class="col-sm-12">

                        <!-- Tooltip Validation card start -->
                        <div class="card">
                            <div class="card-header">
                                <h5>Buat Payment Supplier Baru</h5>
                                <div class="card-header-right">
                                    <i class="icofont icofont-spinner-alt-5"></i>
                                </div>
                            </div>
                            <div class="card-block">
                                {!! Form::open(['id' => 'second', 'method' => 'POST', 'novalidate' => 'novalidate', 'action' => 'PaymentSupController@store']) !!}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Supplier</label>
                                    <div class="col-sm-4">
                                        <select class="form-control js-example-basic-single select2-hidden-accessible" name="supplier" id="supplier" onchange="get_supplier(this.value);">
                                            <option value="0" selected>- Pilih Supplier -</option>
                                            @foreach($supp as $key)
                                                <option value="{!! $key['supplier_id'] !!}">{!! $key['supplier_name'] !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    
                                    <label class="col-sm-2 col-form-label">Tanggal Pembayaran</label>
                                    <div class="col-sm-4">
                                        <input type="date" class="form-control" name="date" value="{!! date('Y-m-d') !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">PO</label>
                                    <div class="col-sm-4">
                                        <select class="form-control js-example-basic-single select2-hidden-accessible" name="po" id="po" onchange="get_po(this.value);">
                                            <option value="0" selected>- Pilih PO -</option>
                                            @foreach($purchase as $row)
                                            <option value="{!! $row['purchase_id'] !!}">{!! $row['purchase_code'] !!}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label class="col-sm-2 col-form-label">Type PO</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="i_type_po" class="form-control" style="background: transparent; border: none;">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">List DO</label>
                                    <div class="col-sm-4">
                                        <textarea name="i_list_do" rows="3" class="form-control" style="background: transparent; border: none;" readonly=""></textarea>
                                    </div>

                                    <label class="col-sm-2 col-form-label">Retur</label>
                                    <div class="col-sm-4">
                                        <select class="form-control js-example-basic-multiple" multiple="multiple" name="i_retur[]">
                                            <option value="0" selected="selected">- Pilih Retur -</option>
                                            @foreach($retur as $row)
                                                @foreach($list_retur as $row2)
                                                <option value="{!! $row['retur_id'] !!}" {!! ($row2['retur_id'] == $row['retur_id']) ? 'selected' : '' !!}>{!! $row['retur_code'] !!}</option>
                                                @endforeach
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <!-- Select 2 js -->
    <script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
    <!-- Multiselect js -->
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>

    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js') }}"></script>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function get_supplier(id){
          $.ajax({
              type: 'POST',
              url: '{{ url('home/pembayaran/payment-sup/read-po') }}',
              data: {id:id},
              dataType: 'JSON',
              success: function(data){
                $('#po').html(data.content);
              } 
            });

        }

        function get_po(id){
          $.ajax({
              type: 'POST',
              url: '{{ url('home/pembayaran/payment-sup/read-po-detail') }}',
              data: {id:id},
              dataType: 'JSON',
              success: function(data){
                $('input[name="i_type_po"]').val(data.content['type']);
                $('textarea[name="i_list_do"]').val(data.content['code']);
              } 
            });
        }
    </script>
@stop