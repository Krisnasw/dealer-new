@extends('template.index')

@section('title')
    Dealer Information System - Buat Payment Supplier
@stop

@section('style')

    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}"/>
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}"/>

@stop

@section('content')
    <div class="main-body">
        <div class="page-wrapper">

            <!-- Page-header start -->
            <div class="page-header card">
                <div class="row align-items-end">
                    <div class="col-lg-8">
                        <div class="page-header-title">
                            <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                            <div class="d-inline">
                                <h4>Buat Payment Supplier</h4>
                                <span>Buat Payment Supplier Baru</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="page-header-breadcrumb">
                            <ul class="breadcrumb-title">
                                <li class="breadcrumb-item">
                                    <a href="{{ url('home/main') }}">
                                        <i class="icofont icofont-home"></i>
                                    </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">Pembelian - Payment Supplier</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Page-header end -->

            <div class="page-body">
                <div class="row">
                    <div class="col-sm-12">

                        <!-- Tooltip Validation card start -->
                        <div class="card">
                            <div class="card-header">
                                <h5>Buat Payment Supplier Baru</h5>
                                <div class="card-header-right">
                                    <i class="icofont icofont-spinner-alt-5"></i>
                                </div>
                            </div>
                            <div class="card-block">
                                {!! Form::open(['id' => 'second', 'method' => 'POST', 'novalidate' => 'novalidate', 'action' => 'PaymentSupController@store']) !!}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Supplier</label>
                                    <div class="col-sm-4">
                                        <select class="form-control js-example-basic-single select2-hidden-accessible" name="supplier" id="supplier" onchange="get_supplier(this.value);">
                                            <option value="0" selected>- Pilih Supplier -</option>
                                            @foreach($supp as $key)
                                                <option value="{!! $key['supplier_id'] !!}" {!! ($payment['supplier_id'] == $key['supplier_id']) ? 'selected' : '' !!}>{!! $key['supplier_name'] !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    
                                    <label class="col-sm-2 col-form-label">Tanggal Pembayaran</label>
                                    <div class="col-sm-4">
                                        <input type="date" class="form-control" name="date" value="{!! $payment['payment_sup_date'] !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">PO</label>
                                    <div class="col-sm-4">
                                        <select class="form-control js-example-basic-single select2-hidden-accessible" name="po" id="po" onchange="get_po(this.value);">
                                            <option value="0" selected>- Pilih PO -</option>
                                            @foreach($purchase as $row)
                                            <option value="{!! $row['purchase_id'] !!}" {!! ($payment['purchase_id'] == $row['purchase_id']) ? 'selected' : '' !!}>{!! $row['purchase_code'] !!}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label class="col-sm-2 col-form-label">Type PO</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="i_type_po" class="form-control" style="background: transparent; border: none;" value="{!! $type_do !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">List DO</label>
                                    <div class="col-sm-4">
                                        <textarea name="i_list_do" rows="3" class="form-control" style="background: transparent; border: none;" readonly="">{!! $list_do !!}</textarea>
                                    </div>

                                    <label class="col-sm-2 col-form-label">Retur</label>
                                    <div class="col-sm-4">
                                        <select class="form-control js-example-basic-multiple" multiple="multiple" name="i_retur[]">
                                            <option value="0" selected="selected">- Pilih Retur -</option>
                                            @foreach($retur as $row)
                                                @foreach($list_retur as $row2)
                                                <option value="{!! $row['retur_id'] !!}" {!! ($row2['retur_id'] == $row['retur_id']) ? 'selected' : '' !!}>{!! $row['retur_code'] !!}</option>
                                                @endforeach
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header table-card-header">
                                        <h5>List Data DO</h5>
                                    </div>
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="basic-btn" class="table table-striped table-bordered nowrap">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Number Part</th>
                                                        <th>Nama Barang</th>
                                                        <th>Kode DO</th>
                                                        <th>Qty DO</th>
                                                        <th>HET</th>
                                                        <th>Diskon</th>
                                                        <th>Total</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @php $grand_total_tagihan = 0; @endphp
                                                @foreach($list_detail as $data => $row)
                                                @php $total = $row['receipt_agency_qty'] * $row['purchase_detail_price']; @endphp
                                                @php $diskon = $total * $row['purchase_detail_discount'] / 100; @endphp
                                                @php $grand_total = $total - $diskon; @endphp
                                                    <tr>
                                                        <td width="1%" align="center">{!! ++$data !!}</td>
                                                        <td>{!! $row['item_code'] !!}</td>
                                                        <td>{!! $row['item_name'] !!}</td>
                                                        <td>{!! $row['receipt_code'] !!}</td>
                                                        <td>{!! $row['receipt_agency_qty'] !!}</td>
                                                        <td>{!! number_format($row['purchase_detail_price'],2) !!}</td>
                                                        <td>{!! $row['purchase_detail_discount'] !!}</td>
                                                        <td>{!! number_format($grand_total, 2) !!}</td>
                                                    </tr>
                                                @php $grand_total_tagihan += $grand_total; @endphp
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label class="col-form-label">Total Tagihan : <input style="border: none; background: transparent; font-weight: bold;" type="text" readonly="readonly" value="{!! number_format($grand_total_tagihan, 2) !!}" name="i_grand_total_payoff"></label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="col-form-label pull-right">Total Dibayar : <input style="border: none; background: transparent; font-weight: bold;" type="text" readonly="readonly" value="{!! number_format($grand_total_tagihan, 2) !!}" name="i_grand_total_dibayar"></label>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label class="col-form-label">Potongan Retur : <input style="border: none; background: transparent; font-weight: bold;" type="text" readonly="readonly" value="{!! round($retur_total['total'], 2) !!}" name="i_grand_total_potongan"></label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="col-form-label pull-right">Sisa Tagihan : <input style="border: none; background: transparent; font-weight: bold;" type="text" readonly="readonly" value="{!! number_format(round($grand_total_tagihan - $retur_total['total']), 2) !!}" name="i_rest"></label>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header table-card-header">
                                        <h5>List History Pembayaran</h5>
                                    </div>
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="basic-btn" class="table table-striped table-bordered nowrap">
                                                <thead>
                                                    <tr>
                                                        <th>Pembayaran</th>
                                                        <th>
                                                            <div class="input-group" style="margin-bottom: 0px !important;"><span class="input-group-addon"><i class="icofont icofont-calendar"></i></span>
                                                                <input placeholder="Tanggal Bayar" id="daterangepicker_3_2" type="date" name="i_detail_date_pay" value="" class="form-control">
                                                                <input type="hidden" class="form-control" value="" name="i_detail_id" >
                                                            </div>
                                                        </th>
                                                        <th>
                                                            <input type="number" class="form-control" value="" name="i_detail_pay" placeholder="Masukkan Nominal Bayar" onkeypress="if (event.keyCode == 13) { save_detail(); }">
                                                        </th>
                                                        <th>
                                                            <input type="text" class="form-control" value="" name="i_detail_desc" placeholder="Masukkan Keterangan" onkeypress="if (event.keyCode == 13) { save_detail(); }">
                                                        </th>
                                                        <th>
                                                            <a href="javascript:void(0)" class="btn btn-danger m-b-0" onclick="save_detail()" >Bayar</a>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Tanggal</th>
                                                        <th>Nominal</th>
                                                        <th>Keterangan</th>
                                                        <th>Config</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($list_detail_pay as $data => $row)
                                                    <tr>
                                                        <td width="1%" align="center">{!! ++$data !!}</td>
                                                        <td>{!! $row['payment_sup_detail_date'] !!}</td>
                                                        <td>{!! number_format($row['payment_sup_detail_pay'],2) !!}</td>
                                                        <td>{!! $row['payment_sup_detail_desc'] !!}</td>
                                                        <td>
                                                            <a href="javascript:void(0)" onclick="edit_history({!! $row['payment_sup_detail_id'] !!},{!! $row['payment_sup_detail_date'] !!},{!! $row['payment_sup_detail_pay'] !!},{!! $row['payment_sup_detail_desc'] !!});" class="btn btn-default" ><i class="icofont icofont-edit"></i></a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="button" class="btn btn-primary m-b-0" onclick="history.go(-1);">Kembali</button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <!-- Select 2 js -->
    <script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
    <!-- Multiselect js -->
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>

    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js') }}"></script>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function get_supplier(id){
          $.ajax({
              type: 'POST',
              url: '{{ url('home/pembayaran/payment-sup/read-po') }}',
              data: {id:id},
              dataType: 'JSON',
              success: function(data){
                $('#po').html(data.content);
              } 
            });

        }

        function get_po(id){
          $.ajax({
              type: 'POST',
              url: '{{ url('home/pembayaran/payment-sup/read-po-detail') }}',
              data: {id:id},
              dataType: 'JSON',
              success: function(data){
                $('input[name="i_type_po"]').val(data.content['type']);
                $('textarea[name="i_list_do"]').val(data.content['code']);
              } 
            });
        }

        function save_detail() {
          var tanggal = $('input[name="i_detail_date_pay"]').val();
          var nominal = $('input[name="i_detail_pay"]').val();
          var ket     = $('input[name="i_detail_desc"]').val();
          var id      = {!! $payment['payment_sup_id'] !!};
          var sisa    = $('input[name="i_rest"]').val();
          var detail_id    = $('input[name="i_detail_id"]').val();

          if (detail_id) {
            $.ajax({
              type: 'POST',
              url: '{{ url('home/pembayaran/payment-sup/save-detail') }}',
              data: {id:id,tanggal:tanggal,nominal:nominal,ket:ket,detail_id:detail_id},
              dataType: 'JSON',
              success: function(data) {
                window.location.reload();
              } 
            });
          }else{
            if (nominal > sisa) {
              swal('Oops!','Pembayaran tidak boleh melebihi sisa tagihan!','error');
              $('input[name="i_detail_pay"]').val(0);
            } else if(nominal == 0) {
              swal('Oops!','Pembayaran tidak boleh 0!', 'error');
            } else {
              $.ajax({
                type: 'POST',
                url: '{{ url('home/pembayaran/payment-sup/save-detail') }}',
                data: {id:id,tanggal:tanggal,nominal:nominal,ket:ket,detail_id:detail_id},
                dataType: 'JSON',
                success: function(data) {
                    window.location.reload();
                } 
              });
            }
          }
            
        }

        function edit_history(id,date,price,ket) {
          var tanggal = date.split("-");
          var date_new = tanggal[1]+"/"+tanggal[2]+"/"+tanggal[0];
          //alert(date_new);
          $('input[name="i_detail_date_pay"]').val(date_new);
          $('input[name="i_detail_pay"]').val(price);
          $('input[name="i_detail_desc"]').val(ket);
          $('input[name="i_detail_id"]').val(id);
        }
    </script>
@stop