@extends('template.index')

@section('title')
Dashboard - Dealer
@stop

@section('style')

    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/css/buttons.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/switchery/css/switchery.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/component.css') }}">
@stop

@section('content')
<div class="main-body">
    <div class="page-wrapper">
<div class="row">
    <!-- card1 start -->
    <div class="col-md-6 col-xl-3">
        <div class="card widget-card-1">
            <div class="card-block-small">
                <i class="icofont icofont-user bg-c-blue card1-icon"></i>
                <span class="text-c-blue f-w-600">Total Customer</span>
                <h4>{!! $customer !!}</h4>
            </div>
        </div>
    </div>
    <!-- card1 end -->
    <!-- card1 start -->
    <div class="col-md-6 col-xl-3">
        <div class="card widget-card-1">
            <div class="card-block-small">
                <i class="icofont icofont-people bg-c-pink card1-icon"></i>
                <span class="text-c-pink f-w-600">Total Sales</span>
                <h4>{!! $sales !!}</h4>
            </div>
        </div>
    </div>
    <!-- card1 end -->
    <!-- card1 start -->
    <div class="col-md-6 col-xl-3">
        <div class="card widget-card-1">
            <div class="card-block-small">
                <i class="icofont icofont-files bg-c-green card1-icon"></i>
                <span class="text-c-green f-w-600">Total Barang</span>
                <h4>{!! $item !!}</h4>
            </div>
        </div>
    </div>
    <!-- card1 end -->
    <!-- card1 start -->
    <div class="col-md-6 col-xl-3">
        <div class="card widget-card-1">
            <div class="card-block-small">
                <i class="icofont icofont-ui-rss bg-c-yellow card1-icon"></i>
                <span class="text-c-yellow f-w-600">Total Faktur</span>
                <h4>{!! $nota !!}</h4>
            </div>
        </div>
    </div>
    <!-- card1 end -->

    <!-- Project overview Start -->
    <div class="col-md-12 col-xl-12">
        <div class="card">
            <div class="card-header table-card-header">
                <h5>List Pending Customer</h5>
            </div>
            <div class="card-block">
                <div class="dt-responsive table-responsive">
                    <table id="item-tbl" class="table table-striped table-bordered nowrap">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Pelanggan</th>
                                <th>Nama</th>
                                <th>Alamat</th>
                                <th>Telepon</th>
                                <th>Grade Pelanggan</th>
                                <th>No. Rekening</th>
                                <th>Bank</th>
                                <th>NPWP</th>
                                <th>Nama NPWP</th>
                                <th>Expedisi</th>
                                <th>Handphone</th>
                                <th>Fax</th>
                                <th>Wilayah</th>
                                <th>Config</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Kode Pelanggan</th>
                                <th>Nama</th>
                                <th>Alamat</th>
                                <th>Telepon</th>
                                <th>Grade Pelanggan</th>
                                <th>No. Rekening</th>
                                <th>Bank</th>
                                <th>NPWP</th>
                                <th>Nama NPWP</th>
                                <th>Expedisi</th>
                                <th>Handphone</th>
                                <th>Fax</th>
                                <th>Wilayah</th>
                                <th>Config</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- Project overview End -->
</div>
</div>
</div>
@stop

@section('script')

    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>

    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>

    <script type="text/javascript" src="{{ asset('bower_components/switchery/js/switchery.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/swithces.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/modalEffects.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/classie.js') }}"></script>
    
    <script type="text/javascript">
        $(document).ready(function () {
            $('#item-tbl').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax":{
                         "url": "{{ url('home/ajax/cust-pending') }}",
                         "dataType": "json",
                         "type": "POST",
                         "data":{ _token: "{{csrf_token()}}"}
                       },
                "columns": [
                    { "data": "customer_id", sorttable: true },
                    { "data": "customer_code", sorttable: true },
                    { "data": "customer_name", sorttable: true },
                    { "data": "customer_addres", sorttable: true },
                    { "data": "customer_phone", sorttable: true },
                    { "data": "grade_id", sorttable: true },
                    { "data" : "customer_rekening", sorttable: true },
                    { "data" : "customer_id_bank", sorttable: true },
                    { "data" : "customer_npwp", sorttable: true },
                    { "data" : "customer_nama_npwp", sorttable: true },
                    { "data" : "customer_expedisi", sorttable: true },
                    { "data" : "customer_hp", sorttable: true },
                    { "data" : "customer_fax", sorttable: true },
                    { "data" : "city_id", sorttable: true },
                    { "data": "config", sorttable: true }
                ]
            });
        });
    </script>
@stop