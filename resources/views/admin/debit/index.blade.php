@extends('template.index')

@section('title')
Pembayaran - Debit / Kredit
@stop

@section('style')

    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/css/buttons.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/switchery/css/switchery.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/component.css') }}">

@stop

@section('content')
    <!-- Main-body start -->
<div class="main-body">
    <div class="page-wrapper">
        <!-- Page-header start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                        <div class="d-inline">
                            <h4>Data Pembayaran - Debit / Kredit</h4>
                            <span>List All Pembayaran - Debit / Kredit</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ url('home/main') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Pembayaran - Debit / Kredit</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-header end -->

        <!-- Page-body start -->
        <div class="page-body">
            <div class="row">
                <div class="col-sm-12">

                    <!-- HTML5 Export Buttons table start -->
                    <div class="card">
                        <div class="card-header table-card-header">
                            <h5>List Pembayaran - Debit / Kredit</h5>
                        </div>
                        <div class="card-block">
                        	<a href="{{ url('home/pembayaran/debit/create') }}" id="addRow" class="btn btn-primary m-b-20">+ Add New</a>
                            <div class="dt-responsive table-responsive">
                                <table id="basic-btn" class="table table-striped table-bordered nowrap">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Kode</th>
                                            <th>Type</th>
                                            <th>Nama Customer</th>
                                            <th>Tanggal</th>
                                            <th>Total</th>
                                            <th>Config</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($list as $data => $row)
		                                <tr>
		                                    <td width="1%" align="center">{!! ++$data !!}</td>
                                            <td>{!! $row['debit_code'] !!}</td>
                                            <td>{!! ($row['debit_type'] == 1) ? 'Debit' : 'Kredit' !!}</td>
		                                    <td>{!! $row['customer']['customer_name'] !!}</td>
                                            <td>{!! $row['debit_date'] !!}</td>
                                            <td>{!! number_format($row['debit_nominal'], 2) !!}</td>
		                                    <td align="center">
		                                        <a href="{{ url('home/pembayaran/debit') }}/{!! base64_encode($row['debit_id']) !!}/edit" class="btn btn-success btn-outline-success"><i class="icofont icofont-edit-alt"></i></a>
		                                        <button type="button" class="btn btn-danger btn-outline-info" data-toggle="modal" data-target="#delete-Modal{{{ $row['debit_id'] }}}"><i class="icofont icofont-delete-alt"></i></button>
		                                    </td>
		                                </tr>

		                                <div class="modal fade" id="delete-Modal{{{ $row['debit_id'] }}}" tabindex="-1" role="dialog">
	                                        <div class="modal-dialog" role="document">
	                                            <div class="modal-content">
	                                                <div class="modal-header bg-danger">
	                                                    <h4 class="modal-title">Hapus Data</h4>
	                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				                                            <span aria-hidden="true">&times;</span>
				                                        </button>
	                                        		</div>
    	                                            <div class="modal-body">
    	                                            	<p>Apakah Anda Yakin Akan Menghapus <b>{!! $row['debit_code'] !!}</b> ?</p>
    	                                            </div>
    	                                            <div class="modal-footer">
                                                        {!! Form::open(['method' => 'DELETE', 'route' => array('debit.destroy', base64_encode($row['debit_id']))]) !!}
                                                            {!! Form::submit("Ya", array('class' => 'btn btn-primary waves-effect waves-light')) !!}
                                                        {!! Form::close() !!}
    	                                                <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
    	                                            </div>
		                                        </div>
		                                    </div>
		                                </div>
		                            @endforeach
		                            </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Kode</th>
                                            <th>Type</th>
                                            <th>Nama Customer</th>
                                            <th>Tanggal</th>
                                            <th>Total</th>
                                            <th>Config</th>
                                        </tr>
                                    </tfoot>
	                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-body end -->
	    </div>
	</div>
</div>
@stop

@section('script')
	<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>

    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>

    <script type="text/javascript" src="{{ asset('bower_components/switchery/js/switchery.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/swithces.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/modalEffects.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/classie.js') }}"></script>
@endsection