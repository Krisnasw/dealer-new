@extends('template.index')

@section('title')
Laporan - Biaya Keliling Sales
@stop

@section('style')

    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/css/buttons.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/switchery/css/switchery.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/component.css') }}">

@stop

@section('content')
    <!-- Main-body start -->
<div class="main-body">
    <div class="page-wrapper">
        <!-- Page-header start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                        <div class="d-inline">
                            <h4>Data Laporan - Biaya Keliling Sales</h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ url('home/main') }}">
                                    <i class="icofont icofont-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{ url('home/report/biaya-sales') }}">Laporan - Biaya Keliling Sales</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-header end -->

        <!-- Page-body start -->
        <div class="page-body">
            <div class="row">
                <div class="col-sm-12">

                    <!-- HTML5 Export Buttons table start -->
                    <div class="card">
                        <div class="card-header table-card-header">
                            <h5>List Laporan - Biaya Keliling Sales</h5>
                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table id="basic-btn" class="table table-striped table-bordered nowrap">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Sales</th>
                                            <th>Biaya Sales</th>
                                            @foreach($merk as $brand)
                                            <th>{!! $brand['brand_name'] !!}</th>
                                            @endforeach
                                            <th>Total Omset</th>
                                            <th>Persentase</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @php $total = 0; @endphp
                                    @foreach($nota as $datas => $row)
		                                <tr>
		                                    {{-- <td width="1%" align="center">{!! ++$datas !!}</td>
                                            <td align="center">{!! $row['sales_name'] !!}</td>
                                            <td align="center">{!! number_format($nota->where('sales_id', $row['sales_id'])->sum('sales_cost_modal')) !!}</td>
                                            <td align="center">{!! number_format($nota->where('brand_id', 6)->sum('nota_total')) !!}</td>
                                            <td align="center">{!! number_format($nota->where('brand_id', 8)->sum('nota_total')) !!}</td>
                                            <td align="center">{!! number_format($nota->where('brand_id', 9)->sum('nota_total')) !!}</td>
                                            <td align="center">{!! number_format($nota->where('brand_id', 10)->sum('nota_total')) !!}</td>
                                            <td align="center">{!! number_format($nota->where('sales_id', $row['sales_id'])->sum('sales_cost_modal') + $nota->where('brand_id', 6)->sum('nota_total') + $nota->where('brand_id', 8)->sum('nota_total') + $nota->where('brand_id', 9)->sum('nota_total') + $nota->where('brand_id', 10)->sum('nota_total')) !!}</td>
                                            <td align="center">{!! number_format($nota->where('sales_id', $row['sales_id'])->sum('sales_cost_modal') / $nota->where('brand_id', 6)->sum('nota_total') + $nota->where('brand_id', 8)->sum('nota_total') + $nota->where('brand_id', 9)->sum('nota_total') + $nota->where('brand_id', 10)->sum('nota_total') * 100) !!}%</td> --}}
                                            <td width="1%" align="center">{!! ++$datas !!}</td>
                                            <td align="center">{!! $row['sales_name'] !!}</td>
                                            <td align="center">{!! number_format($nota->where('sales_id', $row['sales_id'])->sum('sales_cost_modal')) !!}</td>
                                            @foreach($merk as $brand)
                                            <td>{!! (Access::getTotalByBrand($brand['brand_id'], $row['sales_id'], $month) !== null) ? Access::getTotalByBrand($brand['brand_id'], $row['sales_id'], $month) : '0' !!}</td>
                                            @php $total += Access::getTotalByBrand($brand['brand_id'], $row['sales_id'], $month); @endphp
                                            @endforeach
                                            <td align="center">{!! number_format($nota->where('sales_id', $row['sales_id'])->sum('sales_cost_modal') + $total) !!}</td>
                                            <td align="center">{!! round($nota->where('sales_id', $row['sales_id'])->sum('sales_cost_modal') / $total) !!}%</td>
		                                </tr>
                                    @endforeach
		                            </tbody>
	                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-body end -->
	    </div>
	</div>
</div>
@stop

@section('script')

    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>

    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>

    <script type="text/javascript" src="{{ asset('bower_components/switchery/js/switchery.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/swithces.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/modalEffects.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/classie.js') }}"></script>
@endsection