@extends('template.index')

@section('title')
    Laporan - Pembelian Customer
@stop

@section('style')

    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/css/buttons.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/switchery/css/switchery.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/component.css') }}">

    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}"/>
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}"/>

@stop

@section('content')
    <!-- Main-body start -->
    <div class="main-body">
        <div class="page-wrapper">
            <!-- Page-header start -->
            <div class="page-header card">
                <div class="row align-items-end">
                    <div class="col-lg-8">
                        <div class="page-header-title">
                            <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                            <div class="d-inline">
                                <h4>Data Laporan - Pembelian Customer</h4>
                                <span>List Semua Laporan Pembelian Customer</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="page-header-breadcrumb">
                            <ul class="breadcrumb-title">
                                <li class="breadcrumb-item">
                                    <a href="{{ url('home/main') }}">
                                        <i class="icofont icofont-home"></i>
                                    </a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{ url('home/report') }}">Laporan - Customer</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Page-header end -->

            <!-- Page-body start -->
            <div class="page-body">
                <div class="row">
                    <div class="col-sm-12">

                        <!-- Tooltip Validation card start -->
                        <div class="card">
                            <div class="card-header">
                                <h5>Masukkan Pencarian</h5>
                                <div class="card-header-right">
                                    <i class="icofont icofont-spinner-alt-5"></i>
                                </div>
                            </div>
                            <div class="card-block">
                                {!! Form::open(['id' => 'second', 'method' => 'GET', 'novalidate' => 'novalidate', 'action' => 'LaporanController@show']) !!}
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Tanggal Mulai</label>
                                        <div class="col-sm-10">
                                            <input type="date" class="form-control" name="startDate" placeholder="Masukkan Tanggal Mulai">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Tanggal Akhir</label>
                                        <div class="col-sm-10">
                                            <input type="date" class="form-control" name="finishDate" placeholder="Masukkan Tanggal Akhir">
                                        </div>
                                    </div>

                                    <div class="form-group row">

                                        <label class="col-sm-2 col-form-label">Pilih Opsi 1</label>
                                        <div class="col-sm-4">
                                            <select class="col-sm-12 js-example-basic-single select2-hidden-accessible" name="type_one" onchange="getOpsi(this.value);" id="merk">
                                                <option value="0" selected="selected">-- Pilih Opsi --</option>
                                                <option value="1">Merk</option>
                                                <option value="2">Kode Part</option>
                                                <option value="3">Customer</option>
                                                <option value="4">Sales</option>
                                                <option value="5">Wilayah</option>
                                            </select>
                                        </div>
                                        
                                        <label class="col-sm-2 col-form-label">Pilih Opsi 2</label>
                                        <div class="col-sm-4">
                                            <select class="col-sm-12 js-example-basic-single select2-hidden-accessible" name="type_two" id="customer" onchange="getOpsi2(this.value);">
                                                <option value="0" selected="selected">-- Pilih Opsi --</option>
                                                <option value="1">Merk</option>
                                                <option value="2">Kode Part</option>
                                                <option value="3">Customer</option>
                                                <option value="4">Sales</option>
                                                <option value="5">Wilayah</option>
                                            </select>
                                        </div>

                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label"></label>
                                        <div class="col-sm-4">
                                            <select class="col-sm-12 form-control" name="opsi_one" id="kodePart">
                                                <option selected>- Pilih -</option>
                                            </select>
                                        </div>

                                        <label class="col-sm-2 col-form-label"></label>
                                        <div class="col-sm-4">
                                            <select class="col-sm-12 form-control" name="opsi_two" id="sales">
                                                <option selected>- Pilih -</option>
                                            </select>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <label class="col-sm-2"></label>
                                        <div class="col-sm-10">
                                            <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Page-body end -->
        </div>
    </div>
    </div>
@stop

@section('script')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>

    <script type="text/javascript" src="{{ asset('bower_components/switchery/js/switchery.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/swithces.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/modalEffects.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/classie.js') }}"></script>

    <script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
    <!-- Multiselect js -->
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).ready(function () {
            $("#kodePart").hide();
            $("#sales").hide();
        });

        function getOpsi(id) {
            if (id == 1) {
                $.ajax({
                  type: 'POST',
                  url: '{{ url('home/report/merk/list') }}',
                  data: {id:id},
                  dataType: 'JSON',
                  success: function(data) {
                    $("#kodePart").show();
                    $("#kodePart").select2();
                    $('#kodePart').html(data.content);
                  }
                });
            } else if(id == 2) {
                $.ajax({
                  type: 'POST',
                  url: '{{ url('home/report/kodePart') }}',
                  data: {id:id},
                  dataType: 'JSON',
                  success: function(data) {
                    $("#kodePart").show();
                    $("#kodePart").select2();
                    $('#kodePart').html(data.content);
                  } 
                });
            } else if(id == 3) {
                $.ajax({
                  type: 'POST',
                  url: '{{ url('home/report/customer') }}',
                  data: {id:id},
                  dataType: 'JSON',
                  success: function(data) {
                    $("#kodePart").show();
                    $("#kodePart").select2();
                    $('#kodePart').html(data.content);
                  } 
                });
            } else if(id == 4) {
                $.ajax({
                  type: 'POST',
                  url: '{{ url('home/report/sales') }}',
                  data: {id:id},
                  dataType: 'JSON',
                  success: function(data) {
                    $("#kodePart").show();
                    $("#kodePart").select2();
                    $('#kodePart').html(data.content);
                  } 
                });
            } else if (id == 5) {
                $.ajax({
                  type: 'POST',
                  url: '{{ url('home/report/kota/list') }}',
                  data: {id:id},
                  dataType: 'JSON',
                  success: function(data) {
                    $("#kodePart").show();
                    $("#kodePart").select2();
                    $('#kodePart').html(data.content);
                  } 
                });
            }
        }

        function getOpsi2(id) {
            if (id == 1) {
                $.ajax({
                  type: 'POST',
                  url: '{{ url('home/report/merk/list') }}',
                  data: {id:id},
                  dataType: 'JSON',
                  success: function(data) {
                    $("#sales").show();
                    $("#sales").select2();
                    $('#sales').html(data.content);
                  }
                });
            } else if(id == 2) {
                $.ajax({
                  type: 'POST',
                  url: '{{ url('home/report/kodePart') }}',
                  data: {id:id},
                  dataType: 'JSON',
                  success: function(data) {
                    $("#sales").show();
                    $("#sales").select2();
                    $('#sales').html(data.content);
                  } 
                });
            } else if(id == 3) {
                $.ajax({
                  type: 'POST',
                  url: '{{ url('home/report/customer') }}',
                  data: {id:id},
                  dataType: 'JSON',
                  success: function(data) {
                    $("#sales").show();
                    $("#sales").select2();
                    $('#sales').html(data.content);
                  } 
                });
            } else if(id == 4) {
                $.ajax({
                  type: 'POST',
                  url: '{{ url('home/report/sales') }}',
                  data: {id:id},
                  dataType: 'JSON',
                  success: function(data) {
                    $("#sales").show();
                    $("#sales").select2();
                    $('#sales').html(data.content);
                  } 
                });
            } else if (id == 5) {
                $.ajax({
                  type: 'POST',
                  url: '{{ url('home/report/kota/list') }}',
                  data: {id:id},
                  dataType: 'JSON',
                  success: function(data) {
                    $("#sales").show();
                    $("#sales").select2();
                    $('#sales').html(data.content);
                  } 
                });
            }
        }
    </script>
@endsection