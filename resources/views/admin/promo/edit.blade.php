@extends('template.index')

@section('title')
Dealer Information System - Edit Promo
@stop

@section('style')

    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}"/>
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/css/buttons.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css') }}">

    <!-- Date-time picker css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/advance-elements/css/bootstrap-datetimepicker.css') }}">
    <!-- Date-range picker css  -->
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/bootstrap-daterangepicker/css/daterangepicker.css') }}" />

@stop

@section('content')
<div class="main-body">
	<div class="page-wrapper">

        <!-- Page-header start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                        <div class="d-inline">
                            <h4>Edit Promo</h4>
                            <span>Edit Promo - {!! $promo->bonus_name !!}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ url('home/main') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Master Data - Promo</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-header end -->

		<div class="page-body">
			<div class="row">
				<div class="col-sm-12">

					<!-- Tooltip Validation card start -->
                    <div class="card">
                        <div class="card-header">
                            <h5>Edit Promo - {!! $promo->bonus_name !!}</h5>
                            <div class="card-header-right">
                            	<i class="icofont icofont-spinner-alt-5"></i>
                            </div>
                        </div>
                        <div class="card-block">
                        	{!! Form::model($promo, ['id' => 'second', 'method' => 'PATCH', 'novalidate' => 'novalidate', 'action' => ['PromoController@update', base64_encode($promo->bonus_id)]]) !!}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Kode Promo</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="usernameP" name="f_kodePromo" placeholder="Masukkan Kode Supplier" value="{!! $promo->bonus_code !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Nama Promo</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="usernameP" name="f_nama" placeholder="Masukkan Nama Lengkap" value="{!! $promo->bonus_name !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Periode</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="datefilter" class="form-control" value="{!! date('m/d/Y', strtotime($promo->bonus_period_first)) !!} - {!! date('m/d/Y', strtotime($promo->bonus_period_last)) !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Status Bonus</label>
                                    <div class="col-sm-10">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="status" value="1" {!! ($promo->bonus_status == 1) ? 'checked' : '' !!}> Aktif
                                            </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="status" value="0" {!! ($promo->bonus_status == 0) ? 'checked' : '' !!}> Tidak Aktif
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-block">
                            <a href="javascript::void(0)" id="addRow" class="btn btn-primary m-b-20">+ Add New</a>

                            <div class="card" id="form-hidden">
                                <div class="card-block">
                                {!! Form::open(['method' => 'POST', 'action' => 'ApiController@addPromoBonus']) !!}
                                    <h4 class="sub-title">Form Detail</h4>
                                        <input type="hidden" name="bonus_id" value="{!! $promo->bonus_id !!}">
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Barang</label>
                                            <div class="col-sm-10">
                                                <select class="col-sm-12" id="select-barang" name="f_barang">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Jumlah Minimum Pembelian</label>
                                            <div class="col-sm-10">
                                                <input type="number" class="form-control" name="min_buy" placeholder="Min 0">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Harga Khusus</label>
                                            <div class="col-sm-10">
                                                <input type="number" class="form-control" name="harga_khusus" placeholder="Min 0">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Bonus Barang</label>
                                            <div class="col-sm-10">
                                                <select class="col-sm-12" id="select-bonus-barang" name="bonus_barang">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Jumlah Bonus Barang</label>
                                            <div class="col-sm-10">
                                                <input type="number" class="form-control" name="bonus_barang_qty" placeholder="Min 0">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <label class="col-sm-2"></label>
                                            <div class="col-sm-10">
                                                <button type="reset" class="btn btn-danger m-b-0">Reset</button>
                                                <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                            </div>
                                        </div>
                                {!! Form::close() !!}
                                </div>
                            </div>

                            <div class="dt-responsive table-responsive">
                                <table id="basic-btn" class="table table-striped table-bordered nowrap">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Barang</th>
                                            <th>Jumlah Minimum Pembelian</th>
                                            <th>Harga Khusus</th>
                                            <th>Bonus Barang</th>
                                            <th>Jumlah Bonus Barang</th>
                                            <th>Config</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($barang as $val => $keys)
                                        <tr>
                                            <td>{!! ++$val !!}</td>
                                            <td>{!! $keys->items->item_name !!}</td>
                                            <td>{!! $keys->bonus_detail_min !!}</td>
                                            <td>{!! $keys->bonus_detail_price !!}</td>
                                            <td>{!! $keys->barang->item_name !!}</td>
                                            <td>{!! $keys->bonus_detail_qty_item !!}</td>
                                            <td>
                                                <button type="button" class="btn btn-danger btn-outline-info" data-toggle="modal" data-target="#delete-target{{{ $keys->bonus_detail_id }}}"><i class="icofont icofont-delete-alt"></i></button>
                                            </td>
                                        </tr>

                                        <div class="modal fade" id="delete-target{{{ $keys->bonus_detail_id }}}" tabindex="-1" role="dialog">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header bg-danger">
                                                        <h4 class="modal-title">Hapus Data</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Apakah Anda Yakin Akan Menghapus <b>{!! $keys->items->item_name !!}</b> ?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        {!! Form::open(['method' => 'POST', 'action' => 'ApiController@deleteBonusDetail']) !!}
                                                            <input type="hidden" name="id" value="{!! base64_encode($keys->bonus_detail_id) !!}">
                                                            {!! Form::submit("Ya", array('class' => 'btn btn-primary waves-effect waves-light')) !!}
                                                        {!! Form::close() !!}
                                                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Barang</th>
                                            <th>Jumlah Minimum Pembelian</th>
                                            <th>Harga Khusus</th>
                                            <th>Bonus Barang</th>
                                            <th>Jumlah Bonus Barang</th>
                                            <th>Config</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>

                    </div>

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('script')
<!-- Select 2 js -->
<script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
<!-- Multiselect js -->
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>

<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/js/jszip.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/js/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/js/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/jszip.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>

<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/pages/advance-elements/moment-with-locales.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/pages/advance-elements/bootstrap-datetimepicker.min.js') }}"></script>
<!-- Date-range picker js -->
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-daterangepicker/js/daterangepicker.js') }}"></script>

<script type="text/javascript" src="{{ asset('bower_components/datedropper/js/datedropper.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/pages/advance-elements/custom-picker.js') }}"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function formatRepo(data) {
        var markup = "<option value='"+data.id+"'>"+data.text+"</option>";

        return markup;
    }

    function formatRepoSelection(data) {
        return data.text;
    }

    $(document).ready(function () {
        $("#form-hidden").hide();
        $("#addRow").on('click', function () {
            $("#form-hidden").show();
        });

        $("#select-barang").select2({
            ajax: {
                url: "{{ url('home/ajax/items') }}",
                dataType: 'JSON',
                delay: 100,
                data: function(params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function(data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function(markup) {
                return markup;
            }, // let our custom formatter work
            placeholder: 'Pilih Barang',
            minimumInputLength: 1,
            templateResult: formatRepo, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });

        $("#select-bonus-barang").select2({
            ajax: {
                url: "{{ url('home/ajax/items') }}",
                dataType: 'JSON',
                delay: 100,
                data: function(params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function(data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function(markup) {
                return markup;
            }, // let our custom formatter work
            placeholder: 'Pilih Barang',
            minimumInputLength: 1,
            templateResult: formatRepo, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });
    });
</script>
@stop