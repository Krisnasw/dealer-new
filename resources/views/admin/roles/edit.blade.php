@extends('template.index')

@section('title')
    Dealer Information System - Edit Type User
@stop

@section('style')

    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}"/>
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}"/>

@stop

@section('content')
    <div class="main-body">
        <div class="page-wrapper">

            <!-- Page-header start -->
            <div class="page-header card">
                <div class="row align-items-end">
                    <div class="col-lg-8">
                        <div class="page-header-title">
                            <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                            <div class="d-inline">
                                <h4>Edit Type User</h4>
                                <span>Edit Type User - {!! $roles->user_type_name !!}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="page-header-breadcrumb">
                            <ul class="breadcrumb-title">
                                <li class="breadcrumb-item">
                                    <a href="{{ url('home/main') }}">
                                        <i class="icofont icofont-home"></i>
                                    </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">Setup Data - Type User</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Page-header end -->

            <div class="page-body">
                <div class="row">
                    <div class="col-sm-12">

                        <!-- Tooltip Validation card start -->
                        <div class="card">
                            <div class="card-header">
                                <h5>Edit Type User - {!! $roles->user_type_name !!}</h5>
                                <div class="card-header-right">
                                    <i class="icofont icofont-spinner-alt-5"></i>
                                </div>
                            </div>
                            <div class="card-block">
                                {!! Form::model($roles, ['id' => 'second', 'method' => 'PATCH', 'novalidate' => 'novalidate', 'action' => ['RolesController@update', base64_encode($roles->user_type_id)]]) !!}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Nama Type User</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama Type User" value="{!! $roles->user_type_name !!}">
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-lg-12 col-xl-12">
                                        <div class="sub-title">MENU <span class="pull-right" style="margin-right: 17em;">C R U D</span> </div>
                                        @foreach($menu as $key)
                                            <label class="control-label col-md-9 col-sm-9 col-xs-9" for="name" style="text-align:left;">{!! $key->side_menu_name !!}</label>
                                            @foreach($side as $datas)
                                                @if($datas->side_menu_parent == $key->side_menu_id)
                                                    <label class="control-label col-md-9 col-sm-9 col-xs-9" for="name" style="text-align:left; padding-left:5em;">{!! $datas->side_menu_name !!}</label>
                                                    <div class="col-md-3 col-sm-3 col-xs-3 pull-right">
                                                        <div class="col-sm-9 checkbox">
                                                            <label class="cb-checkbox cb-checkbox-dark-2">
                                                                    <input name="permit{!! $datas->side_menu_id !!}[]" type="checkbox" value="c" {!! (strpos($datas->permit_acces, 'c') !== false) ? 'checked' : '' !!}>
                                                            </label>
                                                            <label class="cb-checkbox cb-checkbox-dark-2">
                                                                    <input name="permit{!! $datas->side_menu_id !!}[]" type="checkbox" value="r" {!! (strpos($datas->permit_acces, 'r') !== false) ? 'checked' : '' !!}>
                                                            </label>
                                                            <label class="cb-checkbox cb-checkbox-dark-2">
                                                                    <input name="permit{!! $datas->side_menu_id !!}[]" type="checkbox" value="u" {!! (strpos($datas->permit_acces, 'u') !== false) ? 'checked' : '' !!}>
                                                            </label>
                                                            <label class="cb-checkbox cb-checkbox-dark-2">
                                                                    <input name="permit{!! $datas->side_menu_id !!}[]" type="checkbox" value="d" {!! (strpos($datas->permit_acces, 'd') !== false) ? 'checked' : '' !!}>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    @foreach($child as $val)
                                                        @if($val->side_menu_parent == $datas->side_menu_id)
                                                            <label class="control-label col-md-9 col-sm-9 col-xs-9" for="name" style="text-align:left; padding-left:10em;">{!! $val->side_menu_name !!}</label>
                                                            <div class="col-md-3 col-sm-3 col-xs-3 pull-right">
                                                                <div class="col-sm-9 checkbox">
                                                                    <label class="cb-checkbox cb-checkbox-dark-2">
                                                                        <input name="permit{!! $val->side_menu_id !!}[]" type="checkbox" value="c" {!! (strpos($val->permit_acces, 'c') !== false) ? 'checked' : '' !!}>
                                                                    </label>
                                                                    <label class="cb-checkbox cb-checkbox-dark-2">
                                                                        <input name="permit{!! $val->side_menu_id !!}[]" type="checkbox" value="r" {!! (strpos($val->permit_acces, 'r') !== false) ? 'checked' : '' !!}>
                                                                    </label>
                                                                    <label class="cb-checkbox cb-checkbox-dark-2">
                                                                        <input name="permit{!! $val->side_menu_id !!}[]" type="checkbox" value="u" {!! (strpos($val->permit_acces, 'u') !== false) ? 'checked' : '' !!}>
                                                                    </label>
                                                                    <label class="cb-checkbox cb-checkbox-dark-2">
                                                                        <input name="permit{!! $val->side_menu_id !!}[]" type="checkbox" value="d" {!! (strpos($val->permit_acces, 'd') !== false) ? 'checked' : '' !!}>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        @else
                                                        @endif
                                                    @endforeach
                                                @else
                                                @endif
                                            @endforeach
                                        @endforeach
                                    </div>

                                </div>

                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <!-- Select 2 js -->
    <script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
    <!-- Multiselect js -->
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>

    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js') }}"></script>
@stop