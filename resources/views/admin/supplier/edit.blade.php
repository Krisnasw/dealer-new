@extends('template.index')

@section('title')
Dealer Information System - Buat Supplier Baru
@stop

@section('style')

    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}"/>
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/css/buttons.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css') }}">

@stop

@section('subheader')
Edit Supplier - {!! $supp->supplier_name !!}
@stop

@section('content')
<div class="main-body">
	<div class="page-wrapper">

        <!-- Page-header start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                        <div class="d-inline">
                            <h4>Edit Supplier - {!! $supp->supplier_name !!}</h4>
                            <span>Edit Supplier</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ url('home/main') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Master Data - Supplier</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-header end -->

		<div class="page-body">
			<div class="row">
				<div class="col-sm-12">

					<!-- Tooltip Validation card start -->
                    <div class="card">
                        <div class="card-header">
                            <h5>Edit Supplier - {!! $supp->supplier_name !!}</h5>
                            <div class="card-header-right">
                            	<i class="icofont icofont-spinner-alt-5"></i>
                            </div>
                        </div>
                        <div class="card-block">
                        	{!! Form::model($supp, ['id' => 'second', 'method' => 'PATCH', 'novalidate' => 'novalidate', 'action' => ['SupplierController@update', base64_encode($supp['supplier_id'])]]) !!}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Kode Supplier</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="usernameP" name="f_kodeSupplier" placeholder="Masukkan Kode Supplier" value="{!! $supp->supplier_code !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Nama</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="usernameP" name="f_nama" placeholder="Masukkan Nama Lengkap" value="{!! $supp->supplier_name !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Alamat</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="usernameP" name="f_alamat" placeholder="Masukkan Alamat Lengkap" value="{!! $supp->supplier_addres !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Telepon</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="usernameP" name="f_telepon" placeholder="Masukkan No. Telepon" value="{!! $supp->supplier_phone !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Target Januari</label>
                                    <div class="col-sm-4">
                                        <input type="number" class="form-control" name="f_januari" placeholder="Masukkan Target Bulan Januari" value="{!! $supp->supplier_target_1 !!}">
                                    </div>
                                    <label class="col-sm-2 col-form-label">Target Juli</label>
                                    <div class="col-sm-4">
                                    	<input type="number" class="form-control" name="f_juli" placeholder="Masukkan Target Bulan Juli" value="{!! $supp->supplier_target_7 !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Target February</label>
                                    <div class="col-sm-4">
                                        <input type="number" class="form-control" name="f_february" placeholder="Masukkan Target Bulan February" value="{!! $supp->supplier_target_2 !!}">
                                    </div>
                                    <label class="col-sm-2 col-form-label">Target Agustus</label>
                                    <div class="col-sm-4">
                                        <input type="number" class="form-control" name="f_agustus" placeholder="Masukkan Target Bulan Agustus" value="{!! $supp->supplier_target_8 !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Target Maret</label>
                                    <div class="col-sm-4">
                                        <input type="number" class="form-control" name="f_maret" placeholder="Masukkan Target Bulan Maret" value="{!! $supp->supplier_target_3 !!}">
                                    </div>
                                    <label class="col-sm-2 col-form-label">Target September</label>
                                    <div class="col-sm-4">
                                        <input type="number" class="form-control" name="f_september" placeholder="Masukkan Target Bulan September" value="{!! $supp->supplier_target_9 !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Target April</label>
                                    <div class="col-sm-4">
                                        <input type="number" class="form-control" name="f_april" placeholder="Masukkan Target Bulan April" value="{!! $supp->supplier_target_4 !!}">
                                    </div>
                                    <label class="col-sm-2 col-form-label">Target Oktober</label>
                                    <div class="col-sm-4">
                                        <input type="number" class="form-control" name="f_oktober" placeholder="Masukkan Target Bulan Oktober" value="{!! $supp->supplier_target_10 !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Target Mei</label>
                                    <div class="col-sm-4">
                                        <input type="number" class="form-control" name="f_mei" placeholder="Masukkan Target Bulan Mei" value="{!! $supp->supplier_target_5 !!}">
                                    </div>
                                    <label class="col-sm-2 col-form-label">Target November</label>
                                    <div class="col-sm-4">
                                        <input type="number" class="form-control" name="f_november" placeholder="Masukkan Target Bulan November" value="{!! $supp->supplier_target_11 !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Target Juni</label>
                                    <div class="col-sm-4">
                                        <input type="number" class="form-control" name="f_juni" placeholder="Masukkan Target Bulan Juni" value="{!! $supp->supplier_target_6 !!}">
                                    </div>
                                    <label class="col-sm-2 col-form-label">Target Desember</label>
                                    <div class="col-sm-4">
                                        <input type="number" class="form-control" name="f_desember" placeholder="Masukkan Target Bulan Desember" value="{!! $supp->supplier_target_12 !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Merk</label>
                                    <div class="col-sm-10">
                                        <select class="form-control col-sm-12" name="merk">
                                            @foreach($brand as $key)
                                            <option value="{!! $key->brand_id !!}">{!! $key->brand_name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('script')
<!-- Select 2 js -->
<script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
<!-- Multiselect js -->
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>

<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/js/jszip.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/js/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/js/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/jszip.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>

<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js') }}"></script>
@stop