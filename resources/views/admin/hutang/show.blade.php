@extends('template.index')

@section('title')
Laporan - Hutang
@stop

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/switchery/css/switchery.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/component.css') }}">
@stop

@section('content')
    <!-- Main-body start -->
<div class="main-body">
    <div class="page-wrapper">
        <!-- Page-header start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                        <div class="d-inline">
                            <h4>Data Hutang</h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ url('home/main') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Laporan - Hutang</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-header end -->

        <!-- Page-body start -->
        <div class="page-body">
            <div class="row">
                <div class="col-sm-12">

                    <!-- HTML5 Export Buttons table start -->
                    <div class="card">
                        <div class="card-header table-card-header">
                            <h5>Data Hutang Periode : {!! date('M', strtotime($month)) !!} {!! $year !!} - Supplier : {!! $sup['supplier_name'] !!} </h5>
                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table class="table table-striped table-bordered nowrap">
                                    <thead>
                                        <tr>
                                            <th align="center">Tanggal</th>
                                            <th align="center">Uraian</th>
                                            <th align="center">No. Faktur Pajak</th>
                                            <th align="center">No. Invoice</th>
                                            <th align="center">Debet</th>
                                            <th align="center">Kredit</th>
                                            <th align="center">Saldo</th>
                                            <th align="center">Keterangan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($nota as $index => $row)
                                        <tr align="center">
                                            <td align="center">{!! $row['debt_date'] !!}</td>
                                            <td align="center">{!! $row['receipt_code'] !!}</td>
                                            <td align="center">-</td>
                                            <td align="center">-</td>
                                            <td align="center">{!! ($row['debt_type'] == "Pembayaran") ? number_format($row['debt_nominal']) : '0' !!}</td>
                                            <td align="center">{!! ($row['debt_type'] == "Pembayaran") ? '0' : number_format($row['debt_nominal']) !!}</td>
                                            <td align="center">-</td>
                                            <td align="center">-</td>
                                        </tr>
                                    @endforeach
		                            </tbody>
                                    <tfoot>
                                        <tr>
                                            <th align="center">Tanggal</th>
                                            <th align="center">Uraian</th>
                                            <th align="center">No. Faktur Pajak</th>
                                            <th align="center">No. Invoice</th>
                                            <th align="center">Debet</th>
                                            <th align="center">Kredit</th>
                                            <th align="center">Saldo</th>
                                            <th align="center">Keterangan</th>
                                        </tr>
                                    </tfoot>
	                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-body end -->
	    </div>
	</div>
</div>
@stop

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>

    <script type="text/javascript" src="{{ asset('bower_components/switchery/js/switchery.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/swithces.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/modalEffects.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/classie.js') }}"></script>
@endsection