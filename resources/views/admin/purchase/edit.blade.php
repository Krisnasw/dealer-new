@extends('template.index')

@section('title')
Dealer Information System - Edit Purchasing
@stop

@section('style')

    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}"/>
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}"/>

@stop

@section('content')
<div class="main-body">
	<div class="page-wrapper">

        <!-- Page-header start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                        <div class="d-inline">
                            <h4>Edit Purchasing</h4>
                            <span>Edit Purchasing - {!! $data['purchase_code'] !!}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ url('home/main') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Pembelian - Purchasing</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-header end -->

		<div class="page-body">
			<div class="row">
				<div class="col-sm-12">

					<!-- Tooltip Validation card start -->
                    <div class="card">
                        <div class="card-header">
                            <h5>Edit Purchasing - {!! $data['purchase_code'] !!}</h5>
                            <div class="card-header-right">
                            	<i class="icofont icofont-spinner-alt-5"></i>
                            </div>
                        </div>
                        <div class="card-block">
                        	{!! Form::model($data, ['id' => 'second', 'method' => 'PATCH', 'novalidate' => 'novalidate', 'action' => ['PurchaseController@update', base64_encode($data['purchase_id'])]]) !!}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Merk</label>
                                    <div class="col-sm-10">
                                        <select class="form-control col-sm-12 js-example-basic-single" name="merk" onchange="get_detail(this.value);">
                                            <option value="0">-- Pilih Merk --</option>
                                            @foreach($brand as $row)
                                            <option value="{!! $row['brand_id'] !!}" {!! ($data['brand_id'] == $row['brand_id']) ? 'selected' : '' !!}>{!! $row['brand_name'] !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Supplier</label>
                                    <div class="col-sm-10">
                                        <select class="form-control col-sm-12 js-example-basic-single" name="supplier">
                                            <option value="0" selected>-- Pilih Supplier --</option>
                                            @foreach($supplier as $row)
                                            <option value="{!! $row['supplier_id'] !!}" {!! ($data['supplier_id'] == $row['supplier_id']) ? 'selected' : '' !!}>{!! $row['supplier_name'] !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Jenis Pembelian</label>
                                    <div class="col-sm-10">
                                        <div class="form-radio">
                                            <div class="radio radiofill radio-primary radio-inline">
                                                <label>
                                                    <input type="radio" name="jenis" value="F" {!! ($data['purchase_type'] == 'F') ? 'checked' : '' !!}>
                                                    <i class="helper"></i>Fix
                                                </label>
                                            </div>
                                            <div class="radio radiofill radio-primary radio-inline">
                                                <label>
                                                    <input type="radio" name="jenis" value="A" {!! ($data['purchase_type'] == 'A') ? 'checked' : '' !!}>
                                                    <i class="helper"></i>Additional
                                                </label>
                                            </div>
                                            <div class="radio radiofill radio-primary radio-inline">
                                                <label>
                                                    <input type="radio" name="jenis" value="E" {!! ($data['purchase_type'] == 'E') ? 'checked' : '' !!}>
                                                    <i class="helper"></i> Emergency
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @include('admin.purchase.list_detail_edit')

                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="button" class="btn btn-primary m-b-0" onclick="action_save();">Submit</button>
                                        <button type="button" class="btn btn-danger m-b-0" onclick="history.go(-1);">Kembali</button>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('script')
<!-- Select 2 js -->
<script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
<!-- Multiselect js -->
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function get_detail(id) {
        $.post('{{ url('home/pembelian/purchase/get-brand-detail') }}', {
            id : id
        }, function (data) {
            $('#merkid').html(data.content);
        });

        $.post('{{ url('home/pembelian/purchase/get-item-name') }}', {
            id : id
        }, function (data) {
            $('#select-barang').html(data.content);
        });

        $.post('{{ url('home/pembelian/purchase/get-item-code') }}', {
            id : id
        }, function (data) {
            $('#numberPart').html(data.content);
        });
    }

    function get_item_id(id) {
        var i_item_part      = $('select[name="numberPart"]').val();
        var i_item_name      = $('select[name="namaBarang"]').val();
        if (id == 1) {
            $('select[name="namaBarang"]').val(i_item_part);
        } else {
            $('select[name="numberPart"]').val(i_item_name);
        }
    }

    function save_tmp() {
        var i_item_part         = $('select[name="numberPart"]').val();
        var i_detail_diskon     = $('input[name="i_detail_diskon"]').val();
        var i_qty_detail        = $('input[name="i_qty_detail"]').val();

        $.post('{{ url('home/pembelian/purchase/save-detail') }}', {
            numberPart : i_item_part,
            diskon : i_detail_diskon,
            qty : i_qty_detail,
            id : {!! $data['purchase_id'] !!}
        }, function (data) {
            if (data.status == 'success') {
                swal('Successfully Added', data.message, 'success');
                window.location.reload();
            } else {
                swal('Oops!', data.message, 'error');
            }
        });
    }

    function action_save() {
        $.ajax({
            type: 'POST',
            url : $("#second").attr('action'),
            data : $("#second").serialize(),
            dataType : 'JSON',
            success:function (data) {
                if (data.status == 'success') {
                    swal('Successfully', data.message, 'success');
                    window.location.reload();
                } else {
                    swal('Oops!', data.message, 'error');
                }
            }, error:function () {
                swal('Oops!', 'Terjadi Kesalahan', 'error');
            }
        });
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#numberPart").focus();
        $("#numberPart").focusout(function () {
            $("#select-barang").focus();
        });
        $("#select-barang").focusout(function () {
            $("#i_qty_detail").focus();
        });
        $("#i_qty_detail").focusout(function () {
            $("#i_detail_diskon").focus();
        });
    });
</script>
@stop