<div class="card">
    <div class="card-header table-card-header">
        <h5>Filter - Historis Penjualan</h5>
    </div>
    <div class="card-block">
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Kode Part</label>
            <div class="col-sm-8">
                <select class="form-control col-sm-12 js-example-basic-single" name="merk_det" id="merkid">
                    <option value="0">-- Pilih Kode Part --</option>
                    @foreach($brand_detail as $row)
                    <option value="{!! $row['brand_detail_id'] !!}">{!! $row['brand_detail_name'] !!}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-sm-2">
                <button type="button" class="btn btn-outline-primary m-b-0" onclick="showFilterable();"><i class="icofont icofont-search"></i>Cari Barang</button>
            </div>
        </div>
        <div class="dt-responsive table-responsive">
            <table id="basic-btn" class="table table-striped table-bordered nowrap">
                <thead>
                    <tr>
                        <th>Order</th>
                        <th>Number Part</th>
                        <th>Nama Barang</th>
                        <th>Stok</th>
                        <th>OC</th>
                        <th>OS</th>
                        <th>
                            @php
                                $selisih = 1;
                                for($i = 0; $i <= $selisih; $i++) {
                                    $x = mktime (0 ,0 ,0 ,date("m") -$i, date("d") , date("y"));
                                    $bln= date("m", $x);
                                    $thn= date("y", $x);
                                    switch($bln) {
                                        case 1 : $bln='Jan';   break;
                                        case 2 : $bln='Feb';   break;
                                        case 3 : $bln='Mar';      break;
                                        case 4 : $bln='Apr';      break;
                                        case 5 : $bln='Mei';        break;
                                        case 6 : $bln="Jun";       break;
                                        case 7 : $bln='Jul';       break;
                                        case 8 : $bln='Agu';    break;
                                        case 9 : $bln='Sep';  break;
                                        case 10 : $bln='Okt';   break;     
                                        case 11 : $bln='Nov';  break;
                                        case 12 : $bln='Des';  break;
                                    }
                                }
                            @endphp

                            {!! $bln.' '.$thn !!}
                        </th>
                    </tr>
                </thead>
                <tbody id="tbody-isi">
                </tbody>
            </table>

        </div>
    </div>
</div>