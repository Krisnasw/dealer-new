@extends('template.index')

@section('title')
    Dealer Information System - Buat Memo Baru
@stop

@section('style')
    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}"/>
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}"/>
@stop

@section('content')
    <div class="main-body">
        <div class="page-wrapper">

            <!-- Page-header start -->
            <div class="page-header card">
                <div class="row align-items-end">
                    <div class="col-lg-8">
                        <div class="page-header-title">
                            <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                            <div class="d-inline">
                                <h4>Buat Memo</h4>
                                <span>Buat Memo Baru</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="page-header-breadcrumb">
                            <ul class="breadcrumb-title">
                                <li class="breadcrumb-item">
                                    <a href="{{ url('home/main') }}">
                                        <i class="icofont icofont-home"></i>
                                    </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">Penjualan - Memo</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Page-header end -->

            <div class="page-body">
                <div class="row">
                    <div class="col-sm-12">

                        <!-- Tooltip Validation card start -->
                        <div class="card">
                            <div class="card-header">
                                <h5>Buat Memo Baru</h5>
                                <div class="card-header-right">
                                    <i class="icofont icofont-spinner-alt-5"></i>
                                </div>
                            </div>
                            <div class="card-block">
                            {!! Form::open(['id' => 'second', 'method' => 'POST', 'novalidate' => 'novalidate', 'action' => 'ProsesController@store']) !!}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Pilih Merk</label>
                                    <div class="col-sm-4">
                                        <select class="col-sm-12 js-example-basic-single select2-hidden-accessible" name="merk">
                                            <option value="0" selected>- Pilih Merk -</option>
                                            @foreach($brand as $key)
                                                <option value="{!! $key->brand_id !!}">{!! $key->brand_name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <label class="col-sm-2 col-form-label">Pilih Customer</label>
                                    <div class="col-sm-4">
                                        <select class="col-sm-12 js-example-basic-single select2-hidden-accessible" name="customer">
                                            <option value="0" selected>- Pilih Customer -</option>
                                            @foreach($cust as $key)
                                                <option value="{!! $key->customer_id !!}">{!! $key->customer_name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Pilih Sales</label>
                                    <div class="col-sm-4">
                                        <select class="col-sm-12 js-example-basic-single select2-hidden-accessible" name="sales">
                                            <option value="0" selected>- Pilih Sales -</option>
                                            @foreach($sales as $key)
                                                <option value="{!! $key->sales_id !!}">{!! $key->sales_name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary m-b-0" id="confirm_save">Submit</button>
                                        <button type="reset" class="btn btn-danger m-b-0" onclick="window.history.go(-1); return false;">Cancel</button>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-block">

                                <div class="dt-responsive table-responsive">
                                    <table id="basic" class="table table-striped table-bordered nowrap">
                                        <thead>
                                        <tr>
                                            <td>
                                                <select class="col-sm-12" id="select-barang" name="f_barang"></select>
                                            </td>
                                            <td>
                                                <input type="number" class="form-control" id="qty" name="qty" min="0" style="width: 250px;" onkeydown="if (event.keyCode == 13) { add_detail(); }">
                                            </td>
                                            <td></td>
                                            <td>
                                                <input type="number" class="form-control" id="disk" name="diskon" min="0" style="width: 250px;" onkeydown="if (event.keyCode == 13) { add_detail(); }" value="15">
                                            </td>
                                            <td>
                                                <input type="text" name="i_help_text" style="border: none;background: transparent;width: 1%">
                                                <input type="hidden" name="type" value="create">
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Nama Barang</th>
                                            <th>Quantity</th>
                                            <th>Akumulasi Nota</th>
                                            <th>Diskon</th>
                                            <th>Config</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tb-isi">
                                            @foreach($memoDet as $val => $keys)
                                            <tr>
                                                <td>{!! (empty($keys->item->item_name) ? 'Tidak Ditemukan' : $keys->item->item_name) !!}</td>
                                                <td>{!! $keys->memo_detail_qty !!}</td>
                                                <td>{!! $keys->memo_accumulation !!}</td>
                                                <td>{!! $keys->memo_detail_discount !!}</td>
                                                <td>
                                                <button type="button" class="btn btn-danger btn-outline-info" data-toggle="modal" data-target="#delete-target{{{ $keys->memo_detail_id }}}"><i class="icofont icofont-delete-alt"></i></button>
                                                </td>
                                            </tr>

                                            <div class="modal fade" id="delete-target{{{ $keys->memo_detail_id }}}" tabindex="-1" role="dialog">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header bg-danger">
                                                            <h4 class="modal-title">Hapus Data</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>Apakah Anda Yakin Akan Menghapus <b>{!! $keys->item_name !!}</b> ?</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            {!! Form::open(['method' => 'POST', 'action' => 'ApiController@deleteMemoDetail']) !!}
                                                                <input type="hidden" name="id" value="{!! base64_encode($keys->memo_detail_id) !!}">
                                                                {!! Form::submit("Ya", array('class' => 'btn btn-primary waves-effect waves-light')) !!}
                                                            {!! Form::close() !!}
                                                            <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>Nama Barang</th>
                                            <th>Quantity</th>
                                            <th>Akumulasi Nota</th>
                                            <th>Diskon</th>
                                            <th>Config</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <!-- Select 2 js -->
    <script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
    <!-- Multiselect js -->
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function formatRepo(data) {
            var markup = "<option value='"+data.id+"'>"+data.text+"</option>";

            return markup;
        }

        function formatRepoSelection(data) {
            return data.text;
        }

        $(document).ready(function () {
            $("#select-barang").focus();
            $("#select-barang").focusout(function () {
                $("#qty").focus();
            });
            $("#select-barang").select2({
                ajax: {
                    url: "{{ url('home/ajax/items') }}",
                    dataType: 'JSON',
                    delay: 100,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;

                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }, // let our custom formatter work
                placeholder: 'Pilih Barang',
                minimumInputLength: 3,
                templateResult: formatRepo, // omitted for brevity, see the source of this page
                templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
            });

            // $("#basic").DataTable();
        });

        function save_detail() {
            document.getElementById("confirm_save").click();
        }

        function add_detail() {
            var barang = $("#select-barang").val();
            var qty = $("#qty").val();
            var disk = $("#disk").val();
            $.ajax({
              type : "POST",
              url  : '{{ url('home/ajax/memo-detail/add') }}',
              data : { f_barang : barang, qty : qty, diskon : disk, type : 'create' },
              dataType : "JSON",
              success:function(data) {
                if (data.status == 'success') {
                    swal(data.message);
                    $.ajax({
                        url: '{{ url('home/ajax/memo-detail/create') }}',
                        dataType: 'JSON',
                        success: function (data) {
                            $("table #tb-isi").empty();
                            var html = '';
                            $.each(data, function (key, value) {
                                
                            $("table #tb-isi").append("<tr><td>" + data[key].item_name + "</td>" +
                                            "<td>" + data[key].memo_detail_qty + "</td>" +
                                            "<td>" + data[key].memo_accumulation + "</td>" +
                                            "<td>" + data[key].memo_detail_discount + "</td>" +
                                            "<td><button type='button' class='btn btn-danger btn-outline-info' data-toggle='modal' data-target='#delete-target"+data[key].memo_detail_id+"'><i class='icofont icofont-delete-alt'></i></button></td></tr>");
                            });
                        },
                        error: function (data) {
                        }
                    });
                } else {
                    swal(data.message);
                }
               // search_data_detail();

               $('input[name="i_help_text"]').focus();
              }
            });
        }

        // function search_data_detail() { 
        //     var row_id        = 0;
            
        // }
    </script>
@stop