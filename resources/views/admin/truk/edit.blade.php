@extends('template.index')

@section('title')
Dealer Information System - Edit Truck
@stop

@section('style')

    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}"/>
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}"/>

    <link href="{{ asset('assets/pages/jquery.filer/css/jquery.filer.css') }}" type="text/css" rel="stylesheet" />
    <link href="{{ asset('assets/pages/jquery.filer/css/themes/jquery.filer-dragdropbox-theme.css') }}" type="text/css" rel="stylesheet" />

    <!-- lightbox Fremwork -->
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/lightbox2/css/lightbox.min.css') }}">

@stop

@section('content')
<div class="main-body">
	<div class="page-wrapper">

        <!-- Page-header start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                        <div class="d-inline">
                            <h4>Edit Truck</h4>
                            <span>Edit Truck - {!! $truck->truck_nopol !!}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ url('home/main') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Master Data - Truck</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-header end -->

		<div class="page-body">
			<div class="row">
				<div class="col-sm-12">

					<!-- Tooltip Validation card start -->
                    <div class="card">
                        <div class="card-header">
                            <h5>Edit Truck - {!! $truck->truck_nopol !!}</h5>
                            <div class="card-header-right">
                            	<i class="icofont icofont-spinner-alt-5"></i>
                            </div>
                        </div>
                        <div class="card-block">
                        	{!! Form::model($truck, ['id' => 'second', 'method' => 'PATCH', 'novalidate' => 'novalidate', 'action' => ['TruckController@update', base64_encode($truck->truck_id)], 'files' => true]) !!}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Nopol</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="nopol" placeholder="Masukkan No. Polisi" value="{!! $truck->truck_nopol !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Tanggal Kir</label>
                                    <div class="col-sm-10">
                                        <input type="date" class="form-control" name="kir" placeholder="Masukkan Tanggal Kir" value="{!! $truck->truck_tanggal_kir !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Tanggal Pajak</label>
                                    <div class="col-sm-10">
                                        <input type="date" class="form-control" name="pajak" placeholder="Masukkan Tanggal Pajak" value="{!! $truck->truck_tanggal_pajak !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Merk</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="merk" placeholder="Masukkan Merk" value="{!! $truck->truck_merk !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Tahun Buat</label>
                                    <div class="col-sm-10">
                                        <input type="number" min="1980" class="form-control" name="tahun" placeholder="Masukkan Tahun Buat" value="{!! $truck->truck_tahun_buat !!}">
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                    <!-- Image grid card start -->
                    <div class="card">
                        <div class="card-header">
                            <h5>List Scan Truck</h5>
                            <button class="btn btn-info m-b-0" type="button" data-toggle="modal" data-target="#upload">Upload Scan</button>
                        </div>
                        <div class="card-block">
                            <div class="row">
                                @if(count($gal) <= 0)
                                <div class="col-sm-12">
                                    <center>Tidak Ada Data</center>
                                </div>
                                @else
                                    @foreach($gal as $key => $val)
                                    <div class="col-lg-4 col-sm-6">
                                        <div class="thumbnail">
                                            <div class="thumb">
                                                <a href="{{ asset($val->truck_scan_img) }}" data-lightbox="1">
                                                    <img src="{{ asset($val->truck_scan_img) }}" alt="" class="img-fluid img-thumbnail">
                                                </a>
                                            </div>
                                        </div>

                                        <button type="button" data-toggle="modal" data-target="#delete-gal{!! $val->truck_scan_id !!}" class="btn btn-danger m-b-0">Delete</button>

                                        <div class="modal fade md-effect-1" id="delete-gal{!! $val->truck_scan_id !!}" tabindex="-1" role="dialog">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header bg-info">
                                                        <h5 class="modal-title">Hapus Gallery</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                {!! Form::open(['method' => 'POST', 'action' => 'ApiController@deleteTruckScan']) !!}
                                                    <div class="modal-body">
                                                        <!-- Basic Form Inputs card start -->
                                                        <input type="hidden" name="id" value="{!! $val->truck_scan_id !!}">
                                                        Apakah Anda Yakin Menghapus Gambar Ini ?
                                                        <!-- Basic Form Inputs card end -->
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary waves-effect waves-light" id="submits">Ya</button>
                                                    </div>
                                                {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- Image grid card end -->

                    <div class="modal fade md-effect-1" id="upload" tabindex="-1" role="dialog">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header bg-info">
                                    <h5 class="modal-title">Upload Gallery</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            {!! Form::open(['method' => 'POST', 'action' => 'ApiController@uploadTruckScan', 'files' => true]) !!}
                                <div class="modal-body">
                                    <!-- Basic Form Inputs card start -->
                                    <div class="card">
                                        <div class="card-block">
                                            <h4 class="sub-title">Form Gallery</h4>
                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label">Foto</label>
                                                    <div class="col-sm-10">
                                                        <input type="hidden" name="id" value="{!! $truck->truck_id !!}">
                                                        <input type="file" class="form-control" placeholder="Pilih Foto" name="images">
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                    <!-- Basic Form Inputs card end -->
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light" id="submits">Save changes</button>
                                </div>
                            {!! Form::close() !!}
                            </div>
                        </div>
                    </div>

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('script')
<!-- Select 2 js -->
<script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
<!-- Multiselect js -->
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>

<script src="{{ asset('assets/pages/jquery.filer/js/jquery.filer.min.js') }}"></script>
<script src="{{ asset('assets/pages/filer/custom-filer.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/filer/jquery.fileuploads.init.js') }}" type="text/javascript"></script>

<script type="text/javascript" src="{{ asset('bower_components/lightbox2/js/lightbox.min.js') }}"></script>
<script>
    lightbox.option({
        'resizeDuration': 200,
        'wrapAround': true
    })

</script>
@stop