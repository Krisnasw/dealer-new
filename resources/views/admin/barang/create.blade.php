@extends('template.index')

@section('title')
Dealer Information System - Buat Barang Baru
@stop

@section('style')

    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}"/>
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}"/>

@stop

@section('subheader')
Buat Barang Baru
@stop

@section('content')
<div class="main-body">
	<div class="page-wrapper">

        <!-- Page-header start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                        <div class="d-inline">
                            <h4>Buat Barang</h4>
                            <span>Buat Barang Baru</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ url('home/main') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Master Data - Barang</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-header end -->

		<div class="page-body">
			<div class="row">
				<div class="col-sm-12">

					<!-- Tooltip Validation card start -->
                    <div class="card">
                        <div class="card-header">
                            <h5>Buat Barang Baru</h5>
                            <div class="card-header-right">
                            	<i class="icofont icofont-spinner-alt-5"></i>
                            </div>
                        </div>
                        <div class="card-block">
                        	{!! Form::open(['id' => 'second', 'method' => 'POST', 'novalidate' => 'novalidate', 'action' => 'BarangController@store']) !!}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Number Part</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="f_numberPart" placeholder="Masukkan Number Part">
                                    </div>
                                    <label class="col-sm-2 col-form-label">H.E.T</label>
                                    <div class="col-sm-4">
                                    	<input type="text" class="form-control" name="f_het" placeholder="Masukkan H.E.T">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Nama Barang</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="f_namaBarang" placeholder="Masukkan Nama Barang">
                                    </div>
                                    <label class="col-sm-2 col-form-label">Harga Pokok</label>
                                    <div class="col-sm-4">
                                    	<input type="number" class="form-control" name="f_hargaPokok" placeholder="Masukkan Harga Pokok">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Merk</label>
                                    <div class="col-sm-4">
                                        <select class="col-sm-12 js-example-basic-single select2-hidden-accessible" name="f_merk">
                                        	@foreach($brand as $key)
                                            <option value="{!! $key->brand_id !!}">{!! $key->brand_name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <label class="col-sm-2 col-form-label">Harga Beli</label>
                                    <div class="col-sm-4">
                                    	<input type="number" class="form-control" name="f_hargaBeli" placeholder="Masukkan Harga Beli">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Kode Part</label>
                                    <div class="col-sm-4">
                                        <select class="col-sm-12 js-example-basic-single select2-hidden-accessible" name="f_kodePart">
                                        	@foreach($brandDetail as $key)
                                            <option value="{!! $key->brand_detail_id !!}">{!! $key->brand_detail_name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <label class="col-sm-2 col-form-label">Stok Barang</label>
                                    <div class="col-sm-4">
                                    	<input type="number" class="form-control" name="f_stokBarang" placeholder="Masukkan Stok Barang">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Motor Fix</label>
                                    <div class="col-sm-3">
                                        <select class="col-sm-12 js-example-basic-single select2-hidden-accessible" name="f_motorFix">
                                        	@foreach($character as $key)
                                            <option value="{!! $key->character_id !!}">{!! $key->character_code !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <button type="button" class="btn btn-info m-b-0" data-toggle="modal">+ Motor</button>
                                    <label class="col-sm-2 col-form-label" style="margin-left: 1%;">Minimum Stok</label>
                                    <div class="col-sm-4">
                                    	<input type="number" class="form-control" name="f_minStok" placeholder="Masukkan Minimum Stok">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Motor</label>
                                    <div class="col-sm-4">
                                    	<select class="js-example-basic-multiple col-sm-12" multiple="multiple" name="f_motor[]">
                                            @foreach($detailChar as $key)
                                            <option value="{!! $key->character_id !!}">{!! $key->character_seri !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <label class="col-sm-2 col-form-label">Maximum Stok</label>
                                    <div class="col-sm-4">
                                    	<input type="number" class="form-control" name="f_maxStok" placeholder="Masukkan Maximum Stok">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Satuan</label>
                                    <div class="col-sm-4">
                                    	<select class="form-control col-sm-12" name="f_satuan">
                                            <option value="3">Set</option>
                                            <option value="4">Pcs</option>
                                        </select>
                                    </div>
                                    <label class="col-sm-2 col-form-label">Barcode</label>
                                    <div class="col-sm-4">
                                    	<input type="number" class="form-control" name="f_barcode" placeholder="Masukkan Barcode">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label"></label>
                                    <div class="col-sm-4">
                                    </div>
                                    <label class="col-sm-2 col-form-label">Berat</label>
                                    <div class="col-sm-4">
                                    	<input type="number" class="form-control" name="f_berat" placeholder="Masukkan Berat">
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('script')
<!-- Select 2 js -->
<script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
<!-- Multiselect js -->
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>

<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js') }}"></script>
@stop