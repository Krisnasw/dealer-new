<div class="card">
    <div class="card-header table-card-header">
        <h5>List Kembali</h5>
    </div>
    <div class="card-block">
        <div class="dt-responsive table-responsive">
            <table id="basic-btn" class="table table-striped table-bordered nowrap">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nomor Surat Jalan</th>
                        <th>Catatan</th>
                        <th>Jumlah Seharusnya</th>
                        <th>Jumlah Kembali</th>
                        <th>Config</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($list_back as $key => $row)
                @php $send = Access::qty_should($row['packaging_id']); @endphp
                @php $back = Access::qty_scan($row['delivery_back_id']); @endphp
                    <tr>
                        <td width="1%" align="center">{!! ++$key !!}</td>
                        <td>{!! $row->packaging_sj !!} {!! ($send != $back) ? '<i class="icofont icofont-warning"></i>' : '' !!}</td>
                        <td>{!! $row->delivery_back_note !!}</td>
                        <td>{!! $send !!}</td>
                        <td>{!! $back !!}</td>
                        <td>
                            <a href="{{ url('home/gudang/delivery/scan') }}/0/{!! $row['delivery_back_id'] !!}/{!! $row['packaging_id'] !!}"><i class="icofont icofont-barcode"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Nomor Surat Jalan</th>
                        <th>Catatan</th>
                        <th>Jumlah Seharusnya</th>
                        <th>Jumlah Kembali</th>
                        <th>Config</th>
                    </tr>
                </tfoot>

                <input type="text" class="form-control" id="scan-submit" placeholder="Masukkan Kode Barang" name="textScanned" onkeydown="if (event.keyCode == 13) { submitItem(this, {!! $id !!}); }" autofocus>
                <br />

            </table>
        </div>
    </div>
</div>