@extends('template.index')

@section('title')
Dealer Information System - Buat Delivery Baru
@stop

@section('style')

    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}"/>
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}"/>

@stop

@section('content')
<div class="main-body">
	<div class="page-wrapper">

        <!-- Page-header start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                        <div class="d-inline">
                            <h4>Buat Delivery</h4>
                            <span>Buat Delivery Baru</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ url('home/main') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Gudang - Delivery</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-header end -->

		<div class="page-body">
			<div class="row">
				<div class="col-sm-12">

					<!-- Tooltip Validation card start -->
                    <div class="card">
                        <div class="card-header">
                            <h5>Buat Delivery Baru</h5>
                            <div class="card-header-right">
                            	<i class="icofont icofont-spinner-alt-5"></i>
                            </div>
                        </div>
                        <div class="card-block">
                        	{!! Form::open(['id' => 'second', 'method' => 'POST', 'novalidate' => 'novalidate', 'action' => 'DeliveryController@store']) !!}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Surat Jalan</label>
                                    <div class="col-sm-10">
                                        <select class="js-example-basic-multiple col-sm-12" name="sopir" multiple="multiple">
                                            @foreach($packaging as $row)
                                            <option value="{!! $row->packaging_id !!}" {!! ($data['packaging_id'] == $row['packaging_id']) ? 'selected' : '' !!}>{!! $row->packaging_sj !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Tanggal Pengiriman</label>
                                    <div class="col-sm-10">
                                        <input type="date" class="form-control" name="tgl" placeholder="Masukkan Tanggal" value="{!! $data['delivery_date'] !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Sopir</label>
                                    <div class="col-sm-10">
                                        <select class="js-example-basic-single col-sm-12" name="sopir">
                                            <option value="0">-- Pilih Sopir --</option>
                                            @foreach($sopir as $row)
                                            <option value="{!! $row->employee_id !!}" {!! ($data['delivery_sopir'] == $row['employee_id']) ? 'selected' : '' !!}>{!! $row->employee_name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Kernet</label>
                                    <div class="col-sm-10">
                                        <select class="js-example-basic-single col-sm-12" name="kernet">
                                            <option value="0">-- Pilih Kernet --</option>
                                            @foreach($kernet as $row)
                                            <option value="{!! $row->employee_id !!}" {!! ($data['delivery_kernet'] == $row['employee_id']) ? 'selected' : '' !!}>{!! $row->employee_name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header table-card-header">
                                        <h5>List Kardus</h5>
                                    </div>
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="basic-btn" class="table table-striped table-bordered nowrap">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Barcode Kardus</th>
                                                        <th>Isi Kardus</th>
                                                        <th>Jenis Kardus</th>
                                                        <th>Surat Jalan</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($list_detail as $key => $row)
                                                    <tr>
                                                        <td width="1%" align="center">{!! ++$key !!}</td>
                                                        <td>{!! $row['cardboard_barcode'] !!}</td>
                                                        <td>{!! Access::hitung_isi($row['cardboard_id']) !!}</td>
                                                        <td>{!! $row['cardboard_type_name'] !!}</td>
                                                        <td>{!! $row['packaging_sj'] !!}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Barcode Kardus</th>
                                                        <th>Isi Kardus</th>
                                                        <th>Jenis Kardus</th>
                                                        <th>Surat Jalan</th>
                                                    </tr>
                                                </tfoot>

                                                <input type="text" class="form-control" id="scan-submit" placeholder="Masukkan Kode Barang" name="textScanned" onkeydown="if (event.keyCode == 13) { saveDus({!! $data['delivery_id'] !!}, this.value); }" autofocus>
                                                <br />

                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header table-card-header">
                                        <h5>List Kembali</h5>
                                    </div>
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="basic-btn" class="table table-striped table-bordered nowrap">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nomor Surat Jalan</th>
                                                        <th>Catatan</th>
                                                        <th>Jumlah Seharusnya</th>
                                                        <th>Jumlah Kembali</th>
                                                        <th>Config</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                            <select class="form-control js-example-basic-single col-sm-12" name="surat_jalan">
                                                                <option value="0" selected>-- Pilih Surat Jalan --</option>
                                                                @foreach($list_packaging as $row)
                                                                <option value="{!! $row['packaging_id'] !!}">{!! $row['packaging_sj'] !!}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="catatan" onkeydown="if (event.keyCode == 13) { saveBack({!! $data['delivery_id'] !!},this.value)}">
                                                        </td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                @foreach($list_back as $key => $row)
                                                @php $send = Access::qty_should($row['packaging_id']); @endphp
                                                @php $back = Access::qty_scan($row['delivery_back_id']); @endphp
                                                    <tr>
                                                        <td width="1%" align="center">{!! ++$key !!}</td>
                                                        <td>{!! $row->packaging_sj !!} {!! ($send != $back) ? '<i class="icofont icofont-warning"></i>' : '' !!}</td>
                                                        <td>{!! $row->delivery_back_note !!}</td>
                                                        <td>{!! $send !!}</td>
                                                        <td>{!! $back !!}</td>
                                                        <td>
                                                            <a href="{{ url('home/gudang/delivery/scan') }}/{!! $data['delivery_id'] !!}/{!! $row['delivery_back_id'] !!}/{!! $row['packaging_id'] !!}" class="btn btn-danger btn-outline m-b-0"><i class="icofont icofont-barcode"></i></a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nomor Surat Jalan</th>
                                                        <th>Catatan</th>
                                                        <th>Jumlah Seharusnya</th>
                                                        <th>Jumlah Kembali</th>
                                                        <th>Config</th>
                                                    </tr>
                                                </tfoot>

                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="button" class="btn btn-success m-b-0" onclick="holdButton();">Hold</button>
                                        <button type="button" class="btn btn-danger m-b-0" onclick="doneButton();">Selesai</button>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('script')
<!-- Select 2 js -->
<script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
<!-- Multiselect js -->
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function saveDus(id, data) {
        $.post('{{ url('home/gudang/delivery/add-detail') }}', {
            id : id,
            data : data
        }, function (resp) {
            if (resp.status == 'success') {
                swal('Success', resp.message, 'success');
                window.location.reload();
            } else {
                swal('Oops!', resp.message, 'error');
                // window.location.reload();
            }
        });
    }

    function saveBack(id, data) {
        var packaging_id =  $('select[name="surat_jalan"]').val();

        $.post('{{ url('home/gudang/delivery/add-back') }}', {
            id : id,
            data : data,
            packaging : packaging_id
        }, function (resp) {
            if (resp.status == 'success') {
                swal('Success', resp.message, 'success');
                window.location.reload();
            } else {
                swal('Oops!', resp.message, 'error');
            }
        });
    }

    function holdButton() {
        var id =  {!! $data['delivery_id'] !!};

        $.ajax({
          type: 'POST',
          url: '{{ url('home/gudang/delivery/check-kuota') }}',
          data: {id:id},
          dataType: 'json',
          success: function(data){
            var packaging = (data.content['packaging']);
            var delivery = (data.content['delivery']);

            if (packaging != delivery) {
              var a = confirm("Anda yakin ingin keluar proses ini ?");
              if(a==true){
                window.location.href = '{{ url('home/gudang/delivery') }}';
              }
            }else{
              window.location.href = '{{ url('home/gudang/delivery') }}';
            }
          } 
        });
    }

    function doneButton() {
        var id =  {!! $data['delivery_id'] !!};

        $.ajax({
          type: 'POST',
          url: '{{ url('home/gudang/delivery/check-kuota') }}',
          data: {id:id},
          dataType: 'json',
          success: function(data){
            var packaging = (data.content['packaging']);
            var delivery = (data.content['delivery']);

            if (packaging != delivery) {
              swal("Oops!", "Proses delivery belum selesai!", 'error');
            }else{
              window.location.href = '{{ url('home/gudang/delivery') }}';
            }
          } 
        });
    }
</script>
@stop