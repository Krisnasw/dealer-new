@extends('template.index')

@section('title')
Master Data - Customer Member
@stop

@section('style')

    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/css/buttons.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/switchery/css/switchery.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/component.css') }}">

@stop

@section('content')
    <!-- Main-body start -->
<div class="main-body">
    <div class="page-wrapper">
        <!-- Page-header start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                        <div class="d-inline">
                            <h4>Data Customer</h4>
                            <span>List All Customer</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ url('home/main') }}">
                                    <i class="icofont icofont-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Master Data - Customer</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-header end -->

        <!-- Page-body start -->
        <div class="page-body">
            <div class="row">
                <div class="col-sm-12">

                    <!-- HTML5 Export Buttons table start -->
                    <div class="card">
                        <div class="card-header table-card-header">
                            <h5>List Customer</h5>
                        </div>
                        <div class="card-block">
                        	<a href="{{ url('home/master/customer-member/create') }}" id="addRow" class="btn btn-primary m-b-20">+ Add New</a>
                            <a href="{{ url('home/ajax/export-customer') }}" class="btn btn-danger m-b-20"><i class="icofont icofont-download"></i>Export</a>
                            <div class="dt-responsive table-responsive">
                                <table id="item-tbl" class="table table-striped table-bordered nowrap">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Kode Pelanggan</th>
                                            <th>Nama</th>
                                            <th>Alamat</th>
                                            <th>Telepon</th>
                                            <th>Grade Pelanggan</th>
                                            <th>No. Rekening</th>
                                            <th>Bank</th>
                                            <th>NPWP</th>
                                            <th>Nama NPWP</th>
                                            <th>Expedisi</th>
                                            <th>Handphone</th>
                                            <th>Fax</th>
                                            <th>Wilayah</th>
                                            <th>Config</th>
                                        </tr>
                                    </thead>
                                    <tbody>
		                            </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Kode Pelanggan</th>
                                            <th>Nama</th>
                                            <th>Alamat</th>
                                            <th>Telepon</th>
                                            <th>Grade Pelanggan</th>
                                            <th>No. Rekening</th>
                                            <th>Bank</th>
                                            <th>NPWP</th>
                                            <th>Nama NPWP</th>
                                            <th>Expedisi</th>
                                            <th>Handphone</th>
                                            <th>Fax</th>
                                            <th>Wilayah</th>
                                            <th>Config</th>
                                        </tr>
                                    </tfoot>
	                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-body end -->
	    </div>
	</div>
</div>
@stop

@section('script')
	<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>

    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>

    <script type="text/javascript" src="{{ asset('bower_components/switchery/js/switchery.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/swithces.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/modalEffects.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/classie.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
        $('#item-tbl').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax":{
                     "url": "{{ url('home/ajax/members') }}",
                     "dataType": "json",
                     "type": "POST",
                     "data":{ _token: "{{csrf_token()}}"}
                   },
            "columns": [
                { "data": "customer_id", sorttable: true },
                { "data": "customer_code", sorttable: true },
                { "data": "customer_name", sorttable: true },
                { "data": "customer_addres", sorttable: true },
                { "data": "customer_phone", sorttable: true },
                { "data": "grade_id", sorttable: true },
                { "data" : "customer_rekening", sorttable: true },
                { "data" : "customer_id_bank", sorttable: true },
                { "data" : "customer_npwp", sorttable: true },
                { "data" : "customer_nama_npwp", sorttable: true },
                { "data" : "customer_expedisi", sorttable: true },
                { "data" : "customer_hp", sorttable: true },
                { "data" : "customer_fax", sorttable: true },
                { "data" : "city_id", sorttable: true },
                { "data": "config", sorttable: true }
            ]
        });
    });

    function showDeleteModal(customer_id) {
        swal({
            title: "Are you sure ??",
            text: "Customer Tidak Dapat Kembali Jika Sudah Terhapus", 
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $.post("{{ url('home/ajax/members/delete') }}", {
                customer_id : customer_id,
                _token: "{{ csrf_token() }}"
            }, function (data) {
                if (data.status == "success") {
                    swal(data.message);
                    window.location.reload();
                } else {
                    swal(data.message);
                    window.location.reload();
                }
            });
          } else {
            swal("Customer Anda Tidak Jadi Dihapus");
          }
        });
    }
    </script>
@endsection