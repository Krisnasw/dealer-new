@extends('template.index')

@section('title')
Dealer Information System - Edit Customer
@stop

@section('style')

    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}"/>
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}"/>

@stop

@section('content')
<div class="main-body">
	<div class="page-wrapper">

        <!-- Page-header start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                        <div class="d-inline">
                            <h4>Edit Customer</h4>
                            <span>Edit Customer - {!! $mem->customer_name !!}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ url('home/main') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Master Data - Customer</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-header end -->

		<div class="page-body">
			<div class="row">
				<div class="col-sm-12">

					<!-- Tooltip Validation card start -->
                    <div class="card">
                        <div class="card-header">
                            <h5>Edit Customer - {!! $mem->customer_name !!}</h5>
                            <div class="card-header-right">
                            	<i class="icofont icofont-spinner-alt-5"></i>
                            </div>
                        </div>
                        <div class="card-block">
                        	{!! Form::model($mem, ['id' => 'second', 'method' => 'PATCH', 'novalidate' => 'novalidate', 'action' => ['CustMemController@update', base64_encode($mem->customer_id)]]) !!}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Kode Customer</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="f_customerCode" placeholder="Masukkan Kode Customer" value="{!! $mem->customer_code !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Nama</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="f_nama" placeholder="Masukkan Nama Lengkap" value="{!! $mem->customer_name !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Nama Toko</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="f_namaToko" placeholder="Masukkan Nama Toko" value="{!! $mem->customer_store !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Alamat</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" name="alamat" placeholder="Masukkan Alamat">{!! $mem->customer_addres !!}</textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Telepon</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="f_phone" placeholder="Masukkan Telepon" value="{!! $mem->customer_phone !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" name="f_email" placeholder="Masukkan Email" value="{!! $mem->customer_email !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Grade</label>
                                    <div class="col-sm-10">
                                        <select class="col-sm-12 js-example-basic-single select2-hidden-accessible" name="f_grade">
                                        	@foreach($grade as $key)
                                            <option value="{!! $key->grade_id !!}" {!! ($mem->grade_id == $key->grade_id) ? 'selected' : '' !!}>{!! $key->grade_nama !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">No. Rekening</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="f_noRek" placeholder="Masukkan No. Rekening" value="{!! $mem->customer_rekening !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Bank</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="f_bank" placeholder="Masukkan Bank" value="{!! $mem->customer_id_bank !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">NPWP</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="f_npwp" placeholder="Masukkan NPWP" value="{!! $mem->customer_npwp !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Nama NPWP</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="f_namaNpwp" placeholder="Masukkan Nama NPWP" value="{!! $mem->customer_nama_npwp !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Expedisi</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="f_expedisi" placeholder="Masukkan Expedisi" value="{!! $mem->customer_expedisi !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Handphone</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="f_hp" placeholder="Masukkan No. Handphone" value="{!! $mem->customer_hp !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Fax</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="f_fax" placeholder="Masukkan Fax" value="{!! $mem->customer_fax !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Wilayah</label>
                                    <div class="col-sm-8">
                                        <select class="col-sm-12 js-example-basic-single select2-hidden-accessible" name="f_wilayah" id="select-wil">
                                            @foreach($city as $key)
                                            <option value="{!! $key->city_id !!}" {!! ($mem->city_id == $key->city_id) ? 'selected' : '' !!}>{!! $key->city_name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <button type="button" class="btn btn-info m-b-0" data-toggle="modal" data-target="#add-wilayah">Add New Wilayah</button>
                                </div>

                                <div class="card">
                                    <div class="card-block">

                                        <div class="dt-responsive table-responsive">
                                            <table id="basic-btn" class="table table-striped table-bordered nowrap">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama Merk</th>
                                                        <th>Limit Tagihan</th>
                                                        <th>Max Jatuh Tempo</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if(count($limit) <= 0)
                                                        @foreach($brand as $val => $keys)
                                                        <tr>
                                                            <td>{!! ++$val !!}</td>
                                                            <td><input type="hidden" name="brand_id[]" value="{!! $keys->brand_id !!}">{!! $keys->brand_name !!}</td>
                                                            <td><input type="number" class="form-control" name="limit_tagihan[]" placeholder="Masukkan Limit Tagihan" min="0"></td>
                                                            <td><input type="number" class="form-control" name="jatuh_tempo[]" placeholder="Masukkan Max Jatuh Tempo" min="0"></td>
                                                        </tr>
                                                        @endforeach
                                                    @else
                                                        @foreach($limit as $val => $keys)
                                                        <tr>
                                                            <td>{!! ++$val !!}</td>
                                                            <td><input type="hidden" name="brand_id[]" value="{!! $keys->brand_id !!}">{!! $keys->brand_name !!}</td>
                                                            <td><input type="number" class="form-control" name="limit_tagihan[]" placeholder="Masukkan Limit Tagihan" min="0" value="{!! $keys->customer_limit_bill !!}"></td>
                                                            <td><input type="number" class="form-control" name="jatuh_tempo[]" placeholder="Masukkan Max Jatuh Tempo" min="0" value="{!! $keys->customer_limit_due !!}"></td>
                                                        </tr>
                                                        @endforeach
                                                    @endif
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama Merk</th>
                                                        <th>Limit Tagihan</th>
                                                        <th>Max Jatuh Tempo</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                    <div class="modal fade md-effect-1" id="add-wilayah" tabindex="-1" role="dialog">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header bg-info">
                                    <h5 class="modal-title">Add New Wilayah</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            {!! Form::open(['method' => 'POST', 'action' => 'ApiController@addWilayah', 'id' => 'form-wil']) !!}
                                <div class="modal-body">
                                    <!-- Basic Form Inputs card start -->
                                    <div class="card">
                                        <div class="card-block">
                                            <h4 class="sub-title">Form Add New Wilayah</h4>
                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label">Nama Wilayah</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" min="0" class="form-control" placeholder="Masukkan Nama Wilayah" name="namaWilayah">
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                    <!-- Basic Form Inputs card end -->
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light" id="submits">Save changes</button>
                                </div>
                            {!! Form::close() !!}
                            </div>
                        </div>
                    </div>

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('script')
<!-- Select 2 js -->
<script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
<!-- Multiselect js -->
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#form-wil").submit(function (e) {
            e.preventDefault();
            var formdata = new FormData($("#form-wil")[0]);

            $.ajax({
                url:        $("#form-wil").attr('action'),
                method:     "POST",
                data:       new FormData(this),
                processData: false,
                contentType: false
            })
            .done(function(response) {
                swal({
                    title:  ((response.status === "error") ? "Opps!" : 'Successfully'),
                    text:   response.msg,
                    type:   ((response.status === "error") ? "error" : "success"),
                });

                $.ajax({
                    url: "{{ url('home/ajax/cities') }}",
                    method: "GET",
                    contentType: false
                })
                .done(function(response) {
                    if (response.status == "error") {
                        swal({ title: "Opss!", text: response.msg, type: "error" });
                    } else {
                        var options = '';
                        for (var i = 0; i < response.city.length; i++) {
                            options += "<option value='"+response.city[i].city_id+"'>"+response.city[i].city_name+"</option>";
                        }
                        $("#select-wil").empty();
                        $("#select-wil").html(options);
                    }
                })
                .fail(function() {
                    swal({
                        title:  "Opss!",
                        text:   "Ada Yang Salah! , Silahkan Coba Lagi Nanti",
                        type:   "error",
                    });
                })
                
            })
            .fail(function() {
                swal({
                    title:  "Opss!",
                    text:   "Ada Yang Salah! , Silahkan Coba Lagi Nanti",
                    type:   "error",
                });
            })
        });
    });
</script>
@stop