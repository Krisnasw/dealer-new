@extends('template.index')

@section('title')
Laporan - Komisi Sales
@stop

@section('style')

    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/css/buttons.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/switchery/css/switchery.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/component.css') }}">

@stop

@section('content')
    <!-- Main-body start -->
<div class="main-body">
    <div class="page-wrapper">
        <!-- Page-header start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                        <div class="d-inline">
                            <h4>Data Laporan - Komisi Sales</h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ url('home/main') }}">
                                    <i class="icofont icofont-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{ url('home/report/komisi-sales') }}">Laporan - Komisi Sales</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-header end -->

        <!-- Page-body start -->
        <div class="page-body">
            <div class="row">
                <div class="col-sm-12">

                    <!-- HTML5 Export Buttons table start -->
                    <div class="card">
                        <div class="card-header table-card-header">
                            <h5>List Laporan - Komisi Sales</h5>
                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table id="basic-btn" class="table table-striped table-bordered nowrap">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Tanggal Nota</th>
                                            <th>No. Nota</th>
                                            <th>Nama Customer</th>
                                            <th>Tanggal Bayar</th>
                                            <th>Total</th>
                                            <th>Nilai Komisi</th>
                                            <th>Rupiah Komisi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @php $total = 0; @endphp
                                    @foreach($nota as $datas => $row)
                                    @php $tempo = date('Y-m-d', strtotime('+'.$row['nota_tempo'].' days', strtotime($row['nota_date']))); @endphp
		                                <tr>
		                                    <td width="1%" align="center" style="color: {!! ($row['payment_date'] > $tempo) ? 'red' : '' !!};">{!! ++$datas !!}</td>
                                            <td align="center" style="color: {!! ($row['payment_date'] > $tempo) ? 'red' : '' !!};">{!! $row['nota_date'] !!}</td>
                                            <td align="center" style="color: {!! ($row['payment_date'] > $tempo) ? 'red' : '' !!};">{!! $row['nota_code'] !!}</td>
                                            <td align="center" style="color: {!! ($row['payment_date'] > $tempo) ? 'red' : '' !!};">{!! $row['customer_name'] !!}</td>
                                            <td align="center" style="color: {!! ($row['payment_date'] > $tempo) ? 'red' : '' !!};">{!! $row['payment_date'] !!}</td>
                                            <td align="center" style="color: {!! ($row['payment_date'] > $tempo) ? 'red' : '' !!};">{!! number_format($row['nota_total']) !!}</td>
                                            @if ($row['payment_date'] > $tempo)
                                            <td align="center" style="color: {!! ($row['payment_date'] > $tempo) ? 'red' : '' !!};">0.1%</td>
                                            @elseif ($row['payment_detail_status'] == 0)
                                            <td align="center" style="color: {!! ($row['payment_date'] > $tempo) ? 'red' : '' !!};">0%</td>
                                            @else
                                            <td align="center" style="color: {!! ($row['payment_date'] > $tempo) ? 'red' : '' !!};">1%</td>
                                            @endif
                                            @if ($row['payment_date'] > $tempo)
                                            <td align="center" style="color: {!! ($row['payment_date'] > $tempo) ? 'red' : '' !!};">{!! number_format($row['nota_total'] * 0.1 / 100) !!}</td>
                                            @elseif ($row['payment_detail_status'] == 0)
                                            <td align="center" style="color: {!! ($row['payment_date'] > $tempo) ? 'red' : '' !!};">0</td>
                                            @else
                                            <td align="center" style="color: {!! ($row['payment_date'] > $tempo) ? 'red' : '' !!};">{!! number_format($row['nota_total'] * 1 / 100) !!}</td>
                                            @endif
		                                </tr>
                                    @if ($row['payment_date'] > $tempo)
                                    @php $total += $row['nota_total'] * 0.1 / 100; @endphp
                                    @elseif ($row['payment_detail_status'] == 0)
                                    @php $total += 0; @endphp
                                    @else
                                    @php $total += $row['nota_total'] * 1 / 100; @endphp
                                    @endif
		                            @endforeach
		                            </tbody>
                                    <tfoot>
                                        <tr>
                                            <th align="center">No</th>
                                            <th align="center">Tanggal Nota</th>
                                            <th align="center">No. Nota</th>
                                            <th align="center">Nama Customer</th>
                                            <th align="center">Tanggal Bayar</th>
                                            <th align="center">Total Netto</th>
                                            <th align="center">Nilai Komisi</th>
                                            <th align="center">Rupiah Komisi</th>
                                        </tr>
                                        <tr>
                                            <th colspan="7" style="text-align: right;">Total Komisi : {!! number_format($total) !!}</th>
                                        </tr>
                                    </tfoot>
	                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-body end -->
	    </div>
	</div>
</div>
@stop

@section('script')

    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>

    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>

    <script type="text/javascript" src="{{ asset('bower_components/switchery/js/switchery.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/swithces.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/modalEffects.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/classie.js') }}"></script>
@endsection