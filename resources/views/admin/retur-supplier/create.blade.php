@extends('template.index')

@section('title')
Pembelian - Retur Supplier
@stop

@section('style')
<!-- Select 2 css -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}"/>
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">
@stop

@section('content')
<div class="main-body">
	<div class="page-wrapper">

        <!-- Page-header start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                        <div class="d-inline">
                            <h4>Buat Pembelian - Retur Supplier</h4>
                            <span>Buat Pembelian - Retur Supplier</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ url('home/main') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Pembelian - Retur Supplier</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-header end -->

		<div class="page-body">
			<div class="row">
				<div class="col-sm-12">

					<!-- Tooltip Validation card start -->
                    <div class="card">
                        <div class="card-header">
                            <h5>Buat Pembelian - Retur Supplier</h5>
                            <div class="card-header-right">
                            	<i class="icofont icofont-spinner-alt-5"></i>
                            </div>
                        </div>
                        <div class="card-block">
                        	{!! Form::open(['id' => 'second', 'method' => 'POST', 'novalidate' => 'novalidate', 'action' => 'ReturSupController@store']) !!}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Kode DO</label>
                                    <div class="col-sm-10">
                                        <select class="form-control js-example-basic-single" name="kode_do">
                                            <option value="0">-- Pilih DO --</option>
                                            @foreach($do as $row)
                                            <option value="{!! $row['receipt_id'] !!}">{!! $row['receipt_code'] !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Type Retur</label>
                                    <div class="col-sm-10">
                                        <div class="form-radio">
                                            <div class="radio radiofill radio-primary radio-inline">
                                                <label>
                                                    <input type="radio" name="jenis" value="1">
                                                    <i class="helper"></i>Cash
                                                </label>
                                            </div>
                                            <div class="radio radiofill radio-primary radio-inline">
                                                <label>
                                                    <input type="radio" name="jenis" value="2">
                                                    <i class="helper"></i>Saldo
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('script')
    <!-- Select 2 js -->
    <script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
    <!-- Multiselect js -->
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>
@stop