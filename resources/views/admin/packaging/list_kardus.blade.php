<script type="text/javascript">
	function update_type(val,id) {
        $.post('{{ url('home/ajax/update-kardus-type') }}', {
            data : val.value,
            id : id
        }, function (datas) {
            if (datas.status == 'success') {
                preview();
            } else {
                swal('Oops!', datas.message, 'error');
            }
        });
    }
</script>

<div class="card">
    <div class="card-block">
        <div class="dt-responsive table-responsive">
            <table id="basic-btn" class="table table-striped table-bordered nowrap">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Barcode Kardus</th>
                        <th>Jenis Kardus</th>
                        <th>Isi Kardus</th>
                        <th>Config</th>
                    </tr>
                </thead>
                <tbody id="tbody-isi">
                @foreach($cardboard as $val => $keys)
                    <tr>
                        <td>{!! ++$val !!}</td>
                        <td>{!! $keys->cardboard_barcode !!}</td>
                        <td>
                        	<select class="form-control col-sm-12" name="jenis" id="jenisss{!! ++$val !!}" onchange="update_type(this, {!! $keys->cardboard_barcode !!});">
                        		<option value="0" selected>-- Pilih Jenis --</option>
                        		@foreach($type as $row)
                        		<option value="{!! $row['cardboard_type_id'] !!}" {!! ($keys->cardboard_type_id == $row['cardboard_type_id']) ? 'selected' : '' !!}>{!! $row->cardboard_type_name !!}</option>
                        		@endforeach
                        	</select>
                        </td>
                        <td>{!! Access::hitung_isi($keys->cardboard_id); !!}</td>
                        <td>
                            <a href="{{ url('home/gudang/print-barcode') }}/{!! $keys->cardboard_id !!}" class="btn btn-primary btn-outline-primary" target="_blank"><i class="icofont icofont-print"></i></a>
                            <a href="{{ url('home/gudang/scan-item') }}/{!! $keys->cardboard_id !!}" target="_blank" class="btn btn-danger btn-outline-info"><i class="icofont icofont-barcode"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                	<tr>
                        <th>No</th>
                        <th>Barcode Kardus</th>
                        <th>Jenis Kardus</th>
                        <th>Isi Kardus</th>
                        <th>Config</th>
                    </tr>
                </tfoot>
            </table>

        	<button type="button" class="btn btn-success btn-outline-success" onclick="add_kardus(0);"><i class="icofont icofont-add-alt"></i>Tambah Kardus</button><br />
        </div>
    </div>
</div>