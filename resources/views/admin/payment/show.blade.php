@extends('template.index')

@section('title')
    Dealer Information System - Buat Payment
@stop

@section('style')

    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}"/>
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}"/>

@stop

@section('content')
    <div class="main-body">
        <div class="page-wrapper">

            <!-- Page-header start -->
            <div class="page-header card">
                <div class="row align-items-end">
                    <div class="col-lg-8">
                        <div class="page-header-title">
                            <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                            <div class="d-inline">
                                <h4>Buat Payment</h4>
                                <span>Buat Payment Baru</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="page-header-breadcrumb">
                            <ul class="breadcrumb-title">
                                <li class="breadcrumb-item">
                                    <a href="{{ url('home/main') }}">
                                        <i class="icofont icofont-home"></i>
                                    </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">Pembelian - Payment</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Page-header end -->

            <div class="page-body">
                <div class="row">
                    <div class="col-sm-12">

                        <!-- Tooltip Validation card start -->
                        <div class="card">
                            <div class="card-header">
                                <h5>Buat Payment Baru</h5>
                                <div class="card-header-right">
                                    <i class="icofont icofont-spinner-alt-5"></i>
                                </div>
                            </div>
                            <div class="card-block">
                                {!! Form::open(['id' => 'second', 'method' => 'POST', 'novalidate' => 'novalidate', 'action' => 'PaymentController@doBayar', 'onkeypress' => 'return event.keyCode != 13;']) !!}
                                <input type="hidden" name="id" value="{!! $payment['payment_id'] !!}">
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Customer</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" readonly="readonly" name="customer_id" value="{!! $payment['customer']['customer_name'] !!}">
                                    </div>
                                    
                                    <label class="col-sm-2 col-form-label">Tanggal Pembayaran</label>
                                    <div class="col-sm-4">
                                        <input type="date" class="form-control" name="date" value="{!! $payment['payment_date'] !!}" readonly>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Nota</label>
                                    <div class="col-sm-4">
                                        <select class="form-control js-example-basic-multiple" name="i_nota[]" multiple="multiple">
                                            <option value="0" selected="selected">- Pilih Nota -</option>
                                            @foreach($nota as $row)
                                                @foreach($list_nota as $row2)
                                                <option value="{!! $row['nota_id'] !!}" {!! ($row['nota_id'] == $row2['nota_id']) ? 'selected' : '' !!}>{!! $row['nota_code'] !!}</option>
                                                @endforeach
                                            @endforeach
                                        </select>
                                    </div>
                                    
                                    <label class="col-sm-2 col-form-label">Debit / Kredit</label>
                                    <div class="col-sm-4">
                                        <select class="form-control js-example-basic-multiple" name="i_debit[]" multiple="multiple">
                                            <option value="0" selected="selected">- Pilih Debit -</option>
                                            @foreach($debit as $row)
                                                @foreach($list_debit as $row2)
                                                    <option value="{!! $row['debit_id'] !!}" {!! ($row['debit_id'] == $row2['debit_id']) ? 'selected' : '' !!}>{!! $row['debit_code'] !!}</option>
                                                @endforeach
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Retur</label>
                                    <div class="col-sm-4">
                                        <select class="form-control js-example-basic-multiple" name="i_retur[]" multiple="multiple">
                                            <option value="0" selected>- Pilih Retur -</option>
                                            @foreach($retur as $row)
                                                @foreach($list_retur as $row2)
                                                    <option value="{!! $row['retur_id'] !!}" {!! ($row['retur_id'] == $row2['retur_id']) ? 'selected' : '' !!}>{!! $row['retur_code'] !!}</option>
                                                @endforeach
                                            @endforeach
                                        </select>
                                    </div>
                                    
                                    <label class="col-sm-2 col-form-label">Balance</label>
                                    <div class="col-sm-4">
                                        <select class="form-control js-example-basic-multiple" name="i_entrusted[]" multiple="multiple">
                                            <option value="0" selected="selected">- Pilih Balance -</option>
                                            @foreach($balance as $row)
                                                @foreach($list_entrusted as $row2)
                                                    <option value="{!! $row['entrusted_id'] !!}" {!! ($row['entrusted_id'] == $row2['entrusted_id']) ? 'selected' : '' !!}>{!! $row['entrusted_code'] !!}</option>
                                                @endforeach
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header table-card-header">
                                        <h5>List Detail</h5>
                                    </div>
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="basic-btn" class="table table-striped table-bordered nowrap">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 10%;">
                                                            <select name="i_detail_type" class="form-control" onchange="get_type(this.value)">
                                                                <option value="1">Cash</option>
                                                                <option value="2">Transfer</option>
                                                                <option value="3">Giro</option>
                                                            </select>
                                                        </th>
                                                        <th>
                                                            <div id="no_rek" style="display: none;">
                                                                <input type="text" class="form-control" name="i_detail_rek" placeholder="No Rekening">
                                                              </div>
                                                        </th>
                                                        <th>
                                                            <div id="nama_bank" style="display: none;">
                                                                <input type="text" class="form-control" name="i_detail_bank" placeholder="Nama Bank" >
                                                            </div>
                                                        </th>
                                                        <th>
                                                            <div id="jatuh_tempo" style="display: none;">
                                                                <div class="input-group" style="margin-bottom: 0px !important;">
                                                                    <span class="input-group-addon"><i class="icofont icofont-calendar"></i></span>
                                                                    <input placeholder="Jatuh Tempo" type="date" name="i_detail_tempo" value=""class="form-control">
                                                                </div>
                                                            </div>
                                                        </th>
                                                        <th></th>
                                                        <th>
                                                            <input type="number" class="form-control pull-right" name="i_detail_nominal" placeholder="Nominal" onkeydown="if (event.keyCode == 13) { add_detail_new(); }">
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Tanggal</th>
                                                        <th>Kode Detail</th>
                                                        <th>Tagihan</th>
                                                        <th>Potongan</th>
                                                        <th>Terbayar</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @php $grand_total_tagihan = 0; @endphp
                                                @php $grand_potongan = 0; @endphp
                                                @php $grand_total_terbayar = 0; @endphp
                                                @foreach($list as $data => $row)
                                                @if ($row['payment_detail_type'] == 1)
                                                    @php $code = 'CASH'; @endphp
                                                @elseif ($row['payment_detail_type'] == 2)
                                                    @php $code = 'Transfer'; @endphp
                                                @else
                                                    @php $code = 'Giro'; @endphp
                                                @endif

                                                @if ($row['payment_detail_type'] != 0)
                                                    @php $code = $code; @endphp
                                                    @php $tagihan = 0; @endphp
                                                    @php $terbayar = $row['payment_detail_nominal']; @endphp
                                                @else
                                                    @php $code = $row['nota_code']; @endphp
                                                    @php $tagihan = $row['payment_detail_nominal']; @endphp
                                                    @php $terbayar = 0; @endphp
                                                @endif
                                                    <tr>
                                                        <td>{!! ++$data !!}</td>
                                                        <td>{!! $row['payment_detail_date'] !!}</td>
                                                        <td>{!! $code !!}</td>
                                                        <td>{!! number_format($tagihan,2) !!}</td>
                                                        <td>{!! number_format(0,2) !!}</td>
                                                        <td>{!! number_format($terbayar,2) !!}</td>
                                                    </tr>
                                                @php $grand_total_tagihan += $tagihan; @endphp
                                                @php $grand_total_terbayar += $terbayar; @endphp
                                                @endforeach
                                                @foreach($list_retur as $data => $row)
                                                    <tr>
                                                        <td>{!! ++$data !!}</td>
                                                        <td>{!! $row['retur_date'] !!}</td>
                                                        <td>{!! $row['retur_code'] !!}</td>
                                                        <td>{!! number_format(0,2) !!}</td>
                                                        <td>{!! number_format($row['retur_total'], 2) !!}</td>
                                                        <td>{!! number_format(0,2) !!}</td>
                                                    </tr>
                                                @php $grand_potongan += $row['retur_total']; @endphp
                                                @endforeach
                                                @foreach($list_debit as $data => $row)
                                                @if ($row['debit_type'] == 1)
                                                    @php $debit = $row['debit_nominal']; @endphp
                                                    @php $kredit = 0; @endphp
                                                @elseif ($row['debit_type'] == 2)
                                                    @php $debit = 0; @endphp
                                                    @php $kredit = $row['debit_nominal']; @endphp
                                                @else
                                                    @php $debit = 0; @endphp
                                                    @php $kredit = 0; @endphp
                                                @endif
                                                    <tr>
                                                        <td>{!! ++$data !!}</td>
                                                        <td>{!! $row['debit_date'] !!}</td>
                                                        <td>{!! $row['debit_code'] !!}</td>
                                                        <td>{!! number_format($debit, 0) !!}</td>
                                                        <td>{!! number_format($kredit, 0) !!}</td>
                                                        <td>{!! number_format(0,2) !!}</td>
                                                    </tr>
                                                @php $grand_total_tagihan += $debet; @endphp
                                                @php $grand_potongan += $kredit; @endphp
                                                @endforeach
                                                @foreach($list_entrusted as $data => $row)
                                                    <tr>
                                                        <td>{!! ++$data !!}</td>
                                                        <td>{!! $row['entrusted_date'] !!}</td>
                                                        <td>{!! $row['entrusted_code'] !!}</td>
                                                        <td>{!! number_format(0,2) !!}</td>
                                                        <td>{!! number_format($row['entrusted_total'], 2) !!}</td>
                                                        <td>{!! number_format(0,2) !!}</td>
                                                    </tr>
                                                @php $grand_potongan += $row['entrusted_total']; @endphp
                                                @endforeach
                                                @php $grand_sisa = $grand_total_tagihan - $grand_potongan - $grand_total_terbayar; @endphp
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label class="col-form-label">Potongan : <input style="border: none; background: transparent; font-weight: bold;" type="text" readonly="readonly" value="{!! number_format($grand_potongan, 2) !!}" name="i_grand_total_potongan"></label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="col-form-label pull-right">Total Tagihan : <input style="border: none; background: transparent; font-weight: bold;" type="text" readonly="readonly" value="{!! number_format($grand_total_tagihan, 2) !!}" name="i_grand_total_payoff"></label>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label class="col-form-label">Sisa : <input style="border: none; background: transparent; font-weight: bold;" type="text" readonly="readonly" value="{!! number_format($grand_sisa, 2) !!}" name="i_rest"></label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="col-form-label pull-right">Total Dibayar : <input style="border: none; background: transparent; font-weight: bold;" type="text" readonly="readonly" value="{!! number_format($grand_total_terbayar, 2) !!}" name="i_grand_total_dibayar"></label>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary m-b-0">Bayar</button>
                                        <button type="button" class="btn btn-info m-b-0" onclick="history.go(-1);">Kembali</button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <!-- Select 2 js -->
    <script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
    <!-- Multiselect js -->
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>

    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js') }}"></script>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function get_type(id) {
            if (id == 1) {
              document.getElementById('no_rek').style.display = 'none';
              document.getElementById('nama_bank').style.display = 'none';
              document.getElementById('jatuh_tempo').style.display = 'none';
            }else if(id == 2){
              document.getElementById('jatuh_tempo').style.display = 'none';
              document.getElementById('no_rek').style.display = 'block';
              document.getElementById('nama_bank').style.display = 'block';
            }else if(id == 3){
              document.getElementById('jatuh_tempo').style.display = 'block';
              document.getElementById('no_rek').style.display = 'block';
              document.getElementById('nama_bank').style.display = 'block';
            }
        }

        function add_detail_new() {
          var i_detail_type     = $('select[name="i_detail_type"]').val();
          var i_nota = $('select[name="i_nota[]"]').val();
          var i_detail_rek      = $('input[name="i_detail_rek"]').val();
          var i_detail_bank     = $('input[name="i_detail_bank"]').val();
          var i_detail_tempo    = $('input[name="i_detail_tempo"]').val();
          var i_detail_nominal  = $('input[name="i_detail_nominal"]').val();
          var id = {!! $payment['payment_id'] !!};

          $.ajax({
              type: 'POST',
              url: '{{ url('home/pembayaran/payment/add-detail-new') }}',
              data: { id:id,i_detail_type:i_detail_type,i_detail_rek:i_detail_rek,i_detail_bank:i_detail_bank,i_detail_tempo:i_detail_tempo,i_detail_nominal:i_detail_nominal, i_nota:i_nota },
              dataType: 'JSON',
              success: function(data) {
                if (data.status == 'success') {
                    swal('Success', data.message, 'success');
                    window.location.reload();
                } else {
                    swal('Oops!', data.message, 'error');
                }
              }
            });
        }
    </script>
@stop