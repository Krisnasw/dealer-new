@extends('template.index')

@section('title')
    Dealer Information System - Buat Payment
@stop

@section('style')

    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}"/>
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}"/>

@stop

@section('content')
    <div class="main-body">
        <div class="page-wrapper">

            <!-- Page-header start -->
            <div class="page-header card">
                <div class="row align-items-end">
                    <div class="col-lg-8">
                        <div class="page-header-title">
                            <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                            <div class="d-inline">
                                <h4>Buat Payment</h4>
                                <span>Buat Payment Baru</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="page-header-breadcrumb">
                            <ul class="breadcrumb-title">
                                <li class="breadcrumb-item">
                                    <a href="{{ url('home/main') }}">
                                        <i class="icofont icofont-home"></i>
                                    </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">Pembelian - Payment</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Page-header end -->

            <div class="page-body">
                <div class="row">
                    <div class="col-sm-12">

                        <!-- Tooltip Validation card start -->
                        <div class="card">
                            <div class="card-header">
                                <h5>Buat Payment Baru</h5>
                                <div class="card-header-right">
                                    <i class="icofont icofont-spinner-alt-5"></i>
                                </div>
                            </div>
                            <div class="card-block">
                                @php $id = Cache::get('customer_id'); @endphp
                                {!! Form::open(['id' => 'second', 'method' => 'POST', 'novalidate' => 'novalidate', 'action' => 'PaymentController@store']) !!}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Customer</label>
                                    <div class="col-sm-4">
                                        <select class="form-control js-example-basic-single select2-hidden-accessible" name="customer_id">
                                            <option selected>- Pilih Customer -</option>
                                            @foreach($cust as $key)
                                                <option value="{!! $key['customer_id'] !!}" {!! ($key['customer_id'] == $id) ? 'selected' : '' !!}>{!! $key['customer_name'] !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    
                                    <label class="col-sm-2 col-form-label">Tanggal Pembayaran</label>
                                    <div class="col-sm-4">
                                        <input type="date" class="form-control" name="date" value="{!! date('Y-m-d') !!}">
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header table-card-header">
                                    </div>
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="basic-btn" class="table table-striped table-bordered nowrap">
                                                <thead>
                                                <tr>
                                                    <th>Cek</th>
                                                    <th>Tanggal</th>
                                                    <th>Kode Detail</th>
                                                    <th>Tagihan</th>
                                                    <th>Potongan</th>
                                                    <th>Terbayar</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @php $grand_total_tagihan = 0; @endphp
                                                @php $grand_potongan = 0; @endphp
                                                @php $grand_total_potongan = 0; @endphp
                                                @foreach($list_detail as $row)
                                                    <tr>
                                                        <td>
                                                            <div class="col-sm-9">
                                                                <label class="cb-checkbox cb-checkbox-dark-2">
                                                                  <input name="i_nota{!! $row['nota_id'] !!}" type="checkbox" value="1">
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>{!! $row['nota_date'] !!}</td>
                                                        <td>{!! $row['nota_code'] !!}</td>
                                                        <td>{!! number_format($row['nota_total'],2) !!}</td>
                                                        <td>{!! number_format(0,2) !!}</td>
                                                        <td>{!! number_format(0,2) !!}</td>
                                                    </tr>
                                                @php $grand_total_tagihan += $row['nota_total']; @endphp
                                                @endforeach
                                                @foreach($list_retur as $row)
                                                    <tr>
                                                        <td>
                                                            <div class="col-sm-9">
                                                                <label class="cb-checkbox cb-checkbox-dark-2">
                                                                    <input name="i_retur{!! $row['retur_id'] !!}" type="checkbox" value="1">
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>{!! $row['retur_date'] !!}</td>
                                                        <td>{!! $row['retur_code'] !!}</td>
                                                        <td>{!! number_format(0,2) !!}</td>
                                                        <td>{!! number_format($row['retur_total'], 2) !!}</td>
                                                        <td>{!! number_format(0,2) !!}</td>
                                                    </tr>
                                                @php $grand_potongan += $row['retur_total']; @endphp
                                                @endforeach
                                                @foreach($list_debit as $row)
                                                @if ($row['debit_type'] == 1)
                                                    @php $debit = $row['debit_nominal']; @endphp
                                                    @php $kredit = 0; @endphp
                                                @elseif ($row['debit_type'] == 2)
                                                    @php $debit = 0; @endphp
                                                    @php $kredit = $row['debit_nominal']; @endphp
                                                @else
                                                    @php $debit = 0; @endphp
                                                    @php $kredit = 0; @endphp
                                                @endif
                                                    <tr>
                                                        <td>
                                                            <div class="col-sm-9">
                                                                <label class="cb-checkbox-dark-2 cb-checkbox">
                                                                    <input type="checkbox" value="1" name="i_debit{!! $row['debit_id'] !!}">
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>{!! $row['debit_date'] !!}</td>
                                                        <td>{!! $row['debit_code'] !!}</td>
                                                        <td>{!! number_format($debit, 0) !!}</td>
                                                        <td>{!! number_format($kredit, 0) !!}</td>
                                                        <td>{!! number_format(0,2) !!}</td>
                                                    </tr>
                                                @php $grand_total_tagihan += $debet; @endphp
                                                @php $grand_potongan += $kredit; @endphp
                                                @endforeach
                                                @foreach($list_entrusted as $row)
                                                    <tr>
                                                        <td>
                                                            <div class="col-sm-9">
                                                                <label class="cb-checkbox-dark-2 cb-checkbox">
                                                                    <input type="checkbox" value="1" name="i_entrusted{!! $row['entrusted_id'] !!}">
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>{!! $row['entrusted_date'] !!}</td>
                                                        <td>{!! $row['entrusted_code'] !!}</td>
                                                        <td>{!! number_format(0,2) !!}</td>
                                                        <td>{!! number_format($row['entrusted_total'], 2) !!}</td>
                                                        <td>{!! number_format(0,2) !!}</td>
                                                    </tr>
                                                @php $grand_potongan += $row['entrusted_total']; @endphp
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <!-- Select 2 js -->
    <script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
    <!-- Multiselect js -->
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>

    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js') }}"></script>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
@stop