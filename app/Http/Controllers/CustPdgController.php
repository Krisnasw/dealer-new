<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Access;
use Alert;
use Validator;
use App\Customer;
use App\CustomerLimit;
use App\City;
use App\Grade;
use App\Brand;

class CustPdgController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,50);

            $this->permit = $akses->permit_acces;
                
            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            # code...
            return view('admin.customer.pending.index');
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (strpos($this->permit, 'c') !== null) {
            # code...
            $city = City::orderBy('city_id', 'asc')->get();
            $grade = Grade::orderBy('grade_id', 'asc')->get();
            return view('admin.customer.pending.create', compact('city', 'grade'));
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'f_customerCode' => 'required',
            'f_nama' => 'required',
            'f_namaToko' => 'required',
            'alamat' => 'required',
            'f_phone' => 'required',
            'f_email' => 'required',
            'f_grade' => 'required',
            'f_noRek' => 'required',
            'f_bank' => 'required',
            'f_npwp' => 'required',
            'f_namaNpwp' => 'required',
            'f_expedisi' => 'required',
            'f_hp' => 'required',
            'f_fax' => 'required',
            'f_wilayah' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Customer::create([
                'customer_code' => $request->input('f_customerCode'),
                'customer_name' => $request->input('f_nama'),
                'customer_phone' => $request->input('f_phone'),
                'customer_addres' => $request->input('alamat'),
                'customer_email' => $request->input('f_email'),
                'customer_npwp' => $request->input('f_npwp'),
                'grade_id' => $request->input('f_grade'),
                'customer_rekening' => $request->input('f_noRek'),
                'customer_id_bank' => $request->input('f_bank'),
                'customer_nama_npwp' => $request->input('f_namaNpwp'),
                'customer_expedisi' => $request->input('f_expedisi'),
                'customer_tempo' => 0,
                'customer_limit' => 0,
                'customer_hp' => $request->input('f_hp'),
                'customer_fax' => $request->input('f_fax'),
                'city_id' => $request->input('f_wilayah'),
                'customer_store' => $request->input('f_namaToko'),
                'customer_latitude' => 0,
                'customer_longitude' => 0,
                'customer_status' => 1
            ]);

            if ($create) {
                # code...
                Alert::success('Customer Berhasil Dibuat', 'Success');
                return redirect('home/master/customer-pending');
            } else {
                Alert::error('Gagal Membuat Customer', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (strpos($this->permit, 'u') !== null) {
            # code...
            $mem = Customer::where('customer_id', base64_decode($id))->orderBy('customer_id', 'ASC')->with('grades')->with('kota')->where('customer_status', 1)->first();
            $limit = Brand::join('customer_limits', 'brands.brand_id', 'customer_limits.brand_id')->where('customer_limits.customer_id', base64_decode($id))->get();
            if (empty($mem)) {
                # code...
                abort(404);
            }
            $city = City::orderBy('city_id', 'asc')->get();
            $grade = Grade::orderBy('grade_id', 'asc')->get();
            $brand = Brand::select('brand_id', 'brand_name')->get();
            return view('admin.customer.pending.edit', compact('mem', 'city', 'grade', 'limit', 'brand'));
        } else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
            'f_customerCode' => 'required',
            'f_nama' => 'required',
            'f_namaToko' => 'required',
            'alamat' => 'required',
            'f_phone' => 'required',
            'f_email' => 'required',
            'f_grade' => 'required',
            'f_noRek' => 'required',
            'f_bank' => 'required',
            'f_npwp' => 'required',
            'f_namaNpwp' => 'required',
            'f_expedisi' => 'required',
            'f_hp' => 'required',
            'f_fax' => 'required',
            'f_wilayah' => 'required',
            'brand_id' => 'required',
            'limit_tagihan' => 'required',
            'jatuh_tempo' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Customer::where('customer_id', base64_decode($id))->update([
                'customer_code' => $request->input('f_customerCode'),
                'customer_name' => $request->input('f_nama'),
                'customer_phone' => $request->input('f_phone'),
                'customer_addres' => $request->input('alamat'),
                'customer_email' => $request->input('f_email'),
                'customer_npwp' => $request->input('f_npwp'),
                'grade_id' => $request->input('f_grade'),
                'customer_rekening' => $request->input('f_noRek'),
                'customer_id_bank' => $request->input('f_bank'),
                'customer_nama_npwp' => $request->input('f_namaNpwp'),
                'customer_expedisi' => $request->input('f_expedisi'),
                'customer_tempo' => 0,
                'customer_limit' => 0,
                'customer_hp' => $request->input('f_hp'),
                'customer_fax' => $request->input('f_fax'),
                'city_id' => $request->input('f_wilayah'),
                'customer_store' => $request->input('f_namaToko'),
                'customer_latitude' => 0,
                'customer_longitude' => 0,
                'customer_status' => 1
            ]);

            $findId = CustomerLimit::where('customer_id', base64_decode($id))->get();

            foreach($findId as $key => $val) {
                $val->customer_id = base64_decode($id);
                $val->brand_id = $request->input('brand_id')[$key];
                $val->customer_limit_bill = $request->input('limit_tagihan')[$key];
                $val->customer_limit_due = $request->input('jatuh_tempo')[$key];
                $val->save();
            }

            if ($create) {
                # code...
                Alert::success('Customer Berhasil Diupdate', 'Success');
                return redirect('home/master/customer-pending');
            } else {
                Alert::error('Gagal Update Customer', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (strpos($this->permit, 'd') !== null) {
            # code...
            $del = Customer::findOrFaiL(base64_decode($id));
            if ($del->delete()) {
                # code...
                Alert::success('Customer Berhasil Dihapus', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Menghapus Customer', 'Error');
                return redirect()->back();
            }
        } else {
            abort(403);
        }
    }
}
