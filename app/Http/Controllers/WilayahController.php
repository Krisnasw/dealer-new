<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Access;
use Alert;
use Validator;
use App\City;
use App\CityDetail;
use App\Customer;

class WilayahController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,51);

            $this->permit = $akses->permit_acces;
                
            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            # code...
            $city = City::orderBy('city_id', 'asc')->get();
            return view('admin.city.index', compact('city'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (strpos($this->permit, 'c') !== null) {
            # code...
            return view('admin.city.create');
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'nama' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = City::create([
                'province_id' => 1,
                'city_name' => $request->input('nama')
            ]);

            if ($create) {
                # code...
                Alert::success('Wilayah Berhasil Ditambahkan', 'Success');
                return redirect('home/setup/wilayah');
            } else {
                Alert::success('Wilayah Gagal Ditambahkan', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (strpos($this->permit, 'u') !== null) {
            # code...
            $city = City::where('city_id', base64_decode($id))->first();
            $cityDet = CityDetail::where('city_id', base64_decode($id))->with('cities')->with('customers')->get();
            $cust = Customer::select('customer_id', 'customer_name')->orderBy('customer_id', 'desc')->get();
            return view('admin.city.edit', compact('city', 'cityDet', 'cust'));
        } else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
            'nama' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = City::where('city_id', base64_decode($id))->update([
                'province_id' => 1,
                'city_name' => $request->input('nama')
            ]);

            if ($create) {
                # code...
                Alert::success('Wilayah Berhasil Diupdate', 'Success');
                return redirect('home/setup/wilayah');
            } else {
                Alert::success('Wilayah Gagal Diupdate', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (strpos($this->permit, 'd') !== null) {
            # code...
            $del = City::findOrFail(base64_decode($id));
            if ($del->delete()) {
                # code...
                Alert::success('Wilayah Berhasil Dihapus', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Wilayah Gagal Dihapus', 'Error');
                return redirect()->back();
            }
        } else {
            abort(403);
        }
    }
}
