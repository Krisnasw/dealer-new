<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Alert;
use Validator;
use Access;
use App\Item;
use App\Brand;
use App\BrandDetail;
use App\Character;
use App\ItemSubstitude;
use App\ItemCharacter;

class BarangController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,40);

            $this->permit = $akses->permit_acces;
                
            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            # code...
            return view('admin.barang.index');
        } else {
            abort(403, 'Unauthorized Access.');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (strpos($this->permit, 'c') !== null) {
            # code...
            $brand = Brand::select('brand_id', 'brand_name')->get();
            $brandDetail = BrandDetail::select('brand_detail_id', 'brand_detail_name')->get();
            $character = Character::select('character_id', 'character_code')->get();
            $detailChar = Character::select('character_id', 'character_seri')->get();
            return view('admin.barang.create', compact('brand', 'brandDetail', 'character', 'detailChar'));
        } else {
            abort(403, 'Unauthorized Access.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'f_numberPart' => 'required',
            'f_het' => 'required',
            'f_namaBarang' => 'required',
            'f_hargaPokok' => 'required',
            'f_merk' => 'required',
            'f_hargaBeli' => 'required',
            'f_kodePart' => 'required',
            'f_stokBarang' => 'required',
            'f_motorFix' => 'required',
            'f_minStok' => 'required',
            'f_motor' => 'required',
            'f_maxStok' => 'required',
            'f_satuan' => 'required',
            'f_barcode' => 'required',
            'f_berat' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $item = Item::create([
                'item_code' => $request->input('f_numberPart'),
                'brand_detail_id' => $request->input('f_kodePart'),
                'item_name' => $request->input('f_namaBarang'),
                'unit_id' => $request->input('f_satuan'),
                'character_id' => implode(',', $request->input('f_motor')),
                'item_het' => $request->input('f_het'),
                'item_price_primary' => $request->input('f_hargaPokok'),
                'item_price_buy' => $request->input('f_hargaBeli'),
                'item_stock' => $request->input('f_stokBarang'),
                'item_stock_min' => $request->input('f_minStok'),
                'item_stock_max' => $request->input('f_maxStok'),
                'item_barcode' => $request->input('f_barcode'),
                'item_weight' => $request->input('f_berat'),
                'character_fix' => $request->input('f_motorFix'),
                'item_status' => 0
            ]);

            $arrLength = count($request->input('f_motor'));

            ItemCharacter::where('item_id', $item->item_id)->delete();
            for($x = 0; $x < $arrLength; $x++) {
                ItemCharacter::create([
                    'item_id' => $item->item_id,
                    'item_package_id' => 0,
                    'character_id' => $request->input('f_motor')[$x]
                ]);
            }

            if ($item) {
                # code...
                Alert::success('Barang Berhasil Dibuat', 'Success');
                return redirect('home/master/barang');
            } else {
                Alert::error('Gagal Membuat Barang Baru', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (strpos($this->permit, 'u')) {
            # code...
            $item = Item::where('item_id', base64_decode($id))->first();
            if (empty($item)) {
                # code...
                abort(404);
            }
            $brand = Brand::select('brand_id', 'brand_name')->get();
            $brandDetail = BrandDetail::select('brand_detail_id', 'brand_detail_name')->get();
            $character = Character::select('character_id', 'character_code')->get();
            $detailChar = Character::select('character_id', 'character_seri')->get();
            $barang = ItemSubstitude::select('item_subtitues.*', 'items.item_name', 'items.item_code')->join('items', 'items.item_id', 'item_subtitues.item_detail_id')->where('item_subtitues.item_id', base64_decode($id))->where('item_subtitu_status', 0)->get();
            return view('admin.barang.edit', compact('item', 'brand', 'brandDetail', 'character', 'detailChar', 'barang'));
        } else {
            abort(403, 'Unauthorized Access.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
            'f_numberPart' => 'required',
            'f_het' => 'required',
            'f_namaBarang' => 'required',
            'f_hargaPokok' => 'required',
            'f_merk' => 'required',
            'f_hargaBeli' => 'required',
            'f_kodePart' => 'required',
            'f_stokBarang' => 'required',
            'f_motorFix' => 'required',
            'f_minStok' => 'required',
            'f_motor' => 'required',
            'f_maxStok' => 'required',
            'f_satuan' => 'required',
            'f_barcode' => 'required',
            'f_berat' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $item = Item::where('item_id', base64_decode($id))->update([
                'item_code' => $request->input('f_numberPart'),
                'brand_detail_id' => $request->input('f_kodePart'),
                'item_name' => $request->input('f_namaBarang'),
                'unit_id' => $request->input('f_satuan'),
                'character_id' => implode(',', $request->input('f_motor')),
                'item_het' => $request->input('f_het'),
                'item_price_primary' => $request->input('f_hargaPokok'),
                'item_price_buy' => $request->input('f_hargaBeli'),
                'item_stock' => $request->input('f_stokBarang'),
                'item_stock_min' => $request->input('f_minStok'),
                'item_stock_max' => $request->input('f_maxStok'),
                'item_barcode' => $request->input('f_barcode'),
                'item_weight' => $request->input('f_berat'),
                'character_fix' => $request->input('f_motorFix'),
                'item_status' => 0
            ]);

            $arrLength = count($request->input('f_motor'));

            ItemCharacter::where('item_id', base64_decode($id))->delete();
            for($x = 0; $x < $arrLength; $x++) {
                ItemCharacter::create([
                    'item_id' => base64_decode($id),
                    'item_package_id' => 0,
                    'character_id' => $request->input('f_motor')[$x]
                ]);
            }

            if ($item) {
                # code...
                Alert::success('Barang Berhasil Diupdate', 'Success');
                return redirect('home/master/barang');
            } else {
                Alert::error('Gagal Update Barang', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (strpos($this->permit, 'd') !== null) {
            # code...
            $del = Item::findOrFail(base64_decode($id));
            if ($del->delete()) {
                # code...
                Alert::success('Barang Berhasil Dihapus', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Menghapus Barang', 'Error');
                return redirect()->back();
            }
        } else {
            abort(403, 'Unauthorized Access.');
        }
    }
}
