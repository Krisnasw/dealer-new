<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Access;
use Auth;
use Alert;
use Validator;
use App\Nota;
use App\Brand;
use App\Customer;
use App\Sales;
use App\Payment;
use App\PaymentSup;
use App\Entrusted;
use App\SalesCity;
use App\SalesCost;
use App\City;
use App\Coa;
use App\BrandDetail;
use App\Supplier;
use App\Operational;
use App\Purchase;
use App\Debt;
use App\Credit;
use Illuminate\Support\Facades\Input;
use Excel;

class LaporanController extends Controller
{
    //
    var $permit;
    protected $user;

    function __construct()
    {
    	$this->middleware('auth');
    }

    public function index() 
    {
    	return view('admin.report.index', compact('merk', 'kota'));
    }

    public function indexSupplier()
    {
        $merk = Brand::select('brand_id', 'brand_name')->get();
        $sales = Sales::select('sales_id', 'sales_name')->get();
        return view('admin.report.supplier.index', compact('merk', 'sales'));
    }

    public function indexBukuBesar()
    {
        return view('admin.buku.index');
    }

    public function indexHutang()
    {
        $sup = Supplier::select('supplier_id', 'supplier_name')->get();
        return view('admin.hutang.index', compact('sup'));
    }

    public function indexPiutang()
    {
        return view('admin.piutang.index');
    }

    public function indexKomisi()
    {
        $sales = Sales::select('sales_id', 'sales_name')->get();
        $merk = Brand::select('brand_id', 'brand_name')->get();
        return view('admin.komisi.index', compact('sales', 'merk'));
    }

    public function indexBiayaSales()
    {
        return view('admin.biaya-sales.index');
    }

    public function viewBiayaSales($month)
    {
        $nota = Sales::select('sales.sales_id', 'sales.sales_name', 'b.*', 'c.*')
                        ->join('notas as b', 'b.sales_id', 'sales.sales_id')
                        ->join('sales_costs as c', 'c.sales_id', 'sales.sales_id')
                        ->whereMonth('b.nota_date', $month)
                        ->whereMonth('c.sales_cost_start', $month)
                        ->groupBy('sales.sales_name')
                        ->get();

        $merk = Brand::select('brand_id', 'brand_name')->get();

        return view('admin.biaya-sales.show', compact('merk', 'nota', 'month'));
    }

    public function viewKomisi($month, $year, $sales)
    {
        $nota = Nota::select('notas.*', 'b.payment_date', 'c.customer_name', 'd.payment_detail_status')
                        ->join('payment_details as d', 'd.nota_id', 'notas.nota_id')
                        ->join('payments as b', 'b.payment_id', 'd.payment_id')
                        ->join('customers as c', 'c.customer_id', 'notas.customer_id')
                        ->where('notas.sales_id', $sales)
                        ->whereMonth('b.payment_date', $month)
                        ->where('notas.brand_id', $year)
                        ->get();

        $merk = Brand::select('brand_id', 'brand_name')->get();

        return view('admin.komisi.show', compact('nota', 'merk'));
    }

    public function exportKomisi(Request $request)
    {
        $month = $request->bulan;
        $year = $request->tahun;
        $sales = $request->sales;

        $user = Sales::where('sales_id', $sales)->first();

        $nota = Nota::select('notas.*', 'b.payment_date', 'c.customer_name')
                        ->join('payment_details as d', 'd.nota_id', 'notas.nota_id')
                        ->join('payments as b', 'b.payment_id', 'd.payment_id')
                        ->join('customers as c', 'c.customer_id', 'notas.customer_id')
                        ->where('notas.sales_id', $sales)
                        ->whereMonth('b.payment_date', $month)
                        ->where('notas.brand_id', $year)
                        ->get();

        $excel = Excel::create('Laporan Komisi Sales - '.$month.' '.$year, function ($excel) use ($nota, $month, $year, $user) {

            $excel->sheet('Laporan 1', function ($sheet) use ($nota, $month, $year, $user) {
                $sheet->loadView('export.komisi', compact('nota', 'month', 'year', 'user'));
            });
            
        })->export('xls');
    }

    public function viewPiutang($month, $year)
    {
        $nota = Credit::select('credits.*', 'b.customer_name', 'c.*')
                        ->join('customers as b', 'b.customer_id', 'credits.customer_id')
                        ->join('notas as c', 'c.nota_id', 'credits.nota_id')
                        ->whereMonth('credits.credit_date', $month)->whereYear('credits.credit_date', $year)->get();

        return view('admin.piutang.show', compact('month', 'year', 'nota'));
    }

    public function exportPiutang(Request $request)
    {
        $month = $request->bulan;
        $year = $request->tahun;

        $nota = Credit::select('credits.*', 'b.customer_name', 'c.*')
                        ->join('customers as b', 'b.customer_id', 'credits.customer_id')
                        ->join('notas as c', 'c.nota_id', 'credits.nota_id')
                        ->whereMonth('credits.credit_date', $month)->whereYear('credits.credit_date', $year)->get();
                        
        $excel = Excel::create('Laporan Piutang - '.$month.' '.$year, function ($excel) use ($nota, $month, $year) {

            $excel->sheet('Laporan 1', function ($sheet) use ($nota, $month, $year) {
                $sheet->loadView('export.piutang', compact('nota', 'month', 'year'));
            });
            
        })->export('xls');
    }

    public function viewHutang($month, $year, $supplier)
    {
        $sup = Supplier::where('supplier_id', $supplier)->first();
        $nota = Debt::select('debts.*', 'b.receipt_code', 'c.purchase_netto', 'd.supplier_name')
                    ->join('receipts as b', 'b.receipt_id', 'debts.receipt_id')
                    ->join('purchases as c', 'c.purchase_id', 'b.purchase_id')
                    ->join('suppliers as d', 'd.supplier_id', 'c.supplier_id')
                    ->whereMonth('debts.debt_date', $month)
                    ->whereYear('debts.debt_date', $year)
                    ->where('c.supplier_id', $sup['supplier_id'])
                    ->get();
        return view('admin.hutang.show', compact('month', 'year', 'nota', 'sup'));
    }

    public function exportHutang(Request $request)
    {
        $month = $request->bulan;
        $year = $request->tahun;
        $supplier = $request->supplier;

        $sup = Supplier::where('supplier_id', $supplier)->first();
        $nota = Debt::select('debts.*', 'b.receipt_code', 'c.purchase_netto', 'd.supplier_name')
                    ->join('receipts as b', 'b.receipt_id', 'debts.receipt_id')
                    ->join('purchases as c', 'c.purchase_id', 'b.purchase_id')
                    ->join('suppliers as d', 'd.supplier_id', 'c.supplier_id')
                    ->whereMonth('debts.debt_date', $month)
                    ->whereYear('debts.debt_date', $year)
                    ->where('c.supplier_id', $sup['supplier_id'])
                    ->get();

        $excel = Excel::create('Laporan Buku Besar Pembantu Hutang Periode - '.$month.' '.$year.' Supplier - '.$sup['supplier_name'], function ($excel) use ($supplier, $month, $year, $sup, $nota) {

            $excel->sheet('Laporan 1', function ($sheet) use ($supplier, $month, $year, $sup, $nota) {
                $sheet->loadView('export.hutang', compact('supplier', 'month', 'year', 'sup', 'nota'));
            });

        })->export('xls');
    }

    public function viewXLS($month, $year)
    {
        $nota = Nota::whereMonth('nota_date', $month)->whereYear('nota_date', $year)->where('nota_type_pay', 0)->get();
        $payment = Payment::whereMonth('payment_date', $month)->whereYear('payment_date', $year)->get();
        $entrusted = Entrusted::whereMonth('entrusted_date', $month)->whereYear('entrusted_date', $year)->get();
        $cost = SalesCost::whereMonth('sales_cost_start', $month)->whereYear('sales_cost_start', $year)->get();
        $payment_sup = PaymentSup::whereMonth('payment_sup_date', $month)->whereYear('payment_sup_date', $year)->get();
        $operational = Operational::whereMonth('oprational_date', $month)->whereYear('oprational_date', $year)->get();

        $coaNota = Coa::where('side_menu_id', 52)->first();
        $coaPayment = Coa::where('side_menu_id', 58)->first();
        $coaTitip = Coa::where('side_menu_id', 65)->first();
        $coaCost = Coa::where('side_menu_id', 67)->first();
        $coaSup = Coa::where('side_menu_id', 69)->first();
        $coaOperasional = Coa::where('side_menu_id', 77)->first();

        return view('admin.buku.show', compact('month', 'year', 'nota', 'payment', 'entrusted', 'cost', 'payment_sup', 'operational', 'coaPayment', 'coaNota', 'coaTitip', 'coaCost', 'coaSup', 'coaOperasional'));
    }

    public function exportXLS(Request $request)
    {
        $month = $request->bulan;
        $year = $request->tahun;

        $nota = Nota::whereMonth('nota_date', $month)->whereYear('nota_date', $year)->where('nota_type_pay', 0)->get();
        $payment = Payment::whereMonth('payment_date', $month)->whereYear('payment_date', $year)->get();
        $entrusted = Entrusted::whereMonth('entrusted_date', $month)->whereYear('entrusted_date', $year)->get();
        $cost = SalesCost::whereMonth('sales_cost_start', $month)->whereYear('sales_cost_start', $year)->get();
        $payment_sup = PaymentSup::whereMonth('payment_sup_date', $month)->whereYear('payment_sup_date', $year)->get();
        $operational = Operational::whereMonth('oprational_date', $month)->whereYear('oprational_date', $year)->get();
        $coaNota = Coa::where('side_menu_id', 52)->first();
        $coaPayment = Coa::where('side_menu_id', 58)->first();
        $coaTitip = Coa::where('side_menu_id', 65)->first();
        $coaCost = Coa::where('side_menu_id', 67)->first();
        $coaSup = Coa::where('side_menu_id', 69)->first();
        $coaOperasional = Coa::where('side_menu_id', 77)->first();

        $excel = Excel::create('Laporan Buku Besar Periode - '.$request->bulan.' '.$request->tahun, function($excel) use($month, $year, $nota, $payment, $entrusted, $cost, $payment_sup, $operational, $coaNota, $coaPayment, $coaTitip, $coaCost, $coaSup, $coaOperasional) {

          $excel->sheet('Laporan 1', function($sheet) use($month, $year, $nota, $payment, $entrusted, $cost, $payment_sup, $operational, $coaNota, $coaPayment, $coaTitip, $coaCost, $coaSup, $coaOperasional) {

              $sheet->loadView('export.excel', compact('month', 'year', 'nota', 'payment', 'entrusted', 'cost', 'payment_sup', 'operational', 'coaPayment', 'coaNota', 'coaTitip', 'coaCost', 'coaSup', 'coaOperasional'));

          });

      })->export('xls');

      return $excel;
    }

    public function searchSupplier()
    {
        $t1 = Input::get('type_one');
        $t2 = Input::get('type_two');
        $o1 = Input::get('opsi_one');
        $o2 = Input::get('opsi_two');

        if ($t1 == 1 && $t2 == 1) {
            # code...
            Alert::info('Opsi Tidak Boleh Sama', 'Info');
            return redirect()->back();
        } elseif ($t1 == 1 && $t2 != 2 || $t2 != 3) {
            $data = Nota::select('notas.nota_id', 'notas.nota_code', 'notas.nota_date', 'notas.nota_total', 'b.sales_name', 'c.customer_name', 'd.brand_name')
                    ->join('sales as b', 'b.sales_id', 'notas.sales_id')
                    ->join('customers as c', 'c.customer_id', 'notas.customer_id')
                    ->join('brands as d', 'd.brand_id', 'notas.brand_id')
                    ->whereDate('notas.nota_date', '<=', Input::get('startDate'))
                    ->whereDate('notas.nota_date', '<=', Input::get('finishDate'))
                    ->where('notas.brand_id', $o1)->get();
        } elseif ($t1 == 1 && $t2 == 3 || $t1 == 3 && $t2 == 1) {
            Alert::info('Tidak Ada Kolom Ini Di Nota', 'Info');
            return redirect()->back();
        } elseif ($t1 == 4 && $t2 == 4) {
            $data = Nota::select('notas.nota_id', 'notas.nota_code', 'notas.nota_date', 'notas.nota_total', 'b.sales_name', 'c.customer_name', 'd.brand_name')
                    ->join('sales as b', 'b.sales_id', 'notas.sales_id')
                    ->join('customers as c', 'c.customer_id', 'notas.customer_id')
                    ->join('brands as d', 'd.brand_id', 'notas.brand_id')
                    ->whereDate('notas.nota_date', '<=', Input::get('startDate'))
                    ->whereDate('notas.nota_date', '<=', Input::get('finishDate'))
                    ->where('notas.sales_id', $o2)
                    ->where('notas.brand_id', $o1)->get();
        }

        return view('admin.report.supplier.show', compact('data'));
    }

    public function show()
    {
        $t1 = Input::get('type_one');
        $t2 = Input::get('type_two');
        $o1 = Input::get('opsi_one');
        $o2 = Input::get('opsi_two');

        if ($t1 == 1 && $t2 == 1) {
            # code...
            Alert::info('Opsi Tidak Boleh Sama', 'Info');
            return redirect()->back();
        } elseif ($t1 == 1 && $t2 == 2 || $t1 == 2 && $t2 == 1) {
            # code...
            $data = Nota::select('notas.nota_id', 'notas.nota_code', 'notas.nota_date', 'notas.nota_total', 'b.sales_name', 'c.customer_name', 'd.brand_name')
                    ->join('sales as b', 'b.sales_id', 'notas.sales_id')
                    ->join('customers as c', 'c.customer_id', 'notas.customer_id')
                    ->join('brands as d', 'd.brand_id', 'notas.brand_id')
                    ->whereBetween(\DB::raw('DATE(notas.nota_date)'), array(Input::get('startDate'), Input::get('finishDate')))
                    ->where('notas.brand_id', $o1)->get();
        } elseif ($t1 == 1 && $t2 == 3 || $t1 == 3 && $t2 == 1) {
            $data = Nota::select('notas.nota_id', 'notas.nota_code', 'notas.nota_date', 'notas.nota_total', 'b.sales_name', 'c.customer_name', 'd.brand_name')
                    ->join('sales as b', 'b.sales_id', 'notas.sales_id')
                    ->join('customers as c', 'c.customer_id', 'notas.customer_id')
                    ->join('brands as d', 'd.brand_id', 'notas.brand_id')
                    ->whereBetween(\DB::raw('DATE(notas.nota_date)'), array(Input::get('startDate'), Input::get('finishDate')))
                    ->where('notas.customer_id', $o2)
                    ->where('notas.brand_id', $o1)->get();
        } elseif ($t1 == 1 && $t2 == 4 || $t1 == 4 && $t2 == 1) {
            $data = Nota::select('notas.nota_id', 'notas.nota_code', 'notas.nota_date', 'notas.nota_total', 'b.sales_name', 'c.customer_name', 'd.brand_name')
                    ->join('sales as b', 'b.sales_id', 'notas.sales_id')
                    ->join('customers as c', 'c.customer_id', 'notas.customer_id')
                    ->join('brands as d', 'd.brand_id', 'notas.brand_id')
                    ->whereBetween(\DB::raw('DATE(notas.nota_date)'), array(Input::get('startDate'), Input::get('finishDate')))
                    ->where('notas.sales_id', $o2)
                    ->where('notas.brand_id', $o1)->get();
        } elseif ($t1 == 1 && $t2 == 5 || $t1 == 5 && $t2 == 1) {
            Alert::info('Tidak Ada Filter Wilayah', 'Info');
            return redirect()->back();
        } elseif ($t1 == 2 && $t2 == 2) {
            Alert::info('Opsi Tidak Boleh Sama', 'Info');
            return redirect()->back();
        } elseif ($t1 == 2 && $t2 == 3 || $t1 == 3 && $t2 == 2) {
            $data = Nota::select('notas.nota_id', 'notas.nota_code', 'notas.nota_date', 'notas.nota_total', 'b.sales_name', 'c.customer_name', 'd.brand_name')
                    ->join('sales as b', 'b.sales_id', 'notas.sales_id')
                    ->join('customers as c', 'c.customer_id', 'notas.customer_id')
                    ->join('brands as d', 'd.brand_id', 'notas.brand_id')
                    ->whereBetween(\DB::raw('DATE(notas.nota_date)'), array(Input::get('startDate'), Input::get('finishDate')))
                    ->where('notas.customer_id', $o2)->get();
        } elseif ($t1 == 2 && $t2 == 4 || $t1 == 4 && $t2 == 2) {
            $data = Nota::select('notas.nota_id', 'notas.nota_code', 'notas.nota_date', 'notas.nota_total', 'b.sales_name', 'c.customer_name', 'd.brand_name')
                    ->join('sales as b', 'b.sales_id', 'notas.sales_id')
                    ->join('customers as c', 'c.customer_id', 'notas.customer_id')
                    ->join('brands as d', 'd.brand_id', 'notas.brand_id')
                    ->whereBetween(\DB::raw('DATE(notas.nota_date)'), array(Input::get('startDate'), Input::get('finishDate')))
                    ->where('notas.sales_id', $o1)->get();
        } elseif ($t1 == 2 && $t2 == 5 || $t1 == 5 && $t2 == 2) {
            Alert::info('Tidak Ada Filter Wilayah', 'Info');
            return redirect()->back();
        } elseif ($t1 == 3 && $t2 == 3) {
            Alert::info('Opsi Tidak Boleh Sama', 'Info');
            return redirect()->back();
        } elseif ($t1 == 3 && $t2 == 4 || $t1 == 4 && $t2 == 3) {
            $data = Nota::select('notas.nota_id', 'notas.nota_code', 'notas.nota_date', 'notas.nota_total', 'b.sales_name', 'c.customer_name', 'd.brand_name')
                    ->join('sales as b', 'b.sales_id', 'notas.sales_id')
                    ->join('customers as c', 'c.customer_id', 'notas.customer_id')
                    ->join('brands as d', 'd.brand_id', 'notas.brand_id')
                    ->whereBetween(\DB::raw('DATE(notas.nota_date)'), array(Input::get('startDate'), Input::get('finishDate')))
                    ->where('notas.sales_id', $o2)
                    ->where('notas.customer_id', $o1)->get();
        } elseif ($t1 == 3 && $t2 == 5 || $t1 == 5 && $t2 == 3) {
            Alert::info('Tidak Ada Filter Wilayah', 'Info');
            return redirect()->back();
        } elseif ($t1 == 4 && $t2 == 4) {
            Alert::info('Opsi Tidak Boleh Sama', 'Info');
            return redirect()->back();
        } elseif ($t1 == 4 && $t2 == 5 || $t1 == 5 && $t2 == 4) {
            Alert::info('Tidak Ada Filter Wilayah', 'Info');
            return redirect()->back();
        } elseif ($t1 == 5 && $t2 == 5) {
            Alert::info('Opsi Tidak Boleh Sama', 'Info');
            return redirect()->back();
        }

        return view('admin.report.show', compact('data'));
    }

    public function getBrand(Request $request)
    {
        $brand = Brand::select('brand_id', 'brand_name')->get();
        $data = "<option value='0'>-- Pilih Merk --</option>";

        foreach ($brand as $row) {
            $data .= "<option value=".$row['brand_id'].">".$row['brand_name']."</option>";
        }

        $response['type'] = 'spbe';
        $response['content'] = $data;
        $response['param'] = '';
        return response($response);
    }

    public function getCustomer(Request $request)
    {
        $cust = Customer::select('customer_id', 'customer_name')->where('customer_name', '!=', ' ')->get();
        $data = "<option value='0'>-- Pilih Customer --</option>";

        foreach ($cust as $row) {
            $data .= "<option value=".$row['customer_id'].">".$row['customer_name']."</option>";
        }

        $response['type'] = 'spbe';
        $response['content'] = $data;
        $response['param'] = '';
        return response($response);
    }

    public function getSupplier(Request $request)
    {
        $this->validate($request, ['id' => 'required']);

        $list = Supplier::where('brand_id', $request->id)->get();
        $data = "<option value='0'>-- Pilih Supplier --</option>";

        foreach ($list as $row) {
            $data .= "<option value=".$row['supplier_id'].">".$row['supplier_name']."</option>";
        }

        $response['type']       = 'spbe';
        $response['content']    = $data;
        $response['param']      = '';
        return response($response);
    }

    public function getKodePart(Request $request)
    {
        $purchase = BrandDetail::select('brand_detail_id', 'brand_detail_name')->get();
        $data = "<option value='0'>-- Pilih Kode Part --</option>";

        foreach ($purchase as $row) {
            $data .= "<option value=".$row['brand_detail_id'].">".$row['brand_detail_name']."</option>";
        }

        $response['type']       = 'spbe';
        $response['content']    = $data;
        $response['param']      = '';
        return response($response);
    }

    public function getSales(Request $request)
    {
        $sales = Sales::select('sales_id', 'sales_name')->get();
        $data = "<option value='0'>-- Pilih Sales --</option>";

        foreach ($sales as $row) {
            $data .= "<option value=".$row['sales_id'].">".$row['sales_name']."</option>";
        }

        $response['type'] = 'spbe';
        $response['content'] = $data;
        $response['param'] = '';
        return response($response);
    }

    public function getCity(Request $request)
    {
        $sales = City::select('city_id', 'city_name')->get();
        $data = "<option value='0'>-- Pilih Wilayah --</option>";

        foreach ($sales as $row) {
            $data .= "<option value=".$row['city_id'].">".$row['city_name']."</option>";
        }

        $response['type'] = 'spbe';
        $response['content'] = $data;
        $response['param'] = '';
        return response($response);
    }
}