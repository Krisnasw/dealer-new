<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Access;
use Alert;
use Validator;
use App\Entrusted;
use App\Customer;

class EntrustedController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,52);

            $this->permit = $akses->permit_acces;

            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            # code...
            $list = Entrusted::orderBy('entrusted_id', 'desc')->get();
            return view('admin.entrusted.index', compact('list'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (strpos($this->permit, 'c') !== null) {
            # code...
            $cust = Customer::orderBy('customer_id', 'desc')->get();
            return view('admin.entrusted.create', compact('cust'));
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'customer_id' => 'required',
            'nominal' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Entrusted::create([
                'entrusted_date' => date('Y-m-d'),
                'entrusted_total' => $request->nominal,
                'customer_id' => $request->customer_id,
                'entrusted_status' => 0,
                'entrusted_code' => $this->format_code()
            ]);

            if ($create) {
                # code...
                Alert::success('Berhasil Menitipkan Uang', 'Success');
                return redirect('home/pembayaran/entrusted');
            } else {
                Alert::error('Gagal Menitipkan Uang', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (strpos($this->permit, 'u') !== null) {
            # code...
            $cust = Customer::orderBy('customer_id', 'desc')->get();
            $entrusted = Entrusted::where('entrusted_id', base64_decode($id))->first();
            return view('admin.entrusted.edit', compact('entrusted', 'cust'));
        } else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
            'customer_id' => 'required',
            'nominal' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Entrusted::where('entrusted_id', base64_decode($id))->update([
                'entrusted_total' => $request->nominal,
                'customer_id' => $request->customer_id,
                'entrusted_status' => 0
            ]);

            if ($create) {
                # code...
                Alert::success('Berhasil Update Uang', 'Success');
                return redirect('home/pembayaran/entrusted');
            } else {
                Alert::error('Gagal Update Uang', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (strpos($this->permit, 'd') !== null) {
            # code...
            $del = Entrusted::findOrFail(base64_decode($id));
            if ($del->delete()) {
                # code...
                Alert::success('Uang Titipan Berhasil Dihapus', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Menghapus Uang Titipan', 'Error');
                return redirect()->back();
            }
        } else {
            abort(403);
        }
    }

    protected function format_code()
    {
        $code = '';
        $year =date('y');

        switch (date('m')) {
            case '01': $month = "A"; break;
            case '02': $month = "B"; break;
            case '03': $month = "C"; break;
            case '04': $month = "D"; break;
            case '05': $month = "E"; break;
            case '06': $month = "F"; break;
            case '07': $month = "G"; break;
            case '08': $month = "H"; break;
            case '09': $month = "I"; break;
            case '10': $month = "J"; break;
            case '11': $month = "K"; break;
            case '12': $month = "L"; break;
        }

        $likes = 'TU';
        $findMemo = Entrusted::select('entrusted_code')->where('entrusted_code', 'LIKE', '%'.$likes.'%')->orderBy('entrusted_id', 'desc')->first();
        $count = Entrusted::select('entrusted_code')->where('entrusted_code', 'LIKE', '%'.$likes.'%')->orderBy('entrusted_id', 'desc')->count();

        $number = 0;

        if ($count <= 0) {
            $numbers = sprintf("%0" . 4 . "d", $number + 1);
            $code = 'TU'.$numbers;
            return $code;
        }

        $numbers = intval(substr($findMemo->entrusted_code, -4));
        $number = sprintf("%0" . 4 . "d", $numbers + 1);
        $code = 'TU'.$number;
        return $code;
    }
}
