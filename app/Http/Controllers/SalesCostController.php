<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Access;
use Alert;
use Validator;
use App\SalesCost;
use App\Sales;
use App\Operational;
use App\Cost;
use App\SalesCostDetail;

class SalesCostController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,67);

            $this->permit = $akses->permit_acces;

            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            # code...
            $list = SalesCost::orderBy('sales_cost_id', 'desc')->get();
            return view('admin.sales-cost.index', compact('list'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (strpos($this->permit, 'c') !== null) {
            # code...
            $data = array();
            $data['row_id'] = '';
            $data['sales_cost_modal'] = 0;
            $data['sales_cost_modal_less'] = 0;
            $data['sales_id'] = '';
            $data['sales_cost_desc'] = '';
            $data['sales_cost_total'] = 0;
            $data['sales_cost_period'] = '';
            $data['sales_cost_lock'] = '';
            $sales = Sales::orderBy('sales_id', 'desc')->get();
            return view('admin.sales-cost.create', compact('sales', 'data'));
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'sales' => 'required',
            'keterangan' => 'required',
            'daterange' => 'required',
            'modal_awal' => 'required',
            'i_total' => 'required',
            'i_selisih' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $date = $request->input('daterange');
            $date = explode('-', $date);
            $date1 = $date[0];
            $date1 = explode("/", $date1);
            $date1 = $date1[2]."-".$date1[0]."-".$date1[1];
            $date1 = str_replace(" ", "", $date1);

            $date2 = $date[1];
            $date2 = explode("/", $date2);
            $date2 = $date2[2]."-".$date2[0]."-".$date2[1];
            $date2 = str_replace(" ", "", $date2);

            $create = SalesCost::create([
                'sales_cost_start' => $date1,
                'sales_cost_finish' => $date2,
                'sales_cost_modal' => $request->modal_awal,
                'sales_cost_desc' => $request->keterangan,
                'sales_id' => $request->sales
            ]);

            $saldo = Operational::select('oprational_saldo')->orderBy('oprational_id', 'desc')->first();

            Operational::create([
                'oprational_date' => date('Y-m-d'),
                'oprational_nominal' => $request->modal_awal,
                'oprational_type' => 2,
                'oprational_desc' => 'Biaya Keliling - '.$request->keterangan,
                'oprational_saldo' => (int)($saldo['oprational_saldo'] - $request->modal_awal)
            ]);

            if ($create) {
                # code...
                return redirect('home/pembayaran/sales-cost/'.base64_encode($create->sales_cost_id));
            } else {
                Alert::error('Gagal Memproses Data', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            # code...
            $result = SalesCost::where('sales_cost_id', base64_decode($id))->first();
            $data = $result;
            $data['row_id'] = $data['sales_cost_id'];

            $date1 = explode("-", $result['sales_cost_start']);
            $date1 = $date1[1]."/".$date1[2]."/".$date1[0];

            $date2 = explode("-", $result['sales_cost_finish']);
            $date2 = $date2[1]."/".$date2[2]."/".$date2[0];

            $data['sales_cost_period']              = $date1." - ".$date2;

            $cost = Cost::orderBy('cost_id', 'desc')->get();
            $sales = Sales::orderBy('sales_id', 'desc')->get();
            $list_detail = SalesCostDetail::select('sales_cost_details.*', 'b.cost_name', 'c.sales_name')
                                        ->join('costs as b', 'b.cost_id', 'sales_cost_details.cost_id')
                                        ->join('sales as c', 'c.sales_id', 'sales_cost_details.sales_id')
                                        ->where('sales_cost_details.sales_id', $result['sales_id'])
                                        ->where('sales_cost_details.sales_cost_detail_date', '>=', $result['sales_cost_start'])
                                        ->where('sales_cost_details.sales_cost_detail_date', '<=', $result['sales_cost_finish'])
                                        ->get();

            return view('admin.sales-cost.show', compact('sales', 'cost', 'list_detail', 'data', 'result'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (strpos($this->permit, 'd') !== null) {
            # code...
            $del = SalesCost::findOrFail(base64_decode($id));
            if ($del->delete()) {
                # code...
                Alert::success('Biaya Keliling Berhasil Dihapus', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Menghapus Biaya Keliling', 'Error');
                return redirect()->back();
            }
        } else {
            abort(403);
        }
    }

    public function createDetail(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'id' => 'required',
            'i_detail_sales' => 'required',
            'i_cost_id' => 'required',
            'i_detail_date' => 'required',
            'i_detail_total' => 'required',
            'i_detail_desc' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => 'error', 'message' => 'Form Tidak Lengkap'], 200);
        } else {
            $create = SalesCostDetail::create([
                'sales_cost_id' => $request->id,
                'cost_id' => $request->i_cost_id,
                'sales_id' => $request->i_detail_sales,
                'sales_cost_detail_date' => $request->i_detail_date,
                'sales_cost_detail_total' => $request->i_detail_total,
                'sales_cost_detail_desc' => $request->i_detail_desc
            ]);

            if ($create) {
                # code...
                return response(['status' => 'success', 'message' => 'Detail Berhasil Ditambahkan'], 200);
            } else {
                return response(['status' => 'error', 'message' => 'Gagal Membuat Detail'], 200);
            }
        }
    }

    public function updateStatusDetail(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'id' => 'required',
            'status' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => 'error', 'message' => 'Form Tidak Lengkap'], 200);
        } else {
            $update = SalesCostDetail::where('sales_cost_detail_id', $request->id)->update([
                'sales_cost_detail_status' => $request->status
            ]);

            if ($update) {
                # code...
                return response(['status' => 'success']);
            } else {
                return response(['status' => 'error']);
            }
        }
    }

    public function lockData(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'id' => 'required',
            'less' => 'required',
            'selisih' => 'required',
            'total' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => 'error', 'message' => 'Form Tidak Lengkap'], 200);
        } else {

            $less = $request->less;
            $selisih = $request->selisih;

            $update = SalesCost::where('sales_cost_id', $request->id)->update([
                'sales_cost_modal_less' => $request->less,
                'sales_cost_total' => $request->total
            ]);

            $result = SalesCost::where('sales_cost_id', $request->id)->first();

            if ($less) {
                # code...
                $saldo = Operational::select('oprational_saldo')->orderBy('oprational_id', 'desc')->first();

                Operational::create([
                    'oprational_date' => date('Y-m-d'),
                    'oprational_nominal' => $less,
                    'oprational_type' => 2,
                    'oprational_desc' => 'Modal Tambahan - '.$result['sales_cost_desc'],
                    'oprational_saldo' => (int)($saldo['oprational_saldo'] - $less)
                ]);
            }

            if ($selisih > 0) {
                # code...
                $saldo = Operational::select('oprational_saldo')->orderBy('oprational_id', 'desc')->first();

                Operational::create([
                    'oprational_date' => date('Y-m-d'),
                    'oprational_nominal' => $selisih,
                    'oprational_type' => 1,
                    'oprational_desc' => 'Kelebihan Modal - '.$result['sales_cost_desc'],
                    'oprational_saldo' => (int)($saldo['oprational_saldo'] + $selisih)
                ]);
            } else if ($selisih < 0) {
                $saldo = Operational::select('oprational_saldo')->orderBy('oprational_id', 'desc')->first();

                Operational::create([
                    'oprational_date' => date('Y-m-d'),
                    'oprational_nominal' => $selisih,
                    'oprational_type' => 2,
                    'oprational_desc' => 'Kekurangan Modal - '.$result['sales_cost_desc'],
                    'oprational_saldo' => (int)($saldo['oprational_saldo'] - $selisih)
                ]);
            }

            $lock = SalesCost::where('sales_cost_id', $request->id)->update(['sales_cost_lock' => 1]);

            if ($lock) {
                # code...
                return response(['status' => 'success', 'message' => 'Terkunci'], 200);
            } else {
                return response(['status' => 'error', 'message' => 'Penguncian Gagal, Item Sudah Dikunci'], 200);
            }
        }
    }
}