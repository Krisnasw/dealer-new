<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Access;
use Alert;
use Validator;
use App\Spv;

class SPVController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,45);

            $this->permit = $akses->permit_acces;
                
            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            # code...
            $spv = Spv::orderBy('supervisior_id', 'ASC')->get();
            return view('admin.spv.index', compact('spv'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (strpos($this->permit, 'c') !== null) {
            # code...
            return view('admin.spv.create');
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'f_nama' => 'required',
            'f_telepon' => 'required',
            'alamat' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Spv::create([
                'supervisior_name' => $request->input('f_nama'),
                'supervisior_phone' => $request->input('f_telepon'),
                'supervisior_addres' => $request->input('alamat')
            ]);

            if ($create) {
                # code...
                Alert::success('SPV Berhasil Dibuat', 'Success');
                return redirect('home/master/supervisior');
            } else {
                Alert::error('Gagal Membuat SPV', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (strpos($this->permit, 'u') !== null) {
            # code...
            $spv = Spv::where('supervisior_id', base64_decode($id))->first();
            return view('admin.spv.edit', compact('spv'));
        } else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
            'f_nama' => 'required',
            'f_telepon' => 'required',
            'alamat' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Spv::where('supervisior_id', base64_decode($id))->update([
                'supervisior_name' => $request->input('f_nama'),
                'supervisior_phone' => $request->input('f_telepon'),
                'supervisior_addres' => $request->input('alamat')
            ]);

            if ($create) {
                # code...
                Alert::success('SPV Berhasil Diupdate', 'Success');
                return redirect('home/master/supervisior');
            } else {
                Alert::error('Gagal Update SPV', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (strpos($this->permit, 'd') !== null) {
            # code...
            $del = Spv::findOrFail(base64_decode($id));
            if ($del->delete()) {
                # code...
                Alert::success('SPV Berhasil Dihapus', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Menghapus SPV', 'Error');
                return redirect()->back();
            }
        } else {
            abort(403);
        }
    }
}
