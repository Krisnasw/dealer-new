<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alert;
use Auth;
use Validator;
use App\Coa;
use App\Menu;

class COAController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $list = Coa::where('coa_delete', 0)->get();
        return view('admin.coa.index', compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $menu = Menu::select('side_menu_id', 'side_menu_name')->get();
        return view('admin.coa.create', compact('menu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'i_name' => 'required',
            'menu' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Coa::create([
                'coa_name' => $request->i_name,
                'side_menu_id' => $request->menu,
                'coa_parent' => 0,
                'coa_nomor' => $request->nomor,
                'coa_delete' => 0
            ]);

            if ($create) {
                # code...
                Alert::success('COA Berhasil Dibuat', 'Success');
                return redirect('home/setup/coa');
            } else {
                Alert::error('Gagal Membuat COA', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $coa = Coa::where('coa_id', base64_decode($id))->first();
        $menu = Menu::select('side_menu_id', 'side_menu_name')->get();
        return view('admin.coa.edit', compact('coa', 'menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
            'i_name' => 'required',
            'menu' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Coa::where('coa_id', base64_decode($id))->update([
                'coa_name' => $request->i_name,
                'side_menu_id' => $request->menu,
                'coa_parent' => 0,
                'coa_nomor' => $request->nomor,
                'coa_delete' => 0
            ]);

            if ($create) {
                # code...
                Alert::success('COA Berhasil Diupdate', 'Success');
                return redirect('home/setup/coa');
            } else {
                Alert::error('Gagal Update COA', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $del = Coa::where('coa_id', base64_decode($id))->update([
            'coa_delete' => 1
        ]);

        if ($del) {
            # code...
            Alert::success('COA Berhasil Dihapus', 'Success');
            return redirect()->back();
        } else {
            Alert::error('Gagal Menghapus COA', 'Error');
            return redirect()->back();
        }
    }
}
