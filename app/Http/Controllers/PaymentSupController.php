<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Access;
use Validator;
use Alert;
use DB;
use App\PaymentSup;
use App\PaymentSupDetail;
use App\PaymentSupRetur;
use App\ReceiptPurchase;
use App\Supplier;
use App\Retur;
use App\Purchase;

class PaymentSupController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,58);

            $this->permit = $akses->permit_acces;

            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            # code...
            $list = PaymentSup::all();
            return view('admin.payment-sup.index', compact('list'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (strpos($this->permit, 'c') !== null) {
            # code...
            $supp = Supplier::select('supplier_id', 'supplier_name')->orderBy('supplier_name', 'asc')->get();
            $purchase = Purchase::select('purchase_id', 'purchase_code')->orderBy('purchase_code', 'desc')->get();
            $retur = Retur::where('retur_type', 2)->where('retur_status', 0)->get();
            $list_retur = PaymentSupRetur::select('payment_sup_returs.*', 'b.retur_code')->join('returs as b', 'b.retur_id', 'payment_sup_returs.retur_id')->where('payment_sup_returs.payment_sup_id', 0)->get();
            return view('admin.payment-sup.create', compact('supp', 'purchase', 'list_retur', 'retur'));
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'date' => 'required',
            'supplier' => 'required',
            'po' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = PaymentSup::create([
                'payment_sup_date' => $request->date,
                'payment_sup_code' => $this->format_code(),
                'supplier_id' => $request->supplier,
                'purchase_id' => $request->po
            ]);

            $retur_id = $request->i_retur;
            $arrlength_nota = count($retur_id);
            if ($retur_id) {
                PaymentSupRetur::where('payment_sup_id', $create->payment_sup_id)->delete();
                for($x = 0; $x < $arrlength_nota; $x++) {
                    PaymentSupRetur::create([
                        'payment_sup_id' => $create->payment_sup_id,
                        'retur_id' => $retur_id[$x]
                    ]);

                    Retur::where('retur_id', $retur_id[$x])->update(['retur_status' => 1]);
                }
            }

            return redirect('home/pembayaran/payment-sup/'.base64_encode($create->payment_sup_id));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $payment = PaymentSup::where('payment_sup_id', base64_decode($id))->first();

        $purchase_type = Purchase::select('purchase_type')->where('purchase_id', $payment['purchase_id'])->first();
        $purchase_code = ReceiptPurchase::select('b.receipt_code')->join('receipts as b', 'b.receipt_id', 'receipt_purchases.receipt_id')->where('receipt_purchases.purchase_id', $payment['purchase_id'])->get();

        if ($purchase_code) {
            foreach($purchase_code as $row) {
                $data1[] = $row['receipt_code'];
            }
            $code = implode(',', $data1);
        }else{
            $code = '';
        }

        if ($purchase_type == 'F') {
            $type = 'Fix';
        }elseif ($purchase_type == 'A') {
            $type = 'Additional';
        }else{
            $type = 'Emergency';
        }

        $list_do        = $code;
        $type_do        = $type;

        $retur_total  = Retur::select(DB::raw('sum(returs.retur_total) as total'))->join('payment_sup_returs as b', 'b.retur_id', 'returs.retur_id')->where('b.payment_sup_id', base64_decode($id))->first();
        $list_detail    = ReceiptPurchase::select('receipt_purchases.*', 'f.receipt_code', 'c.receipt_agency_qty', 'd.purchase_detail_price', 'd.purchase_detail_discount', 'e.item_code', 'e.item_name')->join('receipt_cardboards as b', 'b.receipt_id', 'receipt_purchases.receipt_id')->join('receipt_agencies as c', 'c.receipt_cardboard_id', 'b.receipt_cardboard_id')->join('purchase_details as d', 'd.purchase_detail_id', 'c.purchase_detail_id')->join('items as e', 'e.item_id', 'd.item_id')->join('receipts as f', 'f.receipt_id', 'receipt_purchases.receipt_id')->where('receipt_purchases.purchase_id', $payment['purchase_id'])->get();

        $supp = Supplier::select('supplier_id', 'supplier_name')->orderBy('supplier_name', 'asc')->get();
        $purchase = Purchase::select('purchase_id', 'purchase_code')->orderBy('purchase_code', 'desc')->get();
        $retur = Retur::where('retur_type', 2)->where('retur_status', 0)->get();
        $list_retur = PaymentSupRetur::select('payment_sup_returs.*', 'b.retur_code')->join('returs as b', 'b.retur_id', 'payment_sup_returs.retur_id')->where('payment_sup_returs.payment_sup_id', 0)->get();
        $list_detail_pay = PaymentSupDetail::where('payment_sup_id', base64_decode($id))->get();
        return view('admin.payment-sup.show', compact('supp', 'purchase', 'list_retur', 'retur', 'payment', 'list_do', 'type_do', 'retur_total', 'list_detail', 'list_detail_pay'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (strpos($this->permit, 'd') !== null) {
            # code...
        } else {
            abort(403);
        }
    }

    public function read_po(Request $request)
    {
        $id = $request->input('id');
        //echo $id;
        $purchase = Purchase::where('supplier_id', $id)->get();
        $data = "<option value='0'>-- Pilih PO --</option>";

        foreach ($purchase as $row) {
            $data .= "<option value=".$row['purchase_id'].">".$row['purchase_code']."</option>";
        }

        $response['type']       = 'spbe';
        $response['content']    = $data;
        $response['param']      = '';
        return response($response);
    }

    public function read_po_detail(Request $request)
    {
        $id = $request->input('id');

        $purchase_type = Purchase::select('purchase_type')->where('purchase_id', $id)->first();
        $purchase_code = ReceiptPurchase::select('b.receipt_code')->join('receipts as b', 'b.receipt_id', 'receipt_purchases.receipt_id')->where('receipt_purchases.purchase_id', $id)->get();
        $data1 = array();
        if ($purchase_code) {
            foreach($purchase_code as $row) {
                $data1 = $row['receipt_code'];
            }
            $code = implode(',', $data1);
        }else{
            $code = '';
        }

        if ($purchase_type == 'F') {
            $type = 'Fix';
        }elseif ($purchase_type == 'A') {
            $type = 'Additional';
        }else{
            $type = 'Emergency';
        }

        $data['code'] = $code;
        $data['type'] = $type;

        $response['type']       = 'spbe';
        $response['content']    = $data;
        $response['param']      = '';
        return response($response);
    }

    public function saveDetail(Request $request)
    {
        $id = $request->input('id');
        $detail_id = $request->input('detail_id');
        $date = $request->input('tanggal');

        if ($detail_id) {
            PaymentSupDetail::where('payment_sup_detail_id', $detail_id)->update([
                'payment_sup_id' => $id,
                'payment_sup_detail_date' => $date,
                'payment_sup_detail_desc' => $request->input('ket'),
                'payment_sup_detail_pay' => $request->input('nominal')
            ]);
        } else {
            PaymentSupDetail::create([
                'payment_sup_id' => $id,
                'payment_sup_detail_date' => $date,
                'payment_sup_detail_desc' => $request->input('ket'),
                'payment_sup_detail_pay' => $request->input('nominal')
            ]);
        }
        
        $response['type']       = 'spbe';
        $response['content']    = 'success';
        $response['param']      = '';
        return response($response);
    }

    protected function format_code()
    {
        $code = '';
        $year = date('y');

        switch (date('m')) {
            case '01': $month = "A"; break;
            case '02': $month = "B"; break;
            case '03': $month = "C"; break;
            case '04': $month = "D"; break;
            case '05': $month = "E"; break;
            case '06': $month = "F"; break;
            case '07': $month = "G"; break;
            case '08': $month = "H"; break;
            case '09': $month = "I"; break;
            case '10': $month = "J"; break;
            case '11': $month = "K"; break;
            case '12': $month = "L"; break;
        }

        $likes = 'PAY-'.$year.$month;
        $findMemo = PaymentSup::select('payment_sup_code')->where('payment_sup_code', 'LIKE', '%'.$likes.'%')->orderBy('payment_sup_code', 'desc')->first();

        $number = 0;

        if (empty($findMemo)) {
            $number = sprintf("%0" . 4 . "d", $number + 1);
            $code = 'PAY-'.$year.$month.$number;
            return $code;
        }

        $number = intval(substr($findMemo->payment_sup_code, -4));
        $number = sprintf("%0" . 4 . "d", $number + 1);
        $code = 'PAY-'.$year.$month.$number;
        return $code;
    }
}
