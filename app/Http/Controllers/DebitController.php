<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Access;
use Alert;
use Validator;
use App\Debit;
use App\Customer;

class DebitController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,84);

            $this->permit = $akses->permit_acces;
                
            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            # code...
            $list = Debit::orderBy('debit_id', 'desc')->get();
            return view('admin.debit.index', compact('list'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (strpos($this->permit, 'c') !== null) {
            # code...
            $cust = Customer::orderBy('customer_id', 'desc')->get();
            return view('admin.debit.create', compact('cust'));
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'nominal' => 'required',
            'customer_id' => 'required',
            'keterangan' => 'required',
            'jenis' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Debit::create([
                'debit_code' => $this->format_code(),
                'customer_id' => $request->customer_id,
                'debit_nominal' => $request->nominal,
                'debit_type' => $request->jenis,
                'debit_desc' => $request->keterangan,
                'debit_date' => date('Y-m-d'),
                'debit_status' => 0
            ]);

            if ($create) {
                # code...
                Alert::success('Data Berhasil Diproses', 'Success');
                return redirect('home/pembayaran/debit');
            } else {
                Alert::error('Gagal Memproses Data', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (strpos($this->permit, 'u') !== null) {
            # code...
            $cust = Customer::orderBy('customer_id', 'desc')->get();
            $debit = Debit::where('debit_id', base64_decode($id))->first();
            return view('admin.debit.edit', compact('debit', 'cust'));
        } else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
            'nominal' => 'required',
            'customer_id' => 'required',
            'keterangan' => 'required',
            'jenis' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Debit::where('debit_id', base64_decode($id))->update([
                'customer_id' => $request->customer_id,
                'debit_nominal' => $request->nominal,
                'debit_type' => $request->jenis,
                'debit_desc' => $request->keterangan,
                'debit_date' => date('Y-m-d'),
                'debit_status' => 0
            ]);

            if ($create) {
                # code...
                Alert::success('Data Berhasil Diupdate', 'Success');
                return redirect('home/pembayaran/debit');
            } else {
                Alert::error('Gagal Update Data', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (strpos($this->permit, 'd') !== null) {
            # code...
            $del = Debit::findOrFail(base64_decode($id));
            if ($del->delete()) {
                # code...
                Alert::success('Debit Berhasil Dihapus', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Menghapus Debit', 'Error');
                return redirect()->back();
            }
        } else {
            abort(403);
        }
    }

    protected function format_code()
    {
        $code = '';

        $likes = 'DK';
        $findMemo = Debit::select('debit_code')->where('debit_code', 'LIKE', '%'.$likes.'%')->orderBy('debit_id', 'desc')->first();
        $count = Debit::select('debit_code')->where('debit_code', 'LIKE', '%'.$likes.'%')->orderBy('debit_id', 'desc')->count();

        $number = 0;

        if ($count <= 0) {
            $numbers = sprintf("%0" . 4 . "d", $number + 1);
            $code = 'DK'.$numbers;
            return $code;
        }

        $numbers = intval(substr($findMemo->debit_code, -4));
        $number = sprintf("%0" . 4 . "d", $numbers + 1);
        $code = 'DK'.$number;
        return $code;
    }
}
