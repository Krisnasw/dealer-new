<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Access;
use Alert;
use Validator;
use App\Nota;
use App\Retur;
use App\ReturDetail;

class ReturCustomerController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,62);

            $this->permit = $akses->permit_acces;

            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            $retur = Retur::orderBy('retur_id', 'desc')->get();
            return view('admin.retur-customer.index', compact('retur'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (strpos($this->permit, 'c') !== null) {
            $nota = Nota::select('nota_id', 'nota_code')->get();
            return view('admin.retur-customer.create', compact('nota'));
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'nota' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => 'success', 'message' => 'Form Tidak Lengkap'], 200);
        } else {
            $detail = ReturDetail::select('retur_details.*', 'e.unit_name', 'd.item_code', 'd.item_name', 'd.item_het', 'd.item_stock', 'c.memo_detail_qty', 'c.memo_detail_discount',
            'b.nota_fill_qty', 'b.nota_detail_price', 'b.nota_detail_total', 'b.nota_detail_discount')
                            ->join('nota_details as b', 'b.nota_detail_id', 'retur_details.retur_detail_data_id')
                            ->join('memo_details as c', 'c.memo_detail_id', 'b.memo_detail_id')
                            ->join('items as d', 'd.item_id', 'c.item_id')
                            ->join('units as e', 'e.unit_id', 'd.unit_id')
                            ->where('retur_details.retur_id', 0)
                            ->where('retur_details.user_id', Auth::user()->user_id)->get();
                            // dd($detail);
            $grand_total = 0;
            foreach ($detail as $row):

                $persen = $row['nota_detail_discount'] / $row['nota_detail_price'] * 100;

                $total_diskon = $row['nota_detail_price'] * $persen / 100 * $row['retur_detail_qty'];
                $het = $row['nota_detail_price'] * $row['retur_detail_qty'];
                $total = $het - $total_diskon;

                $grand_total += $total;

            endforeach;

            $num = $this->format_code();
            $create = Retur::create([
                'retur_type' => 1,
                'retur_data_id' => $request->nota,
                'retur_code' => $num,
                'retur_date' => date('Y-m-d'),
                'retur_type_saldo' => 2,
                'retur_total' => $grand_total,
                'retur_status' => 0
            ]);

            ReturDetail::where('retur_detail_type', 1)->where('retur_id', 0)->where('user_id', Auth::user()->user_id)->update([
                'retur_id' => $create->retur_id
            ]);

            if ($create) {
                # code...
                Alert::success('Retur Berhasil Dibuat', 'Success');
                return redirect('home/penjualan/retur-customer');
            } else {
                Alert::error('Gagal Membuat Retur', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (strpos($this->permit, 'u') !== null) {
            # code...
            $nota = Nota::select('nota_id', 'nota_code')->get();
            $retur = Retur::where('retur_id', base64_decode($id))->first();
            $detail = ReturDetail::select('retur_details.*', 'e.unit_name', 'd.item_code', 'd.item_name', 'd.item_het', 'd.item_stock', 'c.memo_detail_qty', 'c.memo_detail_discount',
            'b.nota_fill_qty', 'b.nota_detail_price', 'b.nota_detail_total', 'b.nota_detail_discount')
                            ->join('nota_details as b', 'b.nota_detail_id', 'retur_details.retur_detail_data_id')
                            ->join('memo_details as c', 'c.memo_detail_id', 'b.memo_detail_id')
                            ->join('items as d', 'd.item_id', 'c.item_id')
                            ->join('units as e', 'e.unit_id', 'd.unit_id')
                            ->where('retur_details.retur_id', $retur->retur_id)
                            ->where('retur_details.user_id', Auth::user()->user_id)->get();
            return view('admin.retur-customer.edit', compact('retur', 'detail', 'nota'));
        } else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        // $valid = Validator::make($request->all(), [
        //     'nota' => 'required'
        // ]);

        // if ($valid->fails()) {
        //     # code...
        //     return response(['status' => 'success', 'message' => 'Form Tidak Lengkap'], 200);
        // } else {
        //     $detail = ReturDetail::select('retur_details.*', 'e.unit_name', 'd.item_code', 'd.item_name', 'd.item_het', 'd.item_stock', 'c.memo_detail_qty', 'c.memo_detail_discount',
        //     'b.nota_fill_qty', 'b.nota_detail_price', 'b.nota_detail_total', 'b.nota_detail_discount')
        //                     ->join('nota_details as b', 'b.nota_detail_id', 'retur_details.retur_detail_data_id')
        //                     ->join('memo_details as c', 'c.memo_detail_id', 'b.memo_detail_id')
        //                     ->join('items as d', 'd.item_id', 'c.item_id')
        //                     ->join('units as e', 'e.unit_id', 'd.unit_id')
        //                     ->where('retur_details.retur_id', 0)
        //                     ->where('retur_details.user_id', Auth::user()->user_id)->get();
        //                     // dd($detail);
        //     $grand_total = 0;
        //     foreach ($detail as $row):

        //         $persen = $row['nota_detail_discount'] / $row['nota_detail_price'] * 100;

        //         $total_diskon = $row['nota_detail_price'] * $persen / 100 * $row['retur_detail_qty'];
        //         $het = $row['nota_detail_price'] * $row['retur_detail_qty'];
        //         $total = $het - $total_diskon;

        //         $grand_total += $total;

        //     endforeach;

        //     $num = $this->format_code();
        //     $create = Retur::create([
        //         'retur_type' => 1,
        //         'retur_data_id' => $request->nota,
        //         'retur_code' => $num,
        //         'retur_date' => date('Y-m-d'),
        //         'retur_type_saldo' => 2,
        //         'retur_total' => $grand_total,
        //         'retur_status' => 0
        //     ]);

        //     ReturDetail::where('retur_detail_type', 1)->where('retur_id', 0)->where('user_id', Auth::user()->user_id)->update([
        //         'retur_id' => $create->retur_id
        //     ]);

        //     if ($create) {
        //         # code...
        //         Alert::success('Retur Berhasil Dibuat', 'Success');
        //         return redirect('home/penjualan/retur-customer');
        //     } else {
        //         Alert::error('Gagal Membuat Retur', 'Error');
        //         return redirect()->back();
        //     }
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (strpos($this->permit, 'd') !== null) {
            # code...
            $del = Retur::find(base64_decode($id));
            ReturDetail::where('retur_id', $del->retur_id)->delete();
            if ($del->delete()) {
                # code...
                Alert::success('Retur Berhasil Dihapus', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Retur Gagal Dihapus', 'Error');
                return redirect()->back();
            }
        } else {
            abort(403);
        }
    }

    protected function format_code()
    {
        $code = '';

        $likes = "RT-CUS";
        $findMemo = Retur::select('retur_code')->where('retur_code', 'LIKE', '%'.$likes.'%')->orderBy('retur_id', 'desc')->first();
        $count = Retur::select('retur_code')->where('retur_code', 'LIKE', '%'.$likes.'%')->orderBy('retur_id', 'desc')->count();

        $number = 0;

//            dd(sprintf("%04d", $number));
        if ($count <= 0) {
            $numbers = sprintf("%0" . 4 . "d", $number + 1);
            $code = "RT-CUS".$numbers;
            return $code;
        }

        $numbers = intval(substr($findMemo->retur_code, -4));
        $number = sprintf("%0" . 4 . "d", $numbers + 1);
        $code = "RT-CUS".$number;
        return $code;
    }
}
