<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Access;
use Alert;
use Validator;
use App\Employee;
use App\EmployeeGallery;
use App\Position;
use File;

class PegawaiController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,59);

            $this->permit = $akses->permit_acces;
                
            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            # code...
            $pegawai = Employee::orderBy('employee_id', 'asc')->get();
            return view('admin.pegawai.index', compact('pegawai'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (strpos($this->permit, 'c') !== null) {
            # code...
            $pos = Position::all();
            return view('admin.pegawai.create', compact('pos'));
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request->all());
        $valid = Validator::make($request->all(), [
            'nama' => 'required',
            'telp' => 'required',
            'no_ktp' => 'required',
            'posisi' => 'required',
            'gender' => 'required',
            'f_tempatLahir' => 'required',
            'f_tgLahir' => 'required',
            'sim' => 'required',
            'gaji' => 'required',
            'email' => 'required',
            'alamat' => 'required',
            'image' => 'required|image|mimes:jpg,jpeg,png'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Employee::create([
                'employee_name' => $request->nama,
                'position_id' => $request->posisi,
                'employee_gender' => $request->gender,
                'employee_birth_place' => $request->f_tempatLahir,
                'employee_birth_date' => $request->f_tgLahir,
                'employee_ktp' => $request->no_ktp,
                'employee_address_ktp' => $request->alamat,
                'employee_no_telp' => $request->telp,
                'employee_email' => $request->email,
                'employee_sim' => $request->sim,
                'employee_first_salary' => $request->gaji,
                'employee_photo' => $this->savePhoto($request->file('image'))
            ]);

            if ($create) {
                # code...
                Alert::success('Pegawai Berhasil Dibuat', 'Success');
                return redirect('home/master/pegawai');
            } else {
                Alert::error('Gagal Membuat Pegawai Baru', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (strpos($this->permit, 'u') !== null) {
            # code...
            $emp = Employee::where('employee_id', base64_decode($id))->first();
            $pos = Position::all();
            $gal = EmployeeGallery::where('employee_id', base64_decode($id))->get();
            return view('admin.pegawai.edit', compact('emp', 'pos', 'gal'));
        } else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
            'nama' => 'required',
            'telp' => 'required',
            'no_ktp' => 'required',
            'posisi' => 'required',
            'gender' => 'required',
            'f_tempatLahir' => 'required',
            'f_tgLahir' => 'required',
            'sim' => 'required',
            'gaji' => 'required',
            'email' => 'required',
            'alamat' => 'required',
            'image' => 'image|mimes:jpg,jpeg,png'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $exist = Employee::where('employee_id', base64_decode($id))->first();
            if ($request->hasFile('image')) {
                # code...
                $this->deletePhoto($exist->employee_photo);
                $create = Employee::where('employee_id', base64_decode($id))->update([
                    'employee_name' => $request->nama,
                    'position_id' => $request->posisi,
                    'employee_gender' => $request->gender,
                    'employee_birth_place' => $request->f_tempatLahir,
                    'employee_birth_date' => $request->f_tgLahir,
                    'employee_ktp' => $request->no_ktp,
                    'employee_address_ktp' => $request->alamat,
                    'employee_no_telp' => $request->telp,
                    'employee_email' => $request->email,
                    'employee_sim' => $request->sim,
                    'employee_first_salary' => $request->gaji,
                    'employee_photo' => $this->savePhoto($request->file('image'))
                ]);

                if ($create) {
                    # code...
                    Alert::success('Pegawai Berhasil Diupdate', 'Success');
                    return redirect('home/master/pegawai');
                } else {
                    Alert::error('Gagal Update Pegawai', 'Error');
                    return redirect()->back();
                }
            } else {
                $create = Employee::where('employee_id', base64_decode($id))->update([
                    'employee_name' => $request->nama,
                    'position_id' => $request->posisi,
                    'employee_gender' => $request->gender,
                    'employee_birth_place' => $request->f_tempatLahir,
                    'employee_birth_date' => $request->f_tgLahir,
                    'employee_ktp' => $request->no_ktp,
                    'employee_address_ktp' => $request->alamat,
                    'employee_no_telp' => $request->telp,
                    'employee_email' => $request->email,
                    'employee_sim' => $request->sim,
                    'employee_first_salary' => $request->gaji,
                    'employee_photo' => $exist->employee_photo
                ]);

                if ($create) {
                    # code...
                    Alert::success('Pegawai Berhasil Diupdate', 'Success');
                    return redirect('home/master/pegawai');
                } else {
                    Alert::error('Gagal Update Pegawai', 'Error');
                    return redirect()->back();
                }
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (strpos($this->permit, 'd') !== null) {
            # code...
            $del = Employee::findOrFail(base64_decode($id));
            if ($del->delete()) {
                # code...
                Alert::success('Pegawai Berhasil Dihapus', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Menghapus Pegawai', 'Error');
                return redirect()->back();
            }
        } else {
            abort(403);
        }
    }

    protected function savePhoto($photo)
    {
        $destinationPath = 'images';
        $subdestinationPath = 'pegawai';
        $extension = $photo->getClientOriginalExtension();
        $fileName = rand(11111,99999).'.'.$extension;
        $photo->move($destinationPath. '/' . $subdestinationPath , $fileName);
        $create['employee_photo'] = $destinationPath. '/' . $subdestinationPath . '/' . $fileName;

        return $create['employee_photo'];
    }

    protected function deletePhoto($photo)
    {
        File::delete($photo);
        return $photo;
    }
}
