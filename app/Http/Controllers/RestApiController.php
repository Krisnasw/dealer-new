<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Item;
use App\Customer;
use App\Grade;
use App\City;
use App\Brand;
use App\BrandDetail;
use App\Sales;
use App\Memo;
use App\MemoDetail;
use App\Cardboard;
use App\CardboardStatus;
use App\Promo;
use App\Nota;
use App\Packaging;
use App\SalesCost;
use App\Operational;
use App\Cost;
use App\SalesCostDetail;
use Auth;
use Validator;
use JWTAuth;

class RestApiController extends Controller
{
    //
    public function doLogin(Request $request)
    {
        $credentials = $request->only('user_username', 'password');
        
        $rules = [
            'user_username' => 'required',
            'password' => 'required'
        ];

        $validator = Validator::make($credentials, $rules);

        if($validator->fails()) {
            return response()->json(['status'=> false, 'message'=> 'Form tidak valid']);
        }
        
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['status' => false, 'message' => 'Akun Tidak Ditemukan.'], 200);
            }
        } catch (JWTException $e) {
            return response()->json(['status' => false, 'message' => 'Failed to login, please try again.'], 200);
        }

        return response()->json(['status' => true, 'message'=> $token], 200);
    }

    public function getUserData(Request $request)
    {
        $this->validate($request, ['token' => 'required']);

        $user = JWTAuth::toUser($request->token);

        return response(['status' => true, 'message' => 'User Ditemukan', 'data' => $user['user_first_name'], 'email' => $user['user_email'], 'phone' => $user['user_phone']], 200);
    }

    public function getBunchOfItem(Request $request)
    {
        $this->validate($request, ['token' => 'required']);

        $item = Item::select('item_name', 'item_stock', 'item_code')->where('item_name', '!=', "")->orderBy('item_name', 'asc')->get();
        if (count($item) <= 0) {
            # code...
            return response(['status' => false, 'message' => 'Item Kosong', 'data' => []], 200);
        } else {
            return response(['status' => true, 'message' => 'Item Ditemukan', 'data' => $item], 200);
        }
    }

    public function searchItem(Request $request)
    {
        $this->validate($request, ['token' => 'required', 'title' => 'required']);

        $item = Item::select('item_name', 'item_stock', 'item_code')->where('item_name', 'LIKE', '%'.$request->title.'%')->orWhere('item_code', 'LIKE', '%'.$request->title.'%')->orderBy('item_name', 'asc')->get();
        if (count($item) <= 0) {
            # code...
            return response(['status' => false, 'message' => 'Item Tidak Ada', 'data' => []], 200);
        } else {
            return response(['status' => true, 'message' => 'Item Ditemukan', 'data' => $item], 200);
        }
    }

    public function addCustomer(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'token' => 'required',
            'customerCode' => 'required',
            'customerName' => 'required',
            'customerToko' => 'required',
            'customerAddress' => 'required',
            'customerPhone' => 'required',
            'customerEmail' => 'required',
            'customerGrade' => 'required',
            'customerRekening' => 'required',
            'customerBank' => 'required',
            'customerNPWP' => 'required',
            'customerNpwpName' => 'required',
            'customerEkspedisi' => 'required',
            'customerHp' => 'required',
            'customerFax' => 'required',
            'customerWilayah' => 'required',
            'customerLat' => 'required',
            'customerLong' => 'required',
            'customerImg' => 'required|image'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => false, 'message' => 'Form Tidak Lengkap'], 200);
        } else {

            $grade = Grade::where('grade_nama', $request->customerGrade)->first();
            $kota = City::where('city_name', $request->customerWilayah)->first();

            $create = Customer::create([
                'customer_code' => $request->input('customerCode'),
                'customer_name' => $request->input('customerName'),
                'customer_phone' => $request->input('customerPhone'),
                'customer_addres' => $request->input('customerAddress'),
                'customer_email' => $request->input('customerEmail'),
                'customer_npwp' => $request->input('customerNPWP'),
                'grade_id' => $grade['grade_id'],
                'customer_rekening' => $request->input('customerRekening'),
                'customer_id_bank' => $request->input('customerBank'),
                'customer_nama_npwp' => $request->input('customerNpwpName'),
                'customer_expedisi' => $request->input('customerEkspedisi'),
                'customer_tempo' => 0,
                'customer_limit' => 0,
                'customer_hp' => $request->input('customerHp'),
                'customer_fax' => $request->input('customerFax'),
                'city_id' => $kota['city_id'],
                'customer_store' => $request->input('customerToko'),
                'customer_latitude' => $request->customerLat,
                'customer_longitude' => $request->customerLong,
                'customer_status' => 1,
                'customer_img' => ($request->hasFile('customerImg') ? $this->savePhoto($request->file('customerImg')) : NULL)
            ]);

            if ($create) {
                # code...
                return response(['status' => true, 'message' => 'Customer Berhasil Dibuat'], 200);
            } else {
                return response(['status' => false, 'message' => 'Gagal Membuat Customer'], 200);
            }
        }
    }

    public function getGrade(Request $request)
    {
        $this->validate($request, ['token' => 'required']);

        $brand = Grade::select('grade_id', 'grade_nama')->orderBy('grade_nama', 'asc')->get();
        if (count($brand) <= 0) {
            # code...
            return response(['status' => false, 'message' => 'Grade Tidak Ada', 'data' => []], 200);
        } else {
            return response(['status' => true, 'message' => 'Grade Ditemukan', 'data' => $brand], 200);
        }
    }

    public function getWilayah(Request $request)
    {
        $this->validate($request, ['token' => 'required']);

        $city = City::select('city_id', 'city_name')->orderBy('city_name', 'asc')->get();
        if (count($city) <= 0) {
            # code...
            return response(['status' => false, 'message' => 'Wilayah Tidak Ditemukan', 'data' => []], 200);
        } else {
            return response(['status' => true, 'message' => 'Wilayah Ditemukan', 'data' => $city], 200);
        }
    }

    public function addMemo(Request $request)
    {
        $this->validate($request, ['token' => 'required', 'customer' => 'required', 'merk' => 'required', 'sales' => 'required']);

        $customer = Customer::where('customer_name', $request->customer)->first();
        $merk = Brand::where('brand_name', $request->merk)->first();
        $sales = Sales::where('sales_name', $request->sales)->first();

        if ($customer != null && $merk != null && $sales != null) {
            # code...
            $num = $this->formatMemoCode($request->merk);
            $create = Memo::create([
                'memo_code' => $num,
                'memo_date' => date('Y-m-d'),
                'sales_id' => $sales['sales_id'],
                'customer_id' => $customer['customer_id'],
                'memo_type_pay' => 0,
                'brand_id' => $merk['brand_id']
            ]);

            $cek = MemoDetail::where('memo_id', 0)->get();

            for ($i=0; $i < count($cek); $i++) { 
                # code...
                MemoDetail::where('memo_id', 0)->update([
                    'memo_id' => $create->memo_id
                ]);
            }

            if ($create) {
                return response(['status' => true, 'message' => 'Memo Berhasil Dibuat'], 200);
            } else {
                return response(['status' => false, 'message' => 'Gagal Membuat Memo'], 200);
            }
        } else {
            return response(['status' => false, 'message' => 'Form Tidak Lengkap'], 200);
        }
    }

    public function getBunchOfMemoDetail(Request $request)
    {
        $this->validate($request, ['token' => 'required']);

        $memoDet = MemoDetail::select('b.item_name', 'memo_details.memo_detail_qty', 'memo_details.memo_accumulation', 'memo_details.memo_detail_discount', 'memo_details.memo_detail_id')->join('items as b', 'b.item_id', 'memo_details.item_id')->where('memo_id', 0)->get();
        if (count($memoDet) <= 0) {
            # code...
            return response(['status' => false, 'message' => 'Barang Belum Diisi', 'data' => []], 200);
        } else {
            return response(['status' => true, 'message' => 'Barang Ditemukan', 'data' => $memoDet], 200);
        }
    }

    public function addMemoDetail(Request $request)
    {
        $this->validate($request, ['token' => 'required', 'item' => 'required', 'qty' => 'required', 'diskon' => 'required']);

        $user = JWTAuth::toUser($request->token);
        $exp = $request->item;
        $exp = explode(' - ', $exp);

        $item = Item::select('item_id')->where('item_code', $exp[0])->first();

        $create = MemoDetail::create([
                    'memo_id' => 0,
                    'item_id' => $item['item_id'],
                    'memo_detail_qty' => $request->input('qty'),
                    'memo_accumulation' => 0,
                    'memo_detail_discount' => $request->input('diskon'),
                    'memo_detail_cancel' => 0,
                    'user_id' => $user['user_id']
                ]);

        if ($create) {
            # code...
            return response(['status' => true, 'message' => 'Barang Berhasil Ditambahkan'], 200);
        } else {
            return response(['status' => false, 'message' => 'Gagal Menambahkan Barang'], 200);
        }
    }

    public function deleteMemoDetail(Request $request)
    {
        $this->validate($request, ['token' => 'required', 'code' => 'required']);

        $user = JWTAuth::toUser($request->token);

        $del = MemoDetail::where('memo_detail_id', $request->code)->where('user_id', $user['user_id'])->delete();
        if ($del) {
            # code...
            return response(['status' => true, 'message' => 'Barang Berhasil Dihapus'], 200);
        } else {
            return response(['status' => false, 'message' => 'Gagal Menghapus Barang'], 200);
        }
    }

    public function updateMemoDetail(Request $request)
    {
        $this->validate($request, ['token' => 'required', 'code' => 'required', 'qty' => 'required']);

        $user = JWTAuth::toUser($request->token);

        $update = MemoDetail::where('memo_detail_id', $request->code)->where('user_id', $user['user_id'])->update([
            'memo_detail_qty' => $request->qty
        ]);

        if ($update) {
            # code...
            return response(['status' => true, 'message' => 'Barang Berhasil Diupdate'], 200);
        } else {
            return response(['status' => false, 'message' => 'Gagal Update Barang'], 200);
        }
    }

    public function getBunchOfCustomer(Request $request)
    {
        $this->validate($request, ['token' => 'required']);

        $customer = Customer::select('customer_id', 'customer_name')->orderBy('customer_name', 'asc')->where('customer_name', '!=', '')->get();
        if (count($customer) <= 0) {
            # code...
            return response(['status' => false, 'message' => 'Customer Tidak Ada', 'data' => []], 200);
        } else {
            return response(['status' => true, 'message' => 'Customer Ditemukan', 'data' => $customer], 200);
        }
    }

    public function getBunchOfSales(Request $request)
    {
        $this->validate($request, ['token' => 'required']);

        $sales = Sales::select('sales_id', 'sales_name')->orderBy('sales_name', 'asc')->get();
        if (count($sales) <= 0) {
            # code...
            return response(['status' => false, 'message' => 'Sales Tidak Ada', 'data' => []], 200);
        } else {
            return response(['status' => true, 'message' => 'Sales Ditemukan', 'data' => $sales], 200);
        }
    }

    public function getBunchOfMerk(Request $request)
    {
        $this->validate($request, ['token' => 'required']);

        $merk = Brand::select('brand_id', 'brand_name')->orderBy('brand_name', 'asc')->get();
        if (count($merk) <= 0) {
            # code...
            return response(['status' => false, 'message' => 'Merk Tidak Ada', 'data' => []], 200);
        } else {
            return response(['status' => true, 'message' => 'Merk Ditemukan', 'data' => $merk], 200);
        }
    }

    public function searchCardboard(Request $request)
    {
        $this->validate($request, ['token' => 'required', 'code' => 'required']);

        $cek = Cardboard::where('cardboard_barcode', $request->code)->get();
        if (count($cek) <= 0) {
            # code...
            return response(['status' => false, 'message' => 'Kode Tidak Ditemukan'], 200);
        } else {
            return response(['status' => true, 'message' => 'Kode Ditemukan'], 200);
        }
    }

    public function createCardboardStatus(Request $request)
    {
        $this->validate($request, ['token' => 'required', 'code' => 'required', 'type' => 'required']);

        $user = JWTAuth::toUser($request->token);
        $cek = Cardboard::where('cardboard_barcode', $request->code)->first();
        if ($cek == null) {
            # code...
            return response(['status' => false, 'message' => 'Kode Tidak Ditemukan'], 200);
        } else {
            $packaging = Packaging::where('packaging_id', $cek['packaging_id'])->first();
            if ($packaging == null) {
                # code...
                return response(['status' => false, 'message' => 'Packaging Tidak Ditemukan'], 200);
            } else {
                if ($request->type == "Terima") {
                    # code...
                    $create = CardboardStatus::create([
                        'cardboard_id' => $cek['cardboard_id'],
                        'user_id' => $user['user_id'],
                        'cardboard_status_type' => 'terima',
                        'nota_id' => $packaging['nota_id']
                    ]);

                    if ($create) {
                        # code...
                        return response(['status' => true, 'message' => 'Penerimaan Berhasil'], 200);
                    } else {
                        return response(['status' => false, 'message' => 'Gagal Melakukan Penerimaan'], 200);
                    }
                } else {
                    $create = CardboardStatus::create([
                        'cardboard_id' => $cek['cardboard_id'],
                        'user_id' => $user['user_id'],
                        'cardboard_status_type' => 'tolak',
                        'nota_id' => $packaging['nota_id']
                    ]);

                    if ($create) {
                        # code...
                        return response(['status' => true, 'message' => 'Penolakan Berhasil'], 200);
                    } else {
                        return response(['status' => false, 'message' => 'Gagal Melakukan Penolakan'], 200);
                    }
                }
            }
        }
    }

    public function getListPromo(Request $request)
    {
        $this->validate($request, ['token' => 'required']);

        $promo = Promo::orderBy('bonus_name', 'asc')->get();
        if (count($promo) <= 0) {
            # code...
            return response(['status' => false, 'message' => 'Promo Tidak Ada', 'data' => []], 200);
        } else {
            return response(['status' => true, 'message' => 'Promo Ditemukan', 'data' => $promo], 200);
        }
    }

    public function tracking(Request $request)
    {
        $this->validate($request, ['token' => 'required', 'nota' => 'required']);

        $cekNota = Nota::where('nota_code', $request->nota)->first();
        $user = JWTAuth::toUser($request->token);
        if ($cekNota == null) {
            # code...
            return response(['status' => false, 'message' => 'Nota Tidak Ada', 'data' => 'Tidak Ditemukan', 'user' => '', 'date' => ''], 200);
        } else {
            $cekStatus = CardboardStatus::where('nota_id', $cekNota['nota_id'])->first();
            if ($cekStatus == null) {
                # code...
                return response(['status' => true, 'message' => 'Nota Ditemukan', 'data' => 'Dalam Proses', 'user' => $user['user_first_name'], 'date' => '', 'packing' => ''], 200);
            } else {
                $packing = Cardboard::where('cardboard_id', $cekStatus['cardboard_id'])->first();
                if ($cekStatus['cardboard_status_type'] == "terima") {
                    # code...
                    return response(['status' => true, 'message' => 'Nota Ditemukan', 'data' => 'Sudah Diterima', 'user' => $user['user_first_name'], 'date' => date('Y-m-d', strtotime($cekStatus['created_at'])), 'packing' => $packing['cardboard_barcode']], 200);
                } else {
                    return response(['status' => true, 'message' => 'Nota Ditemukan', 'data' => 'Dibatalkan', 'user' => $user['user_first_name'], 'date' => date('Y-m-d', strtotime($cekStatus['created_at'])), 'packing' => $packing['cardboard_barcode']], 200);
                }
            }
        }
    }

    public function postBiayaSales(Request $request)
    {
        $this->validate($request, ['token' => 'required', 'date' => 'required', 'modal' => 'required', 'keterangan' => 'required', 'sales' => 'required']);

        $sales = Sales::where('sales_name', $request->sales)->first();

        $date = $request->input('date');
        $date = explode(' - ', $date);
        $date1 = $date[0];
        $date1 = explode("-", $date1);
        $date1 = $date1[2]."-".$date1[0]."-".$date1[1];
        $date1 = str_replace(" ", "", $date1);

        $date2 = $date[1];
        $date2 = explode("-", $date2);
        $date2 = $date2[2]."-".$date2[0]."-".$date2[1];
        $date2 = str_replace(" ", "", $date2);

        $create = SalesCost::create([
            'sales_cost_start' => $date1,
            'sales_cost_finish' => $date2,
            'sales_cost_modal' => $request->modal,
            'sales_cost_desc' => $request->keterangan,
            'sales_id' => $sales['sales_id']
        ]);

        $saldo = Operational::select('oprational_saldo')->orderBy('oprational_id', 'desc')->first();

        Operational::create([
            'oprational_date' => date('Y-m-d'),
            'oprational_nominal' => $request->modal,
            'oprational_type' => 2,
            'oprational_desc' => 'Biaya Keliling - '.$request->keterangan,
            'oprational_saldo' => (int)($saldo['oprational_saldo'] - $request->modal)
        ]);

        if ($create) {
            # code...
            return response(['status' => true, 'message' => 'Berhasil! Silahkan Lanjut Ke Step Berikutnya', 'code' => $create['sales_cost_id']], 200);
        } else {
            return response(['status' => false, 'message' => 'Gagal Membuat Biaya', 'code' => 0], 200);
        }
    }

    public function getSalesCost(Request $request)
    {
        $this->validate($request, ['token' => 'required', 'code' => 'required']);
        $cost = SalesCost::where('sales_cost_id', $request->code)->first();

        if ($cost == null) {
            # code...
            return response(['status' => false, 'message' => 'Tidak Ada Data', 'sales' => 'Tidak Ada', 'keterangan' => 'Kosong', 'modal' => 0, 'periode' => 'Kosong', 'data' => []], 200);
        } else {

            $costDetail = SalesCostDetail::select('sales_cost_details.*', 'b.cost_name', 'c.sales_name')
                                        ->join('costs as b', 'b.cost_id', 'sales_cost_details.cost_id')
                                        ->join('sales as c', 'c.sales_id', 'sales_cost_details.sales_id')
                                        ->where('sales_cost_details.sales_id', $cost['sales_id'])
                                        ->where('sales_cost_details.sales_cost_detail_date', '>=', $cost['sales_cost_start'])
                                        ->where('sales_cost_details.sales_cost_detail_date', '<=', $cost['sales_cost_finish'])
                                        ->get();
            return response(['status' => true, 'message' => 'Data Ditemukan', 'sales' => $cost['sales']['sales_name'], 'keterangan' => $cost['sales_cost_desc'], 'modal' => $cost['sales_cost_modal'], 'periode' => $cost['sales_cost_start']." - ".$cost['sales_cost_finish'], 'data' => $costDetail], 200);
        }
    }

    public function getKeperluan(Request $request)
    {
        $this->validate($request, ['token' => 'required']);

        $cost = Cost::orderBy('cost_name', 'asc')->get();
        if (count($cost) <= 0) {
            # code...
            return response(['status' => false, 'message' => 'Tidak Ada Data', 'data' => []], 200);
        } else {
            return response(['status' => true, 'message' => 'Keperluan Ditemukan', 'data' => $cost], 200);
        }
    }

    public function postSalesCostDetail(Request $request)
    {
        $this->validate($request, ['token' => 'required', 'code' => 'required', 'sales' => 'required', 'cost' => 'required', 'date' => 'required', 'total' => 'required', 'desc' => 'required']);

        $cost = Cost::where('cost_name', $request->cost)->first();
        $sales = Sales::where('sales_name', $request->sales)->first();

        $create = SalesCostDetail::create([
            'sales_cost_id' => $request->code,
            'cost_id' => $cost['cost_id'],
            'sales_id' => $sales['sales_id'],
            'sales_cost_detail_date' => $request->date,
            'sales_cost_detail_total' => $request->total,
            'sales_cost_detail_desc' => $request->desc
        ]);

        if ($create) {
            # code...
            return response(['status' => 'success', 'message' => 'Detail Berhasil Ditambahkan'], 200);
        } else {
            return response(['status' => 'error', 'message' => 'Gagal Membuat Detail'], 200);
        }
    }

    public function getSalesCostData(Request $request)
    {
        $this->validate($request, ['token' => 'required', 'sales' => 'required']);

        $sales = Sales::where('sales_name', $request->sales)->first();
        $data = SalesCost::select('sales_costs.*', 'b.sales_name')->join('sales as b', 'b.sales_id', 'sales_costs.sales_id')->where('sales_costs.sales_id', $sales['sales_id'])->where('sales_costs.sales_cost_lock', 0)->get();

        if (count($data) <= 0) {
            # code...
            return response(['status' => false, 'message' => 'Tidak Ada Data', 'data' => []], 200);
        } else {
            return response(['status' => true, 'message' => 'Data Ditemukan', 'data' => $data], 200);
        }
    }

    public function lockSalesCostDetail(Request $request)
    {
        $this->validate($request, ['token' => 'required', 'code' => 'required']);

        $update = SalesCostDetail::where('sales_cost_detail_id', $request->code)->update([
            'sales_cost_detail_status' => 1
        ]);

        if ($update) {
            # code...
            return response(['status' => true, 'message' => 'Detail Berhasil Dikunci'], 200);
        } else {
            return response(['status' => false, 'message' => 'Gagal Mengunci Detail'], 200);
        }
    }

    public function lockSalesCost(Request $request)
    {
        $this->validate($request, ['token' => 'required', 'id' => 'required', 'less' => 'required']);

        $less = $request->less;
        $result = SalesCost::where('sales_cost_id', $request->id)->first();
        $selisih = $result['sales_cost_modal'] + $result['sales_cost_modal_less'] - $result['sales_cost_total'];

        $update = SalesCost::where('sales_cost_id', $request->id)->update([
            'sales_cost_modal_less' => $request->less,
            'sales_cost_total' => $request->total
        ]);

        if ($less) {
            # code...
            $saldo = Operational::select('oprational_saldo')->orderBy('oprational_id', 'desc')->first();

            Operational::create([
                'oprational_date' => date('Y-m-d'),
                'oprational_nominal' => $less,
                'oprational_type' => 2,
                'oprational_desc' => 'Modal Tambahan - '.$result['sales_cost_desc'],
                'oprational_saldo' => (int)($saldo['oprational_saldo'] - $less)
            ]);
        }

        if ($selisih > 0) {
            # code...
            $saldo = Operational::select('oprational_saldo')->orderBy('oprational_id', 'desc')->first();

            Operational::create([
                'oprational_date' => date('Y-m-d'),
                'oprational_nominal' => $selisih,
                'oprational_type' => 1,
                'oprational_desc' => 'Kelebihan Modal - '.$result['sales_cost_desc'],
                'oprational_saldo' => (int)($saldo['oprational_saldo'] + $selisih)
            ]);
        } else if ($selisih < 0) {
            $saldo = Operational::select('oprational_saldo')->orderBy('oprational_id', 'desc')->first();

            Operational::create([
                'oprational_date' => date('Y-m-d'),
                'oprational_nominal' => $selisih,
                'oprational_type' => 2,
                'oprational_desc' => 'Kekurangan Modal - '.$result['sales_cost_desc'],
                'oprational_saldo' => (int)($saldo['oprational_saldo'] - $selisih)
            ]);
        }

        $lock = SalesCost::where('sales_cost_id', $request->id)->update(['sales_cost_lock' => 1]);

        if ($lock) {
            # code...
            return response(['status' => true, 'message' => 'Terkunci'], 200);
        } else {
            return response(['status' => false, 'message' => 'Item Sudah Dikunci'], 200);
        }
    }

    public function logout(Request $request)
    {
        $this->validate($request, ['token' => 'required']);
        
        try {
            JWTAuth::invalidate($request->input('token'));
            return response()->json(['success' => true, 'message'=> "You have successfully logged out."]);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false, 'message' => 'Failed to logout, please try again.'], 500);
        }
    }
    
    public function getBrandDetailById(Request $request)
    {
        $data = BrandDetail::where('brand_id', $request->id)->get();
        
        if (count($data) <= 0) {
            return response(['status' => 'error', 'message' => 'Tidak Ada Data', 'data' => []], 200);
        } else {
            return response(['status' => 'success', 'message' => 'Brand Detail Ada', 'data' => $data], 200);
        }
    }
    
    public function getItemByDetailId(Request $request)
    {
        $data = Item::where('brand_detail_id', $request->id)->get();
        
        if (count($data) <= 0) {
            return response(['status' => 'error', 'message' => 'Tidak Ada Data', 'data' => []], 200);
        } else {
            return response(['status' => 'success', 'message' => 'Brand Detail Ada', 'data' => $data], 200);
        }
    }
    
    public function updateUserData(Request $request)
    {
        $this->validate($request, ['token' => 'required', 'name' => 'required', 'email' => 'required', 'phone' => 'required']);
        $user = JWTAuth::toUser($request->token);

        $create = User::where('user_id', $user['user_id'])->update([
            'user_email' => $request->email,
            'user_first_name' => $request->name,
            'user_phone' => $request->phone
        ]);

        if ($create) {
            # code...
            return response(['status' => 'success', 'message' => 'Profile Berhasil Diupdate'], 200);
        } else {
            return response(['status' => 'error', 'message' => 'Gagal Update Profile'], 200);
        }
    }

    protected function savePhoto($photo)
    {
        $destinationPath = 'images';
        $subdestinationPath = 'customer';
        $extension = $photo->getClientOriginalExtension();
        $fileName = rand(11111,99999).'.'.$extension;
        // $fileName = array_pop(explode(DIRECTORY_SEPARATOR, $photo));
        $photo->move($destinationPath. '/' . $subdestinationPath , $fileName);
        $create['customer_img'] = $destinationPath. '/' . $subdestinationPath . '/' . $fileName;

        return $create['customer_img'];
    }

    protected function deletePhoto($photo)
    {
        File::delete($photo);
        return $photo;
    }

    protected function formatMemoCode($brand)
    {
        $code = '';
        $cek = Brand::where('brand_id', $brand)->first();
        if (empty($cek)) {
            return $code;
        } else {
            $year =date('y');

            switch (date('m')) {
                case '01': $month = "A"; break;
                case '02': $month = "B"; break;
                case '03': $month = "C"; break;
                case '04': $month = "D"; break;
                case '05': $month = "E"; break;
                case '06': $month = "F"; break;
                case '07': $month = "G"; break;
                case '08': $month = "H"; break;
                case '09': $month = "I"; break;
                case '10': $month = "J"; break;
                case '11': $month = "K"; break;
                case '12': $month = "L"; break;
            }

            $likes = $cek->brand_code.$year.$month;
            $findMemo = Memo::select('memo_code')->where('memo_code', 'LIKE', '%'.$likes.'%')->orderBy('memo_id', 'desc')->first();

            $number = 0;

            if (empty($findMemo)) {
                $number = sprintf("%0" . 4 . "d", $number + 1);
                $code = $cek->brand_code.$year.$month.$number;
                return $code;
            }

            $number = intval(substr($findMemo->memo_code, -4));
            $number = sprintf("%0" . 4 . "d", $number + 1);
            $code = $cek->brand_code.$year.$month.$number;
            return $code;
        }
    }
}
