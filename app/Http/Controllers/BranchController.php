<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Access;
use Alert;
use Validator;
use App\Branch;
use App\Spv;

class BranchController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,43);

            $this->permit = $akses->permit_acces;
                
            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            # code...
            $branch = Branch::orderBy('branch_id', 'ASC')->with('cabangs')->get();
            return view('admin.branch.index', compact('branch'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (strpos($this->permit, 'c') !== null) {
            # code...
            $spv = Spv::orderBy('supervisior_id', 'asc')->get();
            return view('admin.branch.create', compact('spv'));
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'kode' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'phone' => 'required',
            'spv_id' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Branch::create([
                'branch_code' => $request->input('kode'),
                'branch_name' => $request->input('nama'),
                'branch_addres' => $request->input('alamat'),
                'branch_phone' => $request->input('phone'),
                'supervisior_id' => $request->input('spv_id')
            ]);

            if ($create) {
                # code...
                Alert::success('Branch Baru Berhasil Dibuat', 'Success');
                return redirect('home/setup/branch');
            } else {
                Alert::error('Gagal Membuat Branch Baru', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (strpos($this->permit, 'u') !== null) {
            # code...
            $spv = Spv::orderBy('supervisior_id', 'asc')->get();
            $branch = Branch::where('branch_id', base64_decode($id))->first();
            return view('admin.branch.edit', compact('spv', 'branch'));
        } else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
            'kode' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'phone' => 'required',
            'spv_id' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Branch::where('branch_id', base64_decode($id))->update([
                'branch_code' => $request->input('kode'),
                'branch_name' => $request->input('nama'),
                'branch_addres' => $request->input('alamat'),
                'branch_phone' => $request->input('phone'),
                'supervisior_id' => $request->input('spv_id')
            ]);

            if ($create) {
                # code...
                Alert::success('Branch Berhasil Diupdate', 'Success');
                return redirect('home/setup/branch');
            } else {
                Alert::error('Gagal Update Branch', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (strpos($this->permit, 'd') !== null) {
            # code...
            $del = Branch::findOrFail(base64_decode($id));
            if ($del->delete()) {
                # code...
                Alert::success('Branch Berhasil Dihapus', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Menghapus Branch', 'Error');
                return redirect()->back();
            }
        } else {
            abort(403);
        }
    }
}
