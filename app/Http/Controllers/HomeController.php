<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Alert;
use App\Customer;
use App\Nota;
use App\Item;
use App\Sales;

class HomeController extends Controller
{
    //
	public function __construct()
	{
		$this->middleware('auth');
	}

    public function index() {
        $customer = Customer::count();
        $nota = Nota::count();
        $item = Item::count();
        $sales = Sales::count();
    	return view('admin.main.index', compact('customer', 'nota', 'item', 'sales'));
    }

    public function doLogout() {
    	Auth::logout();
    	Alert::info('Logout Berhasil', 'Info');
    	return redirect('/');
    }
}