<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Access;
use Alert;
use Validator;
use App\Password;
use App\Menu;

class PasswordController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,78);

            $this->permit = $akses->permit_acces;

            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            $password = Password::with('menus')->orderBy('password_id', 'asc')->get();
            return view('admin.password.index', compact('password'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (strpos($this->permit, 'c') !== null) {
            $menu = Menu::select('side_menu_id', 'side_menu_name')->orderBy('side_menu_name', 'asc')->get();
            return view('admin.password.create', compact('menu'));
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'menu' => 'required',
            'password' => 'required'
        ]);

        if ($valid->fails()) {
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Password::create([
                'password_value' => bcrypt($request->input('password')),
                'menu_id' => $request->input('menu')
            ]);

            if ($create) {
                Alert::success('Password Berhasil Dibuat', 'Success');
                return redirect('home/setting/password');
            } else {
                Alert::error('Gagal Membuat Password', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (strpos($this->permit, 'u') !== null) {
            $menu = Menu::select('side_menu_id', 'side_menu_name')->orderBy('side_menu_name', 'asc')->get();
            $pasw = Password::where('password_id', base64_decode($id))->with('menus')->first();
            return view('admin.password.edit', compact('menu', 'pasw'));
        } else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
            'menu' => 'required',
            'password' => 'required'
        ]);

        if ($valid->fails()) {
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Password::where('password_id', base64_decode($id))->update([
                'password_value' => bcrypt($request->input('password')),
                'menu_id' => $request->input('menu')
            ]);

            if ($create) {
                Alert::success('Password Berhasil Diupdate', 'Success');
                return redirect('home/setting/password');
            } else {
                Alert::error('Gagal Update Password', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (strpos($this->permit, 'd') !== null) {
            $del = Password::findOrFail(base64_decode($id));
            if ($del->delete()) {
                Alert::success('Password Berhasil Dihapus', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Menghapus Password', 'Error');
                return redirect()->back();
            }
        } else {
            abort(403);
        }
    }
}
