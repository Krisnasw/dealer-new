<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Access;
use Validator;
use Alert;
use App\Brand;
use App\BrandDetail;

class BrandController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,48);

            $this->permit = $akses->permit_acces;
                
            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            # code...
            $merk = Brand::orderBy('brand_id', 'ASC')->get();
            return view('admin.merk.index', compact('merk'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (strpos($this->permit, 'c') !== null) {
            # code...
            return view('admin.merk.create');
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'kode' => 'required',
            'nama' => 'required',
            'limitCash' => 'required',
            'limitKredit' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Brand::create([
                'brand_code' => $request->input('kode'),
                'brand_name' => $request->input('nama'),
                'brand_limit_cash' => $request->input('limitCash'),
                'brand_limit_kredit' => $request->input('limitKredit')
            ]);

            if ($create) {
                # code...
                Alert::success('Merk Berhasil Dibuat', 'Success');
                return redirect('home/setup/merk');
            } else {
                Alert::error('Gagal Membuat Merk', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (strpos($this->permit, 'u') !== null) {
            # code...
            $merk = Brand::where('brand_id', base64_decode($id))->first();
            $brandDetail = BrandDetail::where('brand_id', base64_decode($id))->get();
            return view('admin.merk.edit', compact('merk', 'brandDetail'));
        } else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
            'kode' => 'required',
            'nama' => 'required',
            'limitCash' => 'required',
            'limitKredit' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Brand::where('brand_detail_id', base64_decode($id))->update([
                'brand_code' => $request->input('kode'),
                'brand_name' => $request->input('nama'),
                'brand_limit_cash' => $request->input('limitCash'),
                'brand_limit_kredit' => $request->input('limitKredit')
            ]);

            if ($create) {
                # code...
                Alert::success('Merk Berhasil Diupdate', 'Success');
                return redirect('home/setup/merk');
            } else {
                Alert::error('Gagal Update Merk', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (strpos($this->permit, 'd') !== null) {
            # code...
            $del = Brand::findOrFail(base64_decode($id));
            if ($del->delete()) {
                # code...
                Alert::success('Merk Berhasil Dihapus', 'Success');
            } else {
                Alert::error('Gagal Hapus Merk', 'Error');
            }

            return redirect()->back();
        } else {
            abort(403);
        }
    }
}
