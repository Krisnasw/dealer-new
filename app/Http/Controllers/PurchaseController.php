<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Access;
use Auth;
use Alert;
use Validator;
use App\Brand;
use App\BrandDetail;
use App\Supplier;
use App\Purchase;
use App\PurchaseDetailTmp;
use App\Item;
use App\MemoDetail;
use App\NotaDetail;
use App\PurchaseDetail;

class PurchaseController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,54);

            $this->permit = $akses->permit_acces;
                
            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            # code...
            $list = Purchase::orderBy('purchase_id', 'desc')->with('brand')->with('supplier')->get();
            return view('admin.purchase.index', compact('list'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (strpos($this->permit, 'c') !== null) {
            # code...
            $brand = Brand::select('brand_id', 'brand_name')->get();
            $supplier = Supplier::select('supplier_id', 'supplier_name')->get();
            $brand_detail = BrandDetail::select('brand_detail_id', 'brand_detail_name', 'brand_id')->get();
            $list_detail = PurchaseDetailTmp::where('user_id', Auth::user()->user_id)->get();
            return view('admin.purchase.create', compact('brand', 'supplier', 'brand_detail', 'list_detail'));
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'merk' => 'required',
            'supplier' => 'required',
            'jenis' => 'required'
        ]);

        // dd($request->all());

        if ($valid->fails()) {
            # code...
            return response(['status' => 'error', 'message' => 'Form Tidak Lengkap'], 200);
        } else {
            $create = Purchase::create([
                'purchase_code' => $this->format_code($request->merk, $request->jenis),
                'purchase_date' => date('Y-m-d'),
                'brand_id' => $request->merk,
                'purchase_type' => $request->jenis,
                'purchase_discount' => $request->i_purchase_diskon,
                'purchase_ppn' => $request->i_cek_ppn,
                'supplier_id' => $request->supplier,
                'purchase_netto' => $request->i_netto
            ]);

            $tmp = PurchaseDetailTmp::where('user_id', Auth::user()->user_id)->get();

            foreach ($tmp as $row) {
                Item::where('item_id', $row['item_id'])->increment('item_stock', $row['purchase_detail_qty']);
                PurchaseDetail::create([
                    'purchase_detail_discount' => $row['purchase_detail_discount'],
                    'item_id' => $row['item_id'],
                    'purchase_id' => $create->purchase_id,
                    'purchase_detail_price' => $row['purchase_detail_price'],
                    'purchase_detail_qty' => $row['purchase_detail_qty'],
                    'user_id' => $row['user_id'],
                    'purchase_detail_total' => $row['purchase_detail_total']
                ]);
            }

            PurchaseDetailTmp::where('user_id', Auth::user()->user_id)->delete();

            if ($create) {
                # code...
                return response(['status' => 'success', 'message' => 'Pembelian Berhasil Dilakukan'], 200);
            } else {
                return response(['status' => 'error', 'message' => 'Gagal Melakukan Pembelian'], 200);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (strpos($this->permit, 'u') !== null) {
            # code...
            $data = Purchase::where('purchase_id', base64_decode($id))->first();
            $brand = Brand::select('brand_id', 'brand_name')->get();
            $supplier = Supplier::select('supplier_id', 'supplier_name')->get();
            $brand_detail = BrandDetail::select('brand_detail_id', 'brand_detail_name', 'brand_id')->get();
            $list_detail = PurchaseDetail::where('user_id', Auth::user()->user_id)->where('purchase_id', base64_decode($id))->get();
            return view('admin.purchase.edit', compact('brand', 'supplier', 'brand_detail', 'list_detail', 'data'));
        } else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
            'merk' => 'required',
            'supplier' => 'required',
            'jenis' => 'required'
        ]);

        // dd($request->all());

        if ($valid->fails()) {
            # code...
            return response(['status' => 'error', 'message' => 'Form Tidak Lengkap'], 200);
        } else {
            $create = Purchase::where('purchase_id', base64_decode($id))->update([
                'brand_id' => $request->merk,
                'purchase_type' => $request->jenis,
                'purchase_discount' => $request->i_purchase_diskon,
                'purchase_ppn' => $request->i_cek_ppn,
                'supplier_id' => $request->supplier,
                'purchase_netto' => $request->i_netto
            ]);

            if ($create) {
                # code...
                return response(['status' => 'success', 'message' => 'Pembelian Berhasil Diupdate'], 200);
            } else {
                return response(['status' => 'error', 'message' => 'Gagal Update Pembelian'], 200);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (strpos($this->permit, 'd') !== null) {
            # code...
            $del = Purchase::find(base64_decode($id));
            PurchaseDetail::where('purchase_id', base64_decode($id))->delete();
            if ($del->delete()) {
                # code...
                Alert::success('Pembelian Berhasil Dihapus', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Menghapus Pembelian', 'Error');
                return redirect()->back();
            }
        } else {
            abort(403);
        }
    }

    public function getBrandDetailById(Request $request)
    {
        $sales = BrandDetail::where('brand_id', $request->id)->get();
        // dd($sales);
        $data = "<option value='0'>-- Pilih Kode Part --</option>";
        foreach ($sales as $row) {
            $data .= "<option value='".$row['brand_detail_id']."'>".$row['brand_detail_name']."</option>";
        }

        $response['type']       = 'spbe';
        $response['content']    = $data;
        $response['param']      = '';
        return response($response);
    }

    public function readItemName(Request $request)
    {
        $type = BrandDetail::where('brand_id', $request->id)->first();
        $data = "<option value='0'>-- Pilih Barang --</option>";
        $item = Item::where('brand_detail_id', $type['brand_detail_id'])->get();
        foreach($item as $row) {
            $data .= "<option value='".$row['item_id']."'>".$row['item_name']."</option>";
        }

        $response['type'] = 'spbe';
        $response['content'] = $data;
        $response['param'] = '';
        return response($response);
    }

    public function readItemCode(Request $request)
    {
        $type = BrandDetail::where('brand_id', $request->id)->first();
        $data = "<option value='0'>-- Pilih Barang --</option>";
        $item = Item::where('brand_detail_id', $type['brand_detail_id'])->get();
        foreach($item as $row) {
            $data .= "<option value='".$row['item_id']."'>".$row['item_code']."</option>";
        }

        $response['type'] = 'spbe';
        $response['content'] = $data;
        $response['param'] = '';
        return response($response);
    }

    public function saveTmp(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'numberPart' => 'required',
            'diskon' => 'required',
            'qty' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => 'error', 'message' => 'Form Tidak Lengkap'], 200);
        } else {
            $item = Item::where('item_id', $request->numberPart)->first();

            if ($item) {
                # code...
                $cek = PurchaseDetailTmp::where('user_id', Auth::user()->user_id)->where('item_id', $item['item_id'])->first();
                if ($cek) {
                    # code...
                    $update = PurchaseDetailTmp::where('purchase_detail_id', $cek['purchase_detail_id'])->update([
                                'purchase_detail_total' => $request->qty * $cek['purchase_detail_total'],
                                'purchase_detail_qty' => $cek['purchase_detail_qty'] + $request->qty
                            ]);

                    if ($update) {
                        # code...
                        return response(['status' => 'success', 'message' => 'Update Detail Berhasil'], 200);
                    } else {
                        return response(['status' => 'error', 'message' => 'Gagal Update Detail'], 200);
                    }
                } else {
                    $create = PurchaseDetailTmp::create([
                        'purchase_id' => $request->id,
                        'item_id' => $item['item_id'],
                        'purchase_detail_price' => $item['item_het'],
                        'purchase_detail_qty' => $request->qty,
                        'user_id' => Auth::user()->user_id,
                        'purchase_detail_total' => $request->qty * $item['item_het'],
                        'purchase_detail_discount' => $request->diskon
                    ]);

                    if ($create) {
                        # code...
                        return response(['status' => 'success', 'message' => 'Detail Berhasil Ditambahkan'], 200);
                    } else {
                        return response(['status' => 'error', 'message' => 'Gagal Menambahkan Detail'], 200);
                    }
                }
            } else {
                return response(['status' => 'error', 'message' => 'Barang Tidak Ditemukan'], 200);
            }
        }
    }

    public function saveDetail(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'numberPart' => 'required',
            'diskon' => 'required',
            'qty' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => 'error', 'message' => 'Form Tidak Lengkap'], 200);
        } else {
            $item = Item::where('item_id', $request->numberPart)->first();

            if ($item) {
                # code...
                $cek = PurchaseDetail::where('user_id', Auth::user()->user_id)->where('item_id', $item['item_id'])->first();
                if ($cek) {
                    # code...
                    $update = PurchaseDetail::where('purchase_detail_id', $cek['purchase_detail_id'])->update([
                                'purchase_detail_total' => $request->qty * $cek['purchase_detail_total'],
                                'purchase_detail_qty' => $cek['purchase_detail_qty'] + $request->qty
                            ]);

                    if ($update) {
                        # code...
                        return response(['status' => 'success', 'message' => 'Update Detail Berhasil'], 200);
                    } else {
                        return response(['status' => 'error', 'message' => 'Gagal Update Detail'], 200);
                    }
                } else {
                    $create = PurchaseDetail::create([
                        'purchase_id' => $request->id,
                        'item_id' => $item['item_id'],
                        'purchase_detail_price' => $item['item_het'],
                        'purchase_detail_qty' => $request->qty,
                        'user_id' => Auth::user()->user_id,
                        'purchase_detail_total' => $request->qty * $item['item_het'],
                        'purchase_detail_discount' => $request->diskon
                    ]);

                    if ($create) {
                        # code...
                        return response(['status' => 'success', 'message' => 'Detail Berhasil Ditambahkan'], 200);
                    } else {
                        return response(['status' => 'error', 'message' => 'Gagal Menambahkan Detail'], 200);
                    }
                }
            } else {
                return response(['status' => 'error', 'message' => 'Barang Tidak Ditemukan'], 200);
            }
        }
    }

    public function deleteTmp(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => 'error', 'message' => 'Form Tidak Lengkap'], 200);
        } else {
            $del = PurchaseDetailTmp::where('purchase_detail_id', $request->id)->delete();

            if ($del) {
                # code...
                return response(['status' => 'success', 'message' => 'Detail Berhasil Dihapus'], 200);
            } else {
                return response(['status' => 'error', 'message' => 'Gagal Menghapus Detail'], 200);
            }
        }
    }

    public function deleteDetail(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => 'error', 'message' => 'Form Tidak Lengkap'], 200);
        } else {
            $del = PurchaseDetail::where('purchase_detail_id', $request->id)->delete();

            if ($del) {
                # code...
                return response(['status' => 'success', 'message' => 'Detail Berhasil Dihapus'], 200);
            } else {
                return response(['status' => 'error', 'message' => 'Gagal Menghapus Detail'], 200);
            }
        }
    }

    public function updateTmpDiskon(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'id' => 'required',
            'diskon' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => 'error', 'message' => 'Form Tidak Lengkap'], 200);
        } else {
            $update = PurchaseDetailTmp::where('purchase_detail_id', $request->id)->update([
                'purchase_detail_discount' => $request->diskon
            ]);

            if ($update) {
                # code...
                return response(['status' => 'success', 'message' => 'Diskon Berhasil Diupdate'], 200);
            } else {
                return response(['status' => 'error', 'message' => 'Gagal Update Diskon'], 200);
            }
        }
    }

    public function updateTmpPrice(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'id' => 'required',
            'harga' => 'required',
            'qty' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => 'error', 'message' => 'Form Tidak Lengkap'], 200);
        } else {
            $update = PurchaseDetailTmp::where('purchase_detail_id', $request->id)->update([
                'purchase_detail_qty' => $request->qty,
                'purchase_detail_total' => $request->harga * $request->qty
            ]);

            if ($update) {
                # code...
                return response(['status' => 'success', 'message' => 'Harga Berhasil Diupdate'], 200);
            } else {
                return response(['status' => 'error', 'message' => 'Gagal Update Harga'], 200);
            }
        }
    }

    public function submitFilterable(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'merk_det' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => 'error', 'message' => 'Form Tidak Lengkap'], 200);
        } else {
            $item = Item::where('brand_detail_id', $request->merk_det)->take(10)->get();
            if (count($item) <= 0) {
                # code...
                return response(['status' => 'error', 'message' => 'Barang Tidak Ada'], 200);
            } else {
                foreach ($item as $row) {
                    $qty = $request->i_qty_filter.$row['item_id'];
                    if ($qty) {
                        # code...
                        $cek = PurchaseDetailTmp::where('user_id', Auth::user()->user_id)->where('item_id', $row['item_id'])->first();
                        if ($cek) {
                            # code...
                            $update = PurchaseDetailTmp::where('purchase_detail_id', $cek['purchase_detail_id'])->update([
                                'purchase_detail_total' => $qty * $cek['purchase_detail_total'],
                                'purchase_detail_qty' => $cek['purchase_detail_qty'] + $qty
                            ]);

                            if ($update) {
                                # code...
                                return response(['status' => 'success', 'message' => 'Update Tmp Berhasil'], 200);
                            } else {
                                return response(['status' => 'error', 'message' => 'Gagal Update Tmp'], 200);
                            }
                        } else {
                            $create = PurchaseDetailTmp::create([
                                'purchase_id' => 0,
                                'item_id' => $row['item_id'],
                                'purchase_detail_price' => $row['item_het'],
                                'purchase_detail_qty' => $qty,
                                'user_id' => Auth::user()->user_id,
                                'purchase_detail_total' => $qty * $row['item_het'],
                                'purchase_detail_discount' => 0
                            ]);

                            if ($create) {
                                # code...
                                return response(['status' => 'success', 'message' => 'Tmp Berhasil Dibuat'], 200);
                            } else {
                                return response(['status' => 'error', 'message' => 'Gagal Menambahkan Tmp'], 200);
                            }
                        }
                    }
                }
            }
        }
    }

    public function getBunchOfFilterable(Request $request)
    {
        $item = Item::where('brand_detail_id', $request->id)->get();
        $data = array();
        if (count($item) <= 0) {
            # code...
            return response(['status' => 'error', 'message' => 'Item Tidak Ditemukan', 'list' => []], 200);
        } else {
            foreach ($item as $rows) {
                $row = array();
                $row['qty'] = '<input type="text" class="form-control" name="i_qty_filter'.$rows['item_id'].'">';
                $row['code'] = $rows['item_code'];
                $row['name'] = $rows['item_name'];
                $row['stok'] = $rows['item_stock'];
                $row['pending'] = ($this->hitung_pending($rows['item_id']) == null) ? 0 : $this->hitung_pending($rows['item_id']);
                $row['order'] = ($this->hitung_order($rows['item_id']) == null) ? 0 : $this->hitung_order($rows['item_id']);

                $selisih = 1;
                for ($i = 0; $i <= $selisih; $i++) {
                    $x = mktime (0 ,0 ,0 ,date("m") -$i, date("d") , date("y"));
                    $bln= date("m", $x);
                    $thn= date("Y", $x);
                                     
                    $row['sold'] = ($this->hitung_sold($rows->item_id,$bln,$thn) == null) ? 0 : $this->hitung_sold($rows->item_id,$bln,$thn);
                }

                // $row['subStok'] = $rows['item_stock'];

                $data[] = $row;
            }

            return response(['status' => 'success', 'message' => 'Listed Data', 'list' => $data], 200);
        }
    }

    public function editPurchaseDetailDiskon(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'id' => 'required',
            'diskon' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => 'error', 'message' => 'Form Tidak Lengkap'], 200);
        } else {
            $update = PurchaseDetail::where('purchase_detail_id', $request->id)->update([
                'purchase_detail_discount' => $request->diskon
            ]);

            if ($update) {
                # code...
                return response(['status' => 'success', 'message' => 'Diskon Berhasil Diupdate'], 200);
            } else {
                return response(['status' => 'error', 'message' => 'Gagal Update Diskon'], 200);
            }
        }
    }

    public function editPurchaseDetailPrice(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'id' => 'required',
            'harga' => 'required',
            'qty' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => 'error', 'message' => 'Form Tidak Lengkap'], 200);
        } else {
            $update = PurchaseDetail::where('purchase_detail_id', $request->id)->update([
                'purchase_detail_qty' => $request->qty,
                'purchase_detail_total' => $request->harga * $request->qty
            ]);

            if ($update) {
                # code...
                return response(['status' => 'success', 'message' => 'Harga Berhasil Diupdate'], 200);
            } else {
                return response(['status' => 'error', 'message' => 'Gagal Update Harga'], 200);
            }
        }
    }

    protected function hitung_pending($id)
    {
        $cek = MemoDetail::select(\DB::raw('(SUM(memo_detail_qty) - SUM(memo_accumulation)) as pending'))->where('item_id', $id)->get();
        $result = null;
        foreach ($cek as $row) $result = ($row);
        return $result['pending'];
    }

    protected function hitung_order($id)
    {
        $cek = PurchaseDetail::select(\DB::raw('(sum(purchase_detail_qty) - sum(purchase_detail_accumulation)) as pending'))->where('item_id', $id)->get();
        $result = null;
        foreach ($cek as $row) $result = ($row);
        return $result['pending'];
    }

    protected function hitung_sold($id, $bln, $thn)
    {
        $cek = NotaDetail::select(\DB::raw('(sum(nota_fill_qty)) as sold'))->join('memo_details as b', 'b.memo_detail_id', 'nota_details.memo_detail_id')
                            ->join('notas as c', 'c.nota_id', 'nota_details.nota_id')
                            ->where('b.item_id', $id)
                            ->whereMonth('c.nota_date', $bln)
                            ->whereYear('c.nota_date', $thn)
                            ->get();
        $result = null;
        foreach ($cek as $row) $result = ($row);
        return $result['sold'];
    }

    protected function format_code($brand_id, $type)
    {
        $code = '';
        $year =date('y');

        switch (date('m')) {
            case '01': $month = "A"; break;
            case '02': $month = "B"; break;
            case '03': $month = "C"; break;
            case '04': $month = "D"; break;
            case '05': $month = "E"; break;
            case '06': $month = "F"; break;
            case '07': $month = "G"; break;
            case '08': $month = "H"; break;
            case '09': $month = "I"; break;
            case '10': $month = "J"; break;
            case '11': $month = "K"; break;
            case '12': $month = "L"; break;
        }

        if ($brand_id == 10) {
            # code...
            $likes = '10045-'.$type.'-'.$year;
            $findMemo = Purchase::select('purchase_code')->where('purchase_code', 'LIKE', '%'.$likes.'%')->orderBy('purchase_code', 'desc')->first();

            $number = 0;

    //            dd(sprintf("%04d", $number));
            if (empty($findMemo)) {
                $number = sprintf("%0" . 4 . "d", $number + 1);
                $code = '10045-'.$type.'-'.$year.'-'.$number;
                return $code;
            }

            $number = intval(substr($findMemo->purchase_code, -4));
            $number = sprintf("%0" . 4 . "d", $number + 1);
            $code = '10045-'.$type.'-'.$year.'-'.$number;
        } else {
            $likes = 'PO-'.$year.$month;
            $findMemo = Purchase::select('purchase_code')->where('purchase_code', 'LIKE', '%'.$likes.'%')->orderBy('purchase_code', 'desc')->first();

            $number = 0;

    //            dd(sprintf("%04d", $number));
            if (empty($findMemo)) {
                $number = sprintf("%0" . 4 . "d", $number + 1);
                $code = 'PO-'.$year.$month.$number;
                return $code;
            }

            $number = intval(substr($findMemo->purchase_code, -4));
            $number = sprintf("%0" . 4 . "d", $number + 1);
            $code = 'PO-'.$year.$month.$number;
        }
        return $code;
    }
}