<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Access;
use Alert;
use Validator;
use App\Operational;

class OprationalController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,77);

            $this->permit = $akses->permit_acces;
                
            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            # code...
            $list = Operational::orderBy('oprational_id', 'desc')->get();
            $saldo = Operational::select('oprational_saldo')->orderBy('oprational_id', 'desc')->take(1)->first();
            return view('admin.oprational.index', compact('list', 'saldo'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (strpos($this->permit, 'c') !== null) {
            # code...
            return view('admin.oprational.create');
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'tanggal' => 'required',
            'jenis' => 'required',
            'keterangan' => 'required',
            'i_nominal' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $saldo = Operational::select('oprational_saldo')->orderBy('oprational_id', 'desc')->first();

            if ($request->jenis == 1) {
                # code...
                $saldo_fix = $saldo['oprational_saldo'] + $request->i_nominal;
            } else {
                $saldo_fix = $saldo['oprational_saldo'] - $request->i_nominal;
            }

            $create = Operational::create([
                'oprational_saldo' => $saldo_fix,
                'oprational_date' => $request->tanggal,
                'oprational_type' => $request->jenis,
                'oprational_nominal' => $request->i_nominal,
                'oprational_desc' => $request->keterangan
            ]);

            if ($create) {
                # code...
                Alert::success('Operasional Berhasil Ditambahkan', 'Success');
                return redirect('home/pembayaran/oprational');
            } else {
                Alert::error('Gagal Membuat Operasional', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (strpos($this->permit, 'd') !== null) {
            # code...
            $del = Operational::findOrFail(base64_decode($id));
            if ($del->delete()) {
                # code...
                Alert::success('Operasional Berhasil Dihapus', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Menghapus Operasional', 'Error');
                return redirect()->back();
            }
        } else {
            abort(403);
        }
    }
}
