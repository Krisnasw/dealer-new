<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Access;
use Auth;
use Alert;
use Validator;
use App\City;
use App\Sales;
use App\Target;
use App\Brand;

class SalesController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,39);

            $this->permit = $akses->permit_acces;
                
            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== false) {
            # code...
            $sales = Sales::orderBy('sales_id', 'DESC')->get();
            return view('admin.sales.index', compact('sales'));
        } else {
            abort(403, 'Unauthorized Access.');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (strpos($this->permit, 'c') !== false) {
            # code...
            $city = City::select('city_id', 'city_name')->orderBy('city_id', 'ASC')->get();
            return view('admin.sales.create', compact('city'));
        } else {
            abort(403, 'Unauthorized Access.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'f_kodeSales' => 'required',
            'f_nama' => 'required',
            'f_alamat' => 'required',
            'f_telepon' => 'required',
            'f_ktp' => 'required',
            'f_tempatLahir' => 'required',
            'f_tgLahir' => 'required',
            'f_sim' => 'required',
            'f_gaji' => 'required',
            'f_email' => 'required',
            'f_wilayah' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Belum Lengkap', 'Info');
            return redirect()->back()->withErrors($valid);
        } else {
            $city = implode(',', $request->input('f_wilayah'));

            $sales = Sales::create([
                'sales_code' => $request->input('f_kodeSales'),
                'sales_name' => $request->input('f_nama'),
                'sales_phone' => $request->input('f_telepon'),
                'sales_addres' => $request->input('f_alamat'),
                'sales_ktp' => $request->input('f_ktp'),
                'sales_birth_place' => $request->input('f_tempatLahir'),
                'sales_birth_date' => $request->input('f_tgLahir'),
                'sales_sim' => $request->input('f_sim'),
                'sales_salary' => $request->input('f_gaji'),
                'sales_mail' => $request->input('f_email'),
                'city_id' => $city,
                'sales_password' => ""
            ]);

            if ($sales) {
                # code...
                Alert::success('Sales Berhasil Dibuat', 'Success');
                return redirect('home/master/sales');
            } else {
                Alert::error('Gagal Membuat Sales', 'Error');
                return redirect()->back()->withErrors($valid);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (strpos($this->permit, 'u')) {
            # code...
            $city = City::select('city_id', 'city_name')->orderBy('city_id', 'ASC')->get();
            $sales = Sales::where('sales_id', base64_decode($id))->first();
            $target = Target::where('sales_id', base64_decode($id))->join('brands', 'brands.brand_id', 'targets.brand_id')->get();
            $brand = Brand::select('brand_id', 'brand_name')->orderBy('brand_id', 'asc')->get();
            return view('admin.sales.edit', compact('sales', 'brand', 'city', 'target'));
        } else {
            abort(403, 'Unauthorized Access.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
            'f_kodeSales' => 'required',
            'f_nama' => 'required',
            'f_alamat' => 'required',
            'f_telepon' => 'required',
            'f_ktp' => 'required',
            'f_tempatLahir' => 'required',
            'f_tgLahir' => 'required',
            'f_sim' => 'required',
            'f_gaji' => 'required',
            'f_email' => 'required',
            'f_wilayah' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Belum Lengkap', 'Info');
            return redirect()->back()->withErrors($valid);
        } else {
            $city = implode(',', $request->input('f_wilayah'));

            $sales = Sales::where('sales_id', base64_decode($id))->update([
                'sales_code' => $request->input('f_kodeSales'),
                'sales_name' => $request->input('f_nama'),
                'sales_phone' => $request->input('f_telepon'),
                'sales_addres' => $request->input('f_alamat'),
                'sales_ktp' => $request->input('f_ktp'),
                'sales_birth_place' => $request->input('f_tempatLahir'),
                'sales_birth_date' => $request->input('f_tgLahir'),
                'sales_sim' => $request->input('f_sim'),
                'sales_salary' => $request->input('f_gaji'),
                'sales_mail' => $request->input('f_email'),
                'city_id' => $city,
                'sales_password' => ""
            ]);

            if ($sales) {
                # code...
                Alert::success('Sales Berhasil Diupdate', 'Success');
                return redirect('home/master/sales');
            } else {
                Alert::error('Gagal Update Sales', 'Error');
                return redirect()->back()->withErrors($valid);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (strpos($this->permit, 'd')) {
            # code...
            $del = Sales::findOrFail(base64_decode($id));
            if ($del->delete()) {
                # code...
                Alert::success('Sales Berhasil Dihapus', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Menghapus Sales', 'Error');
                return redirect()->back();
            }
        } else {
            abort(403, 'Unauthorized Access.');
        }
    }
}
