<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alert;
use Validator;
use Access;
use Auth;
use App\Supplier;
use App\Brand;

class SupplierController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,41);

            $this->permit = $akses->permit_acces;
                
            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            # code...
            $supplier = Supplier::select('supplier_id', 'supplier_code', 'supplier_name', 'supplier_phone', 'supplier_addres')->orderBy('supplier_id', 'ASC')->get();
            return view('admin.supplier.index', compact('supplier'));
        } else {
            abort(403, 'Unauthorized Access.');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (strpos($this->permit, 'c') !== null) {
            # code...
            $brand = Brand::select('brand_id', 'brand_name')->get();
            return view('admin.supplier.create', compact('brand'));
        } else {
            abort(403, 'Unauthorized Access.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'f_kodeSupplier' => 'required',
            'f_nama' => 'required',
            'f_alamat' => 'required',
            'f_telepon' => 'required',
            'f_januari' => 'required',
            'f_february' => 'required',
            'f_maret' => 'required',
            'f_april' => 'required',
            'f_mei' => 'required',
            'f_juni' => 'required',
            'f_juli' => 'required',
            'f_agustus' => 'required',
            'f_september' => 'required',
            'f_oktober' => 'required',
            'f_november' => 'required',
            'f_desember' => 'required',
            'merk' => 'required'
        ]);

        // dd($request->all());

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Supplier::create([
                'supplier_code' => $request->input('f_kodeSupplier'),
                'supplier_name' => $request->input('f_nama'),
                'supplier_target_1' => $request->input('f_januari'),
                'supplier_target_2' => $request->input('f_february'),
                'supplier_target_3' => $request->input('f_maret'),
                'supplier_target_4' => $request->input('f_april'),
                'supplier_target_5' => $request->input('f_mei'),
                'supplier_target_6' => $request->input('f_juni'),
                'supplier_target_7' => $request->input('f_juli'),
                'supplier_target_8' => $request->input('f_agustus'),
                'supplier_target_9' => $request->input('f_september'),
                'supplier_target_10' => $request->input('f_oktober'),
                'supplier_target_11' => $request->input('f_november'),
                'supplier_target_12' => $request->input('f_desember'),
                'supplier_addres' => $request->input('f_alamat'),
                'supplier_phone' => $request->input('f_telepon'),
                'brand_id' => $request->input('merk')
            ]);

            if ($create) {
                # code...
                Alert::success('Supplier Berhasil Ditambahkan', 'Success');
                return redirect('home/master/supplier');
            } else {
                Alert::error('Gagal Membuat Supplier Baru', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (strpos($this->permit, 'e') !== null) {
            # code...
            $supp = Supplier::where('supplier_id', base64_decode($id))->first();
            $brand = Brand::select('brand_id', 'brand_name')->get();
            return view('admin.supplier.edit', compact('brand', 'supp'));
        } else {
            abort(403, 'Unauthorized Access.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
            'f_kodeSupplier' => 'required',
            'f_nama' => 'required',
            'f_alamat' => 'required',
            'f_telepon' => 'required',
            'f_januari' => 'required',
            'f_february' => 'required',
            'f_maret' => 'required',
            'f_april' => 'required',
            'f_mei' => 'required',
            'f_juni' => 'required',
            'f_juli' => 'required',
            'f_agustus' => 'required',
            'f_september' => 'required',
            'f_oktober' => 'required',
            'f_november' => 'required',
            'f_desember' => 'required',
            'merk' => 'required'
        ]);

        // dd($request->all());

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Supplier::where('supplier_id', base64_decode($id))->update([
                'supplier_code' => $request->input('f_kodeSupplier'),
                'supplier_name' => $request->input('f_nama'),
                'supplier_target_1' => $request->input('f_januari'),
                'supplier_target_2' => $request->input('f_february'),
                'supplier_target_3' => $request->input('f_maret'),
                'supplier_target_4' => $request->input('f_april'),
                'supplier_target_5' => $request->input('f_mei'),
                'supplier_target_6' => $request->input('f_juni'),
                'supplier_target_7' => $request->input('f_juli'),
                'supplier_target_8' => $request->input('f_agustus'),
                'supplier_target_9' => $request->input('f_september'),
                'supplier_target_10' => $request->input('f_oktober'),
                'supplier_target_11' => $request->input('f_november'),
                'supplier_target_12' => $request->input('f_desember'),
                'supplier_addres' => $request->input('f_alamat'),
                'supplier_phone' => $request->input('f_telepon'),
                'brand_id' => $request->input('merk')
            ]);

            if ($create) {
                # code...
                Alert::success('Supplier Berhasil Diupdate', 'Success');
                return redirect('home/master/supplier');
            } else {
                Alert::error('Gagal Update Supplier Baru', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (strpos($this->permit, 'd') !== null) {
            # code...
            $del = Supplier::findOrFaiL(base64_decode($id));
            if ($del->delete()) {
                # code...
                Alert::success('Supplier Berhasil Dihapus', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Menghapus Supplier', 'Error');
                return redirect()->back();
            }
        } else {
            abort(403, 'Unauthorized Access.');
        }
    }
}
