<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Access;
use Alert;
use Validator;
use App\Packaging;
use App\PackagingNota;
use App\Nota;
use App\Cardboard;
use App\CardboardDetail;
use App\CardboardType;
use App\Item;
use CodeItNow\BarcodeBundle\Utils\BarcodeGenerator;

class PackagingController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,53);

            $this->permit = $akses->permit_acces;
                
            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            # code...
            $packaging = Packaging::orderBy('packaging_id', 'desc')->get();
            return view('admin.packaging.index', compact('packaging'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (strpos($this->permit, 'c') !== null) {
            # code...
            $nota = Nota::select('nota_id', 'nota_code')->get();
            return view('admin.packaging.create', compact('nota'));
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'faktur' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Packaging::create([
                'packaging_date' => $request->tgl,
                'nota_id' => (is_array($request->faktur) ? implode(',',$request->faktur) : $request->faktur),
                'packaging_sj' => $this->format_code(),
                'packaging_expedisi' => '',
                'packaging_status' => 1
            ]);

            $arrLength = (is_array($request->faktur) ? count($request->faktur) : 1);
            $nota_id = $request->faktur;
            PackagingNota::where('packaging_id', $create->packaging_id)->delete();
            for ($i = 0; $i < $arrLength; $i++) {
                PackagingNota::create([
                    'packaging_id' => $create->packaging_id,
                    'nota_id' => $nota_id[$i]
                ]);

                Nota::where('nota_id', $request->faktur[$i])->update([
                    'nota_status' => 1
                ]);

                Cardboard::where('packaging_id', 0)->update([
                    'packaging_id' => $create->packaging_id
                ]);
            }

            if ($create) {
                # code...
                Alert::success('Packaging Berhasil Dibuat', 'Success');
                return redirect('gudang/packaging');
            } else {
                Alert::error('Gagal Membuat Packaging', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (strpos($this->permit, 'u') !== null) {
            # code...
            $pack = Packaging::where('packaging_id', base64_decode($id))->first();
            $nota = Nota::select('nota_id', 'nota_code')->get();
            return view('admin.packaging.edit', compact('nota', 'pack'));
        } else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
            'faktur' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Packaging::where('packaging_id', base64_decode($id))->update([
                'packaging_date' => $request->tgl,
                'nota_id' => (is_array($request->faktur) ? implode(',',$request->faktur) : $request->faktur),
                'packaging_expedisi' => '',
                'packaging_status' => 1
            ]);

            $arrLength = (is_array($request->faktur) ? count($request->faktur) : 1);
            $nota_id = $request->faktur;
            PackagingNota::where('packaging_id', base64_decode($id))->delete();
            for ($i = 0; $i < $arrLength; $i++) {
                PackagingNota::where('packaging_id', base64_decode($id))->update([
                    'nota_id' => $nota_id[$i]
                ]);

                Nota::where('nota_id', $request->faktur[$i])->update([
                    'nota_status' => 1
                ]);

                Cardboard::where('packaging_id', 0)->update([
                    'packaging_id' => $create->packaging_id
                ]);
            }

            if ($create) {
                # code...
                Alert::success('Packaging Berhasil Diupdate', 'Success');
                return redirect('gudang/packaging');
            } else {
                Alert::error('Gagal Update Packaging', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (strpos($this->permit, 'd') !== null) {
            # code...
            $del = Packaging::find(base64_decode($id));
            if ($del->delete()) {
                # code...
                Alert::success('Packaging Berhasil Dihapus', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Menghapus Packaging', 'Error');
                return redirect()->back();
            }
        } else {
            abort(403);
        }
    }

    public function generateCardboard(Request $request)
    {
        $create = Cardboard::create([
            'cardboard_barcode' => $this->randomNumber(12),
            'packaging_id' => $request->id,
            'cardboard_qty' => 0,
            'cardboard_type_id' => 0,
            'cardboard_status' => 0
        ]);

        if ($create) {
            # code...
            return response(['status' => 'success', 'message' => 'Kardus Berhasil Ditambahkan'], 200);
        } else {
            return response(['status' => 'error', 'message' => 'Terjadi Kesalahan'], 200);
        }
    }

    public function updateCardboardType(Request $request)
    {
        $up = Cardboard::where('cardboard_barcode', $request->id)->update([
            'cardboard_type_id' => $request->data
        ]);

        if ($up) {
            # code...
            return response(['status' => 'success', 'message' => 'Berhasil'], 200);
        } else {
            return response(['status' => 'error', 'message' => 'Terjadi Kesalahan'], 200);
        }
    }

    public function printBarcode($id)
    {
        $det = Cardboard::where('cardboard_id', $id)->first();
        $barcode = new BarcodeGenerator();
        $barcode->setText($det->cardboard_barcode);
        $barcode->setType(BarcodeGenerator::Code128);
        $barcode->setScale(2);
        $barcode->setThickness(25);
        $barcode->setFontSize(10);
        $code = $barcode->generate();
        $pdf = \PDF::loadView('admin.packaging.barcode', ['code' => $code]);
        return $pdf->stream();
    }

    public function list_cardboard($id)
    {
        $get = Cardboard::where('packaging_id', $id)->get();
        $type = CardboardType::orderBy('cardboard_type_name', 'asc')->get();
        return \View::make('admin.packaging.list_kardus', ['cardboard' => $get, 'type' => $type]);
    }

    public function listCardboardDetail($id)
    {
        $list = CardboardDetail::select('cardboard_details.*', 'b.item_code', 'b.item_name', 'b.item_barcode')
                                ->join('items as b', 'b.item_id', 'cardboard_details.item_id')
                                ->where('cardboard_details.cardboard_id', $id)->get();
        return view('admin.packaging.scan_item', compact('list', 'id'));
    }

    public function addDetailItem(Request $request)
    {
        $cek = Item::where('item_code', $request->val)->first();
        if (empty($cek)) {
            # code...
            return response(['status' => 'error', 'message' => 'Barang Tidak Ditemukan'], 200);
        } else {
            $cd = CardboardDetail::where('item_id', $cek->item_id)->where('cardboard_id', 0)->get();
            if (!empty($cd) || $cd != null || $cd != '') {
                # code...
                return response(['status' => 'error', 'message' => 'Barang Sudah Ditambahkan'], 200);
            } else {
                $create = CardboardDetail::create([
                    'cardboard_id' => $request->id,
                    'item_id' => $cek->item_id,
                    'cardboard_detail_qty' => 1
                ]);

                Item::where('item_id', $cek->item_id)->decrement('item_stock', 1);

                if ($create) {
                    # code...
                    return response(['status' => 'success', 'message' => 'Barang Berhasil Ditambahkan'], 200);
                } else {
                    return response(['status' => 'error', 'message' => 'Gagal Menambahkan Barang'], 200);
                }
            }
        }
    }

    protected function randomNumber($length) {
        $result = '';

        for($i = 0; $i < $length; $i++) {
            $result .= mt_rand(0, 9);
        }

        return $result;
    }

    protected function format_code() {
        $code = '';
        $year =date('y');

        switch (date('m')) {
            case '01': $month = "A"; break;
            case '02': $month = "B"; break;
            case '03': $month = "C"; break;
            case '04': $month = "D"; break;
            case '05': $month = "E"; break;
            case '06': $month = "F"; break;
            case '07': $month = "G"; break;
            case '08': $month = "H"; break;
            case '09': $month = "I"; break;
            case '10': $month = "J"; break;
            case '11': $month = "K"; break;
            case '12': $month = "L"; break;
        }

        $likes = 'SJ-'.$year.$month;
        $findMemo = Packaging::select('packaging_sj')->where('packaging_sj', 'LIKE', '%'.$likes.'%')->orderBy('packaging_id', 'desc')->first();

        $number = 0;

        if (empty($findMemo)) {
            $number = sprintf("%0" . 4 . "d", $number + 1);
            $code = 'SJ-'.$year.$month.$number;
            return $code;
        }

        $number = intval(substr($findMemo->packaging_sj, -4));
        $number = sprintf("%0" . 4 . "d", $number + 1);
        $code = 'SJ-'.$year.$month.$number;
        return $code;
    }
}
