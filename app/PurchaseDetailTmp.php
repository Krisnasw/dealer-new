<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseDetailTmp extends Model
{
    //
    protected $table = 'purchase_detail_tmp';
    protected $primaryKey = 'purchase_detail_id';
    protected $fillable = ['purchase_detail_discount', 'item_id', 'purchase_id', 'purchase_detail_price', 'purchase_detail_qty', 'user_id', 'purchase_detail_total'];

    public $timestamps = false;

    public function item()
    {
    	return $this->belongsTo('App\Item', 'item_id', 'item_id');
    }

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id', 'user_id');
    }
}
