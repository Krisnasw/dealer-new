<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    //
    protected $table = 'items';
    protected $primaryKey = 'item_id';
    protected $fillable = ['item_code', 'brand_detail_id', 'item_name', 'unit_id', 'character_id', 'item_het', 'item_price_primary', 'item_price_buy', 'item_stock', 'item_stock_min', 'item_stock_max', 'item_barcode', 'item_weight', 'character_fix', 'item_status'];
    public $timestamps = false;
}
