<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentNota extends Model
{
    //
    protected $table = 'payment_notas';
    protected $primaryKey = 'payment_nota_id';
    protected $fillable = ['payment_id', 'nota_id'];

    public $timestamps = false;
}
