<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entrusted extends Model
{
    //
    protected $table = 'entrusteds';
    protected $primaryKey = 'entrusted_id';
    protected $fillable = ['entrusted_date', 'customer_id', 'entrusted_total', 'entrusted_code', 'entrusted_status'];

    public $timestamps = false;

    public function customer() {
    	return $this->belongsTo('App\Customer', 'customer_id', 'customer_id');
    }
}
