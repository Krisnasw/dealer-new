<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemSubstitude extends Model
{
    //
    protected $table = 'item_subtitues';
    protected $primaryKey = 'item_substitu_id';
    protected $fillable = ['item_id', 'item_subtitu_code', 'item_subtitu_date', 'item_subtitu_status', 'item_detail_id'];

    public $timestamps = false;

    public function item()
    {
    	return $this->belongsTo('App\Item', 'item_detail_id', 'item_id');
    }
}
