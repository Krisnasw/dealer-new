<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    //
    protected $table = 'sales';
    protected $primaryKey = 'sales_id';
    protected $fillable = ['sales_code', 'sales_name', 'sales_phone', 'sales_addres', 'sales_ktp', 'sales_birth_place', 'sales_birth_date', 'sales_sim', 'sales_salary', 'sales_mail', 'city_id', 'sales_password'];
    public $timestamps = false;
}
