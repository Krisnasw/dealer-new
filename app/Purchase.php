<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    //
    protected $table = 'purchases';
    protected $primaryKey = 'purchase_id';
    protected $fillable = ['purchase_code', 'purchase_date', 'brand_id', 'purchase_type', 'purchase_discount', 'purchase_ppn', 'supplier_id', 'purchase_netto'];

    public $timestamps = false;

    public function brand()
    {
    	return $this->belongsTo('App\Brand', 'brand_id', 'brand_id');
    }

    public function supplier()
    {
    	return $this->belongsTo('App\Supplier', 'supplier_id', 'supplier_id');
    }
}
