<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    //
    protected $table = 'branchs';
    protected $primaryKey = 'branch_id';
    protected $fillable = ['branch_code', 'branch_name', 'branch_addres', 'branch_phone', 'supervisior_id'];

    public $timestamps = false;

    public function cabangs()
    {
    	return $this->belongsTo('App\Spv', 'supervisior_id', 'supervisior_id');
    }
}
