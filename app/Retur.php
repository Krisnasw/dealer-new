<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Retur extends Model
{
    //
    protected $table = 'returs';
    protected $primaryKey = 'retur_id';
    protected $fillable = ['retur_type', 'retur_data_id', 'retur_code', 'retur_date', 'retur_type_saldo', 'retur_total', 'retur_status'];

    public $timestamps = false;

    public function nota()
    {
    	return $this->belongsTo('App\Nota', 'retur_data_id', 'nota_id');
    }
}
