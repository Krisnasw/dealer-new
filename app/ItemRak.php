<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemRak extends Model
{
    //
    protected $table = 'item_raks';
    protected $primaryKey = 'item_rak_id';
    protected $fillable = ['item_rak_name'];

    public $timestamps = false;
}
