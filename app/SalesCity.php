<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesCity extends Model
{
    //
    protected $table = 'sales_cities';
    protected $primaryKey = 'sales_city_id';
    protected $fillable = ['sales_id', 'city_id'];

    public $timestamps = false;
}
