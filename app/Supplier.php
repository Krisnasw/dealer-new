<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    //
    protected $table = 'suppliers';
    protected $primaryKey = 'supplier_id';
    protected $fillable = ['supplier_code', 'supplier_name', 'supplier_target_1', 'supplier_target_2', 'supplier_target_3', 'supplier_target_4', 'supplier_target_5', 'supplier_target_6', 'supplier_target_7', 'supplier_target_8', 'supplier_target_9', 'supplier_target_10', 'supplier_target_11', 'supplier_target_12', 'supplier_phone', 'supplier_addres', 'brand_id'];

    public $timestamps = false;
}
