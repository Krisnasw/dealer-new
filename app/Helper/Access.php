<?php

namespace App\Helper;

use Illuminate\Support\Facades\DB;
use App\CardboardDetail;
use App\Cardboard;
use App\DeliveryBack;
use App\DeliveryBackDetail;
use App\DeliveryDetail;
use App\ReceiptAgency;
use App\PurchaseDetail;
use App\ReceiptStorehouseDetail;
use App\ReceiptCardStorehouse;
use App\Entrusted;
use App\Credit;
use App\Nota;

class Access {

	public static function getListTitipUang($id, $bulan, $tahun) {
		$list = Entrusted::whereMonth('entrusted_date', $bulan)->whereYear('entrusted_date', $tahun)->where('entrusted_status' , 0)->where('customer_id', $id)->get();
		return $list;
	}

	public static function getUserAccess($id, $menu_id) {

		$query = DB::table('users')->select('users.*', 'permits.permit_acces', 'side_menus.side_menu_type_parent')
					->join('permits', 'permits.user_type_id', 'users.user_type_id')
					->join('side_menus', 'side_menus.side_menu_id', 'permits.side_menu_id')
					->where('users.user_id', $id)
					->where('permits.side_menu_id', $menu_id)
					->get();

		$result = null;
		foreach($query as $row) $result = ($row);
		return $result;

	}

	public static function list_detail_tmp_count($id, $item_id) {
		$query = DB::table('nota_detail_tmp as a')->select('a.*', 'd.item_name', 'd.item_het', 'd.item_stock', 'c.memo_detail_qty', 'c.memo_detail_discount', 'c.memo_accumulation', 'e.memo_code', 'e.memo_date', 'c.item_id', DB::raw('COUNT(c.item_id) as counter'))
					->join('memo_details as c', 'c.memo_detail_id', 'a.memo_detail_id')
					->join('items as d', 'd.item_id', 'c.item_id')
					->join('memos as e', 'e.memo_id', 'c.memo_id')
					->where('a.user_id', $id)
					->where('c.item_id', $item_id)
					->groupBy('c.item_id')
					->havingRaw("counter > 1")
					->get();

		if (count($query) == 0)
		return array();
		$data = $query;
		foreach ($data as $index => $row) {}
		return $data;
	}

	public static function cek_substitute($item_id) {
		$query = DB::table('item_subtitues')
		->select('*')->where('item_id', $item_id)->get();
		$result = null;
		foreach ($query as $row) $result = ($row);
		return $result;
	}

	public static function get_retur($id) {
		$query = DB::table('retur_details')->select(DB::raw('SUM(retur_detail_qty) as total'))->where('retur_detail_data_id', $id)->where('retur_detail_type', 1)->get();
		$result = null;
		foreach($query as $row) $result = ($row);
		return $result;
	}

	public static function hitung_isi($id) {
		$q = CardboardDetail::select(DB::raw('SUM(cardboard_detail_qty) as total'))->where('cardboard_id', $id)->get();
        $result = null;
        foreach($q as $row) $result = ($row);
        return $result['total'];
	}

	public static function qty_should($id) {
		$q = Cardboard::select(DB::raw('count(cardboard_id) as total'))->where('packaging_id', $id)->get();
		$result = null;
		foreach($q as $row) $result = ($row);
		return $result['total'];
	}

	public static function qty_scan($id) {
		$q = DeliveryBackDetail::select(DB::raw('count(delivery_back_detail_id) as total'))->where('delivery_back_id', $id)->get();
		$result = null;
		foreach($q as $row) $result = ($row);
		return $result['total'];
	}

	public static function check_cardboard_packaging($id) {
		$cek = Cardboard::select(\DB::raw('COUNT(cardboards.cardboard_id) as total'))
                                        ->join('packagings as b', 'b.packaging_id', 'cardboards.packaging_id')
                                        ->join('delivery_packagings as c', 'c.packaging_id', 'cardboards.packaging_id')
                                        ->where('c.delivery_id', $id)->get();
        $result = null;
        foreach($cek as $row) $result = ($row);
        return $result['total'];
	}

	public static function check_cardboard_delivery($id) {
		$cek2 = DeliveryDetail::select(\DB::raw('COUNT(delivery_detail_id) as total'))->where('delivery_id', $id)->get();
        $result2 = null;
        foreach($cek2 as $rows) $result2 = ($rows);
        return $result2['total'];
	}

	public static function hitung_item($id, $cek) {
		if ($cek == 1) {
			# code...
			$query = ReceiptAgency::select(DB::raw('COUNT(receipt_agency_id) as total'))->where('receipt_cardboard_id', $id)->get();
			$result = null;
			foreach($query as $row) $result = ($row);
			return $result['total'];
		} else {
			$query = ReceiptStorehouseDetail::select(DB::raw('COUNT(receipt_storehouse_detail_id) as total'))->where('receipt_storehouse_detail_id', $id)->get();
			$result = null;
			foreach($query as $row) $result = ($row);
			return $result['total'];
		}
	}

	public static function hitung_qty_item($id, $cek) {
		if ($cek == 1) {
			# code...
			$cek = ReceiptAgency::select(DB::raw('SUM(receipt_agency_qty) as total'))->where('receipt_cardboard_id', $id)->get();
			$result = null;
			foreach($cek as $row) $result = ($row);
			return $result['total'];
		} else {
			$query = ReceiptStorehouseDetail::select(DB::raw('sum(receipt_storehouse_detail_qty) as total'))->where('receipt_card_storehouse_id', $id)->get();
			$result = null;
			foreach($query as $row) $result = ($row);
			return $result['total'];
		}
	}

	public static function hitung_outstanding($id) {
		$cek = PurchaseDetail::select(DB::raw('(purchase_detail_qty) - (purchase_detail_accumulation) as pending'))->where('purchase_detail_id', $id)->get();
		$result = null;
		foreach($cek as $row) $result = ($row);
		return $result['pending'];
	}

	public static function get_detail_gudang($id, $date) {
		$query = ReceiptCardStorehouse::select(DB::raw('SUM(y.receipt_storehouse_detail_qty) as gudang'), 'receipt_card_storehouses.receipt_cardboard_id')
										->join('receipt_storehouse_details as y', 'y.receipt_card_storehouse_id', 'receipt_card_storehouses.receipt_card_storehouse_id')
										->join('receipt_storehouses as x', 'x.receipt_storehouse_id', 'receipt_card_storehouses.receipt_storehouse_id')
										->where('receipt_card_storehouses.receipt_cardboard_id', $id)
										->where('x.receipt_storehouse_date', $date)->get();
		$result = null;
		foreach($query as $row) $result = ($row);
		return $result['gudang'];
	}

	public static function getTotalByBrand($id, $sales, $bulan) {
		$query = Nota::select(DB::raw('SUM(nota_total) as total'))->where('brand_id', $id)->where('sales_id', $sales)->whereMonth('nota_date', $bulan)->get();
		$result = null;
		foreach($query as $row) $result = ($row);
		return $result['total'];
	}

	public static function countNotification() {
		$cek = Nota::select('notas.nota_code', 'b.sales_name', 'notas.nota_date', 'notas.nota_tempo')->join('sales as b', 'b.sales_id', 'notas.sales_id')->whereRaw('DATE(nota_date) = DATE_SUB(CURDATE(), INTERVAL 7 DAY)')->get();
		// dd($cek);
		$result = null;
		$result = $cek;
		return $result;
	}
}