<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromoDetail extends Model
{
    //
    protected $table = 'bonus_details';
    protected $primaryKey = 'bonus_detail_id';
    protected $fillable = ['bonus_id', 'item_id', 'bonus_detail_min', 'bonus_detail_price', 'bonus_detail_item_id', 'bonus_detail_qty_item', 'bonus_detail_status'];

    public $timestamps = false;

    public function items()
    {
        return $this->belongsTo('App\Item','item_id', 'item_id');
    }
    public function barang()
    {
        return $this->belongsTo('App\Item', 'bonus_detail_item_id', 'item_id');
    }
}