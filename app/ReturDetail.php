<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReturDetail extends Model
{
    //
    protected $table = 'retur_details';
    protected $primaryKey = 'retur_detail_id';
    protected $fillable = ['retur_detail_id', 'retur_id', 'retur_detail_data_id', 'retur_detail_qty', 'retur_detail_status', 'user_id', 'retur_detail_type'];

    public $timestamps = false;

    public function retur()
    {
    	return $this->belongsTo('App\Retur', 'retur_id', 'retur_id');
    }

    public function nota()
    {
    	return $this->belongsTo('App\NotaDetail', 'retur_detail_data_id', 'nota_detail_id');
    }
}
