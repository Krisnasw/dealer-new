<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CardboardType extends Model
{
    //
    protected $table = 'cardboard_types';
    protected $primaryKey = 'cardboard_type_id';
    protected $fillable = ['cardboard_name'];

    public $timestamps = false;
}
