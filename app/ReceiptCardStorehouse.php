<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReceiptCardStorehouse extends Model
{
    //
    protected $table = 'receipt_card_storehouses';
    protected $primaryKey = 'receipt_card_storehouse_id';
    protected $fillable = ['receipt_cardboard_id', 'receipt_storehouse_id'];

    public $timestamps = false;
}
