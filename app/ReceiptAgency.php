<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReceiptAgency extends Model
{
    //
    protected $table = 'receipt_agencies';
    protected $primaryKey = 'receipt_agency_id';
    protected $fillable = ['receipt_cardboard_id', 'purchase_detail_id', 'receipt_agency_qty', 'item_id'];

    public $timestamps = false;
}
