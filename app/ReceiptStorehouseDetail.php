<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReceiptStorehouseDetail extends Model
{
    //
    protected $table = 'receipt_storehouse_details';
    protected $primaryKey = 'receipt_storehouse_detail_id';
    protected $fillable = ['item_id', 'receipt_storehouse_detail_qty', 'receipt_card_storehouse_id'];

    public $timestamps = false;
}
