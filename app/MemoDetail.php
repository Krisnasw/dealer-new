<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemoDetail extends Model
{
    //
    protected $table = 'memo_details';
    protected $primaryKey = 'memo_detail_id';
    protected $fillable = ['memo_id', 'item_id', 'memo_detail_qty', 'memo_accumulation', 'memo_detail_discount', 'memo_detail_cancel', 'user_id'];

    public $timestamps = false;

    public function item()
    {
        return $this->belongsTo('App\Item', 'item_id', 'item_id');
    }

    public function memos()
    {
        return $this->belongsTo('App\Memo', 'memo_id', 'memo_id');
    }

    public function users()
    {
        return $this->belongsTo('App\User', 'user_id', 'user_id');
    }
}
