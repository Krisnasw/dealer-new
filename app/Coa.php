<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coa extends Model
{
    //
    protected $table = 'coas';
    protected $primaryKey = 'coa_id';
    protected $fillable = ['coa_name', 'coa_parent', 'coa_nomor', 'side_menu_id', 'coa_delete'];

    public $timestamps = false;
}