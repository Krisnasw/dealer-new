<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //
    protected $table = 'customers';
    protected $primaryKey = 'customer_id';
    protected $fillable = ['customer_code', 'customer_name', 'customer_phone', 'customer_addres', 'customer_email', 'customer_npwp', 'grade_id', 'customer_rekening', 'customer_id_bank', 'customer_nama_npwp', 'customer_expedisi', 'customer_tempo', 'customer_limit', 'customer_hp', 'customer_fax', 'city_id', 'customer_store', 'customer_latitude', 'customer_longitude', 'customer_status', 'customer_img'];

    public $timestamps = false;

    public function kota()
    {
    	return $this->belongsTo('App\City', 'city_id', 'city_id');
    }

    public function grades()
    {
    	return $this->belongsTo('App\Grade', 'grade_id', 'grade_id');
    }
}
