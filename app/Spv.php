<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Spv extends Model
{
    //
    protected $table = 'supervisiors';
    protected $primaryKey = 'supervisior_id';
    protected $fillable = ['supervisior_name', 'supervisior_phone', 'supervisior_addres'];

    public $timestamps = false;
}
