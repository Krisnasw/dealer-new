<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesCost extends Model
{
    //
    protected $table = 'sales_costs';
    protected $primaryKey = 'sales_cost_id';
    protected $fillable = ['sales_cost_start', 'sales_cost_finish', 'sales_cost_modal', 'sales_cost_modal_less', 'sales_id', 'sales_cost_desc', 'sales_cost_total', 'sales_cost_lock'];

    public $timestamps = false;

    public function sales() {
    	return $this->belongsTo('App\Sales', 'sales_id', 'sales_id');
    }
}
