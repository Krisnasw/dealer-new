<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentRetur extends Model
{
    //
    protected $table = 'payment_returs';
    protected $primaryKey = 'payment_retur_id';
    protected $fillable = ['payment_id', 'retur_id'];

    public $timestamps = false;
}
