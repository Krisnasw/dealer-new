<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CardboardDetail extends Model
{
    //
    protected $table = 'cardboard_details';
    protected $primaryKey = 'cardboard_detail_id';
    protected $fillable = ['cardboard_id', 'item_id', 'cardboard_detail_qty'];

    public $timestamps = false;

    public function dus()
    {
    	return $this->belongsTo('App\Cardboard', 'cardboard_id', 'cardboard_id');
    }
}
