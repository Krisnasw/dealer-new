<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotaDetail extends Model
{
    //
    protected $table = 'nota_details';
    protected $primaryKey = 'nota_detail_id';
    protected $fillable = ['nota_id', 'memo_detail_id', 'nota_detail_price', 'nota_fill_qty', 'nota_detail_total', 'nota_detail_discount'];

    public $timestamps = false;

    public function nota()
    {
        return $this->belongsTo('App\Nota', 'nota_id', 'nota_id');
    }
}
