<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    //
    protected $table = 'payments';
    protected $primaryKey = 'payment_id';
    protected $fillable = ['payment_date', 'payment_code', 'payment_total', 'payment_total_bayar', 'entrusted_id', 'customer_id', 'payment_cash', 'payment_rest', 'payment_status', 'payment_keel', 'payment_desc'];
    public $timestamps = false;

    public function customer() {
    	return $this->belongsTo('App\Customer', 'customer_id', 'customer_id');
    }
}