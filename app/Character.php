<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Character extends Model
{
    //
    protected $table = 'characters';
    protected $primaryKey = 'character_id';
    protected $fillable = ['character_code', 'character_merk', 'character_seri', 'character_date1', 'character_date2'];

    public $timestamps = false;
}
