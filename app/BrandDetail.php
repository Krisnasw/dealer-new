<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrandDetail extends Model
{
    //
    protected $table = 'brand_details';
    protected $primaryKey = 'brand_detail_id';
    protected $fillable = ['brand_id', 'brand_detail_name', 'brand_detail_target'];

    public $timestamps = false;
}
