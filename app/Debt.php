<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Debt extends Model
{
    //
    protected $table = 'debts';
    protected $fillable = ['receipt_id', 'debt_type', 'debt_nominal'];

    public $timestamps = false;
}