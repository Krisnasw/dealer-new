<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    //
    protected $table = 'units';
    protected $primaryKey = 'unit_id';
    protected $fillable = ['unit_name'];

    public $timestamps = false;
}
