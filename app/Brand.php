<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    //
    protected $table = 'brands';
    protected $primaryKey = 'brand_id';
    protected $fillable = ['brand_code', 'brand_name', 'brand_limit_cash', 'brand_limit_kredit'];
    public $timestamps = false;
}