<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Credit extends Model
{
    //
    protected $table = 'credits';
    protected $primaryKey = 'credit_id';
    protected $fillable = ['customer_id', 'credit_nominal', 'credit_type', 'nota_id'];

    public $timestamps = false;
}
