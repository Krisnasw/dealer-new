<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Memo extends Model
{
    //
    protected $table = 'memos';
    protected $primaryKey = 'memo_id';
    protected $fillable = ['memo_code', 'memo_date', 'sales_id', 'customer_id', 'memo_type_pay', 'brand_id'];

    public $timestamps = false;

    public function sales()
    {
        return $this->belongsTo('App\Sales', 'sales_id', 'sales_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer', 'customer_id', 'customer_id');
    }

    public function brand()
    {
        return $this->belongsTo('App\Brand', 'brand_id', 'brand_id');
    }
}
