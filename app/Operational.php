<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operational extends Model
{
    //
    protected $table = 'oprationals';
    protected $primaryKey = 'oprational_id';
    protected $fillable = ['oprational_date', 'oprational_nominal', 'oprational_type', 'oprational_desc', 'oprational_saldo'];

    public $timestamps = false;
}