<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Password extends Model
{
    //
    protected $table = 'passwords';
    protected $primaryKey = 'password_id';
    protected $fillable = ['password_value', 'menu_id'];

    public $timestamps = false;

    public function menus()
    {
        return $this->belongsTo('App\Menu', 'menu_id', 'side_menu_id');
    }
}
